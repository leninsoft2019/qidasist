﻿<?php 
	//$objform->title =  $objform->mtpv_title;
?>

<script src="{{asset('js/sales.js')}}"></script>
@include('insurance.main_script')		
<div class="cs_body_module">		
	<header>
		@include('layouts.header_module')
	</header>
	<div id="cont_cat" aria-live="polite">
	    <style scoped>
		  #cont_cat{
			  background:#c4c2c2;
		  }
		</style>
		<section>
			@include('layouts.block_module_main_options')
		</section>
		<section class="cs_section_middle_mod">
			<div class="cs_table_middle_mod">
				<div class="cs_row">
						<div class="cs_cell cs_cell_first">
							<h3><p>{{$objform->ly_bmm_a2_tit}}</p></h3>
							</p>{{$objform->ly_bmm_a2_des}}</p>
						</div>
						<div class="cs_cell">
						<i class="ico_masivo_mod"></i>
						<i class="ico_vertical_line_mod"></i>
						
						</div>	
						<div class="cs_cell">
							<button  class="button_module" id="btnMassive">{{$objform->ly_bmm_a2_op1}}</button>
						</div>						
				</div>
			</div>
		</section>
		
		<section class="cs_section_footer_mod">
				@include('layouts.footer_module')
		</section>
	</div>
</div>
