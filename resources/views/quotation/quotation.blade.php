<?php
/*
*	Página que despliega la tabla de productos a cotizar o vender
*	Page that displays the table of products to be quoted or sold
*/

?>



<div class="contenedor_mt">

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<style scoped>


.cs_totales{
	font-size:2em;
}
.cs_totales_input{
	font-size:2em;
	background:transparent;
	border:none;
}


#totales{
		font-size:2em;
	background:transparent;
	border:none;
}
#frmMT{
	width:70%;
	margin:auto;
	
	border:solid 1px;
	border-radius:5px;
	margin-top:20px;
	margin-bottom:20px;
	padding:20px;
	-webkit-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
	box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
	 background-color:white;
	 color:black;
}


.row {
    display: -webkit-box;
    display: -ms-flexbox;
    
	width:100%;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}

.row_n{
	display:table;
	width:100%;
}

.tbl{
	display:table;
	width:100%;	
}
.rw{
	display:table-row;
	width:100%;
}

.col{
    position: relative;
    width: 50%;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
	
	display:table-cell;
}


.col2{
    position: relative;
    width: 50%;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
	
	display:table-cell;
}

.form-control {
    display: block;
    width: 100%;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}


.form-control {
    padding: 2px;
    height: 28px;
    border: 1px solid #dddddd;
}

h2{
	font-family: "Rubik",Helvetica,Arial,sans-serif;
	font-size:2em;
	font-weight:500;
}



.col_add{
	width:25%;
}

.col_minus{
	width:25%;
}

.col_range{
	width:25%;
}

.col_50{
	display:table-cell;
	width:50%;
}


@media (max-width: 767px) {

	.text-xxl {
		font-size: 50px !important;
		line-height: 60px !important;
	}

h1{
		 text-indent:-9999px;
		font-size:0px;
		position:absolute;
	}

	h2, .text-l {
		margin-top:20px;
		font-size: 1.5em !important;
		line-height: 1em !important;
		
	}

	h3 {
		font-size: 18px !important;
		line-height: 28px !important;
	}
	
	
	#frmMT{
		
		width:98%;
		maring:auto;
		padding:5px;
	}
	
	.row{
				display:table;
		width:100%;
	}
	.col{
		display:table-row;
		width:100%;
	}


.contenedor_mt {
    padding: 0px;
    width: 100%;

}


.res_input{
	width:40px;
	height:20px;
}

.cab_r{
	padding-left:20px;
}

.col_add{
	width:25%;
}

.col_minus{
	width:25%;
}

.col_range{
	width:25%;
}


}
</style>


   

	<form action="/action_page.php" id="frmMT">
	{{ csrf_field() }}	
	 <h2>Usted a seleccionado el producto: {{$arrWebProduct->pro_name}}</h2>
		<!-- Titular -->
		<div class="tbl">
		<div class="row cab_r rw">
			<div class="col col_50">
				<div class="form-group">
					<label for="nombre">Nombre:</label>
					<input type="text" class="form-control" id="nombre" name="nombre"  />
				</div>
			</div>
			<div class="col col_50">
				<div class="form-group">
					<label for="apellido">Apellido:</label>
					<input type="text" class="form-control" id="apellido" name="apellido" />
				</div>
			</div>
		</div>
		<!-- Fila: Address -->
		<div class="row cab_r rw">
			<div class="col col_50">
				<div class="form-group">
					<label for="id">Id:</label>
					<input type="id" class="form-control" id="id" name="id" />
				</div>
			</div>
			<div class="col col_50">
				<div class="form-group">
					<label for="email">Correo Electrónico:</label>
					<input type="email" class="form-control" id="email"  name="email" />
				</div>
			</div>
		</div>
		<!-- Fila: Address -->
		<div class="row cab_r rw">
			<div class="col col_50">
				<div class="form-group">
					<label for="id">Direccion:</label>
					<input type="id" class="form-control" id="per_address" name="per_address" />
				</div>
			</div>
			<div class="col col_50">
				<div class="form-group">
					<label for="email">Telefono:</label>
					<input type="email" class="form-control" id="per_phone" name="per_phone" />
				</div>
			</div>
		</div>

		
		</div>
		<!-- Titulos -->
		<div class="row_n">
			<div class="col2 col_range">
				<div class="form-group">
					<label for="tit_range">Rango de Edades</label>
				</div>
			</div>
			<div class="col2 col_add">
				<div class="form-group">
					<label for="tit_add">Agregar</label>
				</div>
			</div>
			<div class="col2 col_minus">
				<div class="form-group">
					<label for="tit_minus">Menorar</label>
				</div>
			</div>
			<div class="col2">
				<div class="form-group">
					<label for="tit_quantity">Cantidad</label>
				</div>
			</div>
		</div>
		<!-- Rangos -->
		<?php 
		$tot_lines = count($arrWebRanges);
		echo "<input type='hidden' name='tot_lines' id='tot_lines' value='".$tot_lines."' />";
		echo "<input type='hidden' name='precio' id='precio' value='".$precio."' />";
		for($i=0; $i<$tot_lines; $i++){
		?>
		<div class="row_n">
			<div class="col2 col_range">
				<div class="form-group">
					<label for="ing_range{{$i}}">{{$arrWebRanges[$i]->ran_interval}}</label>
					<input type="hidden" name="ing_range{{$i}}"  id="ing_range{{$i}}"  value="{{$arrWebRanges[$i]->Id}}"/>
					<input type="hidden" name="val_range{{$i}}"  id="val_range{{$i}}"  value="{{$arrWebRanges[$i]->ran_value}}"/>
				</div>
			</div>
			<div class="col2 col_add">
				<div class="form-group">
					<a href="javascript:add({{$i}})"><img src="{{asset('img/plus_mtpv.svg')}}" width="20px" height="20px" /></a>
				</div>
			</div>
			<div class="col2 col_minus">
				<div class="form-group">
					<a href="javascript:minus({{$i}})"><img src="{{asset('img/minus_mtpv.svg')}}" width="20px" height="20px" /></a>
				</div>
			</div>
			<div class="col2">
				<div class="form-group">
					<input type="text" class="form-control res_input" id="quantity{{$i}}" name="quantity{{$i}}" value="0" readonly />
				</div>
			</div>
		</div>
		<?php 
		}
		?>
		<!-- Totales -->
		<div class="row_n">
			<div class="col2 col_range">
				<div class="form-group"></div>
			</div>
			<div class="col2 col_add">
				<div class="form-group"></div>
			</div>
			<div class="col2 col_minus">
				<div class="form-group cs_totales">
					<label for="totales">Totales</label>
				</div>
			</div>
			<div class="col2">
				<div class="form-group">
					<input type="text" class="form-control res_input cs_totales_input" id="totales" name="totales" disabled />
				</div>
			</div>
		</div>
		<!-- Siguiente -->
		<div class="row_n">
			<div class="col2 col_range">
				<div class="form-group"></div>
			</div>
			<div class="col2 col_add">
				<div class="form-group"></div>
			</div>		
			<div class="col2">
				<button id="btnRegresar" class="btn btn-default">Regresar</button>
			</div>		
			<div class="col2">
				<button id="btnNextPy" class="btn btn-default">Continuar</button>
			</div>
		</div>
		
	
	</form>
	
		<div id="footer_form">

		<div class="tbl_2">
				<div class="row cab_r rw">
					<div class="col col_50">
						
					</div>
					<div class="col col_50">
							<span class="cs_footer_title">LLevamos 15 anios acompaniando a las familias en los momentos mas dificiles</span><br>
							<span class="cs_footer_text">Somos una organizacion lider e innovadora en servicios exequiales tanto en prevision
								  como en necesidad inmediata, con asistencia nacional e internacional   						
							</span>
					</div>
				</div>
		</div>		


		</div>	
	
	
	
</div>

<script>

	$(function () {
	
			
			$("#btnRegresar").click(function( event ) {
				event.preventDefault();
				location.reload();
				
			});
			
			$("#btnNextPy").click(function( event ) {
				event.preventDefault();
				
				var form = $('#frmMT');
				var request = $.ajax({
					type:	'POST',
					url:	'/payment/mult_save',
					data:	form.serialize(),
					success:function(data){
						console.log(data);
						if(data.success==1){
							cargarMenu('/payment/pay_sel');
							return;
						}else{
							alert('Hubo un problema al tratar de guardar la informacion');
							console.log(data);
							return;
						}
					}
				});
				request.fail(function( jqXHR, textStatus ) {
					alert( "Hubo un problema: " + textStatus );
					console.log(jqXHR);
				});				
				
				
			});
		
	
	});
	

	function recalcula(){
		
		var tot_lines = $("#tot_lines").val();
		var precio = $("#precio").val();
		precio  = parseFloat(precio);
		
		
		var qty_line = quantity = 0;
		var total = 0;
		var tot_price = 0;
		console.log(tot_lines);
		console.log(precio);
		console.log(tot_price);
		var ran_val=0;
		for (var i = 0; i < tot_lines; i++) {
			qty_line = $("#quantity"+i).val();
			//quantity += parseInt(qty_line);
			quantity = parseInt(qty_line);
			ran_val = parseFloat($("#val_range"+i).val());
			//alert(qty_line+" - "+quantity);
			console.log('qty='+quantity);
			total = total + parseFloat(quantity * ran_val);
			
			if(total<precio){
				total =precio;
			}
			console.log(total);
			
		}
		//quantity = parseFloat(quantity * precio);
		
		
		//alert(quantity);
		//$("#totales").val(quantity.toFixed(2));
		$("#totales").val(total.toFixed(2));
		
	} // Fin recalcula

	function add(linea){
		var quantity = $("#quantity"+linea).val();
		quantity++;
		$("#quantity"+linea).val(quantity);
		recalcula();
	} // Fin add
	
	function minus(linea){
		var quantity = $("#quantity"+linea).val();
		quantity = quantity - 1;
		$("#quantity"+linea).val(quantity);
		recalcula();
	} // Fin add	
</script>