<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class webProduct extends Model{
    //
	protected $table = 'web_products';
	protected $primaryKey = 'Id';
}
