	<script>
		$(document).ready(function() {
			/*$('#table_search_ins').DataTable({
				"language": {
					"url": "DTables"
				}
			});*/
		} );

			$( function() {
				$( "#tabs" ).tabs();
				 w2utils.locale('/locale/{{$objform->app_location_json}}.json');



			  var $searchlink = $('#searchtoggl i');
			  var $searchbar  = $('#searchbar');
			  
			  $('#searchtoggl').on('click', function(e){
				e.preventDefault();
				
			
				  if(!$searchbar.is(":visible")) { 
					// if invisible we switch the icon to appear collapsable
					$searchlink.removeClass('fa-search').addClass('fa-search-minus');
				  } else {
					// if visible we switch the icon to appear as a toggle
					$searchlink.removeClass('fa-search-minus').addClass('fa-search');
				  }
				  
				  $searchbar.slideToggle(300, function(){
					// callback after search bar animation
				  });

			  });



			$("#btn_buscar").click(function( event ) {
				event.preventDefault();
				Pace.restart();
				var form = $('#frm_insurance');
				var request = $.ajax({
					type:	'POST',
					url:	'/insurance/search_insurance',
					data:	form.serialize(),
					success:function(data){
						console.log(data);
						if(data.success==1){
							if(data.json_product.api_temp_poliza_me.poliza.poliza.length > 0){

								if(typeof w2ui['form'] != 'undefined' ){
                                        w2ui['form'].destroy();
								}
								$('#cont_cat').load('/insurance/list_insurance_s/'+1);
								$('#data_search').empty();
								$('#table_search_ins').DataTable({
									"language": {
									"url": "{{$DTables}}"
									}
								});								
								recargarTracker();
								cargarEstado(2);


								console.log('actualizando datatable');
							}else{
								alert('{{$objform->lbl_noresult}}');
							}
							return;
						}else{
							alert('{{$objform->lbl_noresult}}');
							console.log(data);
							return;
						}
					}
				});
				request.fail(function( jqXHR, textStatus ) {
					alert( "{{$objform->lbl_requestfailed}}: " + textStatus );
					console.log(jqXHR);
				});
			});
			
			
				
				$("#btnMassive").click(function(event){
					event.preventDefault();
					Pace.restart();
					$('#cont_cat').load('/insurance/massive_insurance/');
					
				});
				
			
			
			
				$("#btnCloseAlertPPYA").click(function(event){
					
					$(".modal-alert-ppya").modal('hide');
					
					$('#myModalAd').removeClass('modal-backdrop-hid');
					$('#myModalAdContent').removeClass('modal-backdrop-hid');
					
				});


			} );


		
			/***************Process Form***************************/
			
			function guardarPolizaTmp(){
				e = $.Event();
				e.preventDefault();
				$(".container1 *").prop('disabled',false);
				console.log(w2ui['form'].validate());
			}

			function guardarTitularTmp(dirpg){
				e = $.Event();
				e.preventDefault();
				if($("#edad").val()<=0){
					alertPPYA('La edad del titular debe ser mayor a 0');	
					return;					
				}
							
				
				$("#dir_pag").val(dirpg);
				if(dirpg==0){
					procesarPostTitular();
				}else if(dirpg==3){
					regresarPantalla('/prever/Quotation/{{Session::get("mpt_code")}}',3);
				}
				else{
					console.log(w2ui['form'].validate());
				}
			} // Fin guardarTitularTmp

			function cagarTitular(){
				w2ui['form'].destroy();
				var npc = $("#npc").val();
				$('#cont_cat').load('/insurance/formTitular/'+npc);
				cargarEstado(4);
			}

			function postFormularioCatValidado(){

				var item_cart = $('#cod_cartera').data('selected');
				var cod_cartera_id = item_cart.id;
				var item_pr = $('#cod_producto').data('selected');
				var cod_producto_id = item_pr.id;
				var item_age = $('#agencia').data('selected');
				var agencia_id = item_age.id;
				var item_ali = $('#cod_alianza').data('selected');
				var cod_alianza_id = item_ali.id;
				var item_vend = $('#cod_vendedor').data('selected');
				var cod_vendedor_id = item_vend.id;
                //var cod_paiscob = $('#cod_paiscob').data('selected');
                //var cod_paiscob_id = cod_paiscob.id;

                var cod_paiscob = $('#cod_paiscob').val();
                var cod_paiscob_id = $('#cod_paiscob_id').val();

				$("#cod_cartera_id").val(cod_cartera_id);
				$("#cod_producto_id").val(cod_producto_id);
				$("#cod_alianza_code").val(cod_alianza_id);
				$("#agencia_id").val(agencia_id);
				$("#cod_alianza_id").val(cod_alianza_id);
				$("#cod_vendedor_id").val(cod_vendedor_id);
                $("#cod_paiscob_id").val(cod_paiscob_id);


				var form = $('#frm_insurance_main');
				$.ajax({
					type:	'POST',
					url:	'/insurance/save_insurance',
					data:	form.serialize(),
					success:function(data){
						console.log(data);
						if(data.success==1){
							console.log(data);
							w2ui.form.tabs.click('tab1');
							$('#id_poliza').val(data.arr_product.id_poliza);
							$('#action').val('EDIT');
							//alert(data.message);
							/*$("#btnTitular").removeClass("button_dis").addClass("button");
							$("#btnTitular").prop("disabled",false);*/
							w2ui['form'].destroy();
							//w2ui['form2'].destroy();
							var npc = $("#npc").val();
							$('#cont_cat').load('/insurance/formTitular/'+npc);
							 cargarEstado(4);
							return;
						}else{
							alert('{{$objform->msg_error_default}}');
							return;
						}
					}
				});
			} // Fin postFormularioCatValidado

			function procesarPostTitular(){
				var form = $('#frm_titular');
				$.ajax({
					type:'POST',
					url:'/insurance/create_tmp_titular',
					data:form.serialize(),
					success:function(data){
						console.log(data);
						if(data.success==1){
							var dir_pg = $("#dir_pag").val();
							if(dir_pg==0){
								regresarPantalla('/insurance/formInsurance',3);
							}else{
								w2ui['form'].destroy();
								w2ui['form2'].destroy();
								//$('#cont_cat').load('/insurance/formPay/');
								$('#cont_cat').load('/insurance/previewPoliza/');
							}
							return;
						}else if(data.success==2){
							
							alertPPYA(data.message);
						}
						else{
							alertPPYA('{{$objform->msg_error_default}}');
							return;
						}
					}
				});
			}  // Fin procesarPostTitular

			/******************************************************/
			
			/***************Process PAY METHOD ***************************/
			function previewPoliza(){
				var Radio = $("input[name='optradio']:checked").val();
				switch (parseInt(Radio)){
					case 1:
							var FormSelection = $('#FormCash');
							break;
					case 2:
							var FormSelection = $('#FormCheck');
							break;
					case 3:
							var FormSelection = $('#FormDebit');
							break;
					case 4:
							var FormSelection = $('#FormCard');
							break;
					case 5:
							var FormSelection = $('#FormTransfer');
							break;
				} // Fin switch
				$.ajax({
					type:	'POST',
					url:	'/insurance/save_methodPay/'+Radio,
					data:	FormSelection.serialize(),
					success:function(data){
						console.log(data.message);
						if(data.success==1){
							console.log(data);
							$('#cont_cat').load('/insurance/previewPoliza/');
							 cargarEstado(6);
							return;
						}else{
							alert('{{$objform->msg_error_default}}');
							return;
						}
					}
				});
			} // Fin previewPoliza
			/******************************************************/
			/*****************generar PDF**************************/
			function generarPDF(){
									$.ajax({
					type:	'GET',
					url:	'/insurance/generate_pdf/',
					success:function(data){
						console.log(data);
						if(data.success==1){
							console.log(data);
							var opt = {
									autoOpen: false,
									modal: true,
									width: 550,
									height:650,
									title: 'PDF'
							};
							var filePath = data.filePath;
							var path_f  = "{{asset('/pdf_ins/')}}"+"/"+filePath;
							$('#dialog_pdf iframe').attr('src', path_f);
							$('#mdPDF').modal('show');
							//$('#dialog_pdf object embed').attr('src', path_f);
							/*$( "#dialog_pdf" ).dialog( "open" );*/
							//$('#cont_cat').load('/insurance/previewPoliza/');
							 //cargarEstado(6);
							return;
						}else{
							alert('{{$objform->msg_error_default}}');
							return;
						}
					}
				});
				
			} // Fin  generarPDF
			/*************Procesar Datos Temporales Preview********/
			
			function guardarPreviewPoliza(){
					$.ajax({
					type:	'GET',
					url:	'/insurance/save_preview/',
					success:function(data){
						console.log(data);
						if(data.success==1){
							console.log(data);
							
							$("#btnGuardarTmpPoliza").prop("disabled",true);
							
							alert("{{$objform->ins_message_save}}");
							$('#cont_cat').load('/insurance/previewPoliza/');
							 //cargarEstado(6);
							return;
						}else{
							alert('{{$objform->msg_error_default}}');
							return;
						}
					}
				});
				
			} // Fin guardarPreviewPoliza

			/********************************************************/

			function cagarProductoNuevo(){
				//w2ui['form'].destroy();
				$('#cont_cat').load('/insurance/formInsurance');
				cargarEstado(3);
			} // Fin cagarProductoNuevo
			
			function cagarPolizaEditar(){
				
				if($("#id_sel").val()==0){
					alert('{{$objform->ins_msg_sel_record}}');
					
				}else{
					var id_poliza    = $("#id_sel").val();
					var cod_cli      = $("#cod_cliente").val();
					var cod_pr       = $("#cod_producto_sr").val();	
					var es_adicional = $("#es_adicional").val();

					if(es_adicional==1){
						alert('{{$objform->ins_msg_no_additional}}');
						return;
					}
                   // w2ui['form'].destroy();					
					$('#cont_cat').load('/insurance/formTitularEdit/'+id_poliza+'/'+cod_cli+'/'+cod_pr);
				    cargarEstado(3);
				}
				
				
			} // Fin cagarPolizaEditar


			/**seccion adicionales***/
			function cagarAdicionales(){
				if($("#id_sel").val()==0){
					alert('{{$objform->ins_msg_sel_record}}');
					
				}else{
					var id_poliza = $("#id_sel").val();
					var cod_cli = $("#cod_cliente").val();
					var cod_pr  = $("#cod_producto_sr").val();	
					var tarjeta = $("#tarjeta").val();						
					var cod_add = $("#id_sel").val();
                   // w2ui['form'].destroy();
				   
					//$('#cont_cat').load('/insurance/formAdicional/'+id_poliza+'/'+cod_cli+'/'+cod_pr+'/'+cod_add+'/'+tarjeta+'/');
					$('#dv_form_adicioanles').load('/insurance/formAdicional/'+id_poliza+'/'+cod_cli+'/'+cod_pr+'/'+cod_add+'/'+tarjeta+'/', function(event){
						
						$('#myModalAd').modal('show');
					});
					
				    cargarEstado(3);
				}
				
			}
			

			function validateAdditional(){
				console.log('validateAdditional');
					if(parseInt($("#id_producto").val()) == 0){
						alertPPYA('Debe seleccionar un producto');
						return;
					}
					var FormSelection = $("#frmAdicionales");
					$.ajax({
							type:	'POST',
							url:	'/insurance/validate_additional/',
							data:	FormSelection.serialize(),
							success:function(data){
								console.log(data);
								console.log(data.message);
								if(data.success==1){
									saveAdditional();
									return;
								}else{
									$('#myModalConfirm').modal('show');
									return;
								}
							}
					});    	
			}   


			function saveAdditional(){
					console.log('saveAdditional');
					if(parseInt($("#id_producto").val()) == 0){
						alertPPYA('Debe seleccionar un producto');
						return;
					}

					if(parseInt($("#ad_valor").val()) == 0){
						alertPPYA('Debe ingresar un valor');
						return;
					}			
					var FormSelection = $("#frmAdicionales");
					$.ajax({
							type:	'POST',
							url:	'/insurance/save_additional/',
							data:	FormSelection.serialize(),
							success:function(data){
								console.log(data);
								console.log(data.message);
								if(data.success==1){
									console.log(data);
									$('#myModalConfirm').modal('hide');
									$('#myModalAd').addClass('modal-backdrop-hid');
									$('#myModalAdContent').addClass('modal-backdrop-hid');
									 
									alertPPYA('Registro guardado correctamente');
									/*$('#myModalAd').removeClass('modal-backdrop');
									$('#myModalAdContent').removeClass('modal-backdrop');*/
									 $("#sel_pr").val($("#sel_pr option:first").val());
									 $("#ad_valor").val(0);
									// cargarEstado(6);
									return;
								}else{
									alertPPYA('{{$objform->msg_error_default}}');
									return;
								}
							}
					});
			}			
			
			/***fin de seccion adicionales***/

			function cagarProductoEditar(urlpage){
				$('#cont_cat').load(urlpage);
				cargarEstado(3);
			}

			function regresarPantalla(){
				window.location.href = "/home_product/{{$api_code}}";
				cargarEstado(1);
			}

			function regresarPantallaScr(prev_screen){
				if(prev_screen==4){
					w2ui['form'].destroy();
					$('#cont_cat').load('/catalog_sel_pr');
					cargarEstado(4);
				}
			}

			function regresarPantallaPRV(prev_screen){
				cagarProductoNuevo();
				cargarEstado(1);
			}
		
			function regresarPantallaInicio(m_cod, op){
				//destroyelement();
				var url_pg = '/insurance/intro/14/'+m_cod+'/';
				/*var object =     '<object type="text/html" width="100%" height="900px" id="frame_id" data="'+url_pg+'">'+
								'              <p>Si puede leer este mensaje es que su navegador no soporta correctamente el elemento <code>object</code></p>'+
								'           </object>';
                $('#main_content').empty();
				$('#main_content').html('');
				
                $(object).appendTo('#main_content').ready(function(){

                  });
				  
				  
				  $('#main_content', parent.document).html(object);*/
				  
				   $('#main_content').load(url_pg);
				///cargarEstado(op);
			} // Fin regresarPantallaInicio

			function cargarLoading(){
				Pace.restart();
			}

			function cargaDivPage(urlpage){
				$('#cont_cat').load(urlpage);
				
			}

			function regresarPantalla(urlpage,op){
				if(typeof w2ui['form'] != "undefined"){
					w2ui['form'].destroy();
				}
				if(typeof w2ui['form2'] != "undefined"){
					w2ui['form2'].destroy();
				}
				$('#cont_cat').load(urlpage);
				cargarEstado(op);
			}
		  
			function cargaDivPage2(urlpage){
				$('#cont_cat').load(urlpage);
				cargarEstado(3);
			}

			function cargarEstado(intpag){
			/*	setTimeout(function() {
					$('.progress .circle:nth-of-type(' + intpag + ')').addClass('active');
					$('.progress .circle:nth-of-type(' + (intpag-1) + ')').removeClass('active').addClass('donepgr');
					$('.progress .circle:nth-of-type(' + (intpag-1) + ') .label').html('&#10003;');
					
					if (i==0) {
						$('.progress .bar').removeClass().addClass('bar');
						$('.progress div.circle').removeClass().addClass('circle');
						i = 1;
					}
				}, 1000);*/
			} // Fin cargarEstado


            function recargarTracker(){
					/*var i = 1;
					$('.progress .circle').removeClass().addClass('circle');
					$('.progress .bar').removeClass().addClass('bar');
					$('.progress .circle').addClass('donepgr');
 					setTimeout (function() {
						if(i=1){
							$('.progress .circle:nth-of-type(' + i + ')').addClass('active');
							$('.progress .circle:nth-of-type(' + (i-1) + ')').removeClass('active').addClass('donepgr');
							$('.progress .circle:nth-of-type(' + (i-1) + ') .label').html('&#10003;');
							
							$('.progress .bar:nth-of-type(' + (i-2) + ')').removeClass('active').addClass('donepgr');
						}
					}, 1000);*/

            }
		</script>