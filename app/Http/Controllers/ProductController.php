<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\GenFunction;
use App\webProduct;
use App\WebRanges;
use App\WebPerson;
use App\WebSale;
use App\WebSalesDependent;
use App\GenApiMaster;
use App\GenParam;
use App\GenProductAgeCondition;
use App\GenCoverage;
use Illuminate\Support\Facades\DB;
use App\BtnP2pTran;

class ProductController extends Controller{

	/**
	*	Show home page
	*
	*	@return \Illuminate\Http\Response
	*/
	public function showHome(Request $request){
		$arrWebProduct = webProduct::where('pro_status', 1)->get();
		
		GenFunction::createLBL("wlb_home_us", 23, 2);
		GenFunction::createLBL("wlb_home_client", 23, 2);
		GenFunction::createLBL("wlb_home_contact", 23, 2);
		GenFunction::createLBL("wlb_home_login", 23, 2);
		GenFunction::createLBL("wlb_home_1700", 23, 2);
		GenFunction::createLBL("wlb_home_title", 23, 2);
		
		GenFunction::createLBL("wlb_home_pae", 23, 2);
		GenFunction::createLBL("wlb_home_company", 23, 2);
		GenFunction::createLBL("wlb_home_live", 23, 2);
		GenFunction::createLBL("wlb_home_info", 23, 2);
		
		GenFunction::createLBL("wlb_home_btnQ", 23, 2);
		
		GenFunction::createLBL("wlb_home_titD", 23, 2);
		GenFunction::createLBL("wlb_home_titF", 23, 2);
		
		GenFunction::createLBL("wlb_home_col1_li1", 23, 2);
		GenFunction::createLBL("wlb_home_col1_li2", 23, 2);
		GenFunction::createLBL("wlb_home_col1_li3", 23, 2);
		GenFunction::createLBL("wlb_home_col1_li4", 23, 2);
		GenFunction::createLBL("wlb_home_col1_li5", 23, 2);
		GenFunction::createLBL("wlb_home_col1_li6", 23, 2);
		
		GenFunction::createLBL("wlb_home_titLn1", 23, 2);
		GenFunction::createLBL("wlb_home_titLn2", 23, 2);
		
		GenFunction::createLBL("wlb_home_btnP", 23, 2);
		
		$objlabels = GenFunction::getLabels(2, 23);
		
		$request->session()->forget('requestId');
		$request->session()->forget('first_pay');
		$request->session()->forget('sale_id');
		
		if(\Auth::user()){
				return redirect('/home');
		 }else{
				return view('home.home_product_list')
								->with('arrWebProduct', $arrWebProduct)
								->with('objlabels', $objlabels);			  
		 }				
				
				
	}

	/**
	*	Show frequently asked questions.
	*
	*	@return \Illuminate\Http\Response
	*/
	public function showFrequentQ(){
		$arrWebProduct = webProduct::where('pro_status', 1)->get();
		
		GenFunction::createLBL("wlb_home_us", 23, 2);
		GenFunction::createLBL("wlb_home_client", 23, 2);
		GenFunction::createLBL("wlb_home_contact", 23, 2);
		GenFunction::createLBL("wlb_home_login", 23, 2);
		GenFunction::createLBL("wlb_home_1700", 23, 2);
		GenFunction::createLBL("wlb_home_title", 23, 2);
		
		GenFunction::createLBL("wlb_home_pae", 23, 2);
		GenFunction::createLBL("wlb_home_company", 23, 2);
		GenFunction::createLBL("wlb_home_live", 23, 2);
		GenFunction::createLBL("wlb_home_info", 23, 2);
		
		GenFunction::createLBL("wlb_home_btnQ", 23, 2);
		
		GenFunction::createLBL("wlb_home_titD", 23, 2);
		GenFunction::createLBL("wlb_home_titF", 23, 2);
		
		GenFunction::createLBL("wlb_home_col1_li1", 23, 2);
		GenFunction::createLBL("wlb_home_col1_li2", 23, 2);
		GenFunction::createLBL("wlb_home_col1_li3", 23, 2);
		GenFunction::createLBL("wlb_home_col1_li4", 23, 2);
		GenFunction::createLBL("wlb_home_col1_li5", 23, 2);
		GenFunction::createLBL("wlb_home_col1_li6", 23, 2);
		
		GenFunction::createLBL("wlb_home_titLn1", 23, 2);
		GenFunction::createLBL("wlb_home_titLn2", 23, 2);
		
		GenFunction::createLBL("wlb_home_btnP", 23, 2);
		
		$objlabels = GenFunction::getLabels(2, 23);
		
		return view('home.home_frequent_questions')
				->with('arrWebProduct', $arrWebProduct)
				->with('objlabels', $objlabels);
	}
	
	/**
	*	Display a listing of the Products.
	*
	*	@return \Illuminate\Http\Response
	*/
    public function showWebProduct(){
        //
		$arrWebProduct = webProduct::where('pro_status', 1)->get();
		
		return view('home.products')
				->with('arrWebProduct', $arrWebProduct);
    }

	/**
	*	Quote the selected product.
	*
	*	@return \Illuminate\Http\Response
	*/
    public function detailWebProduct(Request $request, $idProduct){
        //
		$arrWebRanges  = WebRanges::where('ran_status', 1)->get();
		$arrWebProduct = webProduct::find($idProduct);
		$precio = $arrWebProduct->pro_valor;
		$request->session()->put('id_product',$idProduct);

		$objApiMaster   = GenApiMaster::where('api_action','WEB_PRODUCT')->where('api_parent',0)->first();
		//Recover Product API
		$user_name      = 'WEB';
		$lang_id        = 2;
		$api_code       = $objApiMaster->api_code;
		$attr 			= '&id='.$arrWebProduct->pro_code.'&categoria=';
		$json_pr        = GenFunction::procesarApiGen($api_code,$lang_id, 'SEARCH',$user_name,$attr,'');
		$request->session()->put('pr_sel',$json_pr);
		
		$objPrCond     =  GenProductAgeCondition::where('pac_product_code',$arrWebProduct->pro_code)->get();
		$request->session()->put('product_cond',$objPrCond);

		return view('quotation.quotation')
			   ->with('arrWebRanges', $arrWebRanges)
			   ->with('idProduct', $idProduct)
			   ->with('arrWebProduct', $arrWebProduct)
			   ->with('precio', $precio);
	
    } // detailWebProduct

	/**
	*	Quote the selected product.
	*
	*	@return \Illuminate\Http\Response
	*/
    public function detailWebProductTit(Request $request, $idProduct){
        //
		$request->session()->forget('requestId');
		$request->session()->forget('first_pay');		
		
		$arrWebRanges  = WebRanges::where('ran_status', 1)->get();
		$arrWebProduct = webProduct::find($idProduct);
		$precio = $arrWebProduct->pro_price;
		$request->session()->put('id_product',$idProduct);
		
		$objApiMaster   = GenApiMaster::where('api_action','WEB_PRODUCT')->where('api_parent',0)->first();
		//Recover Product API
		$user_name      = 'WEB';
		$lang_id        = 2;
		$api_code       = $objApiMaster->api_code;
		$attr 			= '&id='.$arrWebProduct->pro_code.'&categoria=';
		$json_pr        = GenFunction::procesarApiGen($api_code,$lang_id, 'SEARCH',$user_name,$attr,'');
		$request->session()->put('pr_sel',$json_pr);
		
		$objPrCond     =  GenProductAgeCondition::where('pac_product_code',$arrWebProduct->pro_code)->get();
		$request->session()->put('product_cond',$objPrCond);


		//edit
		$arr_edit = array();
		if($request->session()->has('sale_id')){
				$sale_id =  $request->session()->get('sale_id');
				$objSale =  WebSale::find($sale_id);
				$arr_edit['WebSale'] = $objSale;	
				$wp_id = $request->session()->get('cli_id');
				$objWebPerson =  WebPerson::find($wp_id);
				$arr_edit['WebPerson'] = $objWebPerson;
				
		}


		return view('quotation.titular_data')
									   ->with('arrWebRanges', $arrWebRanges)
									   ->with('idProduct', $idProduct)
									   ->with('arrWebProduct', $arrWebProduct)
									   ->with('arr_edit', $arr_edit)
									   ->with('precio', $precio);
    } // detailWebProduct

	public function detailWebProductTitCol(Request $request, $idProduct,$idCol,$val_sel=0){
//
		$request->session()->forget('requestId');
		$request->session()->forget('first_pay');	
		$request->session()->forget('idCol');
        $request->session()->forget('val_sel_col');		
		
		$arrWebRanges  = WebRanges::where('ran_status', 1)->get();
		$arrWebProduct = webProduct::find($idProduct);
		$precio = $arrWebProduct->pro_price;
		$request->session()->put('id_product',$idProduct);
		$request->session()->put('idCol',$idCol);
		
		$objApiMaster   = GenApiMaster::where('api_action','WEB_PRODUCT')->where('api_parent',0)->first();
		//Recover Product API
		$user_name      = 'WEB';
		$lang_id        = 2;
		$api_code       = $objApiMaster->api_code;
		$attr 			= '&id='.$arrWebProduct->pro_code.'&categoria=';
		$json_pr        = GenFunction::procesarApiGen($api_code,$lang_id, 'SEARCH',$user_name,$attr,'');
		$request->session()->put('pr_sel',$json_pr);
		
		$objPrCond     =  GenProductAgeCondition::where('pac_product_code',$arrWebProduct->pro_code)->get();
		$request->session()->put('product_cond',$objPrCond);
		$request->session()->put('val_sel_col',$val_sel);

		//edit
		$arr_edit = array();
		if($request->session()->has('sale_id')){
				$sale_id =  $request->session()->get('sale_id');
				$objSale =  WebSale::find($sale_id);
				$arr_edit['WebSale'] = $objSale;	
				$wp_id = $request->session()->get('cli_id');
				$objWebPerson =  WebPerson::find($wp_id);
				$arr_edit['WebPerson'] = $objWebPerson;
				
		}


		return view('quotation.titular_data')
									   ->with('arrWebRanges', $arrWebRanges)
									   ->with('idProduct', $idProduct)
									   ->with('arrWebProduct', $arrWebProduct)
									   ->with('arr_edit', $arr_edit)
									   ->with('precio', $precio);		
	}
	
	/**
	*	Get Data Client
	*
	*	@return \Illuminate\Http\Response
	*/
	public function getDataClient(Request $request){
		try{
		   $cli_id    = $request->session()->get('cli_id');
		   $objPerson =  WebPerson::find($cli_id);
		   if(empty($objPerson)){
			 return response()->json(['success'=>0,'message'=>'No existen datos de cliente '.$cli_id]);  
		   }
		   $data_person = array();
		   $data_person['nombre']    = $objPerson->per_name;
		   $data_person['apellido']  = $objPerson->per_surname;
		   $data_person['id']        = $objPerson->per_id;
		   $data_person['direccion'] = $objPerson->per_address;
		   $data_person['email'] = $objPerson->per_email;
		   
		   
		   $status = 1;
			$mensaje='OK';
			$response = array(
				'success' 		 => $status,
				'message' 		 => $mensaje,
				'data_person' 	 => $data_person
			);
			return response()->json($response);		   
		   
		}catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}
   }
   
	/**
	*	Get Product Detail
	*
	*	@return \Illuminate\Http\Response
	*/
	public function getProductDetail(Request $request,$id_pr){
	  		try{

				$arrWebProduct = webProduct::find($id_pr);
				$objApiMaster   = GenApiMaster::where('api_action','WEB_PRODUCT')->where('api_parent',0)->first();
				//Recover Product API
				$user_name      = 'WEB';
				$lang_id        = 2;
				$api_code       = $objApiMaster->api_code;
				$attr 			= 'id='.$arrWebProduct->pro_code.'&categoria=';
				$json_pr        = GenFunction::procesarApiGen($api_code,$lang_id, 'SEARCH',$user_name,$attr,'');
				
				$objPrCond     =  GenProductAgeCondition::where('pac_product_code',$arrWebProduct->pro_code)->get();
				$request->session()->put('product_cond',$objPrCond);
		   
		        $html='<table width="100%">';
				$html.='<tr>';
					$html.='<th>';
					$html.='Plan';
					$html.='</th>';
					$html.='<th>';
					$html.='Total Personas';
					$html.='</th>';		
					$html.='<th>';
					$html.='Renovable';
					$html.='</th>';
				$html.='</tr>';		


		        $html_cov='<table width="100%">';
				$html_cov.='<tr>';
					$html_cov.='<th>';
					$html_cov.='Servicio';
					$html_cov.='</th>';
					$html_cov.='<th>';
					$html_cov.='Cantidad';
					$html_cov.='</th>';		
				$html_cov.='</tr>';						
				
				if(!empty($json_pr[0])){
					$html.='<tr>';
						$html.='<td>';
						$html.=$json_pr[0]['nombre'];
						$html.='</td>';
						$html.='<td>';
						$html.=$json_pr[0]['npc'];
						$html.='</td>';		
						$html.='<td>';
						$renovable = 'Si';
						if($json_pr[0]['renovable']=='N'){
							$renovable = 'No';	
						}
						$html.=$renovable;
						$html.='</td>';
					$html.='</tr>';	

					$id_cov = $json_pr[0]['cobertura'];
				
					$objCobertura  = DB::SELECT("select a.*,  ser_name
												from
													  gen_coverages_details a,  gen_services s
												where a.cov_code  = ".$id_cov."
												and  a.mdv_code = s.id and cvd_status=1");
					foreach($objCobertura as $rw){
						$html_cov.='<tr>';
											$html_cov.='<td>';
											$html_cov.=$rw->ser_name;
											$html_cov.='</td>';
											$html_cov.='<td>';
											$html_cov.=$rw->cvd_quantity;
											$html_cov.='</td>';		
						$html_cov.='</tr>';									
						
					}
				}
				
				$html_cov.='</table>';	
				
		        $html.='</table>';
				
				$html.=$html_cov;
				$status = 1;
				$mensaje='OK';
				$response = array(
					'success' 		 => $status,
					'message' 		 => $mensaje,
					'html' 	 => $html
				);
				return response()->json($response);		   
		   
		}catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		} 
   }

	/**
	*	Save customer data
	*
	*	@return \Illuminate\Http\Response
	*/
	public function titSave(Request $request){
		try{
		    $idProduct = $request->session()->get('id_product');
		    $objPerson =  WebPerson::where('per_id',$request->id)->first();
			if(empty($objPerson)){
				$objPerson = new WebPerson;
			}
			
			$objPerson->per_id      = $request->id;
			$objPerson->per_name    = $request->nombre;
			$objPerson->per_surname = $request->apellido;
			$objPerson->per_email   = $request->email;
			$objPerson->per_address = $request->per_address;
			$objPerson->per_phone   = $request->per_phone;
			$objPerson->per_type    = 1;
			$objPerson->per_status  = 1;
		    $objPerson->save();
			
			if(empty($objPerson)){
				return response()->json(['success'=>0,'message'=>'Hubo un error al guardar la informacion del titular']);
			}
			$request->session()->put('per_id',$request->id);
			$request->session()->put('cli_id',$objPerson->Id);
			
			
			$objSale =  new WebSale;
			$objSale->id_product  = $idProduct;
			$objSale->id_person   = $objPerson->Id;
			$objSale->sal_date    = now();
			$objSale->sal_token   = $idProduct.'_'.now();
			$objSale->sal_reference   ='';
			$objSale->agency_code   =0;
			$objSale->seller_code   =1;
			$objSale->sal_periodo   ='1,12,4';
			$objSale->sal_status   =1;
			$objSale->save();
			
			$request->session()->put('sale_id',$objSale->id);
			
			$status = 1;
			$mensaje='OK';
			$response = array(
				'success' 		 => $status,
				'message' 		 => $mensaje
			);
			return response()->json($response);		   
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}
   }
   
	/**
	*	Save Multiprever
	*
	*	@return \Illuminate\Http\Response
	*/
	public function multSave(Request $request){
		try{
		    
		    $idProduct = $request->session()->get('id_product');
		    $objPerson =  WebPerson::where('per_id',$request->id)->first();
			if(empty($objPerson)){
				$objPerson = new WebPerson;
			}
			
			$objPerson->per_id      = $request->id;
			$objPerson->per_name    = $request->nombre;
			$objPerson->per_surname = $request->apellido;
			$objPerson->per_email   = $request->email;
			$objPerson->per_address = $request->per_address;
			$objPerson->per_phone   = $request->per_phone;
			$objPerson->per_type    = 1;
			$objPerson->per_status  = 1;
		    $objPerson->save();
			
			if(empty($objPerson)){
				return response()->json(['success'=>0,'message'=>'Hubo un error al guardar la informacion del titular']);
			}
			$request->session()->put('per_id',$request->id);
			
			
			$objSale =  new WebSale;
			$objSale->id_product  = $idProduct;
			$objSale->id_person   = $objPerson->Id;
			$objSale->sal_date    = now();
			$objSale->sal_token   = $idProduct.'_'.now();
			$objSale->sal_reference   ='';
			$objSale->sal_status   =1;
			$objSale->save();
			
			$request->session()->put('sale_id',$objSale->id);
			
			
			///dependents
			$arrWebRanges  = WebRanges::where('ran_status', 1)->get();
			for($i=0;$i<count($arrWebRanges);$i++){
				$id_range =  $arrWebRanges[$i]->Id;
				$total    =  $request->input('quantity'.$i);
				for($j=0;$j<$total;$j++){
						$objPersonDep = new WebPerson;
						
						$objPersonDep->per_id      = '';
						$objPersonDep->per_name    = '';
						$objPersonDep->per_surname = '';
						$objPersonDep->per_email   = '';
						$objPersonDep->per_address = '';
						$objPersonDep->per_phone   = '';
						$objPersonDep->per_type    = 2;
						$objPersonDep->per_status  = 1;
						$objPersonDep->save();	

					    $objWSD = new WebSalesDependent;
						$objWSD->id_person = $objPersonDep->Id;
						$objWSD->id_sales  = $objSale->id;
						$objWSD->sxd_status  = 1;
						$objWSD->id_range    = $id_range;
						$objWSD->save();
						 
				}
			}
			
			
			$status = 1;
			$mensaje='OK';
			$response = array(
				'success' 		 => $status,
				'message' 		 => $mensaje
			);
			return response()->json($response);		   
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}
	}   
   
	/**
	*	Dependent last
	*
	*	@return \Illuminate\Http\Response
	*/
	public function dependentLast(Request $request){
	    $idProduct =  $request->session()->get('id_product');
	    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
	    $arrWebProduct = webProduct::find($idProduct);
	    $precio        = $arrWebProduct->pro_price;
		$id_sale  = $request->session()->get('sale_id');
		$objWSD   = WebSalesDependent::where('id_sales',$id_sale)->get();
		
		if(!empty($objWSD) && count($objWSD)>0)	{
			 return view('payment.dependents_detail')
									   ->with('arrWebRanges', $arrWebRanges)
									   ->with('objWSD', $objWSD)
									   ->with('idProduct', $idProduct)
									   ->with('arrWebProduct', $arrWebProduct)
									   ->with('precio', $precio);
		}else{
			 return view('payment.final_detail')
						   ->with('arrWebRanges', $arrWebRanges)
						   ->with('idProduct', $idProduct)
						   ->with('arrWebProduct', $arrWebProduct)
						   ->with('precio', $precio);
		}
	}

	/**
	*	select the payment method
	*
	*	@return \Illuminate\Http\Response
	*/
    public function paySel(Request $request){
		$idProduct 	   = $request->session()->get('id_product');
	    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
	    $arrWebProduct = webProduct::find($idProduct);
	    $precio        = $arrWebProduct->pro_price;
							   
                               
	   return view('payment.payment_select')
									   ->with('arrWebRanges', $arrWebRanges)
									   ->with('idProduct', $idProduct)
									   ->with('arrWebProduct', $arrWebProduct)
									   ->with('precio', $precio);
    } // detailWebProduct

	/*
	*	Funcion que calcula la edad en base a una fecha
	*	Function that calculates age based on a date
	*/
	public function Edad(Request $request){
		$fecha_nacimiento = $request->birthday;
		$dia=date("d");
		$mes=date("m");
		$ano=date("Y");

		$dianaz=date("d",strtotime($fecha_nacimiento));
		$mesnaz=date("m",strtotime($fecha_nacimiento));
		$anonaz=date("Y",strtotime($fecha_nacimiento));

		//si el mes es el mismo pero el día inferior aun no ha cumplido años, le quitaremos un año al actual
		if (($mesnaz == $mes) && ($dianaz > $dia)) {
			$ano=($ano-1);
		}

		//si el mes es superior al actual tampoco habrá cumplido años, por eso le quitamos un año al actual
		if ($mesnaz > $mes) {
			$ano=($ano-1);
		}
		
		if(!$request->session()->has('product_cond')){
			return response()->json(['success'=>0,'message'=>'No existen condiciones de edad']);
		}


		//ya no habría mas condiciones, ahora simplemente restamos los años y mostramos el resultado como su edad
		$edad=($ano-$anonaz);
        
		if($request->session()->has('product_cond')){
			if(!empty($request->session()->get('product_cond'))){
				$arr_pr  = $request->session()->get('product_cond');
				foreach($arr_pr as $rr){
				  if($rr->kin_code==1){	
					if($edad<$rr->pac_age_begin || $edad>$rr->pac_age_end ){
						return response()->json(['success'=>0,'message'=>'la edad ingresada no esta dentro de los rangos permitidos']);
					}
				  }
				}//end of foreach
			}
		}

		if($edad>0)
			return response()->json(['success'=>1,'age'=>$edad]);
		else
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);

	} // Fin Edad
	
	/*
	*	Funcion que calcula la fecha de nacimiento en base a una edad
	*	Function that calculates the date of birth based on an age
	*/
	public function birthday(Request $request){
		$edad = (int) $request->edad;
		$dia  = date("d");
		$mes  = date("m");
		$ano  = date("Y");
		
		$birthday = $ano - $edad;
		$birthday = $birthday . "-" . $mes . "-" . $dia;

		
		if($request->session()->has('product_cond')){
			if(!empty($request->session()->get('product_cond'))){
				$arr_pr  = $request->session()->get('product_cond');
				foreach($arr_pr as $rr){
				  if($rr->kin_code==1){	
					if($edad<$rr->pac_age_begin || $edad>$rr->pac_age_end ){
						return response()->json(['success'=>0,'message'=>'la edad ingresada no esta dentro de los rangos permitidos']);
					}//end if($edad<$rr->pac_age_begi
				  }
				}//end of foreach
			}
		}		
		
		if($birthday != "")
			return response()->json(['success'=>1,'birthday'=>$birthday]);
		else
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		
	} //Fin birthday
	
	/**
	*	Display a listing of the Products.
	*
	*	@return \Illuminate\Http\Response
	*/
	public function transactionList(Request $request){
		GenFunction::createLBL("wlb_trans_id", 23, 2);
		GenFunction::createLBL("wlb_trans_status", 23, 2);
		GenFunction::createLBL("wlb_trans_message", 23, 2);
		GenFunction::createLBL("wlb_trans_date", 23, 2);
		GenFunction::createLBL("wlb_trans_requestid", 23, 2);
		GenFunction::createLBL("wlb_trans_cliname", 23, 2);
		GenFunction::createLBL("wlb_trans_clisurname", 23, 2);
		GenFunction::createLBL("wlb_trans_clinumdoc", 23, 2);
		GenFunction::createLBL("wlb_trans_clienmail", 23, 2);
		GenFunction::createLBL("wlb_trans_cliphone", 23, 2);
		GenFunction::createLBL("wlb_trans_prodname", 23, 2);
		GenFunction::createLBL("wlb_trans_proqty", 23, 2);
		GenFunction::createLBL("wlb_trans_proprice", 23, 2);
		$objlabels = GenFunction::getLabels(2, 23);

		$arrWebtrans = BtnP2pTran::where('bpt_estado', 1)->get();


		return view('transactions.trans_list')
			  ->with('objlabels', $objlabels)
			  ->with('arrWebtrans', $arrWebtrans);        
    }
	

}