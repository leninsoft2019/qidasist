<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalBilling extends Model{
    //
	protected $table = 'sal_billing';
	protected $primaryKey = 'bil_id';
}
