<?php
/*
*	Página que despliega la tabla de productos a cotizar o vender
*	Page that displays the table of products to be quoted or sold
*/

?>



<div class="contenedor_mt">

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<style scoped>


#frmMT{
	width:90%;
	margin:auto;
		
		border:solid 1px  rgb(56, 30, 61,0.9);
		/*border-radius:5px;*/
		margin-top:20px;
		margin-bottom:20px;
		padding:20px;
		/*-webkit-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
		-moz-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
		box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);*/
	 background-color:white;
}

#cont_dep{
	height:550px;
	overflow-y: scroll;	
}


.row {
    display: -webkit-box;
    display: -ms-flexbox;
    
	width:100%;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}

.row_n{
	display:table;
	width:100%;
}

.tbl{
	display:table;
	width:100%;	
}
.rw{
	display:table-row;
	width:100%;
}

.col{
    position: relative;
    width: 50%;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
	
	display:table-cell;
}


.col2{
    position: relative;
    width: 50%;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
	
	display:table-cell;
}

.form-control {
    display: block;
    width: 100%;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}


.form-control {
    padding: 2px;
    height: 28px;
    border: 1px solid #dddddd;
}

h2{
	font-family: "Rubik",Helvetica,Arial,sans-serif;
	font-size:2em;
	font-weight:500;
}



.col_add{
	width:25%;
}

.col_minus{
	width:25%;
}

.col_range{
	width:25%;
}

.col_50{
	display:table-cell;
	width:50%;
}

.col_25{
		display:table-cell;
	width:13%;
}
.col_5{
	display:table-cell;
	width:5%;
}


@media (max-width: 767px) {

	.text-xxl {
		font-size: 50px !important;
		line-height: 60px !important;
	}

h1{
		 text-indent:-9999px;
		font-size:0px;
		position:absolute;
	}

	h2, .text-l {
		margin-top:20px;
		font-size: 1.5em !important;
		line-height: 1em !important;
		
	}

	h3 {
		font-size: 18px !important;
		line-height: 28px !important;
	}
	
	
	#frmMT{
		
		width:98%;
		maring:auto;
		padding:5px;
	}
	
	#frmMT>label{
		font-size:0px;
		text-indent:-9999px;
		position:absolute;
	}
	
	.row{
				display:table;
		width:100%;
	}
	.col{
		display:table-row;
		width:100%;
	}


.contenedor_mt {
    padding: 0px;
    width: 100%;

}


.res_input{
	width:40px;
	height:20px;
}

.cab_r{
	padding-left:20px;
}

.col_add{
	width:25%;
}

.col_minus{
	width:25%;
}

.col_range{
	width:25%;
}


}
</style>


   

	<form  id="frmMT">
	{{ csrf_field() }}	
	 <h2>{{$arrWebProduct->pro_name}}: Ingreso de dependientes</h2>
		<!-- Titular -->
		<?php $j=1;?>
		<div id="cont_dep">
			<?php
				$tot = 0;
      		?>
		
		@if(isset($npc))
			<?php
				$tot =  $npc-1;
      		?>
		@endif	
		@if(isset($objWSD))
			<?php
				$tot =  count($objWSD);
      		?>
		@endif			
		
		<input type="hidden" name="total" id="total" value="{{$tot}}" />
		@for($i=0;$i<$tot;$i++)
		    <fieldset>
			<legend>Dependiente {{$j}}</legend>
				<div class="tbl">
				<div class="row cab_r rw">
					<div class="col col_25">
						<div class="form-group">
							<label for="email">Relacion:</label>
							<select name="kinship[]" id="kinship[]"  class="form-control">
							<option value="0">-----</option>
							@foreach($cmbKinship as $rw)
									<option value="{{$rw->kin_code}}">
									{{$rw->kin_name}}
									</option>
							@endforeach
							</select>
						</div>
					</div>					
					<div class="col col_25">
						<div class="form-group">
							<label for="nombre">Nombre:</label>
							<input type="text" class="form-control" id="nombre[]" name="nombre[]"  placeholder="Nombres" />
						</div>
					</div>
					<div class="col col_25">
						<div class="form-group">
							<label for="apellido">Apellido:</label>
							<input type="text" class="form-control" id="apellido[]" name="apellido[]"  placeholder="Apellidos" />
						</div>
					</div>
				<!--</div>
				<div class="row cab_r rw">-->
					<div class="col col_25">
						<div class="form-group">
							<label for="id">Identificaci&oacute;n:</label>
							<input type="text" class="form-control" id="id.{{$j}}" name="id[]" placeholder="Identificacion"  />
						</div>
					</div>
					<div class="col col_25">
						<div class="form-group">
							<label for="email">Correo Electrónico:</label>
							<input type="email" class="form-control" id="email[]" name="email[]"  placeholder="Email" />
						</div>
					</div>
					<div class="col col_25">
						<div class="form-group">
							<label for="edad">Fecha de nacimiento:</label>
							<input type="text" class="form-control datepicker cls_date" id="fecnac_{{$i}}"  name="fecnac[]"   placeholder="Fecha nacimiento" onChange="calculaEdad(1,'fecnac_{{$i}}',{{$i}});"  autocomplete="off" />
						</div>
					</div>
					<div class="col col_5">
						<div class="form-group">
							<label for="edad">Edad:</label>
							<input type="text" class="form-control cs_int" id="edad_{{$i}}" name="edad[]"  onChange="calculaEdad(2,'edad_{{$i}}',{{$i}});"  placeholder="Edad" />
						</div>
					</div>					
				
				</div>
				</div>
			</fieldset>	
			<?php $j++;?>

			@endfor		
		
		</div>
		<!-- Titulos -->

		
		<!-- Siguiente -->
		<div class="row_n">
			<div class="col2 col_range">
				<div class="form-group"></div>
			</div>
			<div class="col2 col_add">
				<div class="form-group"></div>
			</div>		
			<div class="col2">
				<button id="btnRegresar" class="btn btn-default">Regresar</button>
			</div>		
			<div class="col2">
				<button id="btnNextPy" class="btn btn-default">Continuar</button>
			</div>
		</div>
	</form>
</div>

<script>
	function recalcula(){
		var tot_lines = $("#tot_lines").val();
		var precio = $("#precio").val();
		precio = parseFloat(precio);
		var qty_line = quantity = 0;
		var tot_price = 0;
		for (var i = 0; i < tot_lines; i++) {
			qty_line = $("#quantity"+i).val();
			quantity += parseInt(qty_line);
			//alert(qty_line+" - "+quantity);
		}
		quantity = parseFloat(quantity * precio);
		//alert(quantity);
		$("#totales").val(quantity.toFixed(2));
		
	} // Fin recalcula

	function add(linea){
		var quantity = $("#quantity"+linea).val();
		quantity++;
		$("#quantity"+linea).val(quantity);
		recalcula();
	} // Fin add
	
	function minus(linea){
		var quantity = $("#quantity"+linea).val();
		quantity = quantity - 1;
		$("#quantity"+linea).val(quantity);
		recalcula();
	} // Fin add
	
    function calculaEdad(op,id_s,ind){
		if(op==1){
			/*var birthday = $("#fbirthday").val();*/
			var birthday = $("#"+id_s).val();
			birthday = birthday.replace("/", "-");
			birthday = birthday.replace("/", "-");
			var request = $.ajax({
				type:	'GET',
				url:	'/generic/edad',
				data:	{'birthday':birthday},
				success:function(data){
					console.log(data);
					if(data.success==1){
						$("#edad_"+ind).val(data.age);
						return;
					}else{
						//alert('Hubo un problema al tratar de generar la informacion');
						alertPPYA(data.message);
						$("#edad_"+ind).val(0);
						/*
						$("#edad").trigger('change');*/
						console.log(data);
						return;
					}
				}
			});
			request.fail(function( jqXHR, textStatus ) {
				alert( "Hubo un problema: " + textStatus );
				console.log(jqXHR);
			});
		}else{
			/*var edad = $("#edad").val();*/
			var edad = $("#"+id_s).val();
			var request = $.ajax({
				type:	'GET',
				url:	'/generic/birthday',
				data:	{'edad':edad},
				success:function(data){
					console.log(data);
					if(data.success==1){
						$("#fecnac_"+ind).val(data.birthday);
						return;
					}else{
						/*alert('Hubo un problema al tratar de generar la informacion');*/
						alertPPYA(data.message);
						$("#edad_"+ind).val(0);
						/*$("#edad").val(0);*/
						/*$("#edad").trigger('change');*/
						console.log(data);
						return;
					}
				}
			});
			request.fail(function( jqXHR, textStatus ) {
				alert( "Hubo un problema: " + textStatus );
				console.log(jqXHR);
			});
		}
		
	} // Fin calculaEdad	
	
	
	
	
	$(function () {
	
			$('.datepicker').datepicker({
				dateFormat: 'yy-mm-dd',
				changeMonth: true,
				changeYear: true
				});
				
			$('.datepicker').each(function(){
				$(this).datepicker();
			});	
				
			$('.cls_date').mask('0000-00-00');
			$('.cs_int').mask('##');
		
			
			$("#btnRegresar").click(function( event ) {
				event.preventDefault();
				location.reload();
				
			});
			
			$("#btnNextPy").click(function( event ) {
				event.preventDefault();
				
				var form = $('#frmMT');
				var request = $.ajax({
					type:	'POST',
					url:	'/payment/mult_save',
					data:	form.serialize(),
					success:function(data){
						console.log(data);
						if(data.success==1){
							$('#seccion_detalle_planes').load(data.url_redirect);	
							return;
						}
						else if(data.success==2){
									$('#alert_error').empty();
									var error_str = '';
									$.each( data.errors, function(i, obj) {
										
										error_str += obj + '<br>';
									});
									
									alertPPYA(error_str);
									/*alertPPYA(data.message);*/
									console.log(data);
									return;
						}						
						else{
							alert('Hubo un problema al tratar de guardar la informacion');
							console.log(data);
							return;
						}
					}
				});
				request.fail(function( jqXHR, textStatus ) {
					alert( "Hubo un problema: " + textStatus );
					console.log(jqXHR);
				});				
				
				
			});
		
	
	});
		
</script>