<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\GenFunction;
use App\webProduct;
use App\WebRanges;
use App\WebPerson;
use App\WebSale;
use App\WebSalesDependent;
use App\GenFunctionP2P;
use App\BtnP2pParam;
use App\BtnP2pTran;
use App\SalContract;
use App\SalSale;
use App\SalMethodPay;
use App\SalMethodpaySubscription;
use App\CliClient;
use App\SalSxk;
use Auth;
use Mail;



class P2PController extends Controller{
    //
	/**
	* Display a listing of the resource.
	*
	* @return \Illuminate\Http\Response
	*/
	public function index(){

	} // Fin index

	/**
	* Store a newly created resource in storage.
	*
	* @param  \Illuminate\Http\Request  $request
	* @return \Illuminate\Http\Response
	*/
	public function store(Request $request){
		
	} // Fin store

	/**
	* Display the specified resource.
	*
	* @param  int  $id
	* @return \Illuminate\Http\Response
	*/
	public function show($id){

	} // Fin show
	
	public function notifyP2P(Request $request){
		$data = json_decode($request->getContent(), true);
		$msg  = $data['status']['status'];
		$msg .= $data['status']['message'];
		$msg .= $data['status']['reason'];
		$msg .= $data['status']['date'];
		$msg .= $data['requestId'];
		$msg .= $data['reference'];
		$msg .= $data['signature'];
		$requestId = $request->session()->get('requestId');
		if($data['requestId']){
			$fecha = GenFunction::getDateDB();
			Storage::prepend('log_notifyP2P.log',"****************");
			Storage::prepend('log_notifyP2P.log',"NOTIFY=".$data['status']['status']."::".$fecha);              
			Storage::prepend('log_notifyP2P.log',$data['requestId']);  
			Storage::prepend('log_notifyP2P.log',$msg); 
			Storage::prepend('log_notifyP2P.log',"****************");
			$transaction =  BtnP2pTran::where('bpt_requestId',$url_process['requestId'])->first();
			if(!empty($transaction)){
				$transaction->bpt_status  = $data['status']['status'];
				$transaction->bpt_session = $data['requestId'];
				$transaction->save();
			}
		} else {
			$fecha = GenFunction::getDateDB();
			Storage::prepend('log_notifyP2P.log',"****************");
			Storage::prepend('log_notifyP2P.log',"NOTIFY=ERROR::".$fecha);              
			Storage::prepend('log_notifyP2P.log', $requestId);   
			Storage::prepend('log_notifyP2P.log',"****************");
		}
	} // Fin notifyP2P

	/**
	* Define the application's command schedule.
	*
	* @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	* @return void
	*/
    public function sondaP2P(){
		$arrTransPending = BtnP2pTran::where('bpt_status', 'PENDING')->get();
		
		// Credenciales
		$var_endpoint_pruebas = BtnP2pParam::find(1);
		$endPoint =	$var_endpoint_pruebas->p2p_value;
		
		foreach($arrTransPending as $transPending){
			
			$requestId= $transPending->bpt_requestId;

			//SOLICITAMOS LA CREACION DE SESSION PARA EL PAGO PLACETOPAY
			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => $endPoint.'api/session/'.$requestId,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_HTTPHEADER => array(
					"Cache-Control: no-cache",
					"Content-Type:  application/json",
					"Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
				),
			));

			$response = curl_exec($curl);
			$err      = curl_error($curl);
			curl_close($curl);

			if ($err) {
				$fecha = GenFunction::getDateDB();
				Storage::prepend('log_sondaP2P.log',"****************");
				Storage::prepend('log_sondaP2P.log',"SONDA=ERROR::".$fecha);              
				Storage::prepend('log_sondaP2P.log', $err);   
				Storage::prepend('log_sondaP2P.log',"****************");
			} else {
				$resArray = json_decode($response, true);
				
				$fecha = GenFunction::getDateDB();
				Storage::prepend('log_sondaP2P.log',"****************");
				Storage::prepend('log_sondaP2P.log',"SONDA: ".$resArray['status']['status']."::".$fecha);              
				Storage::prepend('log_sondaP2P.log',$resArray['requestId']);  
				Storage::prepend('log_sondaP2P.log',"La petición ha sido aprobada exitosamente"); 
				Storage::prepend('log_sondaP2P.log',"****************");
				if ($resArray['status']['status'] == 'APPROVED' ) {
					$transApproved = BtnP2pTran::find($transPending->bpt_id);
					$transApproved->bpt_status  = $resArray['status']['status'];
					$transApproved->bpt_message = "La petición ha sido aprobada exitosamente";
					$transApproved->save();
				}
			} // Fin err
		} // Fin foreach
    } // Fin sondaP2P

   

}
