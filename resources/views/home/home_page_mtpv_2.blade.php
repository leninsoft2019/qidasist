<!DOCTYPE html>
<html lang="es" id="html_tg">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="generator" content="wordpress">
	<link rel="pingback" href="https://mutualidad.mippya.com/xmlrpc.php" />
	<link rel="shortcut icon" href="https://mutualidad.mippya.com/wp-content/uploads/2019/05/favicon-1.png" />
	<title>IDASIST</title>
	<link rel='dns-prefetch' href='//fonts.googleapis.com' />
	<link rel='dns-prefetch' href='//s.w.org' />
	<link rel="alternate" type="application/rss+xml" title="Memorial &raquo; Feed" href="https://memorial.ec/feed/" />
	<link rel="alternate" type="application/rss+xml" title="Memorial &raquo; RSS de los comentarios" href="https://memorial.ec/comments/feed/" />
	<script type="text/javascript">
			    var current_effect = 'bounce'; //
		window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/mutualidad.mippya.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.2"}};
		!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
	</script>
	<style type="text/css">
		img.wp-smiley,
		img.emoji {
			display: inline !important;
			border: none !important;
			box-shadow: none !important;
			height: 1em !important;
			width: 1em !important;
			margin: 0 .07em !important;
			vertical-align: -0.1em !important;
			background: none !important;
			padding: 0 !important;
		}

		.contenedor_mt{
			padding:30px;
			width:80%;
			margin:auto; 
			/* background-color:#F3EADD;*/
		}


		@media (min-width: 768px) and (max-width: 979px) {
			.contenedor_mt {
				width:100%;
				padding:5%;
			}

			#frmMT{
				width:100%;
				margin:auto;
			}

			h1{
				text-indent:-9999px;
				font-size:0px;
				position:absolute;
			}
		}


		@media (max-width: 767px) {

			.text-xxl {
				font-size: 50px !important;
				line-height: 60px !important;
			}

			h1{
				text-indent:-9999px;
				font-size:0px;
				position:absolute;
			}

			h2, .text-l {
				font-size: 30px !important;
				line-height: 36px !important;
			}

			h3 {
				font-size: 18px !important;
				line-height: 28px !important;
			}

			.col{
				display:table-row;
			}

		}
	</style>
<script src="https://secure.placetopay.ec/redirection/lightbox.min.js"></script>
<script type='text/javascript' src="{{asset('js/jquery-3.3.1.js')}}"></script>
<script type='text/javascript' src="{{asset('js/jquery-ui.js')}}"></script>
<script type='text/javascript' src="{{asset('js/jquery.validate.js')}}"></script>

<script src="{{asset('js/jquery.mask.js')}}"></script>
<script src="{{asset('js/waitMe.js')}}"></script>

<script type='text/javascript' src="{{asset('js/sales.js')}}"></script>
<script type='text/javascript' src="{{asset('js/js_land_page.js')}}"></script>

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
 
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>



	<!-- Leninsoft, 2019-09-03: Botones para el modulo de Cientes -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>

    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
    <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" />
    <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap4.min.css" />

<!-- css -->


<link rel="stylesheet" href="{{asset('css/waitMe.css')}}">

	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>-->
	<link rel='stylesheet' id='wp-block-library-css'  href='https://mutualidad.mippya.com/wp-includes/css/dist/block-library/style.min.css?ver=5.2.2' type='text/css' media='all' />
	<link rel='stylesheet' id='hc-bootstrap-css'  href='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/scripts/bootstrap/css/bootstrap.css?ver=1.0' type='text/css' media='all' />
	<!--<link rel='stylesheet' id='hc-style-css'  href='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/style.css?ver=1.0' type='text/css' media='all' />-->
	<link rel='stylesheet' id='hc-animations-css'  href='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/css/animations.css?ver=1.0' type='text/css' media='all' />
	<!--
	<link rel='stylesheet' id='hc-css/content-box.css-css'  href='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/css/content-box.css?ver=1.0' type='text/css' media='all' />
	-->
	<link rel='stylesheet' id='hc-css/content-box.css-css'  href='/css/content-box.css?ver=1.0' type='text/css' media='all' />
	<link rel='stylesheet' id='hc-css/components.css-css'  href='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/css/components.css?ver=1.0' type='text/css' media='all' />

	<!--<link rel='stylesheet' id='hc_css_skin-css'  href='https://mutualidad.mippya.com/wp-content/themes/landkit/skin.css?ver=1.0' type='text/css' media='all' />-->

	<link rel='stylesheet' id='hc_css_skin-css'  href='/css/skin.css?ver=1.0' type='text/css' media='all' />
	<link rel='stylesheet' id='hc_css_skin-css'  href='/css/style.css?ver=1.0' type='text/css' media='all' />


	<style id='hc_css_skin-inline-css' type='text/css'>
		.album-box .caption, header .btn-default, .advs-box-side-img hr, .accordion-list .list-group-item:before,.woocommerce .product span.onsale, .white.btn, .circle-button, .btn.circle-button, .header-bootstrap, .popup-banner .panel-body, .pagination > li > a, .pagination > li > span, .pagination > .disabled > a, .pagination > .disabled > a:focus, .header-title hr, .nav.inner.ms-rounded > li.active > a, .btn-default, .panel-default > .panel-heading, .panel-default .panel-footer, .advs-box-top-icon .icon, .advs-box-top-icon-img .icon, i.circle, .advs-box-multiple div.circle, .advs-box-side-img hr, .call-action-box, .title-base hr, .white .title-base hr, .header-video.white .title-base hr, .header-slider.white .title-base hr, .header-animation.white .title-base hr, .header-title.white .title-base hr, .nav.inner.ms-mini, .bg-color, .title-base .scroll-top, .btn, .title-modern .scroll-top, i.square, .header-base, .popup-banner.full-width-top, .popup-banner.full-width-bottom, .progress-bar, .tagbox span, .btn-border, #mc-embedded-subscribe, .widget #searchsubmit, .adv-img-button-content .caption:hover, input[type="submit"], .woocommerce .price_slider_wrapper .ui-slider .ui-slider-handle, .woocommerce .price_slider_wrapper .ui-slider .ui-slider-range, .cart-buttons .cart-view {
			   /* background-color: rgb(149, 106, 45);*/
			   background-color: rgb(227, 192, 66);
				
			color:black;
			}

			.call-action-box, .social-line .social-group i.circle, .bg-color,.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button {
				background-color: rgb(149, 106, 45) !important;
			}

			header .btn-default:hover, .btn-primary:active:hover, .btn-primary.active:hover, .open > .dropdown-toggle.btn-primary:hover, .btn-primary:active:focus, .btn-primary.active:focus, .open > .dropdown-toggle.btn-primary:focus, .btn-primary:active.focus, .btn-primary.active.focus, .open > .dropdown-toggle.btn-primary.focus, .btn-primary:focus, .btn-primary.focus, .btn-primary.btn-default:active, .btn-default.active, .open > .dropdown-toggle.btn-default, .pagination > li > a:focus, .circle-button:hover, .btn.circle-button:hover, .pagination > .active > a:hover, .pagination > .disabled > a:hover, .pagination > li > a:hover, .btn-default:hover, .minisocial-group.circle i:hover, .pagination > .active > a, .btn-border:hover,  .bg-transparent .menu-cta.btn-border:hover {
				background-color: rgb(72, 12, 103);
				border-color: rgb(72, 12, 103);
			}

			#mc-embedded-subscribe:hover, .widget #searchsubmit:hover, input[type="submit"]:hover, .btn:hover {
				background-color: rgb(72, 12, 103);
			}

			::selection {
			background: #956A2D;
			}

			::-moz-selection {
			background: #956A2D;
			}

			.btn-primary:focus, .btn-primary.focus, .slimScrollBar, .white .btn:hover, .white.circle-button:hover,.woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover, .btn-border:hover, .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, .bg-transparent .menu-cta.circle-button:hover, .bg-transparent .menu-cta.btn-default:hover, .bg-transparent .menu-cta.btn-border:hover {
				background-color: rgb(72, 12, 103) !important;
			}

			i.icon, .fullpage-menu .active i, .datepicker-panel > ul > li.picked, .datepicker-panel > ul > li.picked:hover, footer h4, .quote-author, .box-menu-inner .icon-box i,
			.caption-bottom p, div.adv-img .caption-bottom p, .mi-menu li .fa, .fullpage-arrow.arrow-circle .arrow i, .text-color,
			.accordion-list .list-group-item > a i, .mega-menu .fa-ul .fa-li, .adv-circle.adv-circle-center i, .mi-menu a > .fa,
			li.panel-item .fa-li, header .social-group a i, .icon-menu .navbar-collapse ul.nav i, .side-menu i, .side-menu i, .side-menu ul a i, .bs-menu li:hover > a, .bs-menu li.active > a, .img-box.adv-circle i, .advs-box-side .icon, .advs-box-side-icon i, .tag-row i, .tag-row a i.circle, .social-group i.circle, .social-button i.circle,
			.niche-box-testimonails h5, .title-icon i, .fullpage-menu.white li.active a i, ul.list-texts li b, .timeline > li > .timeline-label h4,
			.footer-center .footer-title, .accordion-list .list-group-item > a.active, header .social-group a i.fa, a, .btn-border, .btn-border.btn i, .nav-pills > li > a:hover, .nav.inner.ms-rounded li a, i, .menu-cta.btn-border {
				color: #956A2D;
			}

			a:hover, .anima-button.btn-text:hover i, .btn-text:hover, footer a:hover {
				color: rgb(72, 12, 103);
			}

			.footer-minimal .footer-title, .advs-box-top-icon.boxed .circle-button,.woocommerce div.product p.price, .woocommerce div.product span.price, .text-color, .boxed .circle-button:hover i, .boxed .btn:hover i, .woocommerce .star-rating, .woocommerce .price_slider_wrapper .price_slider_amount .button, .woocommerce ul.products li.product .price {
				color: rgb(149, 106, 45) !important;
			}

			.half-side.left, .mi-menu .side-menu li.active, .tab-box.right .nav-tabs > li.active > a, .bs-menu li:hover > a, .bs-menu li.active > a, .side-menu.ms-minimal li:hover > a, .side-menu.ms-minimal li:hover > span, .adv-circle.adv-circle-center-2 .caption p, .side-menu.ms-simple li.active {
				border-right-color: rgb(149, 106, 45) !important;
			}

			.half-side.right, .tab-box.left .nav-tabs > li.active > a, .tab-box.left .nav-tabs > li.active > a:hover, .tab-box.left .nav-tabs > li.active > a:focus, .bs-menu.menu-left li:hover > a, .bs-menu.menu-left li.active > a, .bs-callout {
				border-left-color: rgb(149, 106, 45) !important;
			}

			.datepicker-top-left, .datepicker-top-right, .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus, .mi-menu .sidebar-nav {
				border-top-color: rgb(149, 106, 45) !important;
			}

			.tab-box.inverse .nav-tabs li.active a, .datepicker-top-left:before, .datepicker-top-right:before, .nav.ms-minimal > li.active > a, .nav.ms-minimal li a:hover, .popover.bottom > .arrow:after, .title-modern h1, .title-modern h2, .title-modern h3, .nav.ms-minimal > li:hover > a, .nav.ms-minimal > li.active > a, .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
				border-bottom-color: rgb(149, 106, 45) !important;
			}

			.twoside-open hr, header .btn-default, .social-line .social-group i.circle, hr.e, div.call-action-box, .white.btn, .circle-button, .btn.circle-button, .pagination > .active > a:hover, .pagination > li.disabled > a:hover, .pagination > li > a:hover, .pagination > li > a:focus, .pagination > li > a, .pagination > li > span, .pagination > li.disabled > a, .nav.inner.ms-rounded li a, .btn-default, .btn, .bs-panel, .tag-row i.circle, .niche-box-team .content-box hr, .call-action-box, .pagination > .active > a, .accordion-list .list-group-item > a.active, .social-group i.circle, .social-button i.circle, .accordion-list .list-group-item > a.active:hover {
				border-color: rgb(149, 106, 45);
			}
			.list-texts-justified li b, .list-texts-justified li span {
				background:  !important;
			}

			.gradient-bg, .gradient-text {
				background-image: linear-gradient( 135deg, rgb(149, 106, 45) 0%, rgb(72, 12, 103) 100%);
			}
			@keyframes borderPulse {
				0% {
					box-shadow: inset 0px 0px 0px 5px [MAIN-COLOR-2],.4), 0px 0px 0px 0px [MAIN-COLOR-2],1);
				}
				100% {
					box-shadow: inset 0px 0px 0px 3px [MAIN-COLOR-2],.2), 0px 0px 0px 15px [MAIN-COLOR-2],0);
				}
		}

			
		.navbar-brand img,header .brand img { margin-top: 10px; }
		.navbar.navbar-main{
		  background-color:#2c0373;
		}

		#table-plans-home{
		 width:100%;
		}

		#table-plans-home tbody tr:nth-child(1) td{
			   border-top-width: 0px;
		}

		#table-plans-home tbody tr:nth-child(3) td{
			   border-bottom-width: 0px;
		}

		#table-plans-home tbody tr td:nth-child(1){
			   border-left-width: 0px;
		}

		#table-plans-home tbody tr td:nth-child(2){
			   border-right-width: 0px;
		}

		#table-plans-home tbody tr td:nth-child(3){
			   border-left-width: 0px;
			   border-right-width: 0px;
		}

		#table-plans-home tbody tr td{
			   border-color:#5D5D5D;
		}

		#table-plans-home tbody tr td:nth-child(2){
				   font-weight: 500;
		}

		table#table-plans-price-home{
		  width:100%;
		}

		table#table-plans-price-home thead tr th{
		  border-top-width:0px;
		}

		table#table-plans-price-home thead tr th:nth-child(1){
		  border-left-width:0px;
		  border-right-width:0px;
		}
		table#table-plans-price-home thead tr th:nth-child(2){
		  border-left-width:0px;
		}

		table#table-plans-price-home thead tr th:nth-child(4){
		  border-right-width:0px;
		}

		table#table-plans-price-home thead tr:nth-child(1){
		  border-bottom-width:0px;
		  line-height: 0px;
		}

		table#table-plans-price-home thead tr:nth-child(1) th{
		  border-bottom-width:0px;
		}

		table#table-plans-price-home thead tr:nth-child(1) th{
		  heigth:10px;
		}

		table#table-plans-price-home thead tr:nth-child(2){
		  height: 10px;
		  border-top-width:0px;
		  border-bottom-width:0px;
		}

		table#table-plans-price-home thead tr:nth-child(1) th:nth-child(2){
		  border-right-width: 2px;
		 border-right-color:#C58D3C;
		}

		table#table-plans-price-home thead tr:nth-child(1) th:nth-child(3){
		  border-right-width: 2px;
		 border-right-color:#C58D3C;
		}

		table#table-plans-price-home tbody tr:nth-child(1) td{
		  border-top-width:0px;
		}

		table#table-plans-price-home tbody tr td{
		  border-left-width:0px;
		  border-right-width:0px;
		}

		table#table-plans-price-home tbody tr:nth-child(3) td{
		  border-bottom-width:0px;
		}

		#boton_ver_planes a{
		   color: #480C67 !important;
		}

		#boton_ver_planes a:hover{
		   color: #FFFFFF !important;
		   background-color:#480C67 !important;
		}

		.center_icon .icon-box-cell{
		  display:table;
		  margin:auto;
		}

		@media screen and (min-width: 10px) and (max-width:910px) {
		  #seccion_detalle_planes_mutual{
				height:770px !important;
		  }
		  #seccion_beneficiarios_mutualidad{
			   height:770px !important;
		   }
		}

		@media screen and (min-width: 10px) and (max-width:770px) {
		  #seccion_detalle_planes_mutual .col-md-12.hc_image_cnt img{
				margin:auto;
				display:table;
		  }
		}


		.circle-button {
			border-radius: 5px;
		}
		input{
			height:30px;
			padding:0px;
		}


		.form-control {
			display: block;
			width: 100%;
			padding: .375rem .75rem;
			font-size: 1rem;
			line-height: 0.5;
			color: #495057;
			background-color: #fff;
			background-clip: padding-box;
			border: 1px solid #ced4da;
			border-radius: .25rem;
			transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
		}

		.cs_wht{
			color:white;
			font-weight:bold;
			font-size:2em;
		}

		#header_hmpt{
			background-color:#2c0373;
			background-image: linear-gradient(to right, #7b3b96, #2c0373); 
		}
		table{
			color:black;
		}
	</style>
	<link rel='stylesheet' id='hc_css_custom-css'  href='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/custom/custom.css?ver=1.0' type='text/css' media='all' />
	<link rel='stylesheet' id='google-font-css'  href='https://fonts.googleapis.com/css?family=Rubik%3A300%2C300i%2C400%2C400i%2C500%2C500i%2C700%2C700i%2C900%2C900i&#038;ver=1.0' type='text/css' media='all' />
	<!--<script type='text/javascript' src='https://mutualidad.mippya.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>
	<script type='text/javascript' src='https://mutualidad.mippya.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>-->
	<link rel='https://api.w.org/' href='https://mutualidad.mippya.com/wp-json/' />
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://mutualidad.mippya.com/xmlrpc.php?rsd" />
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://mutualidad.mippya.com/wp-includes/wlwmanifest.xml" /> 

	<link rel="alternate" type="application/json+oembed" href="https://mutualidad.mippya.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fmutualidad.mippya.com%2F" />
	<link rel="alternate" type="text/xml+oembed" href="https://mutualidad.mippya.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fmutualidad.mippya.com%2F&#038;format=xml" />
	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
</head>
<body class="home page-template-default page page-id-273" data-spy="scroll" data-target="#hc-inner-menu" data-offset="200" >
	<div id="preloader"></div>
	<div class="">
	<style scoped>
		.navbar-default .navbar-nav > li > a {
			color: white;
			font-size: 12px;
			font-weight: 500;
			letter-spacing: 0.6px;
			text-transform: uppercase;
			
		}
		.navbar.navbar-main {
			/*background-color: #381E3D;*/
			 background-color: rgba(56, 30, 61,0.9);
			
		}

		.navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover {
			color: white;
			background-color: #555555;
		}

		#header_hmpt{	
			 background-image: url("/img/LANDING-PAGEHOME1.jpg"), linear-gradient(#eb01a5, #d13531);
		}

		#seccion_detalles_servicio_2{
			/* background-image:linear-gradient(to bottom, rgba(245, 246, 252, 0.52), rgba(117, 19, 93, 0.73)),url("/img/LANDING-PAGEHOME1.jpg");*/
			 /*background-image:linear-gradient(180deg, rgba(255,255,255,0) 0, #381E3D 100%),url("/img/LANDING-PAGEHOME1.jpg");*/
			/* background-image:linear-gradient(180deg, #381E3D 0, #381E3D 100%),url("/img/LANDING-PAGEHOME1.jpg");*/
			 background-image: linear-gradient(180deg, rgba(56, 30, 61,0.8) 0, #381E3D 100%),url("/img/mountain.jpg");
			 width: 100%;
			 /*height: 400px;*/
			 background-size: cover;
			 color:white;
		}

		.cs_info{
			width:80%;
			margin:auto;
			margin-top:50px;
		}

		.cs_cream_text{
			font-family: Helvetica, Arial, sans-serif;
			color:#E2BF9D;
			vertical-align:middle;
			height:100%;
			top:20px;
			margin-top:20px;
			font-size:2em;
			padding:20px;
		}

		.cs_white_info_text{
			color:white;
			font-size:3em;
			padding:20px;
			
		}
		.col_2{
			display:table-cell;
			vertical-align:top;
			margin-left:10px;
			font-size:1.5em;
			padding:10px;
		}


		#seccion_detalles_servicio_3{
		   background:white;	
		   z-index:2001;
		}

		.cs_info2{
			border-radius:5px;
			
			border-color:red;
			background-color:#2c0373;
			position:relative;
			top:-20px;
			width:80%;
			margin:auto;
			z-index:2010;
			padding:20px;
			margin-bottom:0px;
		}


		.cs_info3{
			border-radius:5px;
			
			border-color:red;
			background-color:#2c0373;
			position:relative;
			bottom:0px;
			width:80%;
			margin:auto;
			z-index:2010;
			padding:20px;
			margin-bottom:0px;
		}

		.cs_input_home{
			width:30%;
			padding:0px;
			height:40px;
			border-radius:5px;
		}

		#frmCotizaFormHome{
			width:100%;
			padding:10px;
			display:flex;
		}

		.cs_text_big_bl{
			font-size:3em;
			color:#2c0373;
			font-weight:bold;
		}

		.cs_text_big_nl{
			font-size:3em;
			color:#2c0373;
			font-weight: lighter
			
		}

		.pricing-table{
			background:#2c0373;
		}
		.pricing-table .pricing-price {
			background:#2c0373;
			background-color:#2c0373;
			color:white;
		}

		.list-group{
			background-color:#2c0373;
		}

		#section_cYPdY2{
			background-color:rgba(56, 30, 61,0.9);
			background:rgba(56, 30, 61,0.9);
			color:white;
		}

		#2RazN{
			color:#2c0373;
		}
		ul{
			color:#2c0373;
		}


		#section_cYPdY33{

			 background-image: linear-gradient(180deg, rgba(56, 30, 61,0.8) 0, #381E3D 100%),url("/img/mountain.jpg");
			 width: 100%;
			
			 background-size: cover;
			 color:white;
		}
		
		.cs_ul_footer{
			color:white;
		}
		
			.ico_phone_white:before {
		  content: "";
		  display: inline-block;
		  vertical-align: middle;
		  width: 1.7em;
		  height: 1.7em;
		  background: url(/img/ico_phone_white.svg);
		  background-size: cover;
		}
		
		.cs_contac_wh{
			color:white;
		}
		
		
		.cs_contact_or{
			color:#DEB58C;
			float:right;
		}
	</style>

	<header class="scroll-change fixed-top "  id="header_hmpt">
		<div class="navbar navbar-default navbar-fixed-top " role="navigation">
			<div class="navbar navbar-main ">
				<div class="container">
					<div class="navbar-header">
						<a class='navbar-brand' href='/' style="display:flex;">
								<img  id="img_logo" src="/img/Prever_logo.png" style="width:200px; height:80px;display:block;" alt="PREVER" />
						</a> 
					
						<button type="button" class="navbar-toggle">
							<i class="scn-menu"></i>
						</button>
						<ul id="hc-inner-menu" class="nav navbar-nav  one-page-menu" style="">
							<li class=" " >
				
							</li>
						</ul>
					</div>
					<div class="collapse navbar-collapse">
						<div class="nav navbar-nav navbar-right">
							<ul id="hc-inner-menu" class="nav navbar-nav  one-page-menu" style="">
								<li class=" " >
									<a href="#nosotros">{{$objlabels->wlb_home_us}}</a>
								</li>
								<li class=" " >
									<a href="/client/home/">{{$objlabels->wlb_home_client}}</a></li>
								<li class=" " >
									<a href="#contacto">{{$objlabels->wlb_home_contact}}</a>
								</li>		
								<li class=" " >
									<a href="/login">{{$objlabels->wlb_home_login}}</a>
								</li>	
  								                           
								
							</ul>                                                                        
							<div class="btn-group navbar-social">
								<div class="btn-group social-group">
									<a target='_blank' rel='nofollow' href='https:&#x2F;&#x2F;www.facebook.com&#x2F;memorialint&#x2F;'>
										<i class='ico_phone_white'></i>
										<div style="display:table;">
										  <div style="display:table-row;">
											<div style="display:table-cell;">
												<span class="cs_contac_wh">N&uacute;mero de Atenci&oacute;n 24/7:</span>
											</div>
										   </div>
										  <div style="display:table-row;">
											<div style="display:table-cell;">
												<span class="cs_contact_or">1800-PREVER</span>
											</div>
										   </div>	
											<div style="display:table-row;">
											<div style="display:table-cell;">
												<span class="cs_contact_or">(773837)</span>
											</div>
										   </div>										   
										   
                                        </div>										   
									</a>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<h1>{{$objlabels->wlb_home_title}}</h1>
	</header>

	<div id="main_container">
		<!-- Page 1: Product -->
		<div id="seccion_detalle_planes"  class="section-item section-bg-image parallax-window"
		data-anima="fade-in"  data-timeline="asc"  data-bleed="0" data-parallax="scroll" 
		data-natural-height="805" data-natural-width="1920" data-position="top" 
		data-image-src="/img/LANDING-PAGEHOME1.jpg" style="min-height:450px;">
			@yield('content')
		</div>
		
		<!-- Page 2: Detail -->
		<div id="seccion_detalles_servicio_1" class="section-item section-empty  no-paddings-y"  >
			<div id="cont_ini_home" class="content container " style="">
				<div class="row cs_info">
					<center>
						<img src="/img/home_prie.png"><br>
						<span class="cs_text_big_bl">{{$objlabels->wlb_home_titD}}<br></span>
						<span class="cs_text_big_nl">{{$objlabels->wlb_home_titF}}</span>
					</center>
				</div>		

				<div class="row cs_info">
					<div class="col_2">
					  <ul>
						<li>{{$objlabels->wlb_home_col1_li1}}</li>
						<li>{{$objlabels->wlb_home_col1_li2}}</li>
						<li>{{$objlabels->wlb_home_col1_li3}}</li>
						<li>{{$objlabels->wlb_home_col1_li4}}</li>
						<li>{{$objlabels->wlb_home_col1_li5}}</li>
						<li>{{$objlabels->wlb_home_col1_li6}}</li>			  
					  </ul>
					</div>
					
					<div class="col_2"></div>			
				</div>
				<br /><br />
				<div id="MSXJt" class="hc_column_cnt col-md-8  col-center" style="" >
					<div class="row">
						<div class="col-md-12 hc_title_tag_cnt">
							<h2 id="2RazN" class=' text-center' style=''>{{$objlabels->wlb_home_titLn1}} <br /> {{$objlabels->wlb_home_titLn2}}</h2>
						</div>
					</div>
				</div>
				<br /><br />
				<div class="cs_info2">
					<div>
					<form id='frmCotizaFormHome'>
						<input type="text" placeholder="Nombre" class="form-control cs_input_home"  />&nbsp;&nbsp;
						<input type="text" placeholder="Telefono" class="form-control cs_input_home" />&nbsp;&nbsp;
						<input type="text" placeholder="Email" class="form-control cs_input_home" />&nbsp;&nbsp;
						<a id="btnCotizarHome" href="#" class="btn circle-button">{{$objlabels->wlb_home_btnQ}}</a>				
					</form>
					</div>
				</div>
				
				<div id="cont_ini_home" class="content container " style="">
					<div class="row cs_info">
						<div class="col_2">
							<ul>
								<li>Somos una empresa ecuatoriana con 20 años de experiencia en el mercado</li>
								<li>Somos la corporación más grande de asistencia funeraria internacional</li>
								<li>Contamos con oficinas en 19 países</li>
								<li>Nuestra misión es apoyar a la familia ante la pérdida de un ser querido ofreciendo servicios profesionales</li>
								<!--<li>{{$objlabels->wlb_home_col1_li5}}</li>-->
								<!--<li>{{$objlabels->wlb_home_col1_li6}}</li>-->		  
							</ul>
						</div>
					</div>
				</div>

			</div>
		</div>
		
		<!-- Page 3: Google Maps -->
		<!--<div id="seccion_detalles_servicio_3" class="section-item section-empty  no-paddings-y"  >
			<div id="cont_ini_home" class="content container " style="width:100%;">
				<div class="mapouter" style="width:100%;">
					<div class="gmap_canvas">
					<iframe width="100%" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=university%20of%20san%20francisco&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
					Google Maps Generator by 
					<a href="https://www.embedgooglemap.net">embedgooglemap.net</a>
					</div>
					<style>.mapouter{position:relative;text-align:right;height:500px;width:100%;}.gmap_canvas {overflow:hidden;background:none!important;height:500px;width:100%;}</style></div>
						
					
			</div>
		</div>-->

	</div>

	<footer class="default-wp-footer">
		<div class="content">
			<div class="container cont_footer">
   	

				<div class="col_2">
				  <ul class="cs_ul_footer">
					<li>
						<a href="/freq_quest" style="color:white;">Preguntas Frecuentes</a>
					</li>	  
				  </ul>
				</div>								
			
				<div class="col_2">
				  <ul class="cs_ul_footer">
					<li>QUITO (02) 244 7431</li>
					<li>(02) 225 3890</li>			  
				  </ul>
				</div>
				
				<div class="col_2">
				  <ul class="cs_ul_footer">
					<li>QUITO (02) 261 4497</li>
					<li>(02) 266 1737</li>			  
				  </ul>
				</div>

				<div class="col_2">
				  <ul class="cs_ul_footer">
					<li>GUAYAQUIL (04) 263 1452</li>			  
				  </ul>
				</div>
				
				<div class="col_2">
				  <ul class="cs_ul_footer">
					<li>CUENCA (07) 282 4742</li>
					<li>(07) 283 6658</li>					
				  </ul>
				</div>
				
				<div class="col_2">
				  <ul class="cs_ul_footer">
					<li>MANTA (05) 262 3502</li>
					<li>(07) 262 8865</li>					
				  </ul>
				</div>
				
				<div class="col_2">
				  <ul class="cs_ul_footer">
					<li>1-800 PREVER</li>
					<li>(773837)</li>					
				  </ul>
				</div>
				
			</div>
			<div class="container">
				<div style = "text-align: center;">Copyright Memorial Ecuador. All Rights Reserved. Powered by <a href = "https://memorialtechnologies.com" target = "_blank" style = "color:blue;">MTC</a></div>
			</div>
		</div>
	</footer>   
<?
} else { 
?>
	<!-- WhatsHelp.io widget -->
	<script type="text/javascript">
		(function () {
			var options = {
				call: "+5931800773837", // Call phone number
				call_to_action: "LLamanos", // Call to action
				position: "right", // Position may be 'right' or 'left'
			};
			var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
			var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
			s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
			var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
		})();
		
		function cargarMenu(l_url){			
			$("#seccion_detalle_planes").load(l_url);
		}
	</script>
	<!-- /WhatsHelp.io widget --><script type="text/javascript" id="sns_scripts">jQuery(function() {
		jQuery("#hc-inner-menu li").on("click",function(){
			if(jQuery("button.navbar-toggle:visible").length > 0){
				jQuery("button.navbar-toggle:visible").click();
			}
		});
	});
	</script>
	<script type='text/javascript' src='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/scripts/script.js?ver=1.0'></script>
	<script type='text/javascript'>
	 var ajax_url = 'https://mutualidad.mippya.com/wp-admin/admin-ajax.php';
	</script>
	<script type='text/javascript' src='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/scripts/bootstrap/js/bootstrap.min.js?ver=1.0'></script>
	<script type='text/javascript' src='https://mutualidad.mippya.com/wp-includes/js/imagesloaded.min.js?ver=3.2.0'></script>
	<script type='text/javascript' src='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/custom/custom.js?ver=1.0'></script>
	<script type='text/javascript' src='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/scripts/parallax.min.js?ver=1.0'></script>
	<script type='text/javascript' src='https://mutualidad.mippya.com/wp-includes/js/wp-embed.min.js?ver=5.2.2'></script>

</body>
</html>

	@include('home.modalProductDetail');