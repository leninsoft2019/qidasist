<?php
	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use App\Http\Requests;
	use App\Http\Controllers\Controller;
	use App\UserMtc;
	use App\GenLabel;
	use App\GenModule;
    use App\GenModuleProfile;
    use App\GenProfile;
	use App\GenFunction;
	use Illuminate\Support\Facades\DB;
	use Illuminate\Support\Facades\Hash;
	use App\GenProfileAlianza;
	use Illuminate\Support\Facades\Auth;
	use App\GenLanguage;

	class LoginController extends Controller{

		public function getLabelsLogin($lang_id){
			$form_login['login_title'] =  GenLabel::where('label_code', '=','login_title')->where('lang_code', '=',$lang_id)->first();   
			$form_login['login_language_label'] =  GenLabel::where('label_code', '=','login_language_label')->where('lang_code', '=',$lang_id)->first();
			$form_login['login_form_title'] =  GenLabel::where('label_code', '=','login_form_title')->where('lang_code', '=',$lang_id)->first();
			$form_login['login_user_label'] =  GenLabel::where('label_code', '=','login_user_label')->where('lang_code', '=',$lang_id)->first();
			$form_login['login_password_label'] =  GenLabel::where('label_code', '=','login_password_label')->where('lang_code', '=',$lang_id)->first();
			$form_login['login_button_label'] =  GenLabel::where('label_code', '=','login_button_label')->where('lang_code', '=',$lang_id)->first();
			return $form_login;
		} // Fin getLabelsLogin

		public function defaultLangLogin(Request $request){
			
			if(Auth::check()){
				
			   $this->generateSessions($request);
			   $objLabels =   $request->session()->get('objLabelsAp');
               $modules   =  $request->session()->get('modules');		
               $module_child = $request->session()->get('module_child');	
			   \Artisan::call('route:clear');
			   return view('admin.dashboard')->with('modules',$modules)->with('module_child',$module_child)->with('objLabels',$objLabels);
			}
			$lang_id=2;

			if(!$request->session()->has('obj_lang')){
				$obj_lang = DB::SELECT("select * from gen_lang where lang_state = 1");
				$request->session()->put('obj_lang',$obj_lang);
			}else{
				$obj_lang = $request->session()->get('obj_lang');
			}

			foreach ($obj_lang as $lang) {
				if($lang->lang_default==1){
					$lang_id=$lang->lang_code;
				}
			}

			if($request->session()->has('lang_id')){
				$lang_id=$request->session()->get('lang_id');
			}

			$request->session()->put('lang_id',$lang_id);
			
			$objLangIso =DB::SELECT("select * from gen_lang where lang_state = 1 and lang_code = ".$lang_id);
			$request->session()->put('lang_iso2',$objLangIso[0]->lang_iso2);			

			if($request->session()->has('arr_countries')){
			   $request->session()->forget('arr_countries');
			}

			$arr_countries = DB::SELECT("select * from gen_location_country where glc_status = 1");
			$request->session()->put('arr_countries',$arr_countries);
			
			/*
			*	Libreria de funciones / Function library
			*	Aumentado por leninsoft, el 2019-06-07
			*/
			// 
			GenFunction::createLBL("main_title_system", 23, $lang_id);
			GenFunction::createLBL("login_lbl_questions", 23, $lang_id);
			GenFunction::createLBL("login_lbl_asesor", 23, $lang_id);
			GenFunction::createLBL("login_lbl_infomail", 23, $lang_id);
			GenFunction::createLBL("login_lbl_demo", 23, $lang_id);
			GenFunction::createLBL("login_lbl_mtc", 23, $lang_id);
			GenFunction::createLBL("login_frm_remember", 23, $lang_id);
			GenFunction::createLBL("login_frm_login", 23, $lang_id);
			GenFunction::createLBL("login_frm_forgot", 23, $lang_id);
			// Hasta aqui
			
			$form_login['login_title'] =  GenLabel::where('label_code', '=','login_title')->where('lang_code', '=',$lang_id)->first();   
			$form_login['login_language_label'] =  GenLabel::where('label_code', '=','login_language_label')->where('lang_code', '=',$lang_id)->first();
			$form_login['login_form_title'] =  GenLabel::where('label_code', '=','login_form_title')->where('lang_code', '=',$lang_id)->first();
			$form_login['login_user_label'] =  GenLabel::where('label_code', '=','login_user_label')->where('lang_code', '=',$lang_id)->first();
			$form_login['login_password_label'] =  GenLabel::where('label_code', '=','login_password_label')->where('lang_code', '=',$lang_id)->first();
			$form_login['login_button_label'] =  GenLabel::where('label_code', '=','login_button_label')->where('lang_code', '=',$lang_id)->first();
			$frm_obj = GenLabel::whereIn('mod_code',[1,23])->where('lang_code','=',$lang_id)->get();
			foreach ($frm_obj as  $vl) {
				if($vl->label_code=='login_country_sel'){
					$form_login['login_country_sel'] =$vl;
				}else{
					$form_login[$vl->label_code] =$vl;	
				} 
                			
			} // Fin foreach

			return view('admin.login')->with('form_login',$form_login)->with('obj_lang',$obj_lang)->with('arr_countries',$arr_countries)->with('frm_obj',$frm_obj);
		} // Fin defaultLangLogin

		public function changeLangLogin(Request $request){
			if(!$request->session()->has('obj_lang')){
			   $obj_lang = DB::SELECT("select * from gen_lang where lang_state = 1");
			   $request->session()->put('obj_lang',$obj_lang);
			}else{
				$obj_lang = $request->session()->get('obj_lang');
			}

			$lang_id=$request->lang_id_login;
			$request->session()->put('lang_id',$lang_id);
			
			$objLangIso =DB::SELECT("select * from gen_lang where lang_state = 1 and lang_code = ".$lang_id);
			$request->session()->put('lang_iso2',$objLangIso[0]->lang_iso2);			

			if(!$request->session()->has('arr_countries')){
				$arr_countries = DB::SELECT("select * from gen_location_country where glc_status = 1 and lang_code = ". $lang_id);
			}else{
				$arr_countries = $request->session()->get('arr_countries');
			}
			
			$request->session()->put('arr_countries',$arr_countries);
			$form_login['login_title'] =  GenLabel::where('label_code', '=','login_title')->where('lang_code', '=',$lang_id)->first();   
			$form_login['login_language_label'] =  GenLabel::where('label_code', '=','login_language_label')->where('lang_code', '=',$lang_id)->first();
			$form_login['login_form_title'] =  GenLabel::where('label_code', '=','login_form_title')->where('lang_code', '=',$lang_id)->first();
			$form_login['login_user_label'] =  GenLabel::where('label_code', '=','login_user_label')->where('lang_code', '=',$lang_id)->first();
			$form_login['login_password_label'] =  GenLabel::where('label_code', '=','login_password_label')->where('lang_code', '=',$lang_id)->first();
			$form_login['login_button_label'] =  GenLabel::where('label_code', '=','login_button_label')->where('lang_code', '=',$lang_id)->first();

            $arr_countries = DB::SELECT("select * from gen_location_country where glc_status = 1");
            $frm_obj = GenLabel::whereIn('mod_code',[1,23])->where('lang_code','=',$lang_id)->get();
			foreach ($frm_obj as  $vl) {
				if($vl->label_code=='login_country_sel'){
					$form_login['login_country_sel'] =$vl;
				}else{
					$form_login[$vl->label_code] =$vl;	
				} 
                	    
			} // Fin foreach

			return view('admin.login')->with('form_login',$form_login)->with('obj_lang',$obj_lang)->with('arr_countries',$arr_countries);
		} // Fin changeLangLogin

		public function validate_user(Request $request){

			if(!$request->session()->has('obj_lang')){
			   $obj_lang = DB::SELECT("select * from gen_lang where lang_state = 1");
			   $request->session()->put('obj_lang',$obj_lang);
			}else{
				$obj_lang = $request->session()->get('obj_lang');
			}

			$obj_val_user_arr= UserMtc::where('user_login', '=', $request->txtLogin)->get();
            $user_valid = false;
            $user_code  = 0;

            if($obj_val_user_arr){
            	foreach ($obj_val_user_arr as $rw) {
            		if(Hash::check($request->txtPasswd, $rw->user_password)){
            			 $user_code  = $rw->user_code;
			 	         $user_valid = true;
			 	         break;
			       }
            	}//foreach
            }//end if

			$lang_id = $request->opt_lang;
			
			$objLangIso =DB::SELECT("select * from gen_lang where lang_state = 1 and lang_code = ".$lang_id);
			$request->session()->put('lang_iso2',$objLangIso[0]->lang_iso2);			

			if($user_valid){
				$obj_val_user = UserMtc::where('user_code','=',$user_code)->first();
				$request->session()->put('user_code',$obj_val_user->user_code);
				$request->session()->put('user_name',$obj_val_user->user_name);
				$request->session()->put('user_last_name',$obj_val_user->user_last_name);
				$request->session()->put('obj_val_user',$obj_val_user);
				
				$objProfile  =  GenProfile::find($obj_val_user->pro_code);
				$objModuleProfiles =  GenModuleProfile::where('pro_code','=',$obj_val_user->pro_code)
														->where('gmp_status','=',1)
														->get();
				
				$arr_modules = array();
				$i=0;
				foreach($objModuleProfiles as $mdpr){
					$arr_modules[$i]=$mdpr->mod_code;
					$i++;
				}
				sort($arr_modules);
				$modules 	  = GenModule::where('lang_code','=',$lang_id)->where('mod_state','=',1)
											->whereIn('mod_code',$arr_modules)->where('mod_parent','=',0)
											->get();
				$module_child = GenModule::where('lang_code','=',$lang_id)->where('mod_state','=',1)
											->whereIn('mod_code',$arr_modules)->where('mod_parent','!=',0)
											->get();
				if(!$request->session()->has('arr_countries')){
					$arr_countries = DB::SELECT("select * from gen_location_country where glc_status = 1 and lang_code = ". $lang_id);
				}else{
					$arr_countries = $request->session()->get('arr_countries');
				}
                $objLabels = $this->getLabels($lang_id, 23);  

				$alianza_profile  = GenProfileAlianza::where('pro_code','=',$obj_val_user->pro_code)->where('glc_code','=',$request->country_sel)->get();
				if($objProfile->pro_isadmin==0){

					if(empty($alianza_profile)  || $alianza_profile->count()==0){
							$frm_obj = GenLabel::whereIn('mod_code',[1,23])->where('lang_code','=',$lang_id)->get();
							$form_login=$this->getLabelsLogin($lang_id);
							foreach ($frm_obj as  $vl) {
								if($vl->label_code=='login_country_sel'){
									$form_login['login_country_sel'] =$vl;
								}else{
									$form_login[$vl->label_code] =$vl;	
								}  
							} // Fin foreach		

						$error = 'No tiene alianzas asignadas';
						return view('admin.login')->with('error',$error)->with('form_login',$form_login)->with('obj_lang',$obj_lang)->with('arr_countries',$arr_countries);
					}					

				} //end if($objProfile->pro_isadmin==0){

				$request->session()->put('arr_countries',$arr_countries);      
				$request->session()->put('country_sel',$request->country_sel);   
				$request->session()->put('id_pais',$request->country_sel);  
				$request->session()->put('alianza_profile',$alianza_profile);
				$request->session()->put('isadmin',$objProfile->pro_isadmin);
                $request->session()->put('objLabelsAp',$objLabels);				

				return view('admin.dashboard')->with('modules',$modules)->with('module_child',$module_child)->with('objLabels',$objLabels);
			}else{
				$ojLabels =  GenLabel::where('label_code', '=','error_login')->where('lang_code', '=',$lang_id)->first();
				$error=$ojLabels->label_detail;

				$arr_countries = DB::SELECT("select * from gen_location_country where glc_status = 1");
				$request->session()->put('arr_countries',$arr_countries);
				$form_login['login_title'] =  GenLabel::where('label_code', '=','login_title')->where('lang_code', '=',$lang_id)->first();   
				$form_login['login_language_label'] =  GenLabel::where('label_code', '=','login_language_label')->where('lang_code', '=',$lang_id)->first();
				$form_login['login_form_title'] =  GenLabel::where('label_code', '=','login_form_title')->where('lang_code', '=',$lang_id)->first();
				$form_login['login_user_label'] =  GenLabel::where('label_code', '=','login_user_label')->where('lang_code', '=',$lang_id)->first();
				$form_login['login_password_label'] =  GenLabel::where('label_code', '=','login_password_label')->where('lang_code', '=',$lang_id)->first();
				$form_login['login_button_label'] =  GenLabel::where('label_code', '=','login_button_label')->where('lang_code', '=',$lang_id)->first();				
				
				$frm_obj = GenLabel::whereIn('mod_code',[1,23])->where('lang_code','=',$lang_id)->get();
				$form_login=$this->getLabelsLogin($lang_id);
				foreach ($frm_obj as  $vl) {
					if($vl->label_code=='login_country_sel'){
						$form_login['login_country_sel'] =$vl;
					}else{
						$form_login[$vl->label_code] =$vl;	
					}  
				} // Fin foreach				

				return view('admin.login')->with('error',$error)->with('form_login',$form_login)->with('obj_lang',$obj_lang)->with('arr_countries',$arr_countries);;
			}
		} // Fin validate_user

		public function validate_user_get(Request $request){
			
			if(!$request->session()->has('obj_lang')){
			   $obj_lang = DB::SELECT("select * from gen_lang where lang_state = 1");
			   $request->session()->put('obj_lang',$obj_lang);
			}else{
				$obj_lang = $request->session()->get('obj_lang');
			}		
			
			$country_sel = $request->session()->get('country_sel');
			//dd($request->session()->get('country_sel'));
			
			$obj_val_user_arr = Auth::user();//UserMtc::where('user_login', '=', $request->txtLogin)->get();
            $user_valid       = true;
            $user_code        = 0;

            if($obj_val_user_arr){
      		    $user_code  = $obj_val_user_arr->name;
			 	$user_valid = true;
            	/*foreach ($obj_val_user_arr as $rw) {
            		if(Hash::check($request->txtPasswd, $rw->user_password)){
            			 $user_code  = $rw->user_code;
			 	         $user_valid = true;
			 	         break;
			       }
            	} *///foreach
            	
            }//end if

			$lang_id = $request->session()->get('lang_id');
			
			$objLangIso =DB::SELECT("select * from gen_lang where lang_state = 1 and lang_code = ".$lang_id);
			$request->session()->put('lang_iso2',$objLangIso[0]->lang_iso2);			

			if($user_valid){
				$obj_val_user = $obj_val_user_arr;//UserMtc::where('user_code','=',$user_code)->first();
				$request->session()->put('user_code',$obj_val_user->user_code);
				$request->session()->put('user_name',$obj_val_user->user_name);
				$request->session()->put('user_last_name',$obj_val_user->user_last_name);
				$request->session()->put('obj_val_user',$obj_val_user);
				
				$objProfile  =  GenProfile::find($obj_val_user->pro_code);
				$objModuleProfiles =  GenModuleProfile::where('pro_code','=',$obj_val_user->pro_code)->where('gmp_status','=',1)->get();
				
				$arr_modules = array();
				$i=0;
				foreach($objModuleProfiles as $mdpr){
					$arr_modules[$i]=$mdpr->mod_code;
					$i++;
				}
				
				$modules = GenModule::where('lang_code','=',$lang_id)->where('mod_state','=',1)->whereIn('mod_code',$arr_modules)->where('mod_parent','=',0)->get();	
				$module_child     = GenModule::where('lang_code','=',$lang_id)->where('mod_state','=',1)->whereIn('mod_code',$arr_modules)->where('mod_parent','!=',0)->get();


				if(!$request->session()->has('arr_countries')){
					$arr_countries = DB::SELECT("select * from gen_location_country where glc_status = 1 and lang_code = ". $lang_id);
				}else{
					$arr_countries = $request->session()->get('arr_countries');
				}
                $objLabels = $this->getLabels($lang_id, 23);  

				$alianza_profile  = GenProfileAlianza::where('pro_code','=',$obj_val_user->pro_code)->where('glc_code','=',$country_sel)->get();

				if($objProfile->pro_isadmin==0){

					if(empty($alianza_profile)  || $alianza_profile->count()==0){
							$frm_obj = GenLabel::whereIn('mod_code',[1,23])->where('lang_code','=',$lang_id)->get();
							$form_login=$this->getLabelsLogin($lang_id);
							foreach ($frm_obj as  $vl) {
								if($vl->label_code=='login_country_sel'){
									$form_login['login_country_sel'] =$vl;
								}else{
									$form_login[$vl->label_code] =$vl;	
								}  
							} // Fin foreach		

						$error = 'No tiene alianzas asignadas';
						return view('admin.login')->with('error',$error)->with('form_login',$form_login)->with('obj_lang',$obj_lang)->with('arr_countries',$arr_countries);
					}					

				}//end if($objProfile->pro_isadmin==0){


				$request->session()->put('arr_countries',$arr_countries);      
				$request->session()->put('country_sel',$request->country_sel);   
				$request->session()->put('id_pais',$request->country_sel);  
				$request->session()->put('alianza_profile',$alianza_profile);
				$request->session()->put('isadmin',$objProfile->pro_isadmin);								
                $request->session()->put('objLabelsAp',$objLabels);		
                $request->session()->put('modules',$modules);		
                $request->session()->put('module_child',$module_child);					
                return redirect('/');
				//return view('admin.dashboard')->with('modules',$modules)->with('module_child',$module_child)->with('objLabels',$objLabels);
				
			}
		} // Fin validate_user

	    public function generateSessions($request){
			
			if(!$request->session()->has('obj_lang')){
			   $obj_lang = DB::SELECT("select * from gen_lang where lang_state = 1");
			   $request->session()->put('obj_lang',$obj_lang);
			}else{
				$obj_lang = $request->session()->get('obj_lang');
			}		
			
			$country_sel = $request->session()->get('country_sel');
			//dd($request->session()->get('country_sel'));
			
			$obj_val_user_arr = Auth::user();//UserMtc::where('user_login', '=', $request->txtLogin)->get();
            $user_valid       = true;
            $user_code        = 0;

            if($obj_val_user_arr){
      		    $user_code  = $obj_val_user_arr->name;
			 	$user_valid = true;
            	
            }//end if

			$lang_id = $request->session()->get('lang_id');
			
			$objLangIso =DB::SELECT("select * from gen_lang where lang_state = 1 and lang_code = ".$lang_id);
			$request->session()->put('lang_iso2',$objLangIso[0]->lang_iso2);			
			   

			if($user_valid){

				$obj_val_user = $obj_val_user_arr;//UserMtc::where('user_code','=',$user_code)->first();
				$request->session()->put('user_code',$obj_val_user->user_code);
				$request->session()->put('user_name',$obj_val_user->user_name);
				$request->session()->put('user_last_name',$obj_val_user->user_last_name);
				$request->session()->put('obj_val_user',$obj_val_user);
				
				$objProfile  =  GenProfile::find($obj_val_user->pro_code);
				$objModuleProfiles =  GenModuleProfile::where('pro_code','=',$obj_val_user->pro_code)->where('gmp_status','=',1)->get();

				$arr_modules = array();
				$i=0;
				foreach($objModuleProfiles as $mdpr){
					$arr_modules[$i]=$mdpr->mod_code;
					$i++;
				}
				
				$modules = GenModule::where('lang_code','=',$lang_id)->where('mod_state','=',1)->whereIn('mod_code',$arr_modules)->where('mod_parent','=',0)->get();	
				$module_child     = GenModule::where('lang_code','=',$lang_id)->where('mod_state','=',1)->whereIn('mod_code',$arr_modules)->where('mod_parent','!=',0)->get();

				
				if(!$request->session()->has('arr_countries')){
					$arr_countries = DB::SELECT("select * from gen_location_country where glc_status = 1 and lang_code = ". $lang_id);
				}else{
					$arr_countries = $request->session()->get('arr_countries');
				}
				/*
				*	Etiquetas / Labels
				*	Aumentado por leninsoft, el 2019-06-10
				*/
				// Headers
				
				//general
				GenFunction::createLBL("btn_close", 23, $lang_id);
				
				GenFunction::createLBL("app_name_h1", 23, $lang_id);
				GenFunction::createLBL("ly_header_filter", 23, $lang_id);
				GenFunction::createLBL("ly_header_Id", 23, $lang_id);
				GenFunction::createLBL("ly_header_phone", 23, $lang_id);
				GenFunction::createLBL("ly_header_lname", 23, $lang_id);
				GenFunction::createLBL("ly_header_name", 23, $lang_id);
				GenFunction::createLBL("ly_header_aplly", 23, $lang_id);
				GenFunction::createLBL("ly_header_cancel", 23, $lang_id);
				// Block module
				GenFunction::createLBL("ly_bmm_a0_tit", 23, $lang_id);
				GenFunction::createLBL("ly_bmm_a0_des", 23, $lang_id);
				GenFunction::createLBL("ly_bmm_a0_op1", 23, $lang_id);
				GenFunction::createLBL("ly_bmm_a1_tit", 23, $lang_id);
				GenFunction::createLBL("ly_bmm_a1_des", 23, $lang_id);
				GenFunction::createLBL("ly_bmm_a1_op1", 23, $lang_id);
				GenFunction::createLBL("ly_bmm_a1_op2", 23, $lang_id);
				// home
				GenFunction::createLBL("ly_bmm_a2_tit", 23, $lang_id);
				GenFunction::createLBL("ly_bmm_a2_des", 23, $lang_id);
				GenFunction::createLBL("ly_bmm_a2_op1", 23, $lang_id);
				// Footer
				GenFunction::createLBL("ly_bmm_a3_tit", 23, $lang_id);
				GenFunction::createLBL("ly_bmm_a3_col1", 23, $lang_id);
				GenFunction::createLBL("ly_bmm_a3_col2", 23, $lang_id);
				GenFunction::createLBL("ly_bmm_a3_col3", 23, $lang_id);
				GenFunction::createLBL("ly_bmm_a3_col4", 23, $lang_id);
				GenFunction::createLBL("ly_bmm_a3_col5", 23, $lang_id);
				/*
				*	Libreria de funciones / Function library
				*	Aumentado por leninsoft, el 2019-06-07
				*/
				for($i=0; $i < 4; $i++){
					// Area $i
					GenFunction::createLBL("lp_a".$i."_tit", 23, $lang_id);
					GenFunction::createLBL("lp_a".$i."_des", 23, $lang_id);
					GenFunction::createLBL("lp_a".$i."_op1", 23, $lang_id);
					GenFunction::createLBL("lp_a".$i."_op2", 23, $lang_id);
					GenFunction::createLBL("lp_a".$i."_op3", 23, $lang_id);
				}
				// Hasta aqui
				
                $objLabels = $this->getLabels($lang_id, 23);  

				$alianza_profile  = GenProfileAlianza::where('pro_code','=',$obj_val_user->pro_code)->where('glc_code','=',$country_sel)->get();
				
				if($objProfile->pro_isadmin==0){

					if(empty($alianza_profile)  || $alianza_profile->count()==0){
							$frm_obj = GenLabel::whereIn('mod_code',[1,23])->where('lang_code','=',$lang_id)->get();
							$form_login=$this->getLabelsLogin($lang_id);
							foreach ($frm_obj as  $vl) {
								if($vl->label_code=='login_country_sel'){
									$form_login['login_country_sel'] =$vl;
								}else{
									$form_login[$vl->label_code] =$vl;	
								}  
							} // Fin foreach		

						$error = 'No tiene alianzas asignadas';
						Auth::logout();
						return view('admin.login')->with('error',$error)->with('form_login',$form_login)->with('obj_lang',$obj_lang)->with('arr_countries',$arr_countries);
					}					

				}//end if($objProfile->pro_isadmin==0){

				$DTables = GenLanguage::where('lang_code', $lang_id)
								->pluck('lang_DataTables')
								->first();   

				$request->session()->put('DTables',$DTables);      
				$request->session()->put('arr_countries',$arr_countries);      
				$request->session()->put('country_sel',$country_sel);   
				$request->session()->put('id_pais',$country_sel);  
				$request->session()->put('alianza_profile',$alianza_profile);
				$request->session()->put('isadmin',$objProfile->pro_isadmin);								
                $request->session()->put('objLabelsAp',$objLabels);		
                $request->session()->put('modules',$modules);		
                $request->session()->put('module_child',$module_child);	
			
				//return view('admin.dashboard')->with('modules',$modules)->with('module_child',$module_child)->with('objLabels',$objLabels);
				
			}
		} // Fin generateSessions

       public function mainPage(Request $request){	  
			$objLabels =   $request->session()->get('objLabelsAp');		
			$modules   =  $request->session()->get('modules');		
			$module_child = $request->session()->get('module_child');	
			return view('admin.dashboard')->with('modules',$modules)->with('module_child',$module_child)->with('objLabels',$objLabels);
	   }

       public function getLabels($lang_id,$mod_code){

              $objLabelsGen  = GenLabel::where("mod_code","=",$mod_code)->where("lang_code","=",$lang_id)->get();

              $arr_labels = array();

              foreach ($objLabelsGen as $lbl) {

              		$arr_labels[$lbl->label_code]= $lbl->label_detail;

              }

			$objectLb = (object)[];//new stdClass();
			foreach ($arr_labels as $key => $value)
			{
			    $objectLb->$key = $value;
			}

              return $objectLb;

       } 

		public function signOut(Request $request){

		}

		//logout
		public function logout_s(Request $request){
			dd('saliendo');
			/*
			$this->guard()->logout();
			$request->session()->flush();
			$request->session()->regenerate();
			return redirect('/nonnnsssdddf');*/
		}

		//
		
	} // Fin LoginController
