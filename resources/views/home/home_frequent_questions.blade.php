@extends('home.home_page_mtpv_2')
@section('content')

<div class="content container " style="">
	    <style scoped>
			h3{
				font-size:2em;
				color:black;
			}
			
			.cs_title_pr{
				color:#2c0373;
				font-size:2.8em;
				font-weight:bold;
			}
			
			.dv_feq{color:black;
			        padding:20px;}
					
			p{
				color:black;
			}

ol{
	color:black;
}	


p, body {
    font-size: 13px;
    font-weight: 400;
    line-height: 24px;
    color:black;
    -webkit-font-smoothing: antialiased;
}	

			p{
				color:black;
			}
	#freq_quest p{
		color:black;
	}
		</style>
		<div class="dv_feq" id="freq_quest">
		 <br><br>
  <p><strong>PREGUNTAS FRECUENTES SOBRE PAGOS ELECTR&Oacute;NICOS</strong></p>
<p>&nbsp;</p>
<p>En este documento, usted encontrar&aacute; una secci&oacute;n de preguntas y respuestas que le ayudar&aacute; a sus usuarios a aclarar sus dudas sobre los pagos electr&oacute;nicos procesados a trav&eacute;s de <strong>Place</strong><strong>to</strong><strong>Pay</strong>.&nbsp; Recuerde que debe incluirlas en su sitio web.</p>
<p>&nbsp;</p>
<ol>
<li><strong>&iquest;Qu&eacute; es </strong><strong>Place</strong><strong>to</strong><strong>Pay</strong><strong>? </strong></li>
</ol>
<p><strong>Place</strong><strong>to</strong><strong>Pay</strong> es la plataforma de pagos electr&oacute;nicos que usa <b>Memorial International Ecuador</b> para procesar en l&iacute;nea las transacciones generadas en la tienda virtual con las formas de pago habilitadas para tal fin.&nbsp;</p>
<p>&nbsp;</p>
<ol start="2">
<li><strong>&iquest;C&oacute;mo puedo pagar?</strong></li>
</ol>
<p>En la tienda virtual de <b>Memorial International Ecuador</b> usted podr&aacute; realizar su pago con los medios habilitados para tal fin. Usted, de acuerdo a las opciones de pago escogidas por el comercio, podr&aacute; pagar a trav&eacute;s (<strong> Diners, Discover, Visa y MasterCard</strong>); de todos los bancos con pago corriente y en los diferido, &uacute;nicamente las tarjetas emitidas por Banco Pichincha, Diners, Loja, BGR y Manab&iacute;.</p>
<p>&nbsp;</p>
<ol start="3">
<li><strong>&iquest;Es seguro ingresar mis datos bancarios en este sitio web? </strong></li>
</ol>
<p>Para proteger tus datos&ensp;<b>Memorial International Ecuador</b> delega en <strong>Place</strong><strong>to</strong><strong>Pay </strong>la captura de la informaci&oacute;n sensible. Nuestra plataforma de pagos cumple con los m&aacute;s altos est&aacute;ndares exigidos por la norma internacional PCI DSS de seguridad en transacciones con tarjeta de cr&eacute;dito. Adem&aacute;s tiene certificado de seguridad SSL expedido por GeoTrust una compa&ntilde;&iacute;a Verisign, el cual garantiza comunicaciones seguras mediante la encriptaci&oacute;n de todos los datos hacia y desde el sitio; de esta manera, te podr&aacute;s sentir seguro a la hora de ingresar la informaci&oacute;n de su tarjeta.</p>
<p>&nbsp;</p>
<p>Durante el proceso de pago, en el navegador se muestra el nombre de la organizaci&oacute;n autenticada, la autoridad que lo certifica y la barra de direcci&oacute;n cambia a color verde. Estas caracter&iacute;sticas son visibles de inmediato y dan garant&iacute;a y confianza para completar la transacci&oacute;n en <strong>Place</strong><strong>to</strong><strong>Pay</strong>.</p>
<p>&nbsp;</p>
<p><strong>Place</strong><strong>to</strong><strong>Pay </strong>tambi&eacute;n cuenta con el monitoreo constante de McAfee Secure y la firma de mensajes electr&oacute;nicos con Certic&aacute;mara.</p>
<p>&nbsp;</p>
<p><strong>Place</strong><strong>to</strong><strong>Pay</strong> es una marca de la empresa colombiana EGM Ingenier&iacute;a Sin Fronteras S.A.S.</p>
<p>&nbsp;</p>
<ol start="4">
<li><strong>&iquest;Puedo realizar el pago cualquier d&iacute;a y a cualquier hora? </strong></li>
</ol>
<p>S&iacute;, en <b>Memorial International Ecuador</b> podr&aacute;s realizar tus compras en l&iacute;nea los 7 d&iacute;as de la semana, las 24 horas del d&iacute;a a s&oacute;lo un clic de distancia.</p>
<p>&nbsp;</p>
<ol start="5">
<li><strong>&iquest;Puedo cambiar la forma de pago?</strong></li>
</ol>
<p>Si a&uacute;n no has finalizado tu pago, podr&aacute;s volver al paso inicial y elegir la forma de pago que prefieras. Una vez finalizada la compra no es posible cambiar la forma de pago.</p>
<p><strong><em>&nbsp;</em></strong></p>
<ol start="6">
<li><strong>&iquest;Pagar electr&oacute;nicamente tiene alg&uacute;n valor para m&iacute; como comprador? </strong></li>
</ol>
<p>No, los pagos electr&oacute;nicos realizados a trav&eacute;s de <strong>Place</strong><strong>to</strong><strong>Pay </strong>no generan costos adicionales para el comprador.</p>
<p>&nbsp;</p>
<ol start="7">
<li><strong>&iquest;Qu&eacute; debo hacer si mi transacci&oacute;n no concluy&oacute;?</strong></li>
</ol>
<p>En primera instancia deber&aacute;s revisar si lleg&oacute; un mail de confirmaci&oacute;n del pago en tu cuenta de correo electr&oacute;nico (la inscrita en el momento de realizar el pago), en caso de no haberlo recibido, deber&aacute;s contactar a <b>info@memorialinternational.com</b> para confirmar el estado de la transacci&oacute;n.</p>
<p>&nbsp;</p>
<p>En caso que tu transacci&oacute;n haya declinado, debes verificar si la informaci&oacute;n de la cuenta es v&aacute;lida, est&aacute; habilitada para compras no presenciales y si tienes cupo o saldo disponible. Si despu&eacute;s de esto continua con la declinaci&oacute;n debes comunicarte con <b>Memorial International Ecuador</b>. En &uacute;ltima instancia, puedes remitir tu solicitud a <a href="mailto:servicioposventa@placetopay.ec">servicioposventa@placetopay.ec</a>.</p>
<p>&nbsp;</p>
<ol start="8">
<li><strong>&iquest;Qu&eacute; debo hacer si no recib&iacute; el comprobante de pago?</strong></li>
</ol>
<p>Por cada transacci&oacute;n aprobada a trav&eacute;s de <strong>Place</strong><strong>to</strong><strong>Pay</strong>, recibir&aacute;s un comprobante del pago con la referencia de compra en la direcci&oacute;n de correo electr&oacute;nico que indicaste al momento de pagar.&nbsp; Si no lo recibes, podr&aacute;s contactar a la l&iacute;nea <strong>(+593) 2 - 244 - 7431</strong> o al correo electr&oacute;nico <b>info@memorialinternational.com</b>, para solicitar el reenv&iacute;o del comprobante a la misma direcci&oacute;n de correo electr&oacute;nico registrada al momento de pagar. En &uacute;ltima instancia, puedes remitir tu solicitud a <a href="mailto:servicioposventa@placetopay.ec">servicioposventa@placetopay.ec</a>.</p>
<p>&nbsp;</p>
<ol start="9">
<li><strong> No me lleg&oacute; el producto que compr&eacute; &iquest;qu&eacute; hago?</strong></li>
</ol>
<p>Debes verificar si la transacci&oacute;n fue exitosa en tu extracto bancario. En caso de ser as&iacute;, debes revisar nuestras pol&iacute;ticas de env&iacute;o en el sitio web <b>www.ventas.memorial.ec</b> para identificar los tiempos de entrega.</p>

</div>

</div>