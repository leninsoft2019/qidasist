<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");

?>
<!-- The Modal -->


<div>


<style scoped src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</style>

<style  scoped type="text/css">
	#myModalBDP2P{
		max-width: 1000px;
		

	}
	
#myModalBDP2P{
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 1000px;
}	

	#body_mod_p2p{
		max-height: 500px;	
		overflow-y: scroll;	
	}

	
</style>
<div class="modal" id="myModalP2P">

  <div class="modal-dialog modal-dialog-centered" id="myModalBDP2P">
    <div class="modal-content">
       <style scoped>
	    #frmAdicionales{
			padding:15px;
		}
		
		#myModalConfirm modal-dialog{
			max-width:300px;
		}
	   </style>
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><span class="span-title"><i class="ico_anullment_orange"></i>Place to Pay</span></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>

      <!-- Modal body -->
      <div class="modal-body">
			<div id="body_mod_p2p">
			</div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-danger button_module_wh" data-dismiss="modal">Close</button>-->
&nbsp;
	    <button name="btnGuardarAnul" id="btnGuardarAnul" onclick="<?php
             if(Session::has('first_pay')){
				echo 'nextStageP2P()'; 
			 }else{
				echo 'firstPaymentP2P()'; 
			 }?>"  class="btn btn-default">Regresar a Memorial</button>		
      </div>

    </div>
  </div>
</div>	


<!-- The Modal -->
<div class="modal" id="myModalConfirm">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">



      <!-- Modal body -->
      <div class="modal-body" id="dv_confirm_additional">
			<p>Ya existe una asistencia asociada al producto seleccionado, desea ingresar otra?</p>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger button_module_wh" data-dismiss="modal">Cancelar</button>
&nbsp;
	    <button name="btnGuardarAnul" id="btnGuardarAnul" onclick="saveAdditional()"  class="btn btn-default">Guardar</button>		
      </div>

    </div>
  </div>
</div>	
</div>
<script type="text/javascript">
       $(function(){


          $('#dv_alert_warn').hide();
		  
		  $('#sel_tpr').on('change',function(){
			 loadListChild('sel_tpr','sel_tctg','tipo_cat_pr');
			 $('#sel_tctg').trigger('change'); 
		  });
		  
		  
		  $('#sel_tctg').on('change',function(){
			  
			 loadListChild('sel_tctg','sel_car','cartera_tp');
			  
		  });		  
		  
		  $('#sel_car').on('change',function(){
			  
			 loadListChild('sel_car','sel_categmine','categoria_ctp');
			  
		  });		


       	  $('#sel_categmine').on('change', function() {
				Pace.restart();
				
				var sel_id     = $( "#sel_categmine option:selected" ).val();
				var $child_sel = $('#sel_pr');
				$.ajax({
					type: 'GET',
					url:'/insurance/cmbProduct/'+sel_id+'/',
					success:function(data){
						if(data.success==1){

							$child_sel.find('option').remove();         
							$.each( data.json_product, function(i, obj) {
								if(i==0){
									$child_sel.append('<option value="0" selected>----</option>');
									$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
								}else{
									$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
								}
							});
								
							
							return;
						}else{
							
							alertPPYA(data.message);
							return;
						}
					}
				});	
             
		  });		  
		  
       	  $('#sel_pr').on('change', function() {
       	  	 $("#id_producto").val(this.value);
       	  	 var id_producto  = this.value;


				$.ajax({
						type:	'GET',
						url:	'/insurance/get_data_product/'+id_producto+'/',
						success:function(data){
							console.log(data);
							if(data.success==1){

								var valor_pr  = parseFloat(data.arr_product.precio_unit)*parseFloat(data.arr_product.npc);
								$("#ad_valor").val(valor_pr.toFixed(2));
								return;
							}else if(data.success==2){
								$('#dv_alert_warn').show();
								$('#dv_alert_warn').addClass('in');
								$('#dv_alert_warn').addClass('show');	
								var valor_pr  = parseFloat(data.arr_product.precio_unit)*parseFloat(data.arr_product.npc);
								$("#ad_valor").val(valor_pr.toFixed(2));								
								/*$('#dialog-confirm').dialog('open');*/
								return;
							}
						}
				}); 
             
		  });
		  
		  
		  
		  
		  
       });



		function loadListChild(parent_name,child_name,child_pref){
			Pace.restart();
			
			var sel_id = $( "#"+parent_name+" option:selected" ).val();
			var $child_sel = $('#'+child_name);
			$.ajax({
				type: 'GET',
				url:'/general/getListChild/'+child_pref+'/'+sel_id+'/',
				success:function(data){
					if(data.success==1){

					    if(child_name=='sel_car'){
							$child_sel.find('option').remove();         
							$.each( data.modelData, function(i, obj) {
								  if(i==0){
									  $child_sel.append('<option value="0" selected>----</option>');
								  }
								if(obj.por_additional == 1){
									if(i==0){
										
										$child_sel.append('<option value=' + obj.por_id + ' >' + obj.por_name + '</option>');
									}else{
										$child_sel.append('<option value=' + obj.por_id + ' >' + obj.por_name + '</option>');
									}
								}

							});
						}else{
								$child_sel.find('option').remove();         
								$.each( data.arr_dataw2ui, function(i, obj) {
									if(i==0){
										$child_sel.append('<option value="0" selected>----</option>');
										$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
									}else{
										$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
									}
								});
							
						}
						

							
						
						return;
					}else{
						
						alertPPYA(data.message);
						return;
					}
				}
			});
		}

</script>
