<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalMethodPaySubscription extends Model
{
    //
	
    protected $table = 'sal_methodpay_subscriptions';
	protected $primaryKey = 'mps_id';
}
