<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");

?>
	<!-- The Modal -->
	<div>
		<style scoped src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"></style>

		<style  scoped type="text/css">
			#myModalBDP2P{
				max-width: 1000px;
			}

			#myModalBDP2P{
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate(-50%, -50%);
				width: 1000px;
			}	

			#body_mod_p2p{
				max-height: 500px;	
				overflow-y: scroll;	
			}
		</style>
		
		<div class="modal" id="myModalTransf">

			<div class="modal-dialog modal-dialog-centered" id="myModalBDP2P">
				<div class="modal-content">
					<style scoped>
						#frmAdicionales{
							padding:15px;
						}

						#myModalConfirm modal-dialog{
							max-width:300px;
						}
					</style>
					<!-- Modal Header -->
					<div class="modal-header">
						<h4 class="modal-title"><span class="span-title"><i class="ico_anullment_orange"></i>Transferencia</span></h4>
					</div>

					<!-- Modal body -->
					<div class="modal-body">
					 <form  id="frmTrf">
								{{ csrf_field() }}	
						<div class="tbl">
							<div class="row cab_r rw">
								<div class="col col_50">
									<div class="form-group">
										<label for="nombre">Nombre:</label>
										<input type="text" class="form-control" id="nombre_trf" name="nombre_trf" />
									</div>
								</div>
								<div class="col col_50">
									<div class="form-group">
										<label for="apellido">Apellido:</label>
										<input type="text" class="form-control" id="apellido_trf"  name="apellido_trf" />
									</div>
								</div>
							</div>
							
							<div class="row cab_r rw">
								<div class="col col_50">
									<div class="form-group">
										<label for="nombre">N&uacute;mero de identificaci&oacute;n:</label>
										<input type="text" class="form-control" id="identificacion" name="identificacion"  />
									</div>
								</div>
								<div class="col col_50">
									<div class="form-group">
										<label for="apellido">Banco:</label>
										<?php echo $selectBank; ?>
									</div>
								</div>
							</div>
							
							<div class="row cab_r rw">
								<div class="col col_50">
									<div class="form-group">
										<label for="nombre">Referencia:</label>
										<input type="text" class="form-control" id="referencia" name="referencia" />
									</div>
								</div>
								<div class="col col_50">
									<div class="form-group">
										<label for="apellido">Valor:</label>
										<input type="text" class="form-control" id="valor"  name="valor"  />
									</div>
								</div>
							</div>
							

						</div>
					 </form>
					</div>

					<!-- Modal footer -->
						<div class="modal-footer">
						<button type="button" class="btn btn-danger button_module_wh" data-dismiss="modal">Cerrar</button>
				&nbsp;
						<button name="btnGuardarTrf" id="btnGuardarTrf"   class="btn btn-default">Guardar</button>		
					  </div>
					
					
					
				</div>
			</div>
		</div>	
	</div>
	<script type="text/javascript">
		
		function Numeros(numero){
			var out = '';
			var filtro = '1234567890';//Caracteres validos

			//Recorrer el texto y verificar si el caracter se encuentra en la lista de validos 
			for (var i=0; i<string.length; i++)
				if (filtro.indexOf(string.charAt(i)) != -1) 
			//Se añaden a la salida los caracteres validos
			out += string.charAt(i);

			//Retornar valor filtrado
			return out;
		}
		
		$(function(){

			$('#dv_alert_warn').hide();

          $('#alert_success').hide();
          $('#alert_error').hide();		  			
			

			$("#btnGuardarTrf").click(function( event ) {
				event.preventDefault();

					    var form = $('#frmTrf,#frmMT');
						var request = $.ajax({
							type:	'POST',
							url:	'/sales/payment/save_trf',
							data:	form.serialize(),
							success:function(data){
								console.log(data);
								if(data.success==1){
									$('#alert_success').html(data.message);
									$('#alert_success').show();
									
									$("#btnGuardarDeb").prop('disabled',true);
									$("#btnCerrarDeb").prop('disabled',true);									
									$('#alert_success').html(data.message);
									$('#alert_success').show();
									
									/*setInterval(function(){ 
										
										$('#myModalDebit').fadeIn("slow");
										

									}, 2000);*/
									$('#myModalTransf').modal('hide');
									$('#seccion_detalle_planes').load(data.url_redirect);	
																		
									        			
									return;
								}else if(data.success==2){
									$('#alert_error').empty();
									$.each( data.errors, function(i, obj) {
										$('#alert_error').append(obj+'<br>');
										
									});
									
									$('#alert_error').show();
									/*alertPPYA(data.message);*/
									console.log(data);
									return;
								}
							}
						});
						request.fail(function( jqXHR, textStatus ) {
							alertPPYA( "Hubo un error: " + textStatus );
							console.log(jqXHR);
						});
								
				
			});			
			
       });

		

	</script>
