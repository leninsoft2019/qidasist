<?php
/*
*	Página que despliega la tabla de productos a cotizar o vender
*	Page that displays the table of products to be quoted or sold
*/

?>
<link rel="stylesheet" type="text/css" href="{{asset('css/main_price_table.css')}}" />

<div>
	<section id="pricing-table">
		<div class="container_r">
			<div class="pricing cstable">
				<div class="tr_btn">
					@foreach($arrWebProduct as $webProduct)
					<div class="cstablecell_3">
						<div class="pricing-table">
							<div class="pricing-header">
								<p class="pricing-title">
									<b><a href="{{$webProduct->pro_linkweb}}">{{$webProduct->pro_name}}</a></b>
								</p>
								<p class="pricing-rate"> 
									{{$webProduct->pro_price}}
								</p>
							</div>

							<div class="pricing-list">
								<ul>
									<li>
										<i class="fa fa-envelope"></i>
										<span>{{$webProduct->pro_description1}}</span>
									</li>
									<li>
										<i class="fa fa-signal"></i>
										<span>{{$webProduct->pro_description2}}</span>
									</li>
								</ul>
							</div>
						</div>
					</div>
					@endforeach					
				</div>
			</div>
		</div>
	</section>
</div>