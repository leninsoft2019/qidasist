<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenBank extends Model{
    //
	protected $table = 'gen_bank';
	protected $primaryKey = 'ban_code';
}
