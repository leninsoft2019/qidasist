
@extends('client.home_quoting_template')

@section('content')
	<style scoped>
		#clientData{
			color: black;
			width:90%;
			background:white;
			margin:auto;
			border:solid 1px #f2f4f4;
			border-radius:5px;
			margin-top:30px;
			padding:20px;
		}
		#clientData table{
			color: black;
		}
		ul>li{
			padding-left:10px;
		}
		
		h3{
			color:black;
			font-weight:bold;
			padding-top:20px;
		}
		
		.buttons-html5{
		   background:#f2f4f4;
           color:black;
			border:solid 1px #9C9C9C;	
		   padding-left:10px;
		   margin-left:10px;
		   
			text-align:center;
			
			display: inline-block;
			*display: inline;
			*zoom: 1;
			padding: 4px 12px;
			margin-bottom: 0;
			font-size: 14px;
			line-height: 20px;
			text-align: center;
			vertical-align: middle;
			cursor: pointer;
			color: #333333;
			text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
			background-color: #f5f5f5;
			background-image: -moz-linear-gradient(top, #ffffff, #e6e6e6);
			background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ffffff), to(#e6e6e6));
			background-image: -webkit-linear-gradient(top, #ffffff, #e6e6e6);
			background-image: -o-linear-gradient(top, #ffffff, #e6e6e6);
			background-image: linear-gradient(to bottom, #ffffff, #e6e6e6);
			background-repeat: repeat-x;
			filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe6e6e6', GradientType=0);
			border-color: #e6e6e6 #e6e6e6 #bfbfbf;
			border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
			*background-color: #e6e6e6;
			filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
			border: 1px solid #cccccc;
			*border: 0;
			border-bottom-color: #b3b3b3;
			-webkit-border-radius: 4px;
			-moz-border-radius: 4px;
			border-radius: 4px;
			*margin-left: .3em;
			-webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
			-moz-box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
			box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);			
			
		}
		
		.paginate_button page-item previous disabled{
			color:white;
		}
		
		.paginate_button{
			color:white;
			padding:3px;
			padding-left:0px;
			font-size:small;
		}
		
		.pagination > li.disabled > a {
			background-color: #9C9C9C;
			color: white;
		}
		
		#IdDT_filter{
			float:right;
			position:relative;
		}
		
		.nav-item{
			
			background-color: #f2f2f2;
            border:solid 1px #9C9C9C;
		}
		
		#IdDT_filter>label{
			display:flex;
		}
		
		.form-control-sm{
				height:25px;
		}
	</style>
	
	<center><h3>Metodo de pago: Recurrencia</h3></center>
	<div id="clientData">
		<header>
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<div class="cls_fieldset_menu_2" role="nav">
					<div style="" class="grp_buttons_inline">
						<div class="tc_btn">
							<div class="tc_btn">
								<button name="btnRegresar" class="button_module_wh" onclick="">{{$objform->button_back}}</button>
							</div>				
						</div>
					</div>
				</div>
				
			</nav>
		</header>

		<article>

			<section style="width:100%; margin:auto; padding:0px;" >

				<div id="showResult">
					<div class="cls_fieldset_header">
		    			<table id="methodPay2" class="display hover compact" style="width:100%">
		    				<caption></caption>
		    				<thead>
		    				<tr>
		    					<td>{{$objform->lbl_cxc_MethodPay}}</td>
		    					<td>{{$objform->lbl_cxc_bank}}</td>
		    					<td>{{$objform->lbl_cxc_NumberCheck}}</td>
		    					<td>{{$objform->lbl_cxc_NumberAutho}}</td>
		    					<td>{{$objform->lbl_cxc_Numberlote}}</td>
		    					<td>{{$objform->lbl_cxc_NumberRef}}</td>
		    					<td>{{$objform->lbl_cxc_ExpDate}}</td>
		    					<td>{{$objform->lbl_cxc_Value}}</td>
		    				</tr>
		    				</thead>
		    				<tr>
		    					<td data-label='{{$objform->lbl_cxc_MethodPay}}'>
		    						<select id="lbl_cxc_MethodPay{{$i}}" name="lbl_cxc_MethodPay{{$i}}" onChange="Getbank(this.options[this.selectedIndex].value, {{$i}})"  class="form-control">
		    						<option value="0">Select a option</option>
		    							@foreach($MPay as $val)
		    							<option value="{{$val->pmt_name}}">{{$val->pmt_description}}</option>
		    							@endforeach
		    						</select>
		    					</td>
		    					<td id="td_{{$i}}" data-label='{{$objform->lbl_cxc_bank}}'>
		    						<select id="lbl_cxc_bank{{$i}}" name="lbl_cxc_bank{{$i}}"  class="form-control">
		    							<option value="0">Select a option</option>
		    						</select>
		    					</td>
		    					<td data-label='{{$objform->lbl_cxc_NumberCheck}}'>
		    						<input type="text" id="lbl_cxc_NumberCheck{{$i}}" name="lbl_cxc_NumberCheck{{$i}}" placeholder="{{$objform->lbl_cxc_PHCheck}}" value="" size="15"  class="form-control" />
		    					</td>
		    					<td data-label='{{$objform->lbl_cxc_NumberAutho}}'>
		    						<input type="text" id="lbl_cxc_NumberAutho{{$i}}" name="lbl_cxc_NumberAutho{{$i}}" placeholder="{{$objform->lbl_cxc_PHAutho}}" value="" size="15"  class="form-control" />
		    					</td>
		    					<td data-label='{{$objform->lbl_cxc_Numberlote}}'>
		    						<input type="text" id="lbl_cxc_Numberlote{{$i}}" name="lbl_cxc_Numberlote{{$i}}" placeholder="{{$objform->lbl_cxc_PHlote}}" value="" size="10"  class="form-control" />
		    					</td>
		    					<td data-label='{{$objform->lbl_cxc_NumberRef}}'>
		    						<input type="text" id="lbl_cxc_NumberRef{{$i}}" name="lbl_cxc_NumberRef{{$i}}" placeholder="{{$objform->lbl_cxc_PHRef}}" value="" size="15"  class="form-control" />
		    					</td>
		    					<td data-label='{{$objform->lbl_cxc_ExpDate}}'>
		    						<input type="lenin" id="lbl_cxc_ExpDate{{$i}}" name="lbl_cxc_ExpDate{{$i}}" placeholder="{{$objform->lbl_cxc_PHExpDate}}" value="" size="10"  class="form-control" />
		    					</td>
		    					<td data-label='{{$objform->lbl_cxc_Value}}'>
		    						<input type="text" id="lbl_cxc_Value{{$i}}" name="lbl_cxc_Value{{$i}}" placeholder="{{$objform->lbl_cxc_PHValue}}" value="0.00" size="10"
		    						onkeypress="return (event.charCode >= 48 && event.charCode <= 57) || event.charCode == 8 || event.charCode == 46 ||
		    						event.charCode == 0 || event.charCode == 110" onChange="UpdatePay();"  class="form-control" />
		    					</td>
		    				</tr>
		    			</table>  
	    		    </div>
				</div>

			</section>
		</article>
		
		<aside></aside>

		<script>
			/***************Process Getbank ****************************/
			function Getbank(op, i){
				//alert(op);
				$.ajax({
					type:	'GET',
					url:	'/generic/getCmb/'+op,
					success:function(data){
						if(data.success==1){
							switch(op) {
								case 'EF':
									$('#lbl_cxc_bank'+i).attr('disabled', true);
									$('#lbl_cxc_NumberCheck'+i).attr('disabled', true);
									$('#lbl_cxc_NumberAutho'+i).attr('disabled', true);
									$('#lbl_cxc_Numberlote'+i).attr('disabled', true);
									$('#lbl_cxc_NumberRef'+i).attr('disabled', true);
									$('#lbl_cxc_ExpDate'+i).attr('disabled', true);
									break;
								case 'CO':
									// code block
									break;
								case 'CH':
									// code block
									break;
								case 'CP':
									// code block
									break;
								case 'TJ':
									// code block
									break;
								case 'OT':
									// code block
									break;
								case 'DB':
									// code block
									break;
								case 'DP':
									// code block
									break;
								case 'TR':
									// code block
									break;
								case 'RF':
									// code block
									break;
								case 'RI':
									// code block
									break;
								default:
									// code block
							} // Fin switch
							$('#td_'+i).html(data.hmtl_var);
							return;
						}else{
							console.log(data);
							alert('{{$objform->app_message_error}}');
							return;
						}
					}
				});
			} // Fin Getbank
			/*************** End Process Getbank ***********************/
		
			$(document).ready(function() {
				var fecha = new Date();
				var nombre = "MTC_"+fecha.getFullYear();
				nombre += fecha.getMonth();
				nombre += fecha.getDate();
				nombre += fecha.getHours();
				nombre += fecha.getMinutes();
				nombre += fecha.getSeconds();
				var table = $('#IdDT').DataTable({
					//scrollY:        "300px",
					lengthChange:	false,
					stateSave: 		true,
					scrollX:        true,
					fixedColumns:   {
						leftColumns: 5,
						scrollCollapse: true
						//rightColumns: 1
					},
					"language": {
						"url": "{{$DTables}}"
					},
					dom: 'Bfrtip',
					buttons: [
						{
							extend:    'copyHtml5',
							copyTitle: '{{$objform->button_export}}',
							copyKeys: '{{$objform->btn_copyKeys}}',
							copySuccess: {
								_: '%d {{$objform->btn_copySuccess1}}',
								1: '1  {{$objform->btn_copySuccess2}}'
							}
						},
						{
							extend: 'excelHtml5',
							title: nombre,
							messageTop:  'PDF created by MTC.'
						},
						{
							extend:   	 'pdfHtml5',
							title:	  	 nombre,
							messageTop:  'PDF created by MTC.',
							orientation: 'landscape',
							pageSize:	 'A4'
						},
						{
							text:		 'CSV',
							title:	  	 nombre,
							extend: 	 'csvHtml5',
							fieldSeparator: '<?php echo $delimiter; ?>',
							extension: 	 '.csv',
							className:   'red'
						}
					],
					"footerCallback": function ( row, data, start, end, display ) {
						var api = this.api(), data;
						
						// Remove the formatting to get integer data for summation
						var intVal = function ( i ) {
							return typeof i === 'string' ?
							i.replace(/[\$,]/g, '')*1 :
							typeof i === 'number' ?
							i : 0;
						};
						
						// Total over all pages
						total = api
						.column( <?php echo $colvalue; ?> )
						.data()
						.reduce( function (a, b) {
							return intVal(a) + intVal(b);
						}, 0 );
						
						// Total over this page
						pageTotal = api
						.column( <?php echo $colvalue; ?>, { page: 'current'} )
						.data()
						.reduce( function (a, b) {
							return intVal(a) + intVal(b);
						}, 0 );
						
						// Update footer
						$( api.column( <?php echo $colvalue; ?> ).footer() ).html(
							'$'+pageTotal +' ( $'+ total +' total)'
						);
					}
				});
				table.buttons().container().appendTo( '#IdDT_paginate .col-md-6:eq(0)' );
			} );
			
			
			function setDataTab(){
				console.log('setting table');
				var fecha = new Date();
				var nombre = "MTC_"+fecha.getFullYear();
				nombre += fecha.getMonth();
				nombre += fecha.getDate();
				nombre += fecha.getHours();
				nombre += fecha.getMinutes();
				nombre += fecha.getSeconds();
				var table = $('#IdDT').DataTable({
					lengthChange:	false,
					stateSave: 		true,
					scrollX:        true,
					fixedColumns:   {
						leftColumns: 5,
						scrollCollapse: true
						//rightColumns: 1
					},
					"language": {
						"url": "{{$DTables}}"
					},
					dom: 'Bfrtip',
					buttons: [
						{
							extend:    'copyHtml5',
							copyTitle: '{{$objform->button_export}}',
							copyKeys: '{{$objform->btn_copyKeys}}',
							copySuccess: {
								_: '%d {{$objform->btn_copySuccess1}}',
								1: '1  {{$objform->btn_copySuccess2}}'
							}
						},
						{
							extend: 'excelHtml5',
							title: nombre,
							messageTop:  'PDF created by MTC.'
						},
						{
							extend:   	 'pdfHtml5',
							title:	  	 nombre,
							messageTop:  'PDF created by MTC.',
							orientation: 'landscape',
							pageSize:	 'A4'
						},
						{
							text:		 'CSV',
							title:	  	 nombre,
							extend: 	 'csvHtml5',
							fieldSeparator: '<?php echo $delimiter; ?>',
							extension: 	 '.csv',
							className:   'red'
						}
					]
					
				});
				console.log('luego table sett');
				table.buttons().container().appendTo( '#IdDT_paginate .col-md-6:eq(0)' );				
				
			}

		</script>
	</div>
@endsection