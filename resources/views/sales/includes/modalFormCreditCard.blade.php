							
							
<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");

$cmb_operadora     =  $arr_combos['cmb_operadora'];
$cmb_tipo_tarjeta  =  $arr_combos['cmb_tipo_tarjeta'];
$cmb_banco         =  $arr_combos['cmb_banco'];
				   
			
					
?>
<!-- The Modal -->


<div>


<style scoped src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</style>

<style  scoped type="text/css">
	#myModalBDP2P{
		max-width: 1000px;
		

	}
	
	#myModalCreditC{
	  position: absolute;
	  top: 50%;
	  left: 50%;
	  transform: translate(-50%, -50%);
	  width: 1200px;
	  height: 590px;	
	  font-size:1.5em;
	  
	}	

	#body_mod_creditc{
		max-height: 590px;	
		overflow-y: scroll;	
	}
	
	#md_bdy{
			padding:20px;
	}


	
</style>
<div class="modal" id="myModalCreditC">
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
  <div class="modal-dialog modal-dialog-centered" id="myModalBDP2P">
    <div class="modal-content">
       <style scoped>
	    #frmAdicionales{
			padding:15px;
		}
		
		#myModalConfirm modal-dialog{
			max-width:300px;
		}
		
	   </style>
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><span class="span-title"><i class="ico_anullment_orange"></i>Formulario de Autorizacion para Tarjeta de Credito</span></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>

      <!-- Modal body -->
      <div class="modal-body" id="md_bdy">
	  
	   <div class="alert alert-success" id="alert_success_cc">
		<strong>Success!</strong> This alert box could indicate a successful or positive action.
	  </div>
	  <div class="alert alert-danger" id="alert_error_cc">
		<strong>Danger!</strong> This alert box could indicate a dangerous or potentially negative action.
	  </div>  
			<div id="body_mod_creditc">

							<form  id="frmCreditC">
								{{ csrf_field() }}	

								<hr></hr>
								<!-- Titular -->
								<div class="tbl">
								
									<div class="row cab_r rw">
									<input type="hidden" name="operador_tarjeta" id="operador_tarjeta" value="0" />
										<!--<div class="col col_50">
											<div class="form-group">
												<label for="operador_tarjeta">Operadora:</label>
												<select id="operador_tarjeta" name="operador_tarjeta" class="form-control" >
													<option value="0">-----</option>
													@foreach($cmb_operadora as $cv)
															<?php  
																 $varCh ="";
														
															?>
													<option value="{{$cv->cco_code}}"  {{$varCh}}>{{$cv->cco_name}}</option>
													@endforeach
												</select>
											</div>
										</div>-->
										<div class="col col_50">
											<div class="form-group">
												<label for="banco">Banco:</label>
												<select id="banco" name="banco" class="form-control" >
													<option value="0">-----</option>
													@foreach($cmb_banco as $cv)
															<?php  
																 $varCh ="";
														
															?>
													<option value="{{$cv->ban_code}}"  {{$varCh}}>{{$cv->ban_name}}</option>
													@endforeach
												</select>
											</div>
										</div>										
										<div class="col col_50">
											<div class="form-group">
												<label for="tipo_tarjeta" >Tipo Tarjeta:</label>
											
											 
												<select id="tipo_tarjeta" name="tipo_tarjeta"  class="form-control">
													<option value="0">-----</option>
													@foreach($cmb_tipo_tarjeta as $cv)
															<?php  
																 $varCh ="";
																// if($arr_formapago['tipo_tarjeta'] == $cv->cca_code){
																//	 $varCh ="selected";
																 //}
															?>
															<option value="{{$cv->cca_code}}"  {{$varCh}}>{{$cv->cca_name}}</option>
													@endforeach
												</select>
												
   
											</div>
										</div>
									</div>
									
									
	
									<div class="row cab_r rw">
										<div class="col col_50">
											<div class="form-group">
												 <label>Numero Tarjeta:</label>
                                 
												 <input type="text" id="numero_tarjeta" name="numero_tarjeta" value="" class="form-control" />
                               
											</div>
										</div>
										<div class="col col_50">
											<div class="form-group">
												<label>Fecha Vigencia:</label>
                                   
												<input type="text" id="fecha_vg_tarjeta" name="fecha_vg_tarjeta" value="" class="form-control expiration-date" />
                                  
											</div>
										</div>
									</div>
									
								
									<div class="row cab_r rw">
										<div class="col col_50">
											<div class="form-group">
												<label for="titular_tarjeta">Nombres Titular Tarjeta:</label>
                                               <input type="text" id="titular_tarjeta" name="titular_tarjeta" value="" class="form-control" />
                                    
											</div>
										</div>
										<div class="col col_50">
											<div class="form-group">
												<label for="apellido_deb">Apellidos Titular Tarjeta:</label>
												<input type="text" class="form-control" id="apellido_cd"  name="apellido_cd"  />
											</div>
										</div>
									</div>

									<div class="row cab_r rw">
										<div class="col col_50">
											<div class="form-group">
												<label for="titular_tarjeta">Identificaci&oacute;n:</label>
                                               <input type="text" id="id_cd" name="id_cd" value="" class="form-control" />
                                    
											</div>
										</div>
										<div class="col col_50">
											<div class="form-group">
												<label for="apellido_deb">Telefono:</label>
												<input type="text" class="form-control" id="phone_cd"  name="phone_cd"  />
											</div>
										</div>
									</div>																


								</div>
							</form>
			
			
			</div>
      </div>



      <!-- Modal footer -->
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-danger button_module_wh" data-dismiss="modal">Close</button>-->
		<button type="button" id="btnCerrarDeb" class="btn btn-danger button_module_wh" data-dismiss="modal">Cerrar</button>
&nbsp;
	    <button name="btnGuardarCd" id="btnGuardarCd"   class="btn btn-default">Guardar</button>		
      </div>

    </div>
  </div>
</div>	


<!-- The Modal -->
<div class="modal" id="myModalConfirm">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">



      <!-- Modal body -->
      <div class="modal-body" id="dv_confirm_additional">
			<p>Ya existe una asistencia asociada al producto seleccionado, desea ingresar otra?</p>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger button_module_wh" data-dismiss="modal">Cancelar</button>
&nbsp;
	    <button name="btnGuardarAnul" id="btnGuardarAnul" onclick="saveAdditional()"  class="btn btn-default">Guardar</button>		
      </div>

    </div>
  </div>
</div>	
</div>
<script type="text/javascript">
       $(function(){
		  $('.expiration-date').mask("00/00", {placeholder: "MM/YY"});
          $('#alert_success_cc').hide();
          $('#alert_error_cc').hide();		  
		  
          $('#dv_alert_warn').hide();
		  
		  $('#sel_tpr').on('change',function(){
			 loadListChild('sel_tpr','sel_tctg','tipo_cat_pr');
			 $('#sel_tctg').trigger('change'); 
		  });
		  
		  
		  $('#sel_tctg').on('change',function(){
			  
			 loadListChild('sel_tctg','sel_car','cartera_tp');
			  
		  });		  
		  
		  $('#sel_car').on('change',function(){
			  
			 loadListChild('sel_car','sel_categmine','categoria_ctp');
			  
		  });		
		  
			$("#operador_tarjeta").on('change',  function() {
    		   var operador_tarjeta = $("#operador_tarjeta option:selected").val();

			   
                $.ajax({
                    type:'GET',
                    url:'/sales/payment/cmbCreditCard/'+operador_tarjeta,
                    success:function(data){
                        console.log(data);
                        if(data.success==1){
                            var cmbmp  = data.json_cd;
							var len = cmbmp.length;

							$("#tipo_tarjeta").empty();
							for( var i = 0; i<len; i++){
								var id = cmbmp[i]['cca_code'];
								var name = cmbmp[i]['cca_name'];

								$("#tipo_tarjeta").append("<option value='"+id+"'>"+name+"</option>");
							}						
                            return;
                        }else{
                            
                            return;
                        }
                    }
                });  			   
    		});					  
		  
			$( "#frmCreditC" ).validate({
			  rules: {
				numero_tarjeta: {
				  required: true,
				  creditcard: true
				}
			  },
			messages: {
				numero_tarjeta: {
					required: 'Se requiere este campo.',
					creditcard: 'Numero de tarjeta no valido'
				}		
			}
			});

			$("#btnGuardarCd").click(function( event ) {

				event.preventDefault();


						if(!$( "#frmCreditC" ).valid()){
							
							return;
						}
					    var form = $('#frmCreditC,#frmMT');
						var pay_day  = $("#period_pago option:selected").val();
						var request = $.ajax({
							type:	'POST',
							url:	'/sales/payment/save_credir_card_form_fp/'+pay_day,
							data:	form.serialize(),
							success:function(data){
								console.log(data);
								if(data.success==1){
									$("#btnGuardarDeb").prop('disabled',true);
									$("#btnCerrarDeb").prop('disabled',true);									
									$('#alert_success_cc').html(data.message);
									$('#alert_success_cc').show();
									
									/*setInterval(function(){ 
										
										$('#myModalDebit').fadeIn("slow");
										

									}, 2000);*/
									$('#myModalCreditC').modal('hide');
									$('#seccion_detalle_planes').load(data.url_redirect);	
									
									        			
									return;
								}else if(data.success==2){
									$('#alert_error_cc').empty();
									$.each( data.errors, function(i, obj) {
										$('#alert_error_cc').append(obj+'<br>');
										
									});
									
									$('#alert_error_cc').show();
									/*alertPPYA(data.message);*/
									console.log(data);
									return;
								}
							}
						});
						request.fail(function( jqXHR, textStatus ) {
							alertPPYA( "Hubo un error: " + textStatus );
							console.log(jqXHR);
						});
								
				
			});


       	  $('#sel_categmine').on('change', function() {
				Pace.restart();
				
				var sel_id     = $( "#sel_categmine option:selected" ).val();
				var $child_sel = $('#sel_pr');
				$.ajax({
					type: 'GET',
					url:'/insurance/cmbProduct/'+sel_id+'/',
					success:function(data){
						if(data.success==1){

							$child_sel.find('option').remove();         
							$.each( data.json_product, function(i, obj) {
								if(i==0){
									$child_sel.append('<option value="0" selected>----</option>');
									$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
								}else{
									$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
								}
							});
								
							
							return;
						}else{
							
							alertPPYA(data.message);
							return;
						}
					}
				});	
             
		  });		  
		  
       	  $('#sel_pr').on('change', function() {
       	  	 $("#id_producto").val(this.value);
       	  	 var id_producto  = this.value;


				$.ajax({
						type:	'GET',
						url:	'/insurance/get_data_product/'+id_producto+'/',
						success:function(data){
							console.log(data);
							if(data.success==1){

								var valor_pr  = parseFloat(data.arr_product.precio_unit)*parseFloat(data.arr_product.npc);
								$("#ad_valor").val(valor_pr.toFixed(2));
								return;
							}else if(data.success==2){
								$('#dv_alert_warn').show();
								$('#dv_alert_warn').addClass('in');
								$('#dv_alert_warn').addClass('show');	
								var valor_pr  = parseFloat(data.arr_product.precio_unit)*parseFloat(data.arr_product.npc);
								$("#ad_valor").val(valor_pr.toFixed(2));								
								/*$('#dialog-confirm').dialog('open');*/
								return;
							}
						}
				}); 
             
		  });
		  
		  
		  
		  
		  
       });



		function loadListChild(parent_name,child_name,child_pref){
			Pace.restart();
			
			var sel_id = $( "#"+parent_name+" option:selected" ).val();
			var $child_sel = $('#'+child_name);
			$.ajax({
				type: 'GET',
				url:'/general/getListChild/'+child_pref+'/'+sel_id+'/',
				success:function(data){
					if(data.success==1){

					    if(child_name=='sel_car'){
							$child_sel.find('option').remove();         
							$.each( data.modelData, function(i, obj) {
								  if(i==0){
									  $child_sel.append('<option value="0" selected>----</option>');
								  }
								if(obj.por_additional == 1){
									if(i==0){
										
										$child_sel.append('<option value=' + obj.por_id + ' >' + obj.por_name + '</option>');
									}else{
										$child_sel.append('<option value=' + obj.por_id + ' >' + obj.por_name + '</option>');
									}
								}

							});
						}else{
								$child_sel.find('option').remove();         
								$.each( data.arr_dataw2ui, function(i, obj) {
									if(i==0){
										$child_sel.append('<option value="0" selected>----</option>');
										$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
									}else{
										$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
									}
								});
							
						}
						

							
						
						return;
					}else{
						
						alertPPYA(data.message);
						return;
					}
				}
			});
		}

</script>
							