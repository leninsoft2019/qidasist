

<!-- The Modal -->
<div class="modal" id="modAlert" role="alert">
<style scoped>
.cs_left_grey{
	background-color:#f2f2f2;
	vertical-align:middle;
	text-align:center;
	width:20%;
	height:100%;
	border-top-left-radius:5px;
	border-bottom-left-radius:5px;
	padding:10px;
}
.cs_table_r{
	display:table;
	width:100%;
	height:100%;
	
}
.cs_row{
	display:table-row;
}

.cs_cell{
	display:table-cell;
}
.modal-body {
    padding: 0px;
}
.cs_close_x{
	position:absolute;
	top:0px;
	float:right;
	right:0px;
	padding:10px;
}
.cs_right_alert{
	padding:20px;
}

.modal-alert-ppya {
	max-width: 550px;
	margin: 1.75rem auto;
	z-index:6000;
 }

#alertModalMain{
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
}	

#content_alert_md{
	color:black;
	font-weight:bold;
	font-size:18px;
}

.cs_text_hidden{
	font-size:0px;
	text-indent:-9999px;
	position:absolute;
}
</style>
  <div class="modal-dialog  modal-dialog-centered modal-alert-ppya" id="alertModalMain">
    <div class="modal-content">

		  <!-- Modal body -->
		  <div class="modal-body">
		  
				  <div class="cs_table_r">
					<div class="cs_row">
					  <div class="cs_cell cs_left_grey">
						<img src="/img/ico_alert_red.svg"	 width="60px" height="60px"  alt="icono de alerta" />
					  </div>
					  <div class="cs_cell cs_right_alert">
					    <a href="#" data-dismiss="modal" class="cs_close_x"><img src="/img/ico_close_orange.svg" height="20px" width="20px" alt="close button" alt="icono de cierre" /><span class="cs_text_hidden">Alerta</span></a>
						
						<div id="content_alert_md" aria-live="polite">
							Debe seleccionar un registro
						</div>
						<br/>
						<button type="button" class="btn btn-danger button_module_wh" data-dismiss="modal" id="btnCloseAlertPPYA">Cerrar</button>
					  </div>
					</div>  
				  </div>		  
		  
		  </div>


    </div>
  </div>
</div>	