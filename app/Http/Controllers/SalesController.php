<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\GenFunction;
use App\webProduct;
use App\WebRanges;
use App\WebPerson;
use App\WebSale;
use App\WebSalesDependent;
use App\GenFunctionP2P;
use App\BtnP2pParam;
use App\BtnP2pTran;
use App\SalContract;
use App\SalSale;
use App\SalMethodPay;
use App\SalMethodPaySubscription;
use App\CliClient;
use App\SalSxk;
use App\SalBilling;
use Auth;
use App\GenApiMaster;
use App\BtnP2pRecurrent;
use Mail;

use App\GenModuleWeb;


class SalesController extends Controller{
		
	/**
	 * Show the intro page of the module
	 *
	 * @param  Request  $request
	 * @return \Illuminate\Http\Response
	*/
    public function showHome(Request $request,$api_code,$mod_code){
		$lang_id= $request->session()->get('lang_id');
		$request->session()->put('api_code',$api_code);
		$request->session()->put('mod_code',$mod_code);
		$modules = GenModuleDetail::where('lang_code','=',$lang_id)->get();

        $request->session()->forget('frm_labels');
        $this->createLabelsModule($mod_code, $lang_id);
        $objForm = GenFunction::getLabels($lang_id, $mod_code);

		$request->session()->put('frm_labels',$objForm);
		$request->session()->forget('arr_insurance_tmp');
		$request->session()->forget('arr_tmp_titular');
		$request->session()->forget('arr_tmp_dependiente');
		$request->session()->forget('arr_tmp_titular_find');
		$request->session()->forget('action_ins');
		$request->session()->forget('action');
		$request->session()->forget('lock_edit');
		$request->session()->forget('objGenPrAge');

        $request->session()->forget('edit_only_tit');
        $request->session()->forget('html_var_search');
        $request->session()->forget('tarjeta_virtual');
        $request->session()->forget('string_attach');
        $request->session()->forget('string_cards');  
        $request->session()->forget('cmbMethodPay');  		


		$DTables = GenLanguage::where('lang_code', $lang_id)
								->pluck('lang_DataTables')
								->first();   
		$request->session()->put('DTables',$DTables);     

		return view('insurance.home')
				->with('modules',$modules)
				->with('DTables',$DTables)
				->with('objform',$objForm)
				->with('api_code',$api_code);
    } // Fin showHome

    public function showListProductsSeller(Request $request){
		$request->session()->forget('sale_id');
		$request->session()->forget('id_sale_fn');
		
		$arrWebProduct = webProduct::where('pro_status', 1)->get();
				
		if(\Auth::user()){
			
			$procode = \Auth::user()->pro_code;
			
			if($procode == 5){
				return redirect('/homeClient');
			}else{
				return view('home')->with('arrWebProduct', $arrWebProduct);
			}
		}else{
			return redirect('/');
		}					
	}

    public function showListProducts(){
		$arrWebProduct = webProduct::where('pro_status', 1)->get();
		return view('sales.home_product_list')
				->with('arrWebProduct', $arrWebProduct);
	}

	/**
	* Quote the selected product.
	*
	* @return \Illuminate\Http\Response
	*/
    public function detailWebProduct(Request $request, $idProduct){
		//
		$arrWebRanges  = WebRanges::where('ran_status', 1)->get();
		$arrWebProduct = webProduct::find($idProduct);
		$precio = $arrWebProduct->pro_valor;
		$request->session()->put('id_product',$idProduct);


		return view('sales.quotation.quotation')
				   ->with('arrWebRanges', $arrWebRanges)
				   ->with('idProduct', $idProduct)
				   ->with('arrWebProduct', $arrWebProduct)
				   ->with('precio', $precio);
    } // detailWebProduct

	/**
	* Quote the selected product.
	*
	* @return \Illuminate\Http\Response
	*/
    public function detailWebProductTit(Request $request, $idProduct){
        //
							$request->session()->forget('requestId');
							$request->session()->forget('first_pay');
		
                               $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
							   $arrWebProduct = webProduct::find($idProduct);
                               $precio = $arrWebProduct->pro_price;
							   $request->session()->put('id_product',$idProduct);
                               
                               return view('sales.quotation.titular_data')
                                                               ->with('arrWebRanges', $arrWebRanges)
                                                               ->with('idProduct', $idProduct)
															   ->with('arrWebProduct', $arrWebProduct)
                                                               ->with('precio', $precio);
    } // detailWebProduct
	
	public function getDataClient(Request $request){
	   
	   try{
		   $data_person = array(); 
		   if($request->session()->has('objSales')){
			   
			   $objCli = $request->session()->get('objCli');
			   
			   //$cli_id    = $objSales->id_person;
			   $objPerson = $objCli;//CliClient::find($cli_id);
			   if(empty($objPerson)){
				 return response()->json(['success'=>0,'message'=>'No existen datos de cliente '.$cli_id]);  
			   }

			   $data_person['nombre']    = $objPerson->cli_name;
			   $data_person['apellido']  = $objPerson->cli_surname;
			   $data_person['id']        = $objPerson->cli_id;
			   $data_person['direccion'] = '';
			   $data_person['email'] = $objPerson->cli_email;
			   $data_person['phone'] = '';
		   			   
			   
		   }else{
			   $cli_id    = $request->session()->get('cli_id');
			   $objPerson =  WebPerson::find($cli_id);
			   if(empty($objPerson)){
				 return response()->json(['success'=>0,'message'=>'No existen datos de cliente '.$cli_id]);  
			   }

			   $data_person['nombre']    = $objPerson->per_name;
			   $data_person['apellido']  = $objPerson->per_surname;
			   $data_person['id']        = $objPerson->per_id;
			   $data_person['direccion'] = $objPerson->per_address;
			   $data_person['email'] = $objPerson->per_email;
			   $data_person['phone'] = $objPerson->per_phone;
		   			   
			   
		   }

		   
		   $status = 1;
			$mensaje='OK';
			$response = array(
				'success' 		 => $status,
				'message' 		 => $mensaje,
				'data_person' 	 => $data_person
			);
			return response()->json($response);		   
		   
	   }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
	}
   }

	public function titSave(Request $request){
	   
	   try{
			$validator = \Validator::make($request->all(), [
				'nombre' => 'required',
				'apellido' => 'required',
				'id' => 'required'				
			],['nombre.required' => 'Es necesario ingresar el nombre',
				'apellido.required' => 'Es necesario ingresar el apellido',
				'id.required' => 'Es necesario ingresar la identificacion']);

			//['sc_razon_social.required' => ':attribute  es requerido.']	        
			if ($validator->fails())
			{
				return response()->json(['success'=>2,'errors'=>$validator->errors()->all()]);
			}	
		  
		    $idProduct = $request->session()->get('id_product');
		    $objPerson =  WebPerson::where('per_id',$request->id)->first();
			if(empty($objPerson)){
				$objPerson = new WebPerson;
			}

			/**********File Pic Tit*********************/
		   $file_n= '';
		   if(\Auth::user()){
			   
			   if(!$request->session()->has('sale_id')){
				  
					if(!$request->has('fl_ident')){
							
							$validator = \Validator::make($request->all(), [
								'fl_ident' => 'required|file|max:1024'			
							],['fl_ident.required' => 'Es necesario cargar la foto de identificacion']);

							//['sc_razon_social.required' => ':attribute  es requerido.']	        
							if ($validator->fails())
							{
								return response()->json(['success'=>2,'errors'=>$validator->errors()->all()]);
							}					   
					   
				   }
			       
			       $file_or  = $request->fl_ident->getClientOriginalName();
				   $arr_name = explode('.', $file_or);    
				   $file_n   = $request->id.'_'.$file_or;
				   $request->fl_ident->storeAs('sales_ident',$file_n); 

						/////////////
			   }//end  if(!$request->
			   else{
				   if($request->has('fl_ident')){
					   $file_or  = $request->fl_ident->getClientOriginalName();
					   $arr_name = explode('.', $file_or);    
					   $file_n   = $request->id.'_'.$file_or;
					   $request->fl_ident->storeAs('sales_ident',$file_n); 
				   }else{
					    $file_n   = $objPerson->per_pic;
				   }
			   }
			       
		   }

           /************************************/					
           	
			
			$objPerson->per_id      = $request->id;
			$objPerson->per_name    = $request->nombre;
			$objPerson->per_surname = $request->apellido;
			$objPerson->per_email   = $request->email;
			$objPerson->per_address = $request->per_address;
			$objPerson->per_phone   = $request->per_phone;
			$objPerson->per_pic     = $file_n;
			
			$objPerson->per_fbirthday   = $request->fbirthday;
			$objPerson->per_age   = $request->edad;
			
			$objPerson->per_type    = 1;
			$objPerson->per_status  = 1;
		    $objPerson->save();
		
			
			if(empty($objPerson)){
				return response()->json(['success'=>0,'message'=>'Hubo un error al guardar la informacion del titular']);
			}
			$request->session()->put('per_id',$request->id);
			$request->session()->put('cli_id',$objPerson->Id);
			
		   
			
			if($request->session()->has('sale_id')){
				$sale_id =  $request->session()->get('sale_id');
				$objSale =  WebSale::find($sale_id);
			}else{
				$objSale =  new WebSale;	
			}
			
			$objSale->id_product  = $idProduct;
			$objSale->id_person   = $objPerson->Id;
			$objSale->sal_date    = now();
			$objSale->sal_token   = GenFunction::generarToken(1,16, true);
			$objSale->sal_reference   ='';
			$objSale->agency_code   =1;
			$objSale->seller_code   =1;
			$objSale->sal_periodo   ='1,12,4';
			$objSale->sal_status   =1;
			$objSale->save();
			
			$request->session()->put('sale_id',$objSale->id);
			
			$status = 1;
			$mensaje='OK';
			$response = array(
				'success' 		 => $status,
				'message' 		 => $mensaje
			);
			return response()->json($response);		   
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}
   }
   
	public function multSave_old(Request $request){
	   
	   try{
		    
		    $idProduct = $request->session()->get('id_product');
		    $objPerson =  WebPerson::where('per_id',$request->id)->first();
			if(empty($objPerson)){
				$objPerson = new WebPerson;
			}
			
			$objPerson->per_id      = $request->id;
			$objPerson->per_name    = $request->nombre;
			$objPerson->per_surname = $request->apellido;
			$objPerson->per_email   = $request->email;
			$objPerson->per_address = $request->per_address;
			$objPerson->per_phone   = $request->per_phone;
			$objPerson->per_type    = 1;
			$objPerson->per_status  = 1;
		    $objPerson->save();
			
			if(empty($objPerson)){
				return response()->json(['success'=>0,'message'=>'Hubo un error al guardar la informacion del titular']);
			}
			$request->session()->put('per_id',$request->id);
			
			
			$objSale =  new WebSale;
			$objSale->id_product  = $idProduct;
			$objSale->id_person   = $objPerson->Id;
			$objSale->sal_date    = now();
			$objSale->sal_token   = $idProduct.'_'.now();
			$objSale->sal_reference   ='';
			$objSale->agency_code  =1;
			$objSale->seller_code  =1;
			$objSale->sal_periodo  ='1,4,12';
			$objSale->sal_status   =1;
			$objSale->save();
			
			$request->session()->put('sale_id',$objSale->id);
			
			
			///dependents
			$arrWebRanges  = WebRanges::where('ran_status', 1)->get();
			for($i=0;$i<count($arrWebRanges);$i++){
				$id_range =  $arrWebRanges[$i]->Id;
				$total    =  $request->input('quantity'.$i);
				for($j=0;$j<$total;$j++){
						$objPersonDep = new WebPerson;
						
						$objPersonDep->per_id      = '';
						$objPersonDep->per_name    = '';
						$objPersonDep->per_surname = '';
						$objPersonDep->per_email   = '';
						$objPersonDep->per_address = '';
						$objPersonDep->per_phone   = '';
						$objPersonDep->per_type    = 2;
						$objPersonDep->per_status  = 1;
						$objPersonDep->save();	

					    $objWSD = new WebSalesDependent;
						$objWSD->id_person = $objPersonDep->Id;
						$objWSD->id_sales  = $objSale->id;
						$objWSD->sxd_status  = 1;
						$objWSD->id_range    = $id_range;
						$objWSD->save();
						 
				}
			}
			
			
			$status = 1;
			$mensaje='OK';
			$response = array(
				'success' 		 => $status,
				'message' 		 => $mensaje
			);
			return response()->json($response);		   
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}
   }   
   
	public function placeToPayVal(Request $request){
	   
 	   try{
		    $idProduct =  $request->session()->get('id_product');
		    $objWebPr  = webProduct::find($idProduct);		    
		    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
		    $arrWebProduct = webProduct::find($idProduct);
		    $precio        = $objWebPr->pro_valor;
		    $tax_val       = 0;
			
			$id_sale  = $request->session()->get('sale_id');
			$objWSD   = WebSalesDependent::where('id_sales',$id_sale)->get();
			
			$data_login = GenFunctionP2P::createTranskeyP2P();

			$var_endpoint_return = BtnP2pParam::find(5);
			$var_return = $var_endpoint_return->p2p_value;
			$reference  =  GenFunctionP2P::generarCodigos(1,16,true);
			
			$document_type= 'CI';
			
			switch ($request->tipo_id) {
				case 1:
				    $document_type= 'CI';
					if(!GenFunction::validarCedula($request->id)){
						return response()->json(['success'=>0,'message'=>'Formato de identificacion incorrecto']);
					}					
					break;
				case 2:
					$document_type= 'PPN';
					break;
				case 9:
					$document_type= 'RUC';
					if(!GenFunction::validarRucPersonaNatural($request->id)){
						return response()->json(['success'=>0,'message'=>'Formato de identificacion incorrecto']);
					}						
					break;
				default:
					$document_type= 'CI';	
					break;
			}
			
        $countryObj    = DB::SELECT('select * from gen_location_country where glc_code = 63');

			

			//return response()->json(['success'=>0,'message'=>$idProduct]);
		if(!$request->session()->has('idCol')){	
			$request_arr = [
                          "auth" => [
                              "login"   => $data_login['login'],
                              "seed"    => $data_login['seed'],
                              "nonce"   => $data_login['nonce'],
                              "tranKey" =>  $data_login['tranKey']

                          ],
                          "locale" => 'es_EC',
                           "buyer"=>[
                            "document" => $request->id, 
                            "documentType" =>$document_type, //"CI", //CI cedula, RUC, PPN -> Pasaporte
                            "name" => preg_replace('/[0-9]+/', '', $request->nombre),
                            "surname" => preg_replace('/[0-9]+/', '', $request->apellido),
                            "email" => ( !is_null( $request->email ) ) ? $request->email :'adominguez@memorialtechnologies.com',
                            "mobile" =>$request->phone,
                            "address" =>  [
                              "street" => $request->direccion, 
                              "city" =>  ( is_object($request->ciudad) ) ? $request->ciudad: 'Quito',
                              "country" => "EC"
                            ]
                          ],
						 "subscription"=> [
						        "reference"=>$reference,
						        "description"=>"Supscricion Cliente ".$request->id
						    ],  
                         "skipResult" => true,							
                        /* "payment" => [
                              "reference" =>$reference , //ID generado por el comercio
                              "description" => "Memorial Ecuador",
                              "amount" => [
                                  "currency" => "USD",
                                  "total" => (float)$precio,
                                  "taxes" => [
                                           "kind"=> "iva",
                                           "amount"=> (float)$precio - ( (float)$precio / $tax_val) ,
                                           "base"=> (float)$precio /$tax_val,
                                   ],
                                  "details" => [
                                       [
                                           "kind" => "subtotal",
                                           "amount" => $precio

                                       ]
                                   ]
                              ],
                              "allowPartial" => false,
                          ],*/
                          "expiration" => date('c', strtotime('+30 minutes')), // tiempo para pago antes de caducar sesión
                          "returnUrl" => $var_return,
                          "ipAddress" => GenFunction::getRealIpAddr(),//;"173.230.130.8",
                          "userAgent" => $_SERVER['HTTP_USER_AGENT']//'WEB',
                      ];

	     } else{
				
				$idProduct = $request->session()->get('id_product');	
				$arrWebProduct = webProduct::find($idProduct);
				$aplica_tax    = $arrWebProduct->pro_tax;
			    
				if($request->session()->has('val_sel_col')){
					$precio = $request->session()->get('val_sel_col');
				}
				$base_tx   = $precio;
				$amount_tx = 0;
				if($aplica_tax==1){
					$tax_val =1+$countryObj[0]->glc_percentagetax/100;
					
					$base_tx   = $precio/$tax_val;
					$amount_tx = $precio - $base_tx;
					
				}
								
				
				$idCol    = $request->session()->get('idCol');
				$request_arr = [
                          "auth" => [
                              "login"   => $data_login['login'],
                              "seed"    => $data_login['seed'],
                              "nonce"   => $data_login['nonce'],
                              "tranKey" =>  $data_login['tranKey']

                          ],
                          "locale" => 'es_EC',
                           "payer"=>[
                            "document" => $request->id, 
                            "documentType" =>$document_type, //"CI", //CI cedula, RUC, PPN -> Pasaporte
                            "name" => preg_replace('/[0-9]+/', '', $request->nombre),
                            "surname" => preg_replace('/[0-9]+/', '', $request->apellido),
                            "email" => ( !is_null( $request->email ) ) ? $request->email :'adominguez@memorialtechnologies.com',
                            "mobile" =>$request->phone,
                            "address" =>  [
                              "street" => $request->direccion, 
                              "city" =>  ( is_object($request->ciudad) ) ? $request->ciudad: 'Quito',
                              "country" => "EC"
                            ]
                          ],                  
                         "payment" => [
                              "reference" =>$reference , //ID generado por el comercio
                              "description" => "Memorial Ecuador",
                              "amount" => [
                                  "currency" => "USD",
                                  "total" => number_format((float)$precio,2),
                                 "taxes" => [
								        [
                                           "kind"=> "valueAddedTax",//"iva",
                                           "amount"=> number_format((float)$amount_tx,2),
                                           "base"=> number_format((float)$base_tx,2)  ,//*$tax_val,
                                        ]
								  
								   ],
                                  "details" => [
                                       [
                                           "kind" => "subtotal",
                                           "amount" => number_format((float)$precio,2)

                                       ]
                                   ]
                              ]
							 /* , "allowPartial" => true,*/
                          ],
                          "expiration" => date('c', strtotime('+30 minutes')), // tiempo para pago antes de caducar sesión
                          "returnUrl" => $var_return,
                          "ipAddress" => GenFunction::getRealIpAddr(),//;"173.230.130.8",
                          "userAgent" => $_SERVER['HTTP_USER_AGENT']//'WEB',
                      ];
			}//if($request->session()->has('idCol')
			Storage::prepend('log_p2p_col.log',"****************");
			Storage::prepend('log_p2p_col.log',"API=".json_encode($request_arr));              
			Storage::prepend('log_p2p_col.log','***************');   


           //return response()->json(['success'=>0,'message'=>$request_arr]);
            $url_process = GenFunctionP2P::createSessionP2P($request_arr,$request);
			
            if($url_process['status']['status']=='OK'){


				/*
					$transaction = new BtnP2pTran;
					$transaction->bpt_status = "SOLICITADO";
					$transaction->bpt_reason = $url_process['status']['reason'];
					$transaction->bpt_message = $url_process['status']['message'];
					$transaction->bpt_date = $url_process['status']['date'];
					$transaction->bpt_requestId = $url_process['requestId'];
					$transaction->bpt_session =  $reference;
					$transaction->bpt_processUrl = $url_process['processUrl'];
				    $transaction->bpt_name    = $request_arr['buyer']['name'];
					$transaction->bpt_surname = $request_arr['buyer']['surname'];
					$transaction->bpt_numdoc  = $request->id;
					$transaction->bpt_email = $request->email;
					$transaction->bpt_phone = '';
					$transaction->bpt_product_id = $objWebPr->pro_code;
					$transaction->bpt_product_name = $objWebPr->pro_name;
					$transaction->bpt_quantity = 1;
					$transaction->bpt_price = $objWebPr->pro_valor;
					$transaction->bpt_estado = 1;
					$transaction->save();
					// Hasta aqui el almacenamiento de la transacion

				$request->session()->put('requestId',$url_process['requestId']);
				$status = 1;
				$mensaje='OK';
				$response = array(
					'success' 		 => $status,
					'message' 		 => $mensaje,
					'url_process'    => $url_process
				);
				return response()->json($response);*/	
				
				$request->session()->put('requestId',$url_process['requestId']);
				$status = 1;
				$mensaje='OK';
				$response = array(
					'success' 		 => $status,
					'message' 		 => $mensaje,
					'reference' 		 => $reference,
					'url_process'    => $url_process
				);
				return response()->json($response);
				//return $this->collectP2P($request);

            }else{
            	return response()->json(['success'=>0,'message'=>'Hubo un problema al conectarse con Place to Pay','p2p_msg'=>$url_process,'reference'=> $reference]);
            }
	   
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}
                               	  
   }
   
   public function savePayCol(Request $request){
	   try{
				
			 $requestId = $request->session()->get('requestId');
		    $idProduct =  $request->session()->get('id_product');
		    $objWebPr  = webProduct::find($idProduct);		    
		    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
		    $arrWebProduct = webProduct::find($idProduct);
		    $precio        = $objWebPr->pro_valor;
			if($request->session()->has('val_sel_col')){
				$precio = $request->session()->get('val_sel_col');
			}			
			$countryObj    = DB::SELECT('select * from gen_location_country where glc_code = 63');
			
			$idProduct = $request->session()->get('id_product');	
			$arrWebProduct = webProduct::find($idProduct);

			$id_sale  = $request->session()->get('sale_id');
			$objWSD   = WebSalesDependent::where('id_sales',$id_sale)->get();

			$data_login = GenFunctionP2P::createTranskeyP2P();

			$var_endpoint_return = BtnP2pParam::find(5);
			$var_return =  $var_endpoint_return->p2p_value;
			$reference  =  GenFunctionP2P::generarCodigos(1,16,true);
	

            /*$url_process = GenFunctionP2P::createSessionP2PCollect($request_arr,$request);
			
			*/
			// Credenciales
		    $var_endpoint_pruebas = BtnP2pParam::find(1);			
			$endPoint =	$var_endpoint_pruebas->p2p_value;
			$curl = curl_init();
			curl_setopt_array($curl, array(
				CURLOPT_URL => $endPoint.'api/session/'.$requestId,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_HTTPHEADER => array(
					"Cache-Control: no-cache",
					"Content-Type:  application/json",
					"Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
				),
			));

			/*$response = curl_exec($curl);
			$err      = curl_error($curl);
			curl_close($curl);*/
		    $data_login = GenFunctionP2P::createTranskeyP2P();
			$data = [
                          "auth" => [
                              "login"   => $data_login['login'],
                              "seed"    => $data_login['seed'],
                              "nonce"   => $data_login['nonce'],
                              "tranKey" =>  $data_login['tranKey']

                          ],
						  "payment"=>[],
						  "payer"=>[]
				     ];
			//$data = $arrJson;
			$data = json_encode($data);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			$response = curl_exec($curl);
			$err      = curl_error($curl);
			curl_close($curl);			

			if ($err) {
				$fecha = GenFunction::getDateDB();
			} else {
				$request_arr=$url_process = json_decode($response, true);

				    // return response()->json(['success'=>0,'message'=>'ERRO','urlobj'=>$url_process]);
					if($url_process['status']['status']=='APPROVED' || $url_process['status']['status']=='PENDING'){

							 $estado = 'APROBADO';
							 if($url_process['status']['status']=='PENDING'){
								$estado = 'PENDIENTE'; 
							 }else{
								$estado = 'RECHAZADA'; 
							 }
							
							$transaction = new BtnP2pTran;
							$transaction->bpt_status = $estado;
							$transaction->bpt_reason = $url_process['status']['reason'];
							$transaction->bpt_message = $url_process['status']['message'];
							$transaction->bpt_date = $url_process['status']['date'];
							$transaction->bpt_requestId = $url_process['requestId'];
							$transaction->bpt_session =  $reference;
							$transaction->bpt_internal_reference =  $url_process['payment'][0]['internalReference'];
							$transaction->bpt_processUrl = 'api/collect';//$url_process['processUrl'];
							$transaction->bpt_name    = $request_arr['request']['payer']['name'];
							$transaction->bpt_surname = $request_arr['request']['payer']['surname'];
							$transaction->bpt_numdoc  = $request->id;
							$transaction->bpt_email = $request->email;
							$transaction->bpt_phone = '';
							$transaction->bpt_product_id = $objWebPr->pro_code;
							$transaction->bpt_product_name = $objWebPr->pro_name;
							$transaction->bpt_quantity = 1;
							$transaction->bpt_price = $objWebPr->pro_valor;
							$transaction->bpt_estado = 1;
							
							$transaction->save();
							// Hasta aqui el almacenamiento de la transacion

						$request->session()->put('requestId_collect',$url_process['requestId']);
						$request->session()->put('first_pay',1);
						
						$val_p = $url_process['payment'][0]['amount']['to']['total'];
						try{
							DB::beginTransaction();
							$var = $this->saveDataSale($request,$reference,$val_p);
							DB::commit();
							$sal_id = $request->session()->get('id_sale_fn');
							$objMethodPay  = SalMethodPay::where('smp_status',1)->where('sal_id',$sal_id)->first();
							$objMethodPay->smp_ban_name      = $url_process['payment'][0]['issuerName'];
							$objMethodPay->smp_cd_type       = $url_process['payment'][0]['paymentMethodName'];
							$objMethodPay->smp_authorization = $url_process['payment'][0]['authorization'];
							$objMethodPay->smp_receipt       = $url_process['payment'][0]['receipt'];	
						
							$objMethodPay->save();
							
							
							$this->generateCXC($request);
							$transaction->sal_id = $sal_id;
							$transaction->save();
						}catch(\Exception $e){
							  DB::rollback();
							  $msg  = 'Su pago con referencia '.$reference.' ha sido aprobada';
							  if($url_process['status']['status']=='PENDING'){
								   $msg  = 'Su pago con referencia '.$reference.' esta pendiente';
							  }					  
							  return response()->json(['success'=>0,'error'=>$e->getMessage(),'message'=>$msg.'  sin embargo hubo un problema al generar la asistencia']);
				
						}

						$status = 1;
						$mensaje='Su pago con referencia '.$url_process['payment'][0]['reference'].' ha sido Aprobado. Gracias por su compra';
						$url_redirect='/sales/columbarium';//'/quote/detail_tit/6/';
						
						
						if($url_process['status']['status']=='PENDING'){
							$status = 1;
							$mensaje='Su transaccion con referencia '.$url_process['payment'][0]['reference'].' esta en estado Pendiente, recuerde que su cobertura no entrara en vigencia hasta que se efectivice el pago';
						}
						
						$response = array(
							'success' 		 => $status,
							'message' 		 => $mensaje,
							'p2p_status' 	 => $url_process['status']['status'],
							'url_process'    => $url_process,
							'url_redirect'   => $url_redirect
						);
						return response()->json($response);	

					}else{
						
							 $estado = 'APROBADO';
							 if($url_process['status']['status']=='PENDING'){
								$estado = 'PENDIENTE'; 
							 }else{
								$estado = 'RECHAZADA'; 
							 }
							$transaction = new BtnP2pTran;
							$transaction->bpt_status = $estado;//$url_process['status']['status'];
							$transaction->bpt_reason = $url_process['status']['reason'];
							$transaction->bpt_message = $url_process['status']['message'];
							$transaction->bpt_date = $url_process['status']['date'];
							$transaction->bpt_requestId = $url_process['requestId'];
							$transaction->bpt_session =  $reference;
							$transaction->bpt_internal_reference =  $url_process['payment'][0]['internalReference'];
							$transaction->bpt_processUrl = 'api/collect';//$url_process['processUrl'];
							$transaction->bpt_name    = $request_arr['payer']['name'];
							$transaction->bpt_surname = $request_arr['payer']['surname'];
							$transaction->bpt_numdoc  = $request->id;
							$transaction->bpt_email = $request->email;
							$transaction->bpt_phone = '';
							$transaction->bpt_product_id = $objWebPr->pro_code;
							$transaction->bpt_product_name = $objWebPr->pro_name;
							$transaction->bpt_quantity = 1;
							$transaction->bpt_price = $objWebPr->pro_valor;
							$transaction->bpt_estado = 1;
							$transaction->save();				
						return response()->json(['success'=>0,'message'=>'Su petición con referencia '.$url_process['payment'][0]['reference'].' ha sido Rechazada','p2p_msg'=>$url_process,'request_arr'=>$request_arr,
												  'json'=>json_encode($request_arr,true)]);										  
					}
				
			} // Fin err			
			
            
	   
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}
   }
   
   public function getTokenP2P($request,$requestId){
	   
         if($request->session()->has('requestId')){
		    $requestId = $request->session()->get('requestId');
		 }else{
			$requestId = $requestId;
		 }
		 
   		 $resArr = GenFunctionP2P::responseSessionP2P($requestId,$request);
   		 $msg = 'NO SE PUDO OBTENER INFORMACION DE PLACE TO PAY';
   		 //$resArr['status']['status']=='OK'
		 
		 $status = 0;
		 $token  = '';
   		 if(!empty($resArr['status']['status']))
   		 {
   		 	$status  = $resArr['status']['status'];

   		 	switch ($status) {
   		 		case 'APPROVED':
					 $status=1;
   		 			 $msg = 'TRANSACCION APROBADA';
					
					 foreach($resArr['subscription']['instrument'] as $rr){
						 
						 if($rr['keyword']=='token'){
							  $token  = $rr['value'];
							  break;
						 }
						 
					 }
					 
   		 			break;
				case 'REJECTED':
   		 			 $msg = 'TRANSACCION RECHAZADA';
   		 			break;      		 					 		
   		 		default:
   		 			$msg = 'TRANSACCION PENDIENTE';
   		 			break;
   		 	}

   		 }//end if
		 
		$response = array(
				'success' 		 => $status,
				'message' 		 => $msg,
				'resArr' 		 => $resArr,
				'token' 	 => $token
			);
			
		return $response;	
   		 	   
	   
   }
   
	public function collectP2P(Request $request){

		try{

			 if($request->session()->has('requestId')){
				$requestId = $request->session()->get('requestId');
			 }else{
				$requestId = $requestId;
			 }				
			
			$ret  = $this->getTokenP2P($request, $requestId);
			
			if($ret['success']==0){
				return response()->json(['success'=>0,'message'=>'No fue posible obtener token','res'=>$ret['resArr']]);
			}
			$token = $ret['token'];
			
		    $idProduct =  $request->session()->get('id_product');
		    $objWebPr  = webProduct::find($idProduct);		    
		    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
		    $arrWebProduct = webProduct::find($idProduct);
		    $precio        = $objWebPr->pro_valor;
			$countryObj    = DB::SELECT('select * from gen_location_country where glc_code = 63');
			$aplica_tax    = 0;
		    $tax_val       = 0;
			
			if($request->session()->has('pr_sel')){
				$prApi = $request->session()->get('pr_sel');
				if(!empty($prApi[0]) && !empty($prApi[0]['cobra_impuestos']) ){
					if($prApi[0]['cobra_impuestos']=='S'){
						$aplica_tax    = 1;
					}
				}
				
			}
			
			$idProduct = $request->session()->get('id_product');	
			$arrWebProduct = webProduct::find($idProduct);
			$aplica_tax    = $arrWebProduct->pro_tax;
		
			$base_tx   = $precio;
			$amount_tx = 0;
			if($aplica_tax==1){
				$tax_val =1+$countryObj[0]->glc_percentagetax/100;
				
				$base_tx   = $precio/$tax_val;
				$amount_tx = $precio - $base_tx;
				
			}
			
			$id_sale  = $request->session()->get('sale_id');
			$objWSD   = WebSalesDependent::where('id_sales',$id_sale)->get();

			$data_login = GenFunctionP2P::createTranskeyP2P();

			$var_endpoint_return = BtnP2pParam::find(5);
			$var_return =  $var_endpoint_return->p2p_value;
			$reference  =  GenFunctionP2P::generarCodigos(1,16,true);

			//return response()->json(['success'=>0,'message'=>$idProduct]);
			
			$request_arr = [
                       "auth" => [
                              "login"   => $data_login['login'],
                              "seed"    => $data_login['seed'],
                              "nonce"   => $data_login['nonce'],
                              "tranKey" =>  $data_login['tranKey']

                          ],
                          /*"locale" => 'es_EC',*/
						 "instrument"=> [
							  "token"=> [
										"token"=> $token
									]
						    ],  
                          							
                          "payer"=>[
                            "document" => $request->id, 
                            "documentType" => "CI", //CI cedula, RUC, PPN -> Pasaporte
                            "name" => preg_replace('/[0-9]+/', '', $request->nombre),
                            "surname" => preg_replace('/[0-9]+/', '', $request->apellido),
                            "email" => ( !is_null( $request->email ) ) ? $request->email :'adominguez@memorialtechnologies.com'
                          ],
                          "buyer"=>[
                            "document" => $request->id, 
                            "documentType" => "CI", //CI cedula, RUC, PPN -> Pasaporte
                            "name" => preg_replace('/[0-9]+/', '', $request->nombre),
                            "surname" => preg_replace('/[0-9]+/', '', $request->apellido),
                            "email" => ( !is_null( $request->email ) ) ? $request->email :'adominguez@memorialtechnologies.com'
                          ],						                         
                         "payment" => [
                              "reference" =>$reference , //ID generado por el comercio
                              "description" => "Memorial Ecuador",
                              "amount" => [
                                  "currency" => "USD",
                                  "total" => number_format((float)$precio,2),
                                 "taxes" => [
								        [
                                           "kind"=> "valueAddedTax",//"iva",
                                           "amount"=> number_format((float)$amount_tx,2),
                                           "base"=> number_format((float)$base_tx,2)  ,//*$tax_val,
                                        ]
								  
								   ],
                                  "details" => [
                                       [
                                           "kind" => "subtotal",
                                           "amount" => number_format((float)$precio,2)

                                       ]
                                   ]
                              ]
							 /* , "allowPartial" => false,*/
                          ]
						  /*,"userAgent" => $_SERVER['HTTP_USER_AGENT']*/
						  /*,
                          "expiration" => date('c', strtotime('+30 minutes'))
						  , // tiempo para pago antes de caducar sesión
                          "returnUrl" => $var_return,
                          "ipAddress" => GenFunction::getRealIpAddr(),//;"173.230.130.8",
                          "userAgent" => $_SERVER['HTTP_USER_AGENT']//'WEB',*/
                      ];

            $url_process = GenFunctionP2P::createSessionP2PCollect($request_arr,$request);
            if($url_process['status']['status']=='APPROVED' || $url_process['status']['status']=='PENDING'){

                     $estado = 'APROBADO';
					 if($url_process['status']['status']=='PENDING'){
						$estado = 'PENDIENTE'; 
					 }else if($url_process['status']['status']=='APPROVED'){
						$estado = 'APROBADO'; 
					 }
					 else{
						$estado = 'RECHAZADA'; 
					 }
					
					$transaction = new BtnP2pTran;
					$transaction->bpt_status = $estado;
					$transaction->bpt_reason = $url_process['status']['reason'];
					$transaction->bpt_message = $url_process['status']['message'];
					$transaction->bpt_date = $url_process['status']['date'];
					$transaction->bpt_requestId = $url_process['requestId'];
					$transaction->bpt_session =  $reference;
					$transaction->bpt_internal_reference =  $url_process['payment'][0]['internalReference'];
					$transaction->bpt_processUrl = 'api/collect';//$url_process['processUrl'];
				    $transaction->bpt_name    = $request_arr['payer']['name'];
					$transaction->bpt_surname = $request_arr['payer']['surname'];
					$transaction->bpt_numdoc  = $request->id;
					$transaction->bpt_email = $request->email;
					$transaction->bpt_phone = '';
					$transaction->bpt_product_id = $objWebPr->pro_code;
					$transaction->bpt_product_name = $objWebPr->pro_name;
					$transaction->bpt_quantity = 1;
					$transaction->bpt_price = $objWebPr->pro_valor;
					$transaction->bpt_estado = 1;
					
					$transaction->save();
					// Hasta aqui el almacenamiento de la transacion

				$request->session()->put('requestId_collect',$url_process['requestId']);
				$request->session()->put('first_pay',1);
				
				$val_p = $url_process['payment'][0]['amount']['to']['total'];
				try{
					DB::beginTransaction();
				    $var = $this->saveDataSale($request,$reference,$val_p);
				    DB::commit();
					$sal_id = $request->session()->get('id_sale_fn');
					$objMethodPay  = SalMethodPay::where('smp_status',1)->where('sal_id',$sal_id)->first();
					$objMethodPay->smp_ban_name      = $url_process['payment'][0]['issuerName'];
					$objMethodPay->smp_cd_type       = $url_process['payment'][0]['paymentMethodName'];
					$objMethodPay->smp_authorization = $url_process['payment'][0]['authorization'];
					$objMethodPay->smp_receipt       = $url_process['payment'][0]['receipt'];	
				
					$objMethodPay->save();
					
					
				    $this->generateCXC($request);
					$transaction->sal_id = $sal_id;
					$transaction->save();
				}catch(\Exception $e){
					  DB::rollback();
					  $msg  = 'Su pago con referencia '.$reference.' ha sido aprobada';
                      if($url_process['status']['status']=='PENDING'){
						   $msg  = 'Su pago con referencia '.$reference.' esta pendiente';
					  }					  
					  return response()->json(['success'=>0,'error'=>$e->getMessage(),'message'=>$msg.'  sin embargo hubo un problema al generar la asistencia']);
		
				}
				/*if(!$this->saveDataSale($request)){
					return response()->json(['success'=>0,'message'=>'Hubo un problema al generar la asistencia, sin embargo el pago se realizo correctamente']);
					
				}*/

				//return response()->json(['success'=>0,'message'=>$url_process]);
				$status = 1;
				$mensaje='Su pago con referencia '.$url_process['payment'][0]['reference'].' ha sido Aprobado';
				$url_redirect='/sales/payment/supscription';
				
				
				if($url_process['status']['status']=='PENDING'){
					$status = 1;
					$mensaje='Su transaccion con referencia '.$url_process['payment'][0]['reference'].' esta en estado Pendiente, recuerde que su cobertura no entrara en vigencia hasta que se efectivice el pago';
				}
				
				$response = array(
					'success' 		 => $status,
					'message' 		 => $mensaje,
					'p2p_status' 	 => $url_process['status']['status'],
					'url_process'    => $url_process,
					'url_redirect'   => $url_redirect
				);
				return response()->json($response);	

            }else{
				

                     $estado = 'APROBADO';
					 if($url_process['status']['status']=='PENDING'){
						$estado = 'PENDIENTE'; 
					 }else if($url_process['status']['status']=='APPROVED'){
						$estado = 'APROBADO'; 
					 }
					 else{
						$estado = 'RECHAZADA'; 
					 }
					
					$transaction = new BtnP2pTran;
					$transaction->bpt_status = $estado;//$url_process['status']['status'];
					$transaction->bpt_reason = $url_process['status']['reason'];
					$transaction->bpt_message = $url_process['status']['message'];
					$transaction->bpt_date = $url_process['status']['date'];
					$transaction->bpt_requestId = $url_process['requestId'];
					$transaction->bpt_session =  $reference;
					$transaction->bpt_internal_reference =  $url_process['payment'][0]['internalReference'];
					$transaction->bpt_processUrl = 'api/collect';//$url_process['processUrl'];
				    $transaction->bpt_name    = $request_arr['payer']['name'];
					$transaction->bpt_surname = $request_arr['payer']['surname'];
					$transaction->bpt_numdoc  = $request->id;
					$transaction->bpt_email = $request->email;
					$transaction->bpt_phone = '';
					$transaction->bpt_product_id = $objWebPr->pro_code;
					$transaction->bpt_product_name = $objWebPr->pro_name;
					$transaction->bpt_quantity = 1;
					$transaction->bpt_price = $objWebPr->pro_valor;
					$transaction->bpt_estado = 1;
					$transaction->save();				
            	
				/*return response()->json(['success'=>0,'message'=>'Hubo un problema al conectarse con Place to Pay','p2p_msg'=>$url_process,'request_arr'=>$request_arr,
				                          'json'=>json_encode($request_arr,true)]);*/
										  
					return response()->json(['success'=>0,'message'=>'Su petición con referencia '.$url_process['payment'][0]['reference'].' ha sido Rechazada','p2p_msg'=>$url_process,'request_arr'=>$request_arr,
				                          'json'=>json_encode($request_arr,true)]);										  
            }
	   
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}



   }
   
	public function collectRecurrentP2P(Request $request){
	   try{
	   //$objMethodSu = SalMethodPaySubscription::where('mps_typesales',3)->where('mps_status',1)->get();
	   $objMethodSu = SalMethodPaySubscription::where('mps_typesales',3)->where('mps_status',1)->get();
	   date_default_timezone_set('America/Guayaquil');
	   $currentDay  = date('l');
	   $currentDay  = date('d', strtotime($currentDay));

	   foreach($objMethodSu as $rw){
		   	  
		   if($currentDay==$rw->mps_day_pay){
			  
			    $sal_id    = $rw->sal_id;
			    $objSale   = SalSale::find($sal_id);
			    $reference = $objSale->sal_reference;
				$precio   = $objSale->sal_price_unit;
				$countryObj    = DB::SELECT('select * from gen_location_country where glc_code = 63');
				$aplica_tax    = 1;
				$tax_val       = 0;
				
				$base_tx   = $precio;
				$amount_tx = 0;
				if($aplica_tax==1){
					$tax_val =1+$countryObj[0]->glc_percentagetax/100;
					
					$base_tx   = $precio/$tax_val;
					$amount_tx = $precio - $base_tx;
					
			   }

				$objBtnP2pRe  = BtnP2pRecurrent::where('sal_id',$sal_id)->where('mps_id',$rw->mps_id)->first();
				if(!empty($objBtnP2pRe)){
					if($objBtnP2pRe->bpr_try>=3){
						$this->enviarEmailP2PUpdate($request, $sal_id);
						continue;
					}
				}
				
			    $objP2PTran   = BtnP2pTran::where('bpt_session',$reference)->first();
			    //$requestId  = $objP2PTran->bpt_requestId;
				$requestId  =  $rw->mps_requestId;
			   
				$data_login = GenFunctionP2P::createTranskeyP2P();

				$var_endpoint_return = BtnP2pParam::find(5);
				$var_return =  $var_endpoint_return->p2p_value;
				$reference_n  =  GenFunctionP2P::generarCodigos(1,16,true);
				
				$ret  = $this->getTokenP2P($request, $requestId);
				if($ret['success']==0){
					$transaction = new BtnP2pTran;
					$transaction->bpt_status = 'ERROR_PAYMENT_R';
					$transaction->bpt_reason = '';
					$transaction->bpt_message = 'No fue posible recuperar el token  '.$ret['message'].' - '.$ret['resArr']['status']['status'];
					$transaction->bpt_date = now();
					$transaction->bpt_requestId = $requestId;
					$transaction->bpt_session   = $reference;
					$transaction->bpt_internal_reference =  '';
					$transaction->bpt_processUrl = 'api/collect';//$url_process['processUrl'];
				    $transaction->bpt_name    = $rw->mps_titularname;
					$transaction->bpt_surname = $rw->mps_titularsurname;
					$transaction->bpt_numdoc  = $rw->mps_titularidnumber;
					$transaction->bpt_email   = $rw->mps_titular_email;
					$transaction->bpt_phone   = $rw->mps_titular_phone;
					$transaction->bpt_product_id   = $objSale->pro_code;
					$transaction->bpt_product_name = $objSale->pro_code;
					$transaction->bpt_quantity = 1;
					$transaction->bpt_price    = $precio;
					$transaction->sal_id = $sal_id;
					$transaction->bpt_estado   = 1;
					$transaction->save();
					continue;
				}
				$token = $ret['token'];				
			
			   ///collect from p2p
                $request_arr = [
                       "auth" => [
                              "login"   => $data_login['login'],
                              "seed"    => $data_login['seed'],
                              "nonce"   => $data_login['nonce'],
                              "tranKey" =>  $data_login['tranKey']

                          ],
						 "instrument"=> [
							  "token"=> [
										"token"=> $token
									]
						    ],                          							
                          "payer"=>[
                            "document" => $rw->mps_titularidnumber, 
                            "documentType" => "CI", //CI cedula, RUC, PPN -> Pasaporte
                            "name" => preg_replace('/[0-9]+/', '', $rw->mps_titularname),
                            "surname" => preg_replace('/[0-9]+/', '', $rw->mps_titularsurname),
                            "email" => ( !is_null( $rw->mps_titular_email ) ) ? $rw->mps_titular_email :'adominguez@memorialtechnologies.com'
                          ],
                          "buyer"=>[
                            "document" =>  $rw->mps_titularidnumber,
                            "documentType" => "CI", //CI cedula, RUC, PPN -> Pasaporte
                            "name" => preg_replace('/[0-9]+/', '', $rw->mps_titularname),
                            "surname" => preg_replace('/[0-9]+/', '', $rw->mps_titularsurname),
                            "email" => ( !is_null( $rw->mps_titular_email ) ) ? $rw->mps_titular_email :'adominguez@memorialtechnologies.com'
                          ],						                         
                         "payment" => [
                              "reference" =>$reference_n , //ID generado por el comercio
                              "description" => "Memorial Ecuador",
                              "amount" => [
                                  "currency" => "USD",
                                  "total" => number_format((float)$precio,2),
                                 "taxes" => [
								        [
                                           "kind"=> "valueAddedTax",//"iva",
                                           "amount"=> number_format((float)$amount_tx,2),
                                           "base"=> number_format((float)$base_tx,2) ,//*$tax_val,
                                        ]
								  
								   ],
                                  "details" => [
                                       [
                                           "kind" => "subtotal",
                                           "amount" => number_format((float)$precio,2)

                                       ]
                                   ]
                              ]
                          ]
                      ];

            $url_process = GenFunctionP2P::createSessionP2PCollect($request_arr,$request);
            if($url_process['status']['status']=='APPROVED' || $url_process['status']['status']=='PENDING'){

                     $estado = 'APROBADO';
					 if($url_process['status']['status']=='PENDING'){
						$estado = 'PENDIENTE'; 
					 }else if($url_process['status']['status']=='APPROVED'){
						$estado = 'APROBADO'; 
					 }
					 else{
						$estado = 'RECHAZADA'; 
					 }
									
					$transaction = new BtnP2pTran;
					$transaction->bpt_status = $estado;//$url_process['status']['status'];
					$transaction->bpt_reason = $url_process['status']['reason'];
					$transaction->bpt_message = $url_process['status']['message'];
					$transaction->bpt_date = $url_process['status']['date'];
					$transaction->bpt_requestId = $url_process['requestId'];
					$transaction->bpt_session =  $reference_n;
					$transaction->bpt_internal_reference =  $url_process['payment'][0]['internalReference'];
					$transaction->bpt_processUrl = 'api/collect';//$url_process['processUrl'];
				    $transaction->bpt_name    = $rw->mps_titularname;
					$transaction->bpt_surname = $rw->mps_titularsurname;
					$transaction->bpt_numdoc  = $rw->mps_titularidnumber;
					$transaction->bpt_email = $rw->mps_titular_email;
					$transaction->bpt_phone = $rw->mps_titular_phone;
					$transaction->bpt_product_id   = $objSale->pro_code;
					$transaction->bpt_product_name = $objSale->pro_code;
					$transaction->bpt_quantity = 1;
					$transaction->bpt_price = $precio;
					$transaction->bpt_estado = 1;
					$transaction->sal_id = $sal_id;
					$transaction->save();
					// Hasta aqui el almacenamiento de la transacion
				
				     $val_p = $url_process['payment'][0]['amount']['to']['total'];
					 
					 if($url_process['status']['status']=='APPROVED'){
						 $re = $this->savePayCxC($request,$objSale->sal_contract,$sal_id,$precio);
						 if($re==0){
							 $transaction->bpt_status = 'PENDING_CXC';
							 $transaction->save();
						 }
						 
						$objMethodPay  = new  SalMethodPay;
						$objMethodPay->sal_id            = $sal_id;
						$objMethodPay->smp_ban_name      = $url_process['payment'][0]['issuerName'];
						$objMethodPay->smp_cd_type       = $url_process['payment'][0]['paymentMethodName'];
						$objMethodPay->smp_authorization = $url_process['payment'][0]['authorization'];
						$objMethodPay->smp_receipt       = $url_process['payment'][0]['receipt'];	
						$objMethodPay->smp_typesales     = 2;
						$objMethodPay->smp_typeaccount   = 0;
						$objMethodPay->smp_numberaccount = '';
						$objMethodPay->smp_reference     = $reference_n;
						$objMethodPay->smp_value         = $precio;
						$objMethodPay->smp_titularname   = $rw->mps_titularname;
						$objMethodPay->smp_titularsurname   = $rw->mps_titularsurname;
						$objMethodPay->smp_titularidnumber  = $rw->mps_titularidnumber;
						$objMethodPay->smp_status        = 1;							
						$objMethodPay->save();

					 }//end of if($url_process[


            }else{
                     $estado = 'APROBADO';
					 if($url_process['status']['status']=='PENDING'){
						$estado = 'PENDIENTE'; 
					 }else if($url_process['status']['status']=='APPROVED'){
						$estado = 'APROBADO'; 
					 }
					 else{
						$estado = 'RECHAZADA'; 
					 }				
            	    $transaction = new BtnP2pTran;
					$transaction->bpt_status = $estado;//$url_process['status']['status'];
					$transaction->bpt_reason ='';
					$transaction->bpt_message = 'Pago rechazado';
					$transaction->bpt_date =     now();
					$transaction->bpt_requestId = 0;//$url_process['requestId'];
					$transaction->bpt_session   = $reference_n;
					$transaction->bpt_internal_reference =  '';
					$transaction->bpt_processUrl = 'api/collect';//$url_process['processUrl'];
				    $transaction->bpt_name    = $rw->mps_titularname;
					$transaction->bpt_surname = $rw->mps_titularsurname;
					$transaction->bpt_numdoc  = $rw->mps_titularidnumber;
					$transaction->bpt_email = $rw->mps_titular_email;
					$transaction->bpt_phone = $rw->mps_titular_phone;
					$transaction->bpt_product_id   = $objSale->pro_code;
					$transaction->bpt_product_name = $objSale->pro_code;
					$transaction->bpt_quantity = 1;
					$transaction->bpt_price = $precio;
					$transaction->bpt_estado = 1;
					$transaction->sal_id = $sal_id;
					$transaction->save();
					$bpr_try=0;
					$objBtnP2pRe  = BtnP2pRecurrent::where('sal_id',$sal_id)->where('mps_id',$rw->mps_id)->first();
					if(empty($objBtnP2pRe)){
						$objBtnP2pRe  = new BtnP2pRecurrent;
					}else{
						$bpr_try=$objBtnP2pRe->bpr_try;
					}	
					$objBtnP2pRe->bpr_try = $bpr_try +1;	
					$objBtnP2pRe->bpr_requestID = $requestId;		
					$objBtnP2pRe->sal_id        = $sal_id;	
					$objBtnP2pRe->mps_id        = $rw->mps_id;	
					$objBtnP2pRe->save();					
								  
            }			   
			   
			   
			   /////end of collect from p2p
			   
			   
			   
		   }// if($currentDay
	   }
	   
	   }catch(\Exception $e){
		   		Storage::prepend('log_p2p_collect.log',"****************");
				Storage::prepend('log_p2p_collect.log',$e->getMessage() );  
				Storage::prepend('log_p2p_collect.log',"****************");
	   }
   }

	public function saveRecurrentP2P(Request $request){
	    
	    try{
	        $validator = \Validator::make($request->all(), [
	            'period_pago' => 'required',
	            'paysel' => 'required'	
	        ],['period_pago.required' => 'El periodo de pago es requerido.',
	           'paysel.required' => 'El tipo de pago es requerido.']);

       
	        if ($validator->fails())
	        {
	            return response()->json(['success'=>2,'errors'=>$validator->errors()->all()]);
	        }
			
			
			$idProduct =  $request->session()->get('id_product');
		    $objWebPr  = webProduct::find($idProduct);		   			
			
			
			
			$id_websale  = $request->session()->get('sale_id');

			$sal_id = $request->session()->get('id_sale_fn');
			
			$objWSale    = WebSale::find($id_websale);
			
			//return response()->json(['success'=>0,'errors'=>$sal_id]);
			$objSale    = SalSale::find($sal_id);
			
			$number_months = 12;
			$reference     = $request->referencia;
			$price_unit    = $objWebPr->pro_valor;
			$npc           = $objWebPr->pro_npc; 
			$sal_taxes     = 0.15;
			
			$sql_del = 'update sal_methodpay_subscriptions set mps_status=0 where sal_id = '.$sal_id;
			
			$objD = DB::SELECT($sql_del);
			$objMethodPay  = new SalMethodPaySubscription;

			$sal_total = $price_unit*$number_months + ($price_unit*$number_months)*$sal_taxes;
			$cli_id    = $request->session()->get('cli_id');
			$oblCli    = CliClient::find($objSale->id_person);
					
			$md_code             = 5;
			$num_cuenta_debito   = '';
			$banco_debito        = 0;
			$num_tarjeta_credito = '';
			$tipo_tarjeta        = 0;
			$mes_ven_cc          = 0;
			$anio_ven_cc         = 0;
			

            $requestId = '';			
			if($request->session()->has('requestId')){
				$requestId = $request->session()->get('requestId');
			}						
			

			$objMethodPay->sal_id = $sal_id;
			$objMethodPay->mps_typesales     = 3;
			$objMethodPay->mps_name_company  = '';
			
			$objMethodPay->md_code           = $md_code;			
			$objMethodPay->mps_day_pay       = $request->period_pago;
			$objMethodPay->mps_value         = $price_unit;
			$objMethodPay->mps_type_method_pay      = 1;
			$objMethodPay->mps_titularname          = $oblCli->cli_name;//$request->titular_tarjeta;
			$objMethodPay->mps_titularsurname       = $oblCli->cli_surname;//$request->apellido_cd;
			$objMethodPay->mps_titularidnumber      = $oblCli->cli_id;//$request->titular_tarjeta;
			//$objMethodPay->mps_city_supscription    = $request->ciudad;
			$objMethodPay->mps_requestId            = $requestId;
			$objMethodPay->mps_day_supscription     = $request->dia;
			$objMethodPay->mps_month_supscription   = $request->mes;
			$objMethodPay->mps_year_supscription    = $request->anio;
			$objMethodPay->mps_status        = 1;
			$objMethodPay->save();
			
		    $url_redirect = '/sales/preview';
			if($npc>1){
				$url_redirect = '/sales/dependents';
			}
			
		
			$mensaje      = 'OK';
			$status       =  1;
			
			$response = array(
				'success' 		 => $status,
				'message' 		 => $mensaje,
				'url_redirect' 	 => $url_redirect
			);
			return response()->json($response);		   
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}	
   }  
   
    public function savePayCxC(Request $request,$sal_contract,$sal_id,$price){
		
            $lang_id    = 2;
			$user_name  = "P2P";
			$ip         = $_SERVER['REMOTE_ADDR'];
			$tot_price  = $price.']TJ[undefined[[[[[['.$price.'[&poliza='.$sal_id.','.$price;
			$attribute  = "contrato=".$sal_contract."&valorpago=".$user_name."[".$ip."[".$lang_id."[".$tot_price;
			$json_product = GenFunction::procesarApiGen(56, $lang_id, "SAVE_P", $user_name, $attribute, "");
			$status =1;
			if(empty($json_product[0])){
				$status =0;
			}	
			return $status;

	}
   
	public function notifyP2P(Request $request){
		$data = json_decode($request->getContent(), true);
		$msg  = $data['status']['status'];
		$msg .= $data['status']['message'];
		$msg .= $data['status']['reason'];
		$msg .= $data['status']['date'];
		$msg .= $data['requestId'];
		$msg .= $data['reference'];
		$msg .= $data['signature'];
		$requestId = $request->session()->get('requestId');
		if($data['requestId']){
			$fecha = GenFunction::getDateDB();
			Storage::prepend('log_notifyP2P.log',"****************");
			Storage::prepend('log_notifyP2P.log',"NOTIFY=".$data['status']['status']."::".$fecha);              
			Storage::prepend('log_notifyP2P.log',"RequestId: ".$data['requestId']); 
			Storage::prepend('log_notifyP2P.log',"****************");
			$transaction =  BtnP2pTran::where('bpt_requestId',$data['requestId'])->first();
			if(!empty($transaction)){
				$msg  ='APROBADA';
				if($data['status']['status']=='PENDING')
				{$msg='PENDIENTE';
			    }else{$msg='RECHAZADA';
				}
				
				
				 $msg = 'APROBADO';
				 if($data['status']['status']=='PENDING'){
					$msg = 'PENDIENTE'; 
				 }else if($data['status']['status']=='APPROVED'){
					$msg = 'APROBADO'; 
				 }
				 else{
					$msg = 'RECHAZADA'; 
				 }					
				$transaction->bpt_status  = $msg;//$data['status']['status'];
				//$transaction->bpt_session = $data['requestId'];
				$transaction->save();
			}
		} else {
			$fecha = GenFunction::getDateDB();
			Storage::prepend('log_notifyP2P.log',"****************");
			Storage::prepend('log_notifyP2P.log',"NOTIFY=ERROR::".$fecha);              
			Storage::prepend('log_notifyP2P.log',"RequestId: ".$requestId);   
			Storage::prepend('log_notifyP2P.log',"****************");
		}
	} // Fin notifyP2P

	/**
	* Define the application's command schedule.
	*
	* @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	* @return void
	*/
    public function sondaP2P(){
		$fecha = GenFunction::getDateDB();
		Storage::prepend('log_sondaP2P.log',"****************");
		Storage::prepend('log_sondaP2P.log',"SONDA=BEGINING::".$fecha);
		Storage::prepend('log_sondaP2P.log',"****************");
		
		$arrTransPending = BtnP2pTran::where('bpt_status', 'PENDIENTE')->get();
		
		// Credenciales
		$var_endpoint_pruebas = BtnP2pParam::find(1);
		$endPoint =	$var_endpoint_pruebas->p2p_value;
		
		foreach($arrTransPending as $transPending){
			
			$requestId= $transPending->bpt_requestId;

			//SOLICITAMOS LA CREACION DE SESSION PARA EL PAGO PLACETOPAY
			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => $endPoint.'api/session/'.$requestId,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => array(
					"Cache-Control: no-cache",
					"Content-Type:  application/json",
					"Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
				),
			));

			$response = curl_exec($curl);
			$err      = curl_error($curl);
			curl_close($curl);

			if ($err) {
				$fecha = GenFunction::getDateDB();
				Storage::prepend('log_sondaP2P.log',"****************");
				Storage::prepend('log_sondaP2P.log',"SONDA=ERROR::".$fecha);              
				Storage::prepend('log_sondaP2P.log', $err);
				storage::prepend('log_sondaP2P.log', $endPoint.'api/session/'.$requestId);
				Storage::prepend('log_sondaP2P.log',"****************");
			} else {
				$resArray = json_decode($response, true);
				if($resArray['status']['status'] != 'FAILED'){
					$fecha = GenFunction::getDateDB();
					Storage::prepend('log_sondaP2P.log',"****************");
					Storage::prepend('log_sondaP2P.log',"SONDA: ".$resArray['status']['status']."::".$fecha);              
					Storage::prepend('log_sondaP2P.log',$resArray['requestId']);
					storage::prepend('log_sondaP2P.log', $endPoint.'api/session/'.$requestId);
					Storage::prepend('log_sondaP2P.log',"La petición ha sido aprobada exitosamente"); 
					Storage::prepend('log_sondaP2P.log',"****************");
					if ($resArray['status']['status'] == 'APPROVED' ) {
						$transApproved = BtnP2pTran::find($transPending->bpt_id);
						$transApproved->bpt_status  = $resArray['status']['status'];
						$transApproved->bpt_message = "La petición ha sido aprobada exitosamente";
						$transApproved->save();
					}
				}else{
					$fecha = GenFunction::getDateDB();
					Storage::prepend('log_sondaP2P.log',"****************");
					Storage::prepend('log_sondaP2P.log',"SONDA=".$resArray['status']['status']."::".$fecha);
					storage::prepend('log_sondaP2P.log', $endPoint.'api/session/'.$requestId);
					Storage::prepend('log_sondaP2P.log',"****************");
				}
			} // Fin err
		} // Fin foreach
		$fecha = GenFunction::getDateDB();
		Storage::prepend('log_sondaP2P.log',"****************");
		Storage::prepend('log_sondaP2P.log',"SONDA=ENDING::".$fecha);
		Storage::prepend('log_sondaP2P.log',"****************");
    } // Fin sondaP2P

	public function responseP2P(Request $request){
	     if($request->session()->has('requestId')){
			 $requestId = $request->session()->get('requestId');
		 }else{
			  $requestId = 116263;
		 }
		 
   		 $resArr = GenFunctionP2P::responseSessionP2P($requestId,$request);
   		 $msg = 'NO SE PUDO OBTENER INFORMACION DE PLACE TO PAY';
   		 //$resArr['status']['status']=='OK'
   		 if(!empty($resArr['status']['status']))
   		 {
   		 	$status  = $resArr['status']['status'];

   		 	switch ($status) {
   		 		case 'APPROVED':
   		 			 $msg = '<div class="alert alert-success" id="alert_success">TRANSACCION APROBADA</div>';
   		 			break;
				case 'REJECTED':
   		 			 $msg = '<div class="alert alert-error" id="alert_success">TRANSACCION RECHAZADA</div>';
   		 			break;      		 					 		
   		 		default:
   		 			$msg = '<div class="alert alert-info" id="alert_success">Transaccion Pendiente, <br> Por favor espere unos minutos</div>';
   		 			break;
   		 	}


			$fecha = GenFunction::getDateDB();
			Storage::prepend('log_p2p.log',"****************");
			Storage::prepend('log_p2p.log',"RESPONSE=".$resArr['status']['status']."::".$fecha);              
			//Storage::prepend('log_p2p.log',$resArr['requestId']);  
			Storage::prepend('log_p2p.log',$msg); 
			Storage::prepend('log_p2p.log',"****************"); 		   	

			$transaction =  BtnP2pTran::where('bpt_requestId',$requestId)->first();
			if(!empty($transaction)){

					$transaction->bpt_status  = $resArr['status']['status'];
					$transaction->bpt_session = $requestId;
					$transaction->save();
			}



   		 }//end if
   		 

   		  return view('sales.payment.p2p_notify')->with('msg', $msg);

   }

	public function saveDebit(Request $request){
		DB::beginTransaction();
		try{
	        $validator = \Validator::make($request->all(), [
	            'tipo_cuenta' => 'required',
	            'banco' => 'required',
	            'numero_cuenta' => 'required',
				'id_deb' => 'required',
				'nombre_deb' => 'required',	
				'apellido_deb' => 'required'				
	        ],['tipo_cuenta.required' => 'El tipo de cuenta es requerida.',
	           'banco.required' => 'El banco  es requerido.',
	           'numero_cuenta.required' => 'El numero de cuenta  es requerido.',
			   'nombre_deb.required' => 'El nombre de titular de la cuenta  es requerido.',
			   'apellido_deb.required' => 'El Apellido del titular de la cuenta  es requerido.']);

            //['sc_razon_social.required' => ':attribute  es requerido.']	        
	        if ($validator->fails()){
	            return response()->json(['success'=>2,'errors'=>$validator->errors()->all()]);
	        }
			
			
			$idProduct = $request->session()->get('id_product');
		    $objWebPr  = webProduct::find($idProduct);
			$id_websale= $request->session()->get('sale_id');			
			$objWSale  = WebSale::find($id_websale);
			$number_months = 12;
			$reference     = 'NA';
			$price_unit    = $objWebPr->pro_valor;
			$npc           = $objWebPr->pro_npc; 
			$sal_taxes = 0;
			if($request->session()->has('requestId')){
				$reference = $request->session()->get('requestId');
			}
			$sal_total = ($price_unit*$number_months) + (($price_unit*$number_months)*$sal_taxes);
		    $cli_id  = $request->session()->get('cli_id');
			
			
			$objContract = new SalContract;
			$objContract->sal_token = $objWSale->sal_token;
			$objContract->con_attach   = 0;
			$objContract->con_card     = 0;
			$objContract->con_status   = 1;
			$objContract->save();
			
			
			$objSale = new SalSale;
			$objSale->sal_contract = $objContract->Id;
			$objSale->agency_code = $objWSale->agency_code;
			$objSale->sel_code = Auth::id();
			$objSale->pro_code    = $objWebPr->pro_code;
			$objSale->sal_token   = $objWSale->sal_token;
			$objSale->id_person   = $cli_id;
			$objSale->sal_dateStart      = $objWSale->sal_date;
			$objSale->sal_numberMounth   = $number_months;
			$objSale->sal_reference      = $reference;
			$objSale->sal_frecuencyCollect  = 'NA';
			$objSale->sal_price_unit        = $price_unit;
			$objSale->sal_taxes     = $sal_taxes;
			$objSale->sal_total     = $sal_total;
			$objSale->sal_status    = 1;
			$objSale->save();
			
			$objNewSale = SalSale::find($objSale->id);
			$objSxk = SalSxk::where('cli_code',$objNewSale->id_person)
							  ->where('kin_code',1)
							  ->where('sal_id',$objNewSale->id)->first();
			
			if(empty($objSxk)){
				$objSxk = new SalSxk;
			}	
			$objSxk->sal_id   = $objSale->id;
			$objSxk->cli_code = $objNewSale->id_person;
			$objSxk->kin_code = 1;
			$objSxk->save();			
			
			$objMethodPay  = new SalMethodPay;
			$objMethodPay->sal_id = $objSale->id;
			$objMethodPay->smp_typesales     = 2;
			$objMethodPay->ban_code          = $request->banco;
			$objMethodPay->smp_typeaccount   = $request->tipo_cuenta;
			$objMethodPay->smp_numberaccount = $request->numero_cuenta;
			$objMethodPay->smp_reference     = $reference;
			$objMethodPay->smp_value         = $price_unit;
			$objMethodPay->smp_titularname   = $request->nombre_deb;
			$objMethodPay->smp_titularsurname   = $request->apellido_deb;
			$objMethodPay->smp_titularidnumber  = $request->id_deb;
			$objMethodPay->smp_status        = 1;
			$objMethodPay->save();
			
			
			//Billing
			$objBill = new SalBilling;
			$objBill->bil_numberId = $request->id;
			$objBill->bil_name     = $request->nombre;
			$objBill->bil_surname  = $request->apellido;
			$objBill->bil_address  = $request->direccion;
			$objBill->bil_phone    = $request->phone;
			$objBill->bil_email    = $request->email;
			$objBill->sal_id       = $objSale->id;
			$objBill->save();
			$request->session()->put('id_sale_fn',$objSale->id);
			$request->session()->put('first_pay',1);
			$this->generateCXC($request);
			DB::commit();
		    
			$url_redirect = '/sales/payment/supscription';
			if($npc>1){
				
			}
			
			
			$mensaje      = 'OK';
			$status       =  1;
			
			$response = array(
				'success' 		 => $status,
				'message' 		 => $mensaje,
				'url_redirect' 	 => $url_redirect
			);
			return response()->json($response);		   
		   
	    }catch(\Exception $e){
			 DB::rollback();
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}  		 

		
	}

	public function saveTransfer(Request $request){
		try{
	        $validator = \Validator::make($request->all(), [
	            'nombre' => 'required',
	            'apellido' => 'required',
	            'ban_code' => 'required',
				'identificacion' => 'required',
				'referencia' => 'required',	
				'valor' => 'required'				
	        ],['nombre.required' => 'El nombre es requerido.',
	           'apellido.required' => 'El apellido  es requerido.',
	           'ban_code.required' => 'El banco  es requerido.',
			   'identificacion.required' => 'La identificacion es requerida.',
			   'referencia.required' => 'la referencia es requerida.',
			   'valor.required' => 'El valor es requerido.']);

            //['sc_razon_social.required' => ':attribute  es requerido.']	        
	        if ($validator->fails())
	        {
	            return response()->json(['success'=>2,'errors'=>$validator->errors()->all()]);
	        }
			
			
			$idProduct =  $request->session()->get('id_product');
		    $objWebPr  = webProduct::find($idProduct);		   			
			
			
			
			$id_websale  = $request->session()->get('sale_id');			
			$objWSale    = WebSale::find($id_websale);
			
			$number_months = 12;
			$reference     = $request->referencia;
			
			$price_unit    = $objWebPr->pro_valor;
			$npc           = $objWebPr->pro_npc; 
			$sal_taxes = 0;

			$sal_total = $price_unit*$number_months + ($price_unit*$number_months)*$sal_taxes;
			$cli_id    = $request->session()->get('cli_id');
			
			$objContract = new SalContract;
			$objContract->sal_token = $objWSale->sal_token;
			$objContract->con_attach   = 0;
			$objContract->con_card     = 0;
			$objContract->con_status   = 1;
			$objContract->save();			
			
			$objSale = new SalSale;
			$objSale->sal_contract = $objContract->Id;
			$objSale->agency_code = $objWSale->agency_code;
			$objSale->sel_code    = Auth::id();
			$objSale->id_person   = $cli_id;
			$objSale->pro_code    = $objWebPr->pro_code;
			$objSale->sal_token   = $objWSale->sal_token;
			$objSale->sal_dateStart      = $objWSale->sal_date;
			$objSale->sal_numberMounth   = $number_months;
			$objSale->sal_reference      = $reference;
			$objSale->sal_frecuencyCollect  = 'NA';
			$objSale->sal_price_unit        = $price_unit;
			$objSale->sal_taxes     = $sal_taxes;
			$objSale->sal_total     = $sal_total;
			$objSale->sal_status    = 1;
			$objSale->save();
			
			$objNewSale = SalSale::find($objSale->id);
			$objSxk = SalSxk::where('cli_code',$objNewSale->id_person)
							  ->where('kin_code',1)
							  ->where('sal_id',$objNewSale->id)->first();
			
			if(empty($objSxk)){
				$objSxk = new SalSxk;
			}	
			$objSxk->sal_id   = $objSale->id;
			$objSxk->cli_code = $objNewSale->id_person;
			$objSxk->kin_code = 1;
			$objSxk->save();
			
			
			$objMethodPay  = new SalMethodPay;
			$objMethodPay->sal_id = $objSale->id;
			$objMethodPay->smp_typesales     = 2;
			$objMethodPay->ban_code          = $request->ban_code;
			$objMethodPay->smp_typeaccount   = 0;
			$objMethodPay->smp_numberaccount = '';
			$objMethodPay->smp_reference     = $reference;
			$objMethodPay->smp_value         = $request->valor;
			$objMethodPay->smp_titularname   = $request->nombre_trf;
			$objMethodPay->smp_titularsurname   = $request->apellido_trf;
			$objMethodPay->smp_titularidnumber  = $request->identificacion;
			$objMethodPay->smp_status        = 1;
			$objMethodPay->save();

			//Billing
			$objBill = new SalBilling;
			$objBill->bil_numberId = $request->id;
			$objBill->bil_name     = $request->nombre;
			$objBill->bil_surname  = $request->apellido;
			$objBill->bil_address  = $request->direccion;
			$objBill->bil_phone    = $request->phone;
			$objBill->bil_email    = $request->email;
			$objBill->sal_id       = $objSale->id;
			$objBill->save();


			
			$request->session()->put('id_sale_fn',$objSale->id);
			$request->session()->put('first_pay',1);
			$this->generateCXC($request);		
		    $url_redirect = '/sales/payment/supscription';
			if($npc>1){
				$url_redirect = '/sales/payment/supscription';
			}
			

			$mensaje      = 'OK';
			$status       =  1;
			
			$response = array(
				'success' 		 => $status,
				'message' 		 => $mensaje,
				'url_redirect' 	 => $url_redirect
			);
			return response()->json($response);		   
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}	
	  
	}

	public function generateCXC($request){
	
		$id_sale_fn = $request->session()->get('id_sale_fn');
		$objSale = SalSale::find($id_sale_fn);
		//Recover Product API
		$objApiMaster   = GenApiMaster::where('api_action','WEB_PRODUCT')->where('api_parent',0)->first();
		$user_name      = 'WEB';
		$lang_id        = 2;
		$api_code       = $objApiMaster->api_code;
		$attr 			= 'poliza='.$objSale->id;
		$json_cxc        = GenFunction::procesarApiGen($api_code,$lang_id, 'LIST_S_CXC',$user_name,$attr,'');
		if(empty($json_cxc[0])){
			Storage::prepend('log_api_cxc_gen.log',"CXC: NO HUBO RESPUESTA");
			Storage::prepend('log_api_cxc_gen.log',$attr);
			Storage::prepend('log_api_cxc_gen.log',"****************");
		}else{
			Storage::prepend('log_api_cxc_gen.log',$attr);
			Storage::prepend('log_api_cxc_gen.log',"CXC: OK");
			Storage::prepend('log_api_cxc_gen.log',"Status: ".$json_cxc[0]['statuspago']);
			Storage::prepend('log_api_cxc_gen.log',"****************");		
		}

		$request->session()->put('pr_sel',$json_cxc);
  }


	public function saveFormCreditCard(Request $request,$pay_day){
	 
	try{
	        $validator = \Validator::make($request->all(), [
	            'titular_tarjeta' => 'required',
	            'numero_tarjeta' => 'required',
				'numero_tarjeta' => 'required',
			    'fecha_vg_tarjeta' => 'required',
				'tipo_tarjeta' => 'required',
				'apellido_cd' => 'required'
				
				
	        ],['titular_tarjeta.required' => 'Los nombres son requeridos.',
	           'numero_tarjeta.required' => 'El numero de tarjeta es requerido.',
			   'fecha_vg_tarjeta.required' => 'La fecha de vigencia es requerida.',
			   'tipo_tarjeta.required' => 'El tipo de tarjeta es requerido.',
			   'apellido_cd.required' => 'El apellido es requerido.'
			   ]);

       
	        if ($validator->fails())
	        {
	            return response()->json(['success'=>2,'errors'=>$validator->errors()->all()]);
	        }
			
			
			$idProduct =  $request->session()->get('id_product');
		    $objWebPr  = webProduct::find($idProduct);		   			
			
			
			
			$id_websale  = $request->session()->get('sale_id');			
			$objWSale    = WebSale::find($id_websale);
			
			$number_months = 12;
			$reference     = $request->referencia;
			
			$price_unit    = $objWebPr->pro_valor;
			$npc           = $objWebPr->pro_npc; 
			$sal_taxes = 0.15;

			$objMethodPay  = new SalMethodPaySubscription;


			$sal_total = $price_unit*$number_months + ($price_unit*$number_months)*$sal_taxes;
			$cli_id    = $request->session()->get('cli_id');
			$md_code     = 1;
			$num_cuenta_debito = '';
			$banco_debito = $request->banco;
			$num_tarjeta_credito = '';
			$tipo_tarjeta=0;
			$mes_ven_cc = 0;
			$anio_ven_cc=0;
			if($request->input('rol_pagos')){
				$md_code   = 1;
			}elseif($request->input('cuenta_corriente')){
				$md_code   = 2;
				$num_cuenta_debito = $request->input('num_cuenta_corriente');
				$banco_debito      = $request->input('banco_cc');
				$objMethodPay->mps_typeaccount          = 2;
				$objMethodPay->mps_numberaccount        = $num_cuenta_debito;
				$objMethodPay->ban_code                 = $banco_debito;
				
			}elseif($request->input('cuenta_ahorros')){
				$md_code   = 3;
				$num_cuenta_debito = $request->input('num_cuenta_ahorros');
				$banco_debito      = $request->input('banco_ca');
				$objMethodPay->mps_typeaccount          = 1;
				$objMethodPay->mps_numberaccount        = $num_cuenta_debito;
				$objMethodPay->ban_code                 = $banco_debito;				
			}elseif($request->input('tarjeta_credito')){
				$md_code   = 4;

				$num_tarjeta_credito = $request->input('num_tarjeta_credito');
				$tipo_tarjeta        = $request->input('tipo_tarjeta');	
                $mesanio             = explode('-',$request->input('mes_anio'));	
				$mes_ven_cc          = 	$mesanio[0];
				$anio_ven_cc         = 	$mesanio[1];	
				
				$objMethodPay->mps_cc_type          = $request->tipo_tarjeta;
				$objMethodPay->mps_cc_number        = $request->numero_tarjeta;
				$objMethodPay->mps_cc_date_end      = $request->fecha_vg_tarjeta;
				$objMethodPay->mps_cc_year_exp      = $request->anio_ven_cc;
				$objMethodPay->mps_cc_month_exp     = $request->mes_ven_cc;
				$objMethodPay->ban_code             = $banco_debito;
			}			
						
			
			$sal_id = $request->session()->get('id_sale_fn');

			$objMethodPay->sal_id = $sal_id;
			$objMethodPay->mps_typesales     = 3;
			$objMethodPay->mps_name_company  = $request->destino_nombres;
			
			$objMethodPay->md_code           = $md_code;			
			$objMethodPay->mps_day_pay       = $pay_day;
			$objMethodPay->mps_value         = $price_unit;
			
			if($request->has('chk_cancelado')){
				$objMethodPay->mps_type_method_pay         = $request->chk_cancelado;
			}else{
			    $objMethodPay->mps_type_method_pay         = $request->chk_acreditado;
				$objMethodPay->mps_account_number_cp       = $request->num_cuenta_mem;
				$objMethodPay->mps_bank_code_cp            = $request->banco_mem;
			}
				
			$names = explode(' ',$request->nombres_tit);
			
			$objMethodPay->mps_titularname          = $names[0];//$request->titular_tarjeta;
			if(!empty($names[1])){
				$objMethodPay->mps_titularsurname   = $names[0];//$request->apellido_cd;
			}
			$objMethodPay->mps_titularidnumber      = $request->identificacion;//$request->titular_tarjeta;
			$objMethodPay->mps_city_supscription    = $request->ciudad;
			$objMethodPay->mps_day_supscription     = $request->dia;
			$objMethodPay->mps_month_supscription   = $request->mes;
			$objMethodPay->mps_year_supscription    = $request->anio;
			$objMethodPay->mps_status        = 1;
			$objMethodPay->save();
			
		    $url_redirect = '/sales/preview';
			if($npc>1){
				$url_redirect = '/sales/dependents';
			}
			

			$mensaje      = 'OK';
			$status       =  1;
			
			$response = array(
				'success' 		 => $status,
				'message' 		 => $mensaje,
				'url_redirect' 	 => $url_redirect
			);
			return response()->json($response);		   
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}	
	  
  }


	public function saveFormCreditCardFP(Request $request,$pay_day){
	 
	try{
	        $validator = \Validator::make($request->all(), [
	            'titular_tarjeta' => 'required',
	            'numero_tarjeta' => 'required',
				'numero_tarjeta' => 'required',
			    'fecha_vg_tarjeta' => 'required',
				'tipo_tarjeta' => 'required',
				'apellido_cd' => 'required'
				
				
	        ],['titular_tarjeta.required' => 'Los nombres son requeridos.',
	           'numero_tarjeta.required' => 'El numero de tarjeta es requerido.',
			   'fecha_vg_tarjeta.required' => 'La fecha de vigencia es requerida.',
			   'tipo_tarjeta.required' => 'El tipo de tarjeta es requerido.',
			   'apellido_cd.required' => 'El apellido es requerido.'
			   ]);

       
	        if ($validator->fails())
	        {
	            return response()->json(['success'=>2,'errors'=>$validator->errors()->all()]);
	        }
			
			
			$idProduct =  $request->session()->get('id_product');
		    $objWebPr  = webProduct::find($idProduct);		   			
			
			
			
			$id_websale  = $request->session()->get('sale_id');			
			$objWSale    = WebSale::find($id_websale);
			
			$number_months = 12;
			$reference     = $request->numero_tarjeta;
			
			$price_unit    = $objWebPr->pro_valor;
			$npc           = $objWebPr->pro_npc; 
			$sal_taxes = 0;

			$sal_total = $price_unit*$number_months + ($price_unit*$number_months)*$sal_taxes;
			$cli_id    = $request->session()->get('cli_id');
			
			$objContract = new SalContract;
			$objContract->sal_token = $objWSale->sal_token;
			$objContract->con_attach   = 0;
			$objContract->con_card     = 0;
			$objContract->con_status   = 1;
			$objContract->save();			
			
			$objSale = new SalSale;
			$objSale->sal_contract = $objContract->Id;
			$objSale->agency_code = $objWSale->agency_code;
			$objSale->sel_code    = Auth::id();
			$objSale->id_person   = $cli_id;
			$objSale->pro_code    = $objWebPr->pro_code;
			$objSale->sal_token   = $objWSale->sal_token;
			$objSale->sal_dateStart      = $objWSale->sal_date;
			$objSale->sal_numberMounth   = $number_months;
			$objSale->sal_reference      = $reference;
			$objSale->sal_frecuencyCollect  = 'NA';
			$objSale->sal_price_unit        = $price_unit;
			$objSale->sal_taxes     = $sal_taxes;
			$objSale->sal_total     = $sal_total;
			$objSale->sal_status    = 1;
			$objSale->save();
			
			$objNewSale = SalSale::find($objSale->id);
			$objSxk = SalSxk::where('cli_code',$objNewSale->id_person)
							  ->where('kin_code',1)
							  ->where('sal_id',$objNewSale->id)->first();
			
			if(empty($objSxk)){
				$objSxk = new SalSxk;
			}	
			$objSxk->sal_id   = $objSale->id;
			$objSxk->cli_code = $objNewSale->id_person;
			$objSxk->kin_code = 1;
			$objSxk->save();
			
			
			$objMethodPay  = new SalMethodPay;
			$objMethodPay->sal_id = $objSale->id;
			$objMethodPay->smp_typesales     = 2;
			$objMethodPay->ban_code          = $request->banco;
			$objMethodPay->smp_typeaccount   = 0;
			$objMethodPay->smp_numberaccount = '';
			$objMethodPay->smp_reference     = $reference;
			$objMethodPay->smp_value         = $price_unit;
			$objMethodPay->smp_titularname   = $request->titular_tarjeta;
			$objMethodPay->smp_titularsurname   = $request->apellido_cd;
			$objMethodPay->smp_titularidnumber  = '' ;//$request->identificacion;
			$objMethodPay->smp_status        = 1;
			$objMethodPay->save();

			//Billing
			$objBill = new SalBilling;
			$objBill->bil_numberId = $request->id;
			$objBill->bil_name     = $request->nombre;
			$objBill->bil_surname  = $request->apellido;
			$objBill->bil_address  = $request->direccion;
			$objBill->bil_phone    = $request->phone;
			$objBill->bil_email    = $request->email;
			$objBill->sal_id       = $objSale->id;
			$objBill->save();


			
			$request->session()->put('id_sale_fn',$objSale->id);
			$request->session()->put('first_pay',1);
			$this->generateCXC($request);		
		    $url_redirect = '/sales/payment/supscription';
			if($npc>1){
				$url_redirect = '/sales/payment/supscription';
			}
			

			$mensaje      = 'OK';
			$status       =  1;
			
			$response = array(
				'success' 		 => $status,
				'message' 		 => $mensaje,
				'url_redirect' 	 => $url_redirect
			);
			return response()->json($response);	
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}	
	  
  }

	public function saveDataSale($request,$reference,$pay_val=0){
	  
	 // try{
	  
	        $idProduct =  $request->session()->get('id_product');
		    $objWebPr  = webProduct::find($idProduct);		   			
			
			
			
			$id_websale  = $request->session()->get('sale_id');			
			$objWSale    = WebSale::find($id_websale);
			
			$number_months = 12;
			//$reference     = $request->referencia;
			
			$price_unit    = $objWebPr->pro_valor;
			$npc           = $objWebPr->pro_npc; 
			$sal_taxes = 0;

			$sal_total = $price_unit*$number_months;// + ($price_unit*$number_months)*$sal_taxes;
			$cli_id    = $request->session()->get('cli_id');
			
			$objContract = new SalContract;
			$objContract->sal_token = $objWSale->sal_token;
			$objContract->con_attach   = 0;
			$objContract->con_card     = 0;
			$objContract->con_status   = 1;
			$objContract->save();			
			
			$objSale = new SalSale;
			$objSale->sal_contract = $objContract->Id;
			$objSale->agency_code = $objWSale->agency_code;
			$objSale->sel_code    = Auth::id();
			$objSale->id_person   = $cli_id;
			$objSale->pro_code    = $objWebPr->pro_code;
			$objSale->sal_token   = $objWSale->sal_token;
			$objSale->sal_dateStart      = $objWSale->sal_date;
			$objSale->sal_numberMounth   = $number_months;
			$objSale->sal_reference      = $reference;
			$objSale->sal_frecuencyCollect  = 'NA';
			$objSale->sal_price_unit        = $price_unit;
			$objSale->sal_taxes     = $sal_taxes;
			$objSale->sal_total     = $sal_total;
			$objSale->sal_payment_value     = $pay_val;
			
			if($request->session()->has('idCol')){
				$idCol = $request->session()->get('idCol');
				$objSale->sal_prod_col_code     = $idCol;
			}

			$objSale->sal_status    = 1;
			$objSale->save();
			
			$objNewSale = SalSale::find($objSale->id);
			$objSxk = SalSxk::where('cli_code',$objNewSale->id_person)
							  ->where('kin_code',1)
							  ->where('sal_id',$objNewSale->id)->first();
			
			if(empty($objSxk)){
				$objSxk = new SalSxk;
			}	
			$objSxk->sal_id   = $objSale->id;
			$objSxk->cli_code = $objNewSale->id_person;
			$objSxk->kin_code = 1;
			$objSxk->save();
			
			
			$objMethodPay  = new SalMethodPay;
			$objMethodPay->sal_id = $objSale->id;
			$objMethodPay->smp_typesales     = 2;
			//$objMethodPay->ban_code          = $request->ban_code;
			$objMethodPay->smp_typeaccount   = 0;
			$objMethodPay->smp_numberaccount = '';
			$objMethodPay->smp_reference     = $reference;
			$objMethodPay->smp_value         = $price_unit;
			$objMethodPay->smp_titularname   = $request->nombre;
			$objMethodPay->smp_titularsurname   = $request->apellido;
			$objMethodPay->smp_titularidnumber  = $request->id;
			$objMethodPay->smp_status        = 1;
			$objMethodPay->save();
			$request->session()->put('id_sale_fn',$objSale->id);
			
			
			
			//Billing
			$objBill = new SalBilling;
			$objBill->bil_numberId = $request->id;
			$objBill->bil_name     = $request->nombre;
			$objBill->bil_surname  = $request->apellido;
			$objBill->bil_address  = $request->direccion;
			$objBill->bil_phone    = $request->phone;
			$objBill->bil_email    = $request->email;
			$objBill->sal_id       = $objSale->id;
			$objBill->save();
			

			//try{
				$this->enviarEmail($request, $objSale->id);
           // }catch(\Exception $e){
			//	return response()->json(['success'=>0,'message'=>'No fue posible enviar correo a titular']);
			//}			
			return 1;
	     //}catch(\Exception $e){
			
			//return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		//}	
  }

	public function showFormDependents(Request $request){
	$idProduct =  $request->session()->get('id_product');
	    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
	    $arrWebProduct = webProduct::find($idProduct);
	    $precio        = $arrWebProduct->pro_valor;
	    $tax_val       = 0.15;
		
		$id_sale  = $request->session()->get('sale_id');
		$objWSD   = WebSalesDependent::where('id_sales',$id_sale)->get();
		$cmbKinship = DB::SELECT("select * from gen_kinship where lang_code=2 and kin_code!=1 ");
			
		$npc  = $arrWebProduct->pro_npc;
		
		if($npc==0){
			 return view('sales.dependents_detail')
									   ->with('arrWebRanges', $arrWebRanges)
									   ->with('objWSD', $objWSD)
									   ->with('idProduct', $idProduct)
									   ->with('cmbKinship', $cmbKinship)
									   ->with('arrWebProduct', $arrWebProduct)
									   ->with('precio', $precio);
		}elseif($npc>1)	{
			 return view('sales.dependents_detail')
									   ->with('arrWebRanges', $arrWebRanges)
									   ->with('npc', $npc)
									   ->with('idProduct', $idProduct)
									   ->with('cmbKinship', $cmbKinship)
									   ->with('arrWebProduct', $arrWebProduct)
									   ->with('precio', $precio);
		}
		else{
			 return view('sales.payment.final_detail')
						   ->with('arrWebRanges', $arrWebRanges)
						   ->with('idProduct', $idProduct)
						   ->with('npc', $npc)
						   ->with('arrWebProduct', $arrWebProduct)
						   ->with('precio', $precio);
		}	
}

	public function multSave(Request $request){
		DB::beginTransaction();
		try{
			$tot =$request->total;
			$rules = [];
			for($j=1;$j<=$tot;$j++){
				$tmp=array('id.'.$j.'.required'=>'La identificacion del dependiente '.$j.' es requerida');
				$rules=array_merge($rules,$tmp);
			}
			//return response()->json(['success'=>0,'errors'=>$rules]);
			$validator = \Validator::make($request->all(), [
				'nombre' => 'required',
				'id.*' => 'required|max:255',
				'apellido' => 'required',
				'email' => 'required',
				'kinship' => 'required'				
			],$rules);

			//['sc_razon_social.required' => ':attribute  es requerido.']	        
			if ($validator->fails()){
			  return response()->json(['success'=>2,'errors'=>$validator->errors()->all()]);
			}
			
			$idProduct =  $request->session()->get('id_product');
			$objWebPr  = webProduct::find($idProduct);		   			
			
			$id_websale  = $request->session()->get('sale_id');			
			$objWSale    = WebSale::find($id_websale);
			
			$number_months = 12;
			$reference     = 'NA';
			
			$price_unit    = $objWebPr->pro_valor;
			$npc           = $objWebPr->pro_npc; 
			
			$id_sales  = $request->session()->get('id_sale_fn');

			$objSale = SalSale::find($id_sales);
			//CliClient

						
			$data = [];
			
			for($i=0;$i<$tot;$i++){
				if(empty($request->input('id')[$i])){
					continue;
				}
				$ident = $request->input('id')[$i];
				$objCliClient =  CliClient::find($ident);
				if(empty($objCliClient)){
					$objCliClient = new CliClient;
				}
				$nombre = '';
				if(!empty($request->input('nombre')[$i])){
					$nombre = $request->input('nombre')[$i];
				}	
				$apellido = '';
				if(!empty($request->input('apellido')[$i])){
					$apellido = $request->input('apellido')[$i];
				}			

				$kinship = 2;
				if(!empty($request->input('kinship')[$i])){
					$kinship = $request->input('kinship')[$i];
					
				}	

				$birthday = now();
				if(!empty($request->input('fecnac')[$i])){
					$birthday = $request->input('fecnac')[$i];
				}		

				$age = now();
				if(!empty($request->input('edad')[$i])){
					$age = $request->input('edad')[$i];
				}			
				$objCliClient->gen_code = 1;
				$objCliClient->tdo_code = 1;
				$objCliClient->gsc_code = 1;
				$objCliClient->toc_code = 1;
				$objCliClient->cli_parent  = $objSale->id_person;
				$objCliClient->cli_id      = $ident;
				$objCliClient->cli_name    = $nombre;
				$objCliClient->cli_surname = $apellido;
				$objCliClient->cli_birthday = $birthday;
				$objCliClient->cli_age      = $age;
				
				
				$objCliClient->save();
				
				$objSxk = SalSxk::where('cli_code',$objCliClient->cli_code)
								  ->where('kin_code',$kinship)
								  ->where('sal_id',$objSale->id)->first();
				
				if(empty($objSxk)){
					$objSxk = new SalSxk;
				}	
				$objSxk->sal_id   = $objSale->id;
				$objSxk->cli_code = $objCliClient->cli_code;
				$objSxk->kin_code = $kinship;
				$objSxk->save();
					
						/* $data[]= array ('item_id'=>$input[$i]['item_id'],
										 'item_description'=>$input[$i]['item_description']);
										 'units'=>$input[$i]['units']);
										 'rate'=>$input[$i]['rate']);
										 'initial_amount'=>$input[$i]['initial_amount']);*/
			}
				DB::commit();
			$url_redirect = '/sales/preview';
			$status       = 1;
			$mensaje      = 'OK';
			$response = array(
				'success' 		 => $status,
				'url_redirect'   => $url_redirect,
				'message' 		 => $mensaje
			);
			return response()->json($response);		   
		   
		}catch(\Exception $e){
			DB::rollback();
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}
	} // Fin  multSave

	public function showPreview(Request $request){
	  
	  	$idProduct =  $request->session()->get('id_product');
	    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
	    $arrWebProduct = webProduct::find($idProduct);
	    $precio        = $arrWebProduct->pro_valor;
	    $tax_val       = 0.15;
		
		$id_sale  = $request->session()->get('sale_id');
		$objWSD   = WebSalesDependent::where('id_sales',$id_sale)->get();
		
		$idProduct =  $request->session()->get('id_product');
		$objWebPr  = webProduct::find($idProduct);		   			
		
		$id_websale  = $request->session()->get('sale_id');			
		$objWSale    = WebSale::find($id_websale);
		
		$number_months = 12;
		$reference     = 'NA';
	
		
		$id_sales  = $request->session()->get('id_sale_fn');

		$objSale      = SalSale::find($id_sales);	
        $id_titular   = $objSale->id_person;	
        $objCliClient = CliClient::find($id_titular);	
        
		
		
		$sql = "select b.cli_name, b.cli_surname, c.kin_name
				from  sal_sxk a
				left join cli_clients b ON b.cli_code = a.cli_code
				left join gen_kinship c ON c.kin_code = a.kin_code
				where sal_id  = ".$id_sales;
		$objSalSxk =  DB::SELECT($sql);//SalSxk::where('sal_id',$id_sales)->get();		
        $tot_dep =  0;
		if(!empty($objSalSxk)){
			$tot_dep = count($objSalSxk);
		}
		
	  return view('sales.payment.final_detail')
						   ->with('arrWebRanges', $arrWebRanges)
						   ->with('idProduct', $idProduct)
						   ->with('objSale', $objSale)
						   ->with('objCliClient', $objCliClient)
						   ->with('tot_dep', $tot_dep)
						   ->with('objSalSxk', $objSalSxk)
						   ->with('arrWebProduct', $arrWebProduct)
						   ->with('precio', $precio);
  }

	public function cmbCreditCard(Request $request, $id_operadora){
		$status  = 0;
		$mensaje = 'Success';
		if($request->session()->has('lang_id')){
						$lang_id  = $request->session()->get('lang_id');
		}else{
						$lang_id  = 2;
		}
		if($request->session()->has('user_name')){
						$user_name  = $request->session()->get('user_name');
		}else{
						$user_name  = "user";
		}
		// Product
		try{

  			$sql  = "select * from gen_credit_cards where cco_code=?";
			$objCD  = DB::SELECT($sql,[$id_operadora]);
			

			$request->session()->put('cmbCards',$objCD);
			$status = 1;
			$response = array(
				'success' 	 => $status,
				'json_cd' 	 => $objCD,
				'message' 	 => $mensaje
			);
			return response()->json($response);
		}catch(\Exception $e){
						return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}
	}	

	public function supscriptionMethod(Request $request){
		$idProduct =  $request->session()->get('id_product');
	    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
	    $arrWebProduct = webProduct::find($idProduct);
	    $precio        = $arrWebProduct->pro_price;
		$arr_combos    = $this->getListsMethodPay($request);
		$selectBank    = GenFunction::getCmbBankMysql($request,1,'ban_code','B');
	    $fecha_start   =date("M jS, Y", strtotime("2019-01-05"));
		
		$arr_prod=array('valor_gad'=>0);

	    $objform =  $request->session()->get('frm_labels');						   
                               
	   return view('sales.payment.payment_supscription')
									   ->with('arrWebRanges', $arrWebRanges)
									   ->with('objform', $objform)
									   ->with('selectBank', $selectBank)
									   ->with('arr_combos', $arr_combos)
									   ->with('arr_prod', $arr_prod)
									   ->with('fecha_start', $fecha_start)
									   ->with('idProduct', $idProduct)
									   ->with('arrWebProduct', $arrWebProduct)
									   ->with('precio', $precio);		
		
	}
	
	public function testFormAuthDeb(Request $request){
		$idProduct =  $request->session()->get('id_product');
	    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
	    $arrWebProduct = webProduct::find($idProduct);
	    $precio        = $arrWebProduct->pro_price;
		$arr_combos    = $this->getListsMethodPay($request);
		$selectBank    = GenFunction::getCmbBankMysql($request,1,'ban_code','B');
	    $fecha_start   =date("M jS, Y", strtotime("2019-01-05"));
		
		$arr_prod=array('valor_gad'=>0);

	    $objform =  $request->session()->get('frm_labels');						   
                               
	   return view('sales.includes.modalFormDebitCreditCard')
									   ->with('arrWebRanges', $arrWebRanges)
									   ->with('objform', $objform)
									   ->with('selectBank', $selectBank)
									   ->with('arr_combos', $arr_combos)
									   ->with('arr_prod', $arr_prod)
									   ->with('fecha_start', $fecha_start)
									   ->with('idProduct', $idProduct)
									   ->with('arrWebProduct', $arrWebProduct)
									   ->with('precio', $precio);		
		
	}
	
	public function dependentLast(Request $request){
	    $idProduct =  $request->session()->get('id_product');
	    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
	    $arrWebProduct = webProduct::find($idProduct);
	    $precio        = $arrWebProduct->pro_valor;
	    $tax_val       = 0.15;
		
		$id_sale  = $request->session()->get('sale_id');
		$objWSD   = WebSalesDependent::where('id_sales',$id_sale)->get();

		
		if(!empty($objWSD) && count($objWSD)>0)	{
			 return view('payment.dependents_detail')
									   ->with('arrWebRanges', $arrWebRanges)
									   ->with('objWSD', $objWSD)
									   ->with('idProduct', $idProduct)
									   ->with('arrWebProduct', $arrWebProduct)
									   ->with('precio', $precio);
		}else{
			 return view('sales.payment.final_detail')
						   ->with('arrWebRanges', $arrWebRanges)
						   ->with('idProduct', $idProduct)
						   ->with('arrWebProduct', $arrWebProduct)
						   ->with('precio', $precio);
		}	
                               
	  
   }
   
	public function validateStateP2P(Request $request){

   		try{
	   	     $requestId = $request->session()->get('requestId');
	   		 $resArr = GenFunctionP2P::responseSessionP2P($requestId,$request);
	   		 $msg = 'NO SE PUDO OBTENER INFORMACION DE PLACE TO PAY';
	   		 $url_redirect = '';
	   		 $status_r       = 1;
	   		 //$resArr['status']['status']=='OK'
	   		 if(!empty($resArr['status']['status']))
	   		 {
	   		 	$status  = $resArr['status']['status'];

	   		 	switch ($status) {
	   		 		case 'APPROVED':

						$idProduct =  $request->session()->get('id_product');
					    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
					    $arrWebProduct = webProduct::find($idProduct);
					    $precio        = $arrWebProduct->pro_valor;
					    $tax_val       = 0.15;
						$id_sale       = $request->session()->get('sale_id');
						$objWSD        = WebSalesDependent::where('id_sales',$id_sale)->get();
						
						$msg = 'TRANSACCION APROBADA';
						//$url_redirect= '/sales/preview';
						$url_redirect= '/sales/payment/supscription';

						$request->session()->put('aplica_p2p',1);
						
						//if(!empty($objWSD) && count($objWSD)>0)	{
						//		$url_redirect= '/sales/dependents';
						//}

						$status_r       = 1;
	   		 			break;
					case 'REJECTED':
					     $status       = 0;
						 $status_r       = 1;
	   		 			 $msg = 'TRANSACCION RECHAZADA';
	   		 			break;      		 					 		
	   		 		default:
	   		 		    $status       = 0;
						$status_r       = 1;
	   		 			$msg = 'TRANSACCION PENDIENTE';
	   		 			break;
	   		 	}


	   		 }//end if
	   		 

 			
			$mensaje='OK';
			$response = array(
				'success' 		 => $status_r,
				'message' 		 => $msg,
				'url_redirect' 	 => $url_redirect
			);
			return response()->json($response);		   
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}  		 



   }

	public function redirectNextStepP2p(){
   				$idProduct =  $request->session()->get('id_product');
			    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
			    $arrWebProduct = webProduct::find($idProduct);
				$id_sale  = $request->session()->get('sale_id');
				$objWSD   = WebSalesDependent::where('id_sales',$id_sale)->get();
		 		$msg = 'TRANSACCION APROBADA';


	 			if(!empty($objWSD) && count($objWSD)>0)	{
					return view('sales.dependents_detail')
					   ->with('arrWebRanges', $arrWebRanges)
					   ->with('idProduct', $idProduct)
					   ->with('arrWebProduct', $arrWebProduct)
					   ->with('precio', $precio);

	 			}else{

					return view('sales.certificate_prev')
					   ->with('arrWebRanges', $arrWebRanges)
					   ->with('idProduct', $idProduct)
					   ->with('arrWebProduct', $arrWebProduct)
					   ->with('precio', $precio); 		 				
	 			}


   }
   
	/**
	* Quote the selected product.
	*
	* @return \Illuminate\Http\Response
	*/
    public function paySel(Request $request){
        //
		$request->session()->forget('aplica_p2p');
		$idProduct =  $request->session()->get('id_product');
	    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
	    $arrWebProduct = webProduct::find($idProduct);
	    $precio        = $arrWebProduct->pro_price;
		$arr_combos    = $this->getListsMethodPay($request);
		$selectBank    = GenFunction::getCmbBankMysql($request,1,'ban_code','B');
	
	    $objform =  $request->session()->get('frm_labels');		

		$arr_edit = array();
		if($request->session()->has('id_sale_fn')){
			$id_sale_fn          = $request->session()->get('id_sale_fn');
			$arr_edit['objSale'] = SalSale::find($id_sale_fn);
			$arr_edit['objBilling'] = SalBilling::where('sal_id',$sal_id)->first();
		}
                               
	   return view('sales.payment.payment_select')
									   ->with('arrWebRanges', $arrWebRanges)
									   ->with('objform', $objform)
									   ->with('selectBank', $selectBank)
									   ->with('arr_combos', $arr_combos)
									   ->with('idProduct', $idProduct)
									   ->with('arr_edit', $arr_edit)
									   ->with('arrWebProduct', $arrWebProduct)
									   ->with('precio', $precio);
    } // detailWebProduct

	public function getListsMethodPay($request){
		$lang_id = 2;
		
		$sql = "SELECT a.*
                FROM	 gen_typepayment a
                WHERE	lang_code = ".$lang_id;
        $cmb_tipo_pago = DB::SELECT($sql);		
		$default_m = 0;
		if($cmb_tipo_pago){
		  $default_m = $cmb_tipo_pago[0]->id;
		}
		

		$sql = "SELECT a.*
                FROM	 gen_methodpay a
                WHERE	lang_code = ".$lang_id." and pmt_method_code = ".$default_m;
        $cmb_forma_pago = DB::SELECT($sql);	

		$sql = "SELECT a.*
                FROM	 gen_credit_cards_operators a
                ";
        $cmb_operadora = DB::SELECT($sql);	
		$default_tipt = 0;
		if($cmb_operadora){
		  $default_tipt = $cmb_operadora[0]->cco_code;
		}		
		
		$sql = "SELECT a.*
                FROM	 gen_credit_cards a
                where cco_code = ".$default_tipt;
		$sql = "SELECT a.*
                FROM	 gen_credit_cards a
                ";				
        $cmb_tipo_tarjeta = DB::SELECT($sql);	
		
		$sql = "SELECT a.*
                FROM	 gen_bank_account a
                where lan_code = ".$lang_id;
        $cmb_tipo_cuenta = DB::SELECT($sql);	

		$sql = "SELECT a.*
		FROM	 gen_bank a";
        $cmb_banco = DB::SELECT($sql);

		
		//tipo de documento
		
		$sql = "SELECT a.*
                FROM	 gen_typedocument a
                WHERE	tdo_status = 1 and tdo_code in(1,2,9)";
        $cmb_typedocument = DB::SELECT($sql);	
		
        $arr_combos  = array();
		$arr_combos['cmb_tipo_pago']    =$cmb_tipo_pago;
		$arr_combos['cmb_forma_pago']   =$cmb_forma_pago;
		if($request->session()->has('cmbMethodPay')){
			$arr_combos['cmb_forma_pago']   =$request->session()->get('cmbMethodPay');
		}
		
		//states dependent
		//$objStateDep = GenStateDependent::where('lang_code',$lang_id)->get();
		
		
		$arr_combos['cmb_operadora']    =$cmb_operadora;
		$arr_combos['cmb_tipo_tarjeta'] =$cmb_tipo_tarjeta;
		$arr_combos['cmb_tipo_cuenta']  =$cmb_tipo_cuenta;	
		$arr_combos['cmb_banco']        =$cmb_banco;
		$arr_combos['cmb_typedocument'] =$cmb_typedocument;
		//$arr_combos['cmb_statedep']     =$objStateDep;	
		return $arr_combos;
	}

	/**
	* Send pdf by email
	*
	* Request request
	* @return
	*/
	public function enviarEmail($request, $id_sale){
		$lang_id=1;
	    $k=0;
        $emails[$k] = 'adominguez@memorialtechnologies.com';
        //$emails[$k+1] = $arr_tmp_titular['email_tit'];
		$nombre   = 'NombreTitular';
		$apellido = 'ApellidoTitular';
        $cid ='blackstone';
		$email_msg_body   ='Gracias por contratar nuestros servicios. Le recordamos que la cobertura de la Asistencia contratada entrara en vigencia
		                  en el momento que su primer pago sea recibido y validado';		
						  
		$email_msg_footer ='Gracias por confiar en nosotros';				  
        Mail::send('layouts.first_sale', ['id_sale' => $id_sale,  'email_msg_body'=>$email_msg_body,'nombre'=>$nombre,'apellido'=>$apellido,'email_msg_footer'=>$email_msg_footer], 
			function ($message) use ($emails){
						$message->from('noreply@memorialtechnologies.com', 'Memorial Ecuador');
						// $message->to($emails)->subject($objform->sal_title_subject_email);
						$message->to($emails)->subject('Asitencia Contratada!!');
						//$message->attachData($snappy->getOutputFromHtml($html), $pdf_name);
			});
	} // Fin enviarEmailPoliza

	/**
	* Send pdf by email
	*
	* Request request
	* @return
	*/
	public function enviarEmailP2PUpdate($request, $id_sale){
		$lang_id=1;
	    $k=0;
        $emails[$k] = 'adominguez@memorialtechnologies.com';
        //$emails[$k+1] = $arr_tmp_titular['email_tit'];
		$nombre   = 'NombreTitular';
		$apellido = 'ApellidoTitular';
		$contrato = '';
		
        $cid ='blackstone';
		
		$sql = "select a.sal_contract, a.sal_token, cli_name,cli_surname,cli_id
						from sal_sales a, cli_clients b
						where a.id_person = b.cli_code and a.id= ".$id_sale;
		$objCliSale = DB::SELECT($sql);	
		$nombre   = $objCliSale[0]->cli_name;
		$apellido = $objCliSale[0]->cli_surname;
		$contrato = $objCliSale[0]->sal_contract;
		
		$email_msg_body   ='Le informamos que el cliente '.$nombre.' '.$apellido.', con contrato '.$contrato.' ha excedido el numero permitido de rechazos desde Place to Pay en cobros recurrentes. 
		                    Ya no podran enviarse mas cobros hasta que actualice la tarjeta';		
						  
		$email_msg_footer ='Gracias por confiar en nosotros';				  
        Mail::send('layouts.first_sale', ['id_sale' => $id_sale,  'email_msg_body'=>$email_msg_body,'nombre'=>$nombre,'apellido'=>$apellido,'email_msg_footer'=>$email_msg_footer], 
		          function ($message) use ($emails)
        {
            $message->from('noreply@memorialtechnologies.com', 'Memorial Ecuador');
           // $message->to($emails)->subject($objform->sal_title_subject_email);
		    $message->to($emails)->subject('Asitencia Contratada!!');
			//$message->attachData($snappy->getOutputFromHtml($html), $pdf_name);
        });
	} // Fin enviarEmailPoliza

    public function previousPage(Request $request,$op=1){
		switch ($op) {
			case 1:
				if(\Auth::user()){
					return redirect('/home');
				}else{
					return redirect('/');
				}
				
				break;
			case 2:
					$idProduct = $request->session()->get('id_product'); 
					$arrWebProduct = webProduct::find($idProduct);
					
					return redirect($arrWebProduct->pro_linkweb);
				break;
			case 3:
					return redirect('/payment/pay_sel');
				break;
		}

	} // Fin previousPage

	/*
	*	Muestra los columbarios disponibles para la venta
	*	Show the columbariums available for sale
	*
	*	@return \Illuminate\Http\Response
	*/
	public function getColumbariums(Request $request, $cod=0, $typeAjax=0){
		try {
			$cod  = $cod.'],A16';

			$fecha = GenFunction::getDateDB();
			Storage::prepend('log_columbarium.log',"****************");
			Storage::prepend('log_columbarium.log',"Columbariums: Inicio de busqueda");
			Storage::prepend('log_columbarium.log',"Fecha: ".$fecha.", Codigo: ".$cod);
			Storage::prepend('log_columbarium.log',"****************");
			// API para obtener los columbarios segun un piso
			// API to obtain columbariums according to a floor
			$attr = "id=".$cod."&datos=";
            $consultResp = GenFunction::procesarApiGen(50, 1, "LIST_COLUMBARIUM", 1, $attr, "L");
			//dd($consultResp);
			$newArray = array();
			if(!empty($consultResp) && is_array($consultResp)){
				foreach($consultResp as $resp){
					if($resp['estado'] == 'BLANCO-DISPONIBLE') $newArray[] = $resp;
				} // Fin foreach
			} // Validacion $consult
			$numbReg = count($newArray);
            Storage::prepend('log_columbarium.log',"Numero de registros: ".$numbReg);
			$listfields = "codigo,descripcion,piso,sector,fila,columna,tipo,contrato,costocero,ventaspor,estado,estadoubicacion,subcategoria";
			$tabla = GenFunction::getDTArraySel($listfields, 1, $newArray, "IdDT", 2, 23,'RAD','codigo');
			
			$fecha = GenFunction::getDateDB();
			Storage::prepend('log_columbarium.log',"****************");
			Storage::prepend('log_columbarium.log',"Fecha: ".$fecha.", Piso: ".$cod);
			Storage::prepend('log_columbarium.log',"Columbariums: Fin de busqueda");
			Storage::prepend('log_columbarium.log',"****************");
			
			if($typeAjax == 1){
				return response()->json(['success' => 1,'message' => 'Ok', 'tabla' => $tabla ]);
			}else{
				return $tabla;
			}

		}catch (Exception $e){
			if($typeAjax == 1){
				return response()->json(['success' => 'error','message' => $e->getMessage()]);
			}else{
				return $e->getMessage();
			}
		}
	} // Fin showColumbariums

	/*
	*	Muestra los columbarios disponibles para la venta
	*	Show the columbariums available for sale
	*
	*	@return \Illuminate\Http\Response
	*/
	public function showColumbariums(Request $request,$idProduct=''){
		$lang_id   = $request->session()->get('lang_id');	
		$objlabels = GenFunction::getLabels(2, 23);
		
		$arrWebProduct = webProduct::find($idProduct);
		$precio = $arrWebProduct->pro_valor;		
		
		$sql ="select * from gen_param where par_name='min_price_columbario'";
		$objParMin = DB::SELECT($sql);
		$precio_min = $objParMin[0]->par_value;
		// Valor por defecto del piso
		$strPiso = "1";
		// API para obtener los PISOS / API to obtain the FLOORS
		$floors = GenFunction::procesarApiGen(50, 1, "LIST_PRODUCTOS", $request->session()->get('user_code'), "", "");
		if(!empty($floors)){
			$cmbFloors = "<select id='cmbFloors' name='cmbFloors' onchange='selectCmbFloors()' >";
			$cmbFloors .= "<option value='0'>".$objlabels->lbl_select_record."</option>";
			foreach($floors as $floor){
				if($strPiso == $floor['codigo']) $selected = "selected";
				else $selected = "";
				$cmbFloors .= "<option value='".$floor['codigo']."' ".$selected." >".$floor['descripcion']."</option>";
			} // Fin $floors
			$cmbFloors .= "</select>";
		}else {
			$cmbFloors = "<select id='cmbFloors' name='cmbFloors' >";
			$cmbFloors .= "<option value='0'>".$objlabels->lbl_select_record."</option>";
			$cmbFloors .= "</select>";
		} // exist $floors
		
		$dataTable = $this->getColumbariums($request, $strPiso, 0);
		$DTables = "";
		
		return view("sales.columbarium")
				->with('dataTable', $dataTable)
				->with('objlabels', $objlabels)
				->with('cmbFloors', $cmbFloors)
				->with('DTables', $DTables)
				->with('precio', $precio)
				->with('precio_min', $precio_min)
				->with('Nbnk', 7)
				->with('delimiter', ',')
				->with('colvalue', 7);
		
	} // Fin showColumbariums
	
}