
	<section>
	<?php

		$cmb_typedocument         =  $arr_combos['cmb_typedocument'];
	
	?>



	<div class="container1">
	<div class="items" id='content_form'>


						<div class="contenedor_mt">

						<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
						<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
						
						<style scoped>
						/*
						.contenedor_mt {
						    padding: 10px;
						    width: 98%;
						    margin: auto;
						    background-color: #F3EADD;
						}*/
						#frmMT{
							width:90%;
							margin:auto;
							
							border:solid 1px  rgb(56, 30, 61,0.9);
							/*border-radius:5px;*/
							margin-top:20px;
							margin-bottom:20px;
							padding:20px;
							/*-webkit-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
							-moz-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
							box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);*/
						 background-color:white;
							color:white;
						}
						.row {
						    display: -webkit-box;
						    display: -ms-flexbox;
						    
							width:100%;
						    -ms-flex-wrap: wrap;
						    flex-wrap: wrap;
						    margin-right: -15px;
						    margin-left: -15px;
						}

						.row_n{
							display:table;
							width:100%;
						}

						.tbl{
							display:table;
							width:100%;	
							
						}
						.rw{
							display:table-row;
							width:100%;
						}

						.col{
						    position: relative;
						    width: 50%;
						    min-height: 1px;
						    padding-right: 15px;
						    padding-left: 15px;
							
							display:table-cell;
						}


						.col2{
						    position: relative;
						    width: 50%;
						    min-height: 1px;
						    padding-right: 15px;
						    padding-left: 15px;
							
							display:table-cell;
						}

						.form-control {
						    display: block;
						    width: 100%;
						    padding: .375rem .75rem;
						    font-size: 1rem;
						    line-height: 1.5;
						    color: #495057;
						    background-color: #fff;
						    background-clip: padding-box;
						    border: 1px solid #ced4da;
						    border-radius: .25rem;
						    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
						}


						.form-control {
						    padding: 2px;
						    height: 28px;
						    border: 1px solid #dddddd;
						}

						h2{
							font-family: "Rubik",Helvetica,Arial,sans-serif;
							font-size:2em;
							font-weight:500;
						}

						.col_slim{
							width:10%;
						}

						.col_add{
							width:25%;
						}

						.col_minus{
							width:25%;
						}

						.col_range{
							width:25%;
						}

						.col_50{
							display:table-cell;
							width:50%;
						}
						.col_cont{
							border:solid 1px;
							border-radius:5px;
						}


						label{
							color:rgb(56, 30, 61,0.9);
						}

						.cs_footer_title{
							color:rgb(56, 30, 61,0.9);
							font-size:2em;
							font-weight:bold;
						}

						.cs_footer_text{
							color:black;
						}

						#footer_form{
							background-color:#E8DDF6;
							padding:20px;
							width:100%
						}



						.tbl_2{
							display:table;
							padding:20px;
							width:80%;
						    margin:auto;	
							
						}



						.cs_head_title{
							color: black;
							font-weight: bold;
						}


						@media (max-width: 767px) {

							.text-xxl {
								font-size: 50px !important;
								line-height: 60px !important;
							}

						h1{
								 text-indent:-9999px;
								font-size:0px;
								position:absolute;
							}

							h2, .text-l {
								margin-top:20px;
								font-size: 1.5em !important;
								line-height: 1em !important;
								
							}

							h3 {
								font-size: 18px !important;
								line-height: 28px !important;
							}
							
							
							#frmMT{
								
								width:98%;
								maring:auto;
								padding:5px;
							}
							
							.row{
										display:table;
								width:100%;
							}
							.col{
								display:table-row;
								width:100%;
							}


						.contenedor_mt {
						    padding: 0px;
						    width: 100%;

						}


						.res_input{
							width:40px;
							height:20px;
						}

						.cab_r{
							padding-left:20px;
						}

						.col_add{
							width:25%;
						}

						.col_minus{
							width:25%;
						}

						.col_range{
							width:25%;
						}


						}
						
						
						#dv_terms_cond{
							display:none;
						}
						
						.cs_link_dash_2{
						   color:#956A2D;
							font-weight:bold;	
                           text-decoration:underline;							
						}
						</style>


							<center><h3><span class="cs_head_title">Detalle de Cobros Recurrentes</span></h3></center>

							<form id="frmMT">
								{{ csrf_field() }}	


								<!-- Titular -->
						<div class="tbl">		
							<div class="row cab_r rw">
							  <div class="col col_50">
										<div class="tbl">
										<div class="row cab_r rw">
											<div class="col col_slim">
												<div class="form-group">
													<input type="radio" class="form-control" id="paysel"  name="paysel" value="1" />
												</div>
											</div>
											<div class="col ">
												<div class="form-group">
													<img src="/img/p2p/Logo_PlacetoPay.png" width="300px" height="60px;" alt="place to pay" />
												</div>
											</div>
										</div>
														

										<div class="row cab_r rw">
											<div class="col col_slim">
												<div class="form-group">
													<input type="radio" class="form-control" id="paysel"  name="paysel" value="2" />
												</div>
											</div>
											<div class="col ">
												<div class="form-group">
													<span class="cs_head_title">Formulario Autorizaci&oacute;n de D&eacute;bito</span>
												</div>
											</div>
										</div>
										
								

							



										<div class="row cab_r rw" id="dv_terms_cond">
												<div class="col col_slim">
													<div class="form-group">
														<input type="checkbox" class="form-control" id="terminos" style="min-width: 10px" />
													</div>
												</div>
												<div class="col col_90">
													<div class="form-group">
														<a href="javascript:showTerms();" class="cs_link_dash_2">Aceptar Terminos y Condiciones</a>
														<br>
														<img src="/img/p2p/mastercard.png" width="70px" height="60px" alt="LOGO MASTERCARD" />
														<br>
														<img src="/img/p2p/logosEC.png" width="500px" height="60px" alt="logos VISA, DINNERS CLUB" />

														
													</div>
											
											     </div>
										</div>		
														
										
										</div>
								
							  </div>	
							  
							    <div class="col col_50 col_cont">
										<h3><span class="cs_footer_text">Datos de recurrencia de pago</span></h3>
										<div class="tbl">
											
											
											<div class="row cab_r rw">
												<div class="col col_50">
													<div class="form-group">
														<label for="period_pago">Periodicidad Cobro</label>
														
													</div>
												</div>
												<div class="col col_50">
													<div class="form-group">
														<select name="period_pago" id="period_pago" class="form-control">
															<option value="1">Dia 1</option>
															<option value="15">15</option>
															<option value="30">30</option>
																														
														</select>
													</div>
												</div>
											</div>		
											
											
											<div class="row cab_r rw">
												<div class="col col_50">
													<div class="form-group">
														<label for="id">Copiar datos de Titular:</label>
														<input type="checkbox" class="form-control" id="chkCopia"  name="chkCopia"  />
													</div>
												</div>
												<div class="col col_50">
													<div class="form-group">
														<label for="email">Correo Electrónico:</label>
														<input type="email" class="form-control" id="email"  name="email" />
													</div>
												</div>
											</div>		
											<div class="row cab_r rw">
												<div class="col col_50">
													<div class="form-group">
														<label for="nombre">Nombre:</label>
														<input type="text" class="form-control" id="nombre" name="nombre" />
													</div>
												</div>
												<div class="col col_50">
													<div class="form-group">
														<label for="apellido">Apellido:</label>
														<input type="text" class="form-control" id="apellido"  name="apellido" />
													</div>
												</div>
											</div>					
											<div class="row cab_r rw">
												<div class="col col_50">
													<div class="form-group">
														<label for="id">Tipo Identifaci&oacute;n:</label>
														<select id="tipo_id" name="tipo_id" class="form-control">
														    @foreach($cmb_typedocument as $rw)
																<option value="{{$rw->tdo_code}}">{{$rw->tdo_name}}</option>
															@endforeach
															
														</select>
													</div>
												</div>
												<div class="col col_50">
													<div class="form-group">
														<label for="id">Identifaci&oacute;n:</label>
														<input type="text" class="form-control" id="id" name="id" />
													</div>
												</div>
											</div>
											
											<div class="row cab_r rw">
												<div class="col col_50">
													<div class="form-group">
														<label for="id">Celular:</label>
														<input type="text" class="form-control" id="phone" name="phone" />
													</div>
												</div>
												<div class="col col_50">
													<div class="form-group">
														<label for="email">Direcci&oacute;n:</label>
														<input type="text" class="form-control" id="direccion" name="direccion" />
													</div>
												</div>
											</div>											
											
											<div class="row cab_r rw">
												<div class="col col_50">
													<div class="form-group">
														<button id="btnRegresar" class="btn btn-default">Regresar</button>
													</div>
												</div>
												<div class="col col_50">
													<div class="form-group">
														<button id="btnNextStage" name="btnNextStage" class="btn btn-default">Continuar</button>
													</div>
												</div>
											</div>
										</div>
							  </div> 
						    </div>
						  </div>	
							</form>
							
							
							<br>


							
						</div>



	</div>
	</div>
	</section>


	@include('sales.includes.modalP2P');
    @include('sales.includes.modalTerms');

<script>
	$(function(){
	
	        console.log('SUBSCRIPCION');
			$('form').submit(false);
			
			$("#btnRegresar").click(function( event ) {
				event.preventDefault();
				location.reload();
				
			});
				


						
				$.validator.addMethod("character", validateCharacter);
				$.validator.addMethod("numbers", validateNumbers);
				
				jQuery.validator.addMethod("accept", function(value, element, param) {
				  return value.match(new RegExp("." + param + "$"));
				});
			$('#frmMT').validate({
					rules: {
						nombre: { 
							required: true,
							character:true,
							minlength: 4,
                             accept: "[a-zA-Z]+" 
						},
						apellido: { 
							required: true,
							character:true,
							minlength: 4,
                             accept: "[a-zA-Z]+" 
						},
						id: { 
							required: true,
							character:true
						},
                        phone: {
							required: true
						}						
					},
					messages: {
						nombre: {
							required: 'Se requiere este campo.',
							character: 'No se aceptan caracteres especiales.',
							minlength: 'Debe ingresar al menos 4 caracteres',
							accept: 'No se admiten numeros'
						},
						apellido: {
							required: 'Se requiere este campo.',
							character: 'No se aceptan caracteres especiales.',
							minlength: 'Debe ingresar al menos 4 caracteres',
							accept: 'No se admiten numeros'							
						},
						id: {
							required: 'Se requiere este campo.',
							character: 'No se aceptan caracteres especiales.'
						},
						phone: {
							required: 'Se requiere este campo.'
							
						}					
					},
					onfocusout: false,
					errorElement: 'div',
					invalidHandler: function(form, validator) {
						var errors = validator.numberOfInvalids();
						if (errors) {                    
							validator.errorList[0].element.focus();
						}
					},
					highlight: function (element) { 
						$(element)
						.closest('.form-group')
						.addClass('has-error'); 
					},
					errorPlacement: function(error, element){
						error.appendTo(element.parent());
					},
					success: function (element) {
						element
						.closest('.form-group')
						.removeClass('has-error');
						element.remove();
					},
					submitHandler: function(form){
						
					}
			});
		
		

			P.on('response', function(data) {
				console.log(data);
				
				if(data.status.status=="APPROVED"){
					/*alertPPYA('Su subscripcion con referencia '+data.request.subscription.reference+' , ha sido Aprobada');*/
					alertPPYA('Su subscripcion con referencia '+data.reference+' , ha sido Aprobada');
					var messg = 'Su subscripcion con referencia '+data.reference+' , ha sido Aprobada';
					savePlaceToPay(messg);
				}else if(data.status.status=="PENDING"){
					/*alertPPYA('Su peticion esta en estado Pendiente, por favor vuelva a tratar de nuevo en 5 minutos');*/
					alertPPYA('Su subscripcion con referencia '+data.reference+' , esta PENDIENTE, para poder proseguir con el proceso intente de nuevo');
					return;
				}else{
					/*alertPPYA('Su peticion ha sido rechazada, por favor ingrese otra tarjeta o seleccione un metodo de pago diferente');*/
					alertPPYA('Su subscripcion con referencia '+data.reference+' , ha sido RECHAZADA');
					return;
				}
				/*$("#lightbox-response").html(JSON.stringify(data, null, 2));*/
			});				
			
			$("#btnNextStage").click(function( event ) {
				
				event.preventDefault();
				console.log('Opening....');

				var rdSel  = $("input:radio[name ='paysel']:checked").val();
				if(rdSel==1){
					@if(Session::has('requestId'))
						savePlaceToPay('');
					@else
						if(!$("#terminos").is(':checked')){
							alert('Es necesario aceptar los terminos y condiciones para continuar');
							return;
						}					
						validarPlaceToPay();
					@endif	
				}else if(rdSel==2){
				    $('#myModalDebitCreditC').modal({backdrop: 'static', keyboard: false});  
					var width = $(window).width()-100;
					var height = $(window).height()-100;
					$('#myModalDebitCreditC').css('height',height+'px');
					$('#myModalDebitCreditC').css('width',width+'px');
					
					$('.modal-dialog').css('height',height+'px');
					$('.modal-dialog').css('width',width+'px');					
					
					
					$('#myModalDebitCreditC').modal('show');
				}else if(rdSel==3){
				    $('#myModalCreditC').modal({backdrop: 'static', keyboard: false});  
					$('#myModalCreditC').modal('show');
				}
		
				
				
			});
			
			
			$("#chkCopia").change(function( event ) {
				if($("#chkCopia").is(':checked')){
					recuperarDatosFactura();
				}
			
				
			});
			
		   @if(!Session::has('requestId'))	
			$("input:radio[name ='paysel']").click(function( event ) {
				
				console.log('paysel');
				var rdSel  = $("input:radio[name ='paysel']:checked").val();
				console.log(rdSel);
				if(rdSel==1){
					
					$("#dv_terms_cond").show();	
					$("#dv_terms_cond").prop('style',"display:table-row");
				
					
				}else{
					
					$("#terminos").prop('checked', false);
					$("#dv_terms_cond").hide();
				}
				
				
			});				
		   @endif	



			$("#chkCopia").change(function( event ) {
				if($("#chkCopia").is(':checked')){
					recuperarDatosFactura();
				}
			
				
			});		   
		
	
	});
	
	
	function recuperarDatosFactura(){
				event.preventDefault();
				
				var request = $.ajax({
					type:	'GET',
					url:	'/payment/get_data_client',
					success:function(data){
						console.log(data);
						if(data.success==1){
							
							$('#nombre').val(data.data_person.nombre);
							$('#apellido').val(data.data_person.apellido);
							$('#id').val(data.data_person.id);
							$('#email').val(data.data_person.email);
							$('#direccion').val(data.data_person.direccion);
							
							return;
						}
					}
				});
				request.fail(function( jqXHR, textStatus ) {
					alert( "Hubo un problema: " + textStatus );
					console.log(jqXHR);
				});				
	}


	function savePlaceToPay(messg){
		console.log('savePlaceToPay');
	    var form = $('#frmMT');
		var request = $.ajax({
			type:	'POST',
			url:	'/p2p/save_recurrent',
			data:	form.serialize(),
			success:function(data){
				console.log(data);
				if(data.success==1){
					alertPPYA(messg+'<br> Sus cambios han sido guardados satisfactoriamente');
					/*$('#seccion_detalle_planes').load(data.url_redirect);	*/
					return;
				}else{

					alertPPYA(data.message);
					console.log(data);
					return;
				}
			}
		});
		request.fail(function( jqXHR, textStatus ) {
			alertPPYA( "Hubo un error: " + textStatus );
			console.log(jqXHR);
		});		
	}

	function validarPlaceToPay(){

	    var form = $('#frmMT');
		var request = $.ajax({
			type:	'POST',
			url:	'/payment/place_to_pay_val',
			data:	form.serialize(),
			success:function(data){
				console.log(data);
				if(data.success==1){
						/*$.ajax({
						  url: data.url_process.processUrl,
						  headers: {  'Access-Control-Allow-Origin': '*' },
						  type:'GET',
						   beforeSend: function(xhr){xhr.setRequestHeader('Access-Control-Allow-Origin', '*');},     
						  }).done(function(data) {
						     $('#body_mod_p2p').html(data);
						});*/

							P.init(data.url_process.processUrl);
						  var object =     '<object type="text/html" width="100%" height="900px" id="frame_id" data="'+data.url_process.processUrl+'">'+
        '              <p>Si puede leer este mensaje es que su navegador no soporta correctamente el elemento <code>object</code></p>'+
        '           </object>';

								/*$('#body_mod_p2p').empty();
              

			              $(object).appendTo('#body_mod_p2p').ready(function(){

			                  });*/

						/*$('#body_mod_p2p').load(data.url_process.processUrl);	*/
						
						
						/*$('#myModalP2P').modal({backdrop: 'static', keyboard: false})  
						$('#myModalP2P').modal('show');*/
										
					return;
				}else{

					alertPPYA(data.message);
					console.log(data);
					return;
				}
			}
		});
		request.fail(function( jqXHR, textStatus ) {
			alertPPYA( "Hubo un error: " + textStatus );
			console.log(jqXHR);
		});
    }


	function showTerms(){
		console.log('showing terms');
			$('#myModalTerms').modal({backdrop: 'static', keyboard: false})  
			$('#myModalTerms').modal('show');
	}	
</script>

