@extends('home.home_page_mtpv_2')

@section('content')
	<div class="content2 " style="">
	<style scoped>
		.cs_tb{
			display:table;
			width:90%;
		}
		.cs_row{
			display:table-row;
			padding-top:25px;
			margin-top:15px;
		}
		.col_3{
			display:table-cell;
			width:30%;
			margin-right:15px;
			padding-right:25px;
			padding-bottom:25px;
		}
		
		#showResult>table{
			color:black;
			background:white;
		}
		
		#showResult>table>thead>tr>th{
			color:black;
			background-color:#f2f2f2;
		}		
	</style>
		<div class="row ">
			<div id="5b1PZ" class="hc_column_cnt col-md-12" style="" >
				<div class="row">
					<div class="col-md-12 hc_space_cnt"><hr class="space l" /></div>
				</div>
			</div>
	
			<div id="5xNL6" class="hc_column_cnt col-md-12" style="" >
				<div class="row">
					<div class="col-md-12 hc_space_cnt"><hr class="space xs"  />
					</div>
				</div>
			</div>
	
			<div id="column_Q1bd4" class="hc_column_cnt col-md-8  col-center" style="" >
				<div class="row">
					<!-- Formulario de Busqueda -->
					<form class="cs_tb">
						<div class="form-row cs_row">
							<div class="col_3">
								<input id="searchid" name="searchid" type="text" class="form-control" placeholder="{{$objlabels->cli_search_Id}}" />
							</div>
							<div class="col_3">
								<input id="searchname" name="searchname" type="text" class="form-control" placeholder="{{$objlabels->cli_search_Name}}" />
							</div>
							<div class="col_3">
								<input id="searchassist" name="searchassist" type="text" class="form-control" placeholder="{{$objlabels->cli_search_Assist}}" />
							</div>
						</div>
						<div class="form-row cs_row">
							<div class="col_3">
								
							</div>						
							<div class="col_3">
								
							</div>
							<div class="col_3">
								<button id="searchreset" name="searchreset" class="btn btn-secondary btn-sm">{{$objlabels->cli_search_Reset}}</button>
								
								<button id="searchsubmit" name="searchsubmit" class="btn btn-primary btn-sm">{{$objlabels->cli_search_Submit}}</button>
							</div>

						</div>
					</form>
				</div>
				
				<div class="row" id="showResult"></div>
				
			</div>
		</div>
	</div>
	
	<script>
		function getAssist(){
			
		} // Fin getAssist
		
		$(function(){
			$('#searchsubmit').click(function(event){
				event.preventDefault();
				var searchid = $("#searchid").val();
				var searchname = $("#searchname").val();
				var searchassist = $("#searchassist").val();
				
				if((searchid != "") || (searchname != "") || (searchassist != "")){
					//alertPPYA("....Buscando....");
					var request = $.ajax({
						type:	'GET',
						url:	'/client/search',
						data:	{'searchid': searchid, 'searchname': searchname, 'searchassist': searchassist},
						success:function(data){
							console.log(data);
							if(data.success == 1){
								$("#showResult").html(data.table);
								return;
							}
						}
					});
					request.fail(function(jqXHR, textStatus){
						alert( "Hubo un problema: " + textStatus );
						console.log(jqXHR);
					});
				}else{
					alert("{{$objlabels->cli_search_input}}");
					$("#searchid").focus();
					return;
				}
			});
		});
	</script>
	
@stop	