<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\GenFunction;
use App\GenLanguage;
use App\webProduct;
use App\WebRanges;
use App\WebPerson;
use App\WebSale;
use App\WebSalesDependent;
use App\GenFunctionP2P;
use App\BtnP2pParam;
use App\BtnP2pTran;
use App\SalContract;
use App\SalSale;
use App\SalMethodPay;
use App\SalMethodPaySubscription;
use App\CliClient;
use App\SalSxk;
use App\SalBilling;
use Auth;
use App\GenApiMaster;
use App\BtnP2pRecurrent;
use Mail;

use App\GenModuleWeb;
use App\GenMethodpay;

class TmpSalesController extends Controller{
    //
	/**
	 *	Method that creates the tags
	 *
	 *	@param  Request  $request
	 *	@return \Illuminate\Http\Response
	*/
    public function createLabels($request){
		// Forget session
		GenFunction::deleteSessionData($request, 'objlabels');
		GenFunction::deleteSessionData($request, 'DTables');

		// labels
		if($request->has('lang_id')) $lang_id = $request->session()->get('lang_id');
		else{ 
			$lang_id = 2;
			$request->session()->put('lang_id', $lang_id);
		}
		
		// Modulo de solicitud de tarjetas
		GenFunction::createLBL("button_back", 23, $lang_id);
		GenFunction::createLBL("lbl_cxc_MethodPay", 23, $lang_id);
		GenFunction::createLBL("lbl_cxc_bank", 23, $lang_id);
		GenFunction::createLBL("lbl_cxc_NumberCheck", 23, $lang_id);
		GenFunction::createLBL("lbl_cxc_NumberAutho", 23, $lang_id);
		GenFunction::createLBL("lbl_cxc_Numberlote", 23, $lang_id);
		GenFunction::createLBL("lbl_cxc_NumberRef", 23, $lang_id);
		GenFunction::createLBL("lbl_cxc_ExpDate", 23, $lang_id);
		GenFunction::createLBL("lbl_cxc_Value", 23, $lang_id);
		GenFunction::createLBL("lbl_cxc_PHCheck", 23, $lang_id);
		GenFunction::createLBL("lbl_cxc_PHAutho", 23, $lang_id);
		GenFunction::createLBL("lbl_cxc_PHlote", 23, $lang_id);
		GenFunction::createLBL("lbl_cxc_PHRef", 23, $lang_id);
		GenFunction::createLBL("lbl_cxc_PHExpDate", 23, $lang_id);
		GenFunction::createLBL("lbl_cxc_PHValue", 23, $lang_id);
		GenFunction::createLBL("app_message_error", 23, $lang_id);
		
		$objlabels1 = GenFunction::getLabels($lang_id, 23);

		GenFunction::storeSessionData($request, 'objlabels', $objlabels1);

		$DTables = GenLanguage::where('lang_code', $lang_id)
								->pluck('lang_DataTables')
								->first();
		GenFunction::storeSessionData($request, 'DTables', $DTables);
		
		return $objlabels1;

    } // Fin createLabels
	
	/**
	 * get data for Select Bank
	 *
	 * @param  Request  $request
	 * @param  integer  $mp
	 *
	 * @return view		Json
	*/
	public function getCmb(Request $request, $mp){
		try{
			// http://137.117.80.33:8181/mtc/api_sel_bantj?formap=CP,BANCOS
			$status  = 0;
			$mensaje = 'Success';
			$hmtl_var = "";

			if ($mp=="EF") {

				$hmtl_var = '<input type="text" class="form-control" readonly placeholder="Directo" >';

			}else{

				$File = GenMethodpay::where('pmt_name',$mp)
									->pluck('pmt_file')
									->first();
				$mp .= ','.$File;

				$objform = GenFunction::getLabels($request->session()->get('lang_id'), $request->session()->get('mod_code'));
				$attribute="formap=".$mp; 
				$lbos = GenFunction::procesarApiGen(26,$request->session()->get('lang_id'),"BANK_C",$request->session()->get('user_code'),$attribute,""); 

				$hmtl_var = '<select id="lbl_cxc_bank0" name="lbl_cxc_bank0" class="form-control" data-style="btn-primary">';
				$hmtl_var .= '<option value="0">'.$objform->lbl_select_record.'</option>';
				
				if(is_array($lbos)){
					$i = 0;
					foreach($lbos as $lbo){
						$row = explode(',',$lbo);
						$hmtl_var .= '<option value="'.$row[0].'">'.$row[1].'</option>';
					}
				}else{
					if(!empty($lbos)){
						$row = explode(',',$lbos);
						$hmtl_var .= '<option value="'.$row[0].'">'.$row[1].'</option>';
					}
				}
				$hmtl_var .= '</select>';
				
				
			}

			$status = 1;
			$response = array(
				'success'		=> $status,
				'message'		=> $mensaje,
				'hmtl_var'		=> $hmtl_var
			);
			return response()->json($response);

		}catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}
	} // Fin getCmb
	
	/**
	 * Show the intro page of the module
	 *
	 * @param  Request  $request
	 * @return \Illuminate\Http\Response
	*/
    public function showHome(Request $request){
		$objlabels = $this->createLabels($request);
		$lang_id   = $request->session()->get('lang_id');		
		
		$MPay = GenMethodpay::where('pmt_status', 1)
							->where('lang_code', $lang_id)
							->get();

		return view('tmp.homeCollect')
				->with('DTables', $request->session()->get("DTables"))
				->with('objform', $request->session()->get("objlabels"))
				->with('delimiter', ",")
				->with('colvalue', 5)
				->with('i', 1)
				->with('MPay', $MPay);
    } // Fin showHome
}
