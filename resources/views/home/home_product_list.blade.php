@extends('home.home_page_mtpv_2')

@section('content')
	<div class="content container " style="">
	    <style scoped>
			h3{
				font-size:2em;
			}
			
			.cs_title_pr{
				color:#2c0373;
				font-size:2.8em;
				font-weight:bold;
			}
		</style>
		<div class="row ">
			<div id="5b1PZ" class="hc_column_cnt col-md-12" style="" >
				<div class="row">
					<div class="col-md-12 hc_space_cnt"><hr class="space l" /></div>
				</div>
			</div>
	
			<div id="5xNL6" class="hc_column_cnt col-md-12" style="" >
				<div class="row">
					<div class="col-md-12 hc_space_cnt">
					<center><span class="cs_title_pr">ESCOJE TU PLAN</span></center>
					<hr class="space xs"  />
					</div>
				</div>
			</div>
	
			<div id="column_Q1bd4" class="hc_column_cnt col-md-8  col-center" style="" >
				<div class="row">
				   @foreach($arrWebProduct as $webProduct)
					<div id="column_JmteR" class="hc_column_cnt col-md-4" style="" >
						<div class="row">
							<div class="col-md-12 hc_niche_content_box_pricing_table_cnt">
								<div class="list-group pricing-table" style="">
									<div class="list-group-item pricing-price"><h3 style="font-size:0.7em;">{{$webProduct->pro_name}}</h3>{{$webProduct->pro_price}}</div>
									<div class="list-group-item pricing-name"></div> 
									<div class="list-group-item">{{$webProduct->pro_description1}} </div> 
									<div class="list-group-item">{{$webProduct->pro_description2}}</div>
									<div class="list-group-item pricing-btn">
										<a class="btn circle-button" href="javascript:cargarMenu('{{$webProduct->pro_linkweb}}')">{{$objlabels->wlb_home_btnP}}</a>
										<br><br>
										<a class="btn circle-button" href="javascript:cargarDetallePr('{{$webProduct->Id}}')">Detalle</a>
									</div>
								</div>
						   </div>
						</div>
					</div>
				   @endforeach
				</div>
			</div>
		</div>
	</div>
@stop	