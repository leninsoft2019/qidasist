<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CliClient extends Model
{
    //
	
	protected $table = 'cli_clients';
	protected $primaryKey = 'cli_code';
}
