<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

use App\Http\Requests; 

use App\GenFunction;
use App\BtnP2pTran;
use App\BtnP2pParam;

class GenFunctionP2P extends Model{
	
	/**
    *	Crea llave de transaccion / Create Trans Key
    *
    * @param  string $url
    * @return object $data
    */
	public static function createTranskeyP2P(){
		$arrReturn = array();
		// Credenciales
		$var_endpoint_pruebas = BtnP2pParam::find(3);
		$var_passw = $var_endpoint_pruebas->p2p_value;
		$var_endpoint_pruebas = BtnP2pParam::find(2);
		$var_login = $var_endpoint_pruebas->p2p_value;
        date_default_timezone_set('America/Guayaquil'); 
		$seed = date('c');
		if (function_exists('random_bytes')) {
			$nonce = bin2hex(random_bytes(16));
		} elseif (function_exists('openssl_random_pseudo_bytes')) {
			$nonce = bin2hex(openssl_random_pseudo_bytes(16));
		} else {
			$nonce = mt_rand();
		}

		$nonceBase64 = base64_encode($nonce);
		$tranKey = base64_encode(sha1($nonce . $seed . $var_passw, true));
		
		$arrReturn['login']   = $var_login;
		$arrReturn['seed']    = $seed;
		$arrReturn['nonce']   = $nonceBase64;
		$arrReturn['tranKey'] = $tranKey;
		return $arrReturn;
	
	} // Fin createTranskeyP2P
	
	/*
	*	Tarjetas de prueba / Test cards
	*/
	public static function testCards(){		
		// Tarjetas / Cards
		$dinerIdNumber1 = "36545400000008";		// Aprueba
		$dinerIdNumber2 = "36545400000248";		// Rechaza
		$dinerIdNumber3 = "36545400000701";		// Queda pendiente alrededor de 5 minutos, luego aprueba
		$dinerIdNumber4 = "36545407030701";		// Queda pendiente alrededor de 5 minutos, luego rechaza
		$dinerIdNumber5 = "36545400000438";		// Aprueba con Delay de 180 segundos
		$dinerIdNumber6 = "36545407032780";		// Deja la operación pendiente como modo de captura, la operación debe ser autorizada o cancelada en el panel de Place to Pay o de otra manera por operaciones de VOID o SETTLE.

		$visaIdNumber1 = "4111111111111111";	// 	Aprueba
		$visaIdNumber2 = "4716375184092180";	// 	Rechaza
		$visaIdNumber3 = "4381080000000003";	// 	Queda pendiente alrededor de 5 minutos, luego rechaza

		return $arrCards = array($dinerIdNumber1, $dinerIdNumber2, $dinerIdNumber3, $dinerIdNumber4,
								 $dinerIdNumber5, $dinerIdNumber6, $visaIdNumber1, $visaIdNumber2,
								 $visaIdNumber3);
		
	} // Fin credential
	
	/*
	*	Genera una clave acorde a una longitud
	*	Generates a key according to a length
	*/
	public static function generarCodigos($cantidad=1, $longitud=16, $incluyeChar=true){ 
		$caracteres = "1234567890"; 
		if($incluyeChar) 
			$caracteres .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 

		$arrPassResult=array(); 
		$index=0; 
		while($index<$cantidad){ 
			$tmp=""; 
			for($i=0;$i<$longitud;$i++){ 
				$tmp.=$caracteres[rand(0,strlen($caracteres)-1)]; 
			} 
			if(!in_array($tmp, $arrPassResult)){ 
				$arrPassResult[]=$tmp; 
				$index++; 
			} 
		} 
		return $tmp; 
	} // Fin function generarCodigos
	
	/*
	*	Crea estructura JSON para P2P / Create JSON structure for P2P
	*/
	public static function createJson(){
		try {
			$var_endpoint_return = BtnP2pParam::find(5);
			$var_return = $var_endpoint_return->p2p_value;
			$arrCredential = GenFunctionP2P::createTranskeyP2P();
			$arrCards = GenFunctionP2P::testCards();
			
			/////GENERACION DE JSON//////////////////////////
			$arrRe1['auth']	  = $arrCredential;
			$arrRe1['locale'] = "es_EC";
			/*
			$adrr['address']  = array('street' => $street, "city" => $city,  "phone" => $phone,  "country" => $country);
			$adrr2['address'] = array('street' => $street_bill, "city" => $city_bill,  "phone" => $phone_bill,  "country" => $country_bill);

			$arrRe1['payer']  = array("name"    => $nombre_py,
									  "surname" => $apellido_py,
									  "email"	=> $email_py,
									  "document"=>$cedula_py,
									  "documentType"=>$documentType_py,
									  $adrr2,
									  "mobile"  => $phone_bill);

			$arrRe1['buyer']  = array("name"    => $nombre,
									  "surname" => $apellido,
									  "email"	=> $email,
									  "document"=>$cedula,
									  "documentType"=>$documentType,
									  $adrr,
									  "mobile"  => $phone);

			$arrRe1['payment'] = array("reference"	 => $reference,
									   "description" => "Pago en PlacetoPay: ".$reference,
									   "amount"		 => array(
															"taxes"    => array(array("kind"   => "valueAddedTax",
																					  "amount" => (float) $total_tax,
																					  "base"   => (float) $subtotal)
																				),
															"details"  =>  array(array('kind'   =>  "subtotal",
																					   'amount' =>  (float) $subtotal),
																				 array('kind'   =>  "discount",
																					   'amount' =>  (float) 0),
																				 array('kind'   =>  "shipping",
																					   'amount' =>  (float) round(0, 2) )
																				),
															"currency" => "USD",
															"total"    => (float) round($subtotal + $total_tax, 2)
														)
									);

			$arrRe1['fields'] = array(
									  array("keyword" => "Base 12%", 
											"value" => (float) round($shopd_tax1, 2),  
											"displayOn" => "payment"),
									  array("keyword" => "Base  0%", 
											"value" => (float) round($shopd_tax2, 2),  
											"displayOn" => "payment")
									);

			// Desglose de items
			for($i=0; $i < sizeof($product_id); $i++){
				// Validacion de los item para los impuestos
				$sql = "SELECT desglosabases FROM shopmve.wp_posts WHERE ID = ?";
				$wp_posts = $database->get_row($sql, array( $product_id[$i] ) );
				if(empty($wp_posts->desglosabases)) $coniva = "N";
				else $coniva = $wp_posts->desglosabases;
				if($coniva == "S"){
					$cincuentaycuatro = (float) $price[$i] * $wp_posts_tax1->par_valor;
					$cuarentayseis	  = (float) $price[$i] * $wp_posts_tax2->par_valor;
					$base1 = (float) round(($cincuentaycuatro / $wp_posts_iva->par_valor) * $quantity[$i], 2);
					$iva   = (float) round(($cincuentaycuatro - $base1) * $quantity[$i], 2);
					$base2 = (float) round(($cuarentayseis + $base1) * $quantity[$i], 2);
					$arrRe1['payment']['items'][$i] = array("sku"	  => $product_id[$i],
														   "name"	  => $product_name[$i],
														   "category" => "Paquetes Turisticos",
														   "qty" 	  => $quantity[$i],
														   "price"	  => $base2,
														   "tax"	  => $iva);
				}else{
					$iva = (float) round(($price[$i] / $wp_posts_iva->par_valor) * $quantity[$i], 2);
					$iva = (float) round(($price[$i] - $iva) * $quantity[$i], 2);
					$arrRe1['payment']['items'][$i] = array("sku"	  => $product_id[$i],
														   "name"	  => $product_name[$i],
														   "category" => "Productos - Servicios COMTUMARK",
														   "qty" 	  => $quantity[$i],
														   "price"	  => (float) round($price[$i]-$iva, 2),
														   "tax"	  => (float) $iva);
				}
			} //for($i=0;

			$arrRe1['payment']['shipping'] =  array('name' => $nombre, "surname" => $apellido,  "email" => $email, $adrr, "mobile" => '');
			$arrRe1['returnUrl']  = $var_return;
			$arrRe1['expiration'] = date('c', strtotime('+20 minutes'));
			$arrRe1['ipAddress']  = GenFunction::getRealIpAddr();
			$arrRe1['userAgent']  = $_SERVER['HTTP_USER_AGENT'];
			///// FIN GENERACION DE JSON//////////////////////////
			*/
			///////////////////////////////////////////////////////////
			// METODO Yii
			/*
			$request = [
                          "auth" => [
                              "login" => Yii::$app->params['placetopay']['login'],
                              "seed" => $seed,
                              "nonce" => $nonceBase64,
                              "tranKey" => $tranKey
                          ],
                          "locale" => 'es_EC',
                          "buyer"=>[
                            "document" => $atencion->paciente->identificacion, 
                            "documentType" => "CI", //CI cedula, RUC, PPN -> Pasaporte
                            "name" => preg_replace('/[0-9]+/', '', $atencion->paciente->nombres),
                            "surname" => preg_replace('/[0-9]+/', '', $atencion->paciente->apellidos),
                            "email" => ( !is_null( $atencion->paciente->email ) ) ? $atencion->paciente->email : time().'@utim.com.ec',
                            "mobile" => $atencion->paciente->numero_celular,
                            "address" =>  [
                              "street" => $atencion->paciente->direccion_facturacion, 
                              "city" =>  ( is_object($atencion->paciente->ciudad) ) ? $atencion->paciente->ciudad->nombre : 'Quito',
                              "country" => "EC"
                            ]
                          ],
                          "payment" => [
                              "reference" => $reference, //ID generado por el comercio
                              "description" => "Cobro desde UTIM APP",
                              "amount" => [
                                  "currency" => "USD",
                                  "total" => (float)$atencion->precio_atencion,
                                  // "taxes" => [
                                  //         "kind"=> "iva",
                                  //         "amount"=> (float)$atencion->precio_atencion - ( (float)$atencion->precio_atencion / Yii::$app->params['placetopay']['iva'] ) ,
                                  //         "base"=> (float)$atencion->precio_atencion / Yii::$app->params['placetopay']['iva'],
                                  // ],
                                  // "details" => [
                                  //     [
                                  //         "kind" => "subtotal",
                                  //         "amount" => 22.72

                                  //     ]
                                  // ]
                              ],
                              "allowPartial" => false,
                          ],
                          "expiration" => date('c', strtotime('+30 minutes')), // tiempo para pago antes de caducar sesión
                          "returnUrl" => Url::to([ 'api/placetopayresponse' ], true),
                          "ipAddress" => "173.230.130.8",
                          "userAgent" => 'IONIC',
                      ];
			*/
			///////////////////////////////////////////////////////////
			
			return $arrRe1;
			
		}catch(\Exception $e){
			return $e->getMessage();
		}
	} // Fin createJson
	
	/**
    * File get contents via URL
    *
    * @param  string $url
    * @return object $data
    */
	public static function createSessionP2P($arrJson,$request) {

			// Credenciales
			$var_endpoint_pruebas = BtnP2pParam::find(1);
			$endPoint =	$var_endpoint_pruebas->p2p_value;
			$idProduct = $request->session()->get('id_product');
		    $objWebPr  = WebRanges::find($idProduct);
			
			//SOLICITAMOS LA CREACION DE SESSION PARA EL PAGO PLACETOPAY
			$curl = curl_init();

			curl_setopt_array($curl, array(
				CURLOPT_URL => $endPoint.'api/session',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				/*CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,*/
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_HTTPHEADER => array(
				"Cache-Control: no-cache",
				"Content-Type: application/json",
				"Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
				),
			));

			//$arrJson = GenFunctionP2P::createJson();
			//$data = json_encode($arrJson);
			$data = $arrJson;
			$data = json_encode($data);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			$response = curl_exec($curl);
			$err      = curl_error($curl);
			curl_close($curl);

			if ($err) {
				return $err;
			} else {
				$resArray = json_decode($response, true);
				return $resArray;
				if ($resArray['status']['status'] == 'PENDING' ) {
				   $msg = "//....SIGUE PENDIENTE";
				} else if ($resArray['status']['status'] == 'OK') {
					// Registro de transaccion / Transaction Registration
					
					$msg = "//.....SE APROBO";
					$msg .= "<br>Mensaje: ".$resArray['status']['message'];
					$msg .= "<br>Url    : ".$resArray['processUrl'];
					$msg .= "<br>Sess   : ".$resArray['requestId'];

					/*if (isset($_COOKIE['requestId'])) {
						unset($_COOKIE['requestId']);
					}*/

					//*setcookie('requestId', $resArray['requestId'], time() + (86400), "/");*/
					return $resArray['processUrl'];
					//header('Location: '.$resArray['processUrl']);
					//exit;

				} else {
          			var_dump($resArray);
          			var_dump($response);
					$msg = "//...SE RECHAZO";
					$msg .= "<br>Mensaje: ".$resArray['status']['message'];
					$msg .= "<br>Fecha: ".$resArray['status']['date'];	
					
				}

				//*/
			} // Fin err
			
			return '';
			
		
	} // Fin createSessionP2P

	/**
    * File get contents via URL
    *
    * @param  string $url
    * @return object $data
    */
	public static function createSessionP2PCollect($arrJson,$request) {

			// Credenciales
			$var_endpoint_pruebas = BtnP2pParam::find(1);
			$endPoint =	$var_endpoint_pruebas->p2p_value;
			$idProduct = $request->session()->get('id_product');
		    $objWebPr  = WebRanges::find($idProduct);
			
			//SOLICITAMOS LA CREACION DE SESSION PARA EL PAGO PLACETOPAY
			$curl = curl_init();
			
			curl_setopt_array($curl, array(
				CURLOPT_URL => $endPoint.'api/collect',
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				/*CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,*/
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_HTTPHEADER => array(
				"Cache-Control: no-cache",
				"Content-Type: application/json",
				"Accept: application/vnd.github.v3+json",
				"User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36"
				//,	"Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
				),
			));

			//$arrJson = GenFunctionP2P::createJson();
			//$data = json_encode($arrJson);
			$data = $arrJson;
			$data = json_encode($data);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			$response = curl_exec($curl);
			$err      = curl_error($curl);
			curl_close($curl);

			if ($err) {
				Storage::prepend('log_p2p_collect.log',"****************");
				Storage::prepend('log_p2p_collect.log','ERROR');  
				Storage::prepend('log_p2p_collect.log',$err );  	
				Storage::prepend('log_p2p_collect.log',"****************");					
				return $err;
				
					
			} else {
				$resArray = json_decode($response, true);
				Storage::prepend('log_p2p_collect.log',"****************");
				Storage::prepend('log_p2p_collect.log',$resArray['status']['status'] );  
				Storage::prepend('log_p2p_collect.log',$response );  
				Storage::prepend('log_p2p_collect.log',$data );  	
				Storage::prepend('log_p2p_collect.log',"****************");				
				return $resArray;
				if ($resArray['status']['status'] == 'PENDING' ) {
				   $msg = "//....SIGUE PENDIENTE";
				} else if ($resArray['status']['status'] == 'OK') {
					// Registro de transaccion / Transaction Registration
					
					$msg = "//.....SE APROBO";
					//$msg .= "<br>Mensaje: ".$resArray['status']['message'];
					//$msg .= "<br>Url    : ".$resArray['processUrl'];
					//$msg .= "<br>Sess   : ".$resArray['requestId'];

					/*if (isset($_COOKIE['requestId'])) {
						unset($_COOKIE['requestId']);
					}*/

					//*setcookie('requestId', $resArray['requestId'], time() + (86400), "/");*/
					return $resArray;
					//header('Location: '.$resArray['processUrl']);
					//exit;

				} else {
          			var_dump($resArray);
          			var_dump($response);
					$msg = "//...SE RECHAZO";
					$msg .= "<br>Mensaje: ".$resArray['status']['message'];
					$msg .= "<br>Fecha: ".$resArray['status']['date'];	
					
				}


	
				//*/
			} // Fin err
			
			return '';
			
		
	} // Fin createSessionP2PCollect

	/**
    * File get contents via URL
    *
    * @param  string $url
    * @return object $data
    */
	public static function responseSessionP2P($requestId,$request) {

			// Credenciales
			$var_endpoint_pruebas = BtnP2pParam::find(1);
			$endPoint =	$var_endpoint_pruebas->p2p_value;
			$idProduct = $request->session()->get('id_product');
		    $objWebPr  = WebRanges::find($idProduct);
			
			//SOLICITAMOS LA CREACION DE SESSION PARA EL PAGO PLACETOPAY
			$curl = curl_init();
			$data_login  = GenFunctionP2P::createTranskeyP2P();
			$request_arr = [
			  "auth" => [
				  "login"   => $data_login['login'],
				  "seed"    => $data_login['seed'],
				  "nonce"   => $data_login['nonce'],
				  "tranKey" => $data_login['tranKey']

				]];
			curl_setopt_array($curl, array(
				CURLOPT_URL => $endPoint.'api/session/'.$requestId,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				/*CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,*/
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_HTTPHEADER => array(
				"Cache-Control: no-cache",
				"Content-Type: application/json",
				"Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
				),
			));


			Storage::prepend('log_p2p.log',"****************");
			Storage::prepend('log_p2p.log',"URL=".$endPoint.'api/session/'.$requestId);   
			Storage::prepend('log_p2p.log',$data_login['login']); 
			Storage::prepend('log_p2p.log',$data_login['seed']);   	
			Storage::prepend('log_p2p.log',$data_login['nonce']);   	
			Storage::prepend('log_p2p.log',$data_login['tranKey']);   				

		
			//$arrJson = GenFunctionP2P::createJson();
			
			$data = json_encode($request_arr);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);	
			
			$response = curl_exec($curl);
			$err      = curl_error($curl);
			curl_close($curl);

			if ($err) {
				return $err;
			} else {
				$resArray = json_decode($response, true);
				return $resArray;
				//*/
			} // Fin err
			
			return '';
			
		
	} // Fin createSessionP2P

	/**
     * Funcion que realiza la llamada al proceso de Reversa de una transaccion
     *
     * Example usage:
     * 		Reverse($requestId, $var_endpoint_pruebas, $var_login, $seed, $nonceBase64, $tranKey);
     *
	 * Parametros:
     * @param int $requestId 				Numero de session iniciada en PLACETOPAY
     * @param string $var_endpoint_pruebas 	Url del sitio: Produccion o Desarrollo
     * @param string $var_login 			Usuario para el ingreso en PLACETOPAY
     * @param string $seed 					Semilla generada de forma aleatoria
     * @param string $nonceBase64 			Numero aleatorio codificado en base64
     * @param string $tranKey 				Codigo generado de acuerdo a lo indicado por PLACETOPAY
     *
     * @access public
     * @return mixed $msg
     */
	public static function Reverse($requestId,$request){
			// Credenciales
			$var_endpoint_pruebas = BtnP2pParam::find(1);
			$endPoint =	$var_endpoint_pruebas->p2p_value;
			$idProduct = $request->session()->get('id_product');
		    $objWebPr  = WebRanges::find($idProduct);
			
			//SOLICITAMOS LA CREACION DE SESSION PARA EL PAGO PLACETOPAY
			$curl = curl_init();
			$data_login  = GenFunctionP2P::createTranskeyP2P();
			$request_arr = [
			  "auth" => [
				  "login"   => $data_login['login'],
				  "seed"    => $data_login['seed'],
				  "nonce"   => $data_login['nonce'],
				  "tranKey" => $data_login['tranKey']

				]];
			curl_setopt_array($curl, array(
				CURLOPT_URL => $endPoint.'api/reverse/'.$requestId,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				/*CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,*/
				CURLOPT_CUSTOMREQUEST => "POST",
				CURLOPT_HTTPHEADER => array(
				"Cache-Control: no-cache",
				"Content-Type: application/json",
				"Postman-Token: 96983e26-ba1b-4dfa-bbe9-54f877428d08"
				),
			));


			Storage::prepend('log_p2p.log',"****************");
			Storage::prepend('log_p2p.log',"URL=".$endPoint.'api/reverse/'.$requestId);   
			Storage::prepend('log_p2p.log',$data_login['login']); 
			Storage::prepend('log_p2p.log',$data_login['seed']);   	
			Storage::prepend('log_p2p.log',$data_login['nonce']);   	
			Storage::prepend('log_p2p.log',$data_login['tranKey']);   				

		
			//$arrJson = GenFunctionP2P::createJson();
			
			$data = json_encode($request_arr);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);	
			
			$response = curl_exec($curl);
			$err      = curl_error($curl);
			curl_close($curl);

			if ($err) {
				return $err;
			} else {
				$resArray = json_decode($response, true);
				return $resArray;
				//*/
			} // Fin err
			
			return '';
	} // end function Reverse

 }