<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenLanguage extends Model
{
    // 
	protected $table = 'gen_lang';
	protected $primaryKey = 'lang_code';
}
