
@extends('client.home_quoting_template')

@section('content')
	<style scoped>
		#clientData{
			color: black;
			width:90%;
			background:white;
			margin:auto;
			border:solid 1px #f2f4f4;
			border-radius:5px;
			margin-top:30px;
			padding:20px;
		}
		#clientData table{
			color: black;
		}
		ul>li{
			padding-left:10px;
		}
		
		h3{
			color:black;
			font-weight:bold;
			padding-top:20px;
		}
		
		.buttons-html5{
		   background:#f2f4f4;
           color:black;
			border:solid 1px #9C9C9C;	
		   padding-left:10px;
		   margin-left:10px;
		   
			text-align:center;
			
			display: inline-block;
			*display: inline;
			*zoom: 1;
			padding: 4px 12px;
			margin-bottom: 0;
			font-size: 14px;
			line-height: 20px;
			text-align: center;
			vertical-align: middle;
			cursor: pointer;
			color: #333333;
			text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
			background-color: #f5f5f5;
			background-image: -moz-linear-gradient(top, #ffffff, #e6e6e6);
			background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ffffff), to(#e6e6e6));
			background-image: -webkit-linear-gradient(top, #ffffff, #e6e6e6);
			background-image: -o-linear-gradient(top, #ffffff, #e6e6e6);
			background-image: linear-gradient(to bottom, #ffffff, #e6e6e6);
			background-repeat: repeat-x;
			filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe6e6e6', GradientType=0);
			border-color: #e6e6e6 #e6e6e6 #bfbfbf;
			border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
			*background-color: #e6e6e6;
			filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
			border: 1px solid #cccccc;
			*border: 0;
			border-bottom-color: #b3b3b3;
			-webkit-border-radius: 4px;
			-moz-border-radius: 4px;
			border-radius: 4px;
			*margin-left: .3em;
			-webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
			-moz-box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
			box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);			
			
		}
		
		.paginate_button page-item previous disabled{
			color:white;
		}
		
		.paginate_button{
			color:white;
			padding:3px;
			padding-left:0px;
			font-size:small;
		}
		
		.pagination > li.disabled > a {
			background-color: #9C9C9C;
			color: white;
		}
		
		#IdDT_filter{
			float:right;
			position:relative;
		}
		
		.nav-item{
			
			background-color: #f2f2f2;
            border:solid 1px #9C9C9C;
		}
		
		#IdDT_filter>label{
			display:flex;
		}
		
		.form-control-sm{
				height:25px;
		}
	</style>
	
	<center><h3>Informaci&oacute;n del Cliente: {{$surnameClient.", ".$nameClient}}</h3></center>
	<div id="clientData">
		<header>
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
			
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="nav nav-tabs">
						<li class="nav-item ">
							<a class="nav-link active" href="/homeClient">{{$objform->lbl_menu_assistance}} <span class="sr-only">(current)</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link"  href="javascript:searchTrans();">{{$objform->lbl_menu_transaction}}</a>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<input type="hidden" id="idclient" name="idclient" value="<?php echo $idclient; ?>" />
		<article>

			<section style="width:100%; margin:auto; padding:0px;" >

				<div id="showResult"><?php echo $dataTable; ?>
				
				<a class="nav-link btn" href="javascript:subscription();">{{$objform->lbl_menu_subscripcion}}</a>
				
				</div>

			</section>
		</article>
		
		<aside>	
			
		</aside>

		<script>
			function searchTrans(){
				var idclient = $("#idclient").val();
				var path = "/transactionClient";
				console.log(path+'/'+idclient);
				$("#showResult").html("");
				$.ajax({
					type:	'GET',
					url:	path+'/'+idclient,
					success:function(data){
						console.info(data);
						if(data.success==1){
							$("#showResult").html("");
							$("#showResult").empty();
							$("#showResult").html(data.table);
							setDataTab();
							return;
						}else{
							$("#showResult").html("");
							$("#showResult").empty();
							$("#showResult").html(data.message);
							return;
						}
					}
				}); // Fin AJAX
			} // Fin searchTrans
		
		    function subscription(){
				 if($("input:radio[name ='opAssistance']:checked").val()){
					 var idS = $("input:radio[name ='opAssistance']:checked").val();
					 console.log('/client/edit_data/'+idS);
					 $('#seccion_detalle_planes').load('/client/edit_data/'+idS);
				 }else{
					 alertPPYA('Debe seleccionar un registro');
				 }
			}
			 
			function searchAssistance(){
				var idclient = $("#idclient").val();
				var path = "/assistanceClient";
				$("#showResult").html("");
				$.ajax({
					type:	'GET',
					url:	path+'/'+idclient,
					success:function(data){
						console.info(data);
						if(data.success==1){
							$("#showResult").html("");
							$("#showResult").html(data.table);
							return;
						}else{
							$("#showResult").html("");
							$("#showResult").empty();
							$("#showResult").html(data.message);
							return;
						}
					}
				}); // Fin AJAX
			} // Fin searchTrans
		
			
			$(document).ready(function() {
				var fecha = new Date();
				var nombre = "MTC_"+fecha.getFullYear();
				nombre += fecha.getMonth();
				nombre += fecha.getDate();
				nombre += fecha.getHours();
				nombre += fecha.getMinutes();
				nombre += fecha.getSeconds();
				var table = $('#IdDT').DataTable({
					//scrollY:        "300px",
					lengthChange:	false,
					stateSave: 		true,
					scrollX:        true,
					fixedColumns:   {
						leftColumns: 5,
						scrollCollapse: true
						//rightColumns: 1
					},
					"language": {
						"url": "{{$DTables}}"
					},
					dom: 'Bfrtip',
					buttons: [
						{
							extend:    'copyHtml5',
							copyTitle: '{{$objform->button_export}} - {{$Nbnk}}',
							copyKeys: '{{$objform->btn_copyKeys}}',
							copySuccess: {
								_: '%d {{$objform->btn_copySuccess1}}',
								1: '1  {{$objform->btn_copySuccess2}}'
							}
						},
						{
							extend: 'excelHtml5',
							title: nombre,
							messageTop:  'PDF created by MTC: {{$Nbnk}}.'
						},
						{
							extend:   	 'pdfHtml5',
							title:	  	 nombre,
							messageTop:  'PDF created by MTC: {{$Nbnk}}.',
							orientation: 'landscape',
							pageSize:	 'A4'
						},
						{
							text:		 'CSV',
							title:	  	 nombre,
							extend: 	 'csvHtml5',
							fieldSeparator: '<?php echo $delimiter; ?>',
							extension: 	 '.csv',
							className:   'red'
						}
					],
					"footerCallback": function ( row, data, start, end, display ) {
						var api = this.api(), data;
						
						// Remove the formatting to get integer data for summation
						var intVal = function ( i ) {
							return typeof i === 'string' ?
							i.replace(/[\$,]/g, '')*1 :
							typeof i === 'number' ?
							i : 0;
						};
						
						// Total over all pages
						total = api
						.column( <?php echo $colvalue; ?> )
						.data()
						.reduce( function (a, b) {
							return intVal(a) + intVal(b);
						}, 0 );
						
						// Total over this page
						pageTotal = api
						.column( <?php echo $colvalue; ?>, { page: 'current'} )
						.data()
						.reduce( function (a, b) {
							return intVal(a) + intVal(b);
						}, 0 );
						
						// Update footer
						$( api.column( <?php echo $colvalue; ?> ).footer() ).html(
							'$'+pageTotal +' ( $'+ total +' total)'
						);
					}
				});
				table.buttons().container().appendTo( '#IdDT_paginate .col-md-6:eq(0)' );
			} );
			
			
			function setDataTab(){
				console.log('setting table');
				var fecha = new Date();
				var nombre = "MTC_"+fecha.getFullYear();
				nombre += fecha.getMonth();
				nombre += fecha.getDate();
				nombre += fecha.getHours();
				nombre += fecha.getMinutes();
				nombre += fecha.getSeconds();
				var table = $('#IdDT').DataTable({
					lengthChange:	false,
					stateSave: 		true,
					scrollX:        true,
					fixedColumns:   {
						leftColumns: 5,
						scrollCollapse: true
						//rightColumns: 1
					},
					"language": {
						"url": "{{$DTables}}"
					},
					dom: 'Bfrtip',
					buttons: [
						{
							extend:    'copyHtml5',
							copyTitle: '{{$objform->button_export}} - {{$Nbnk}}',
							copyKeys: '{{$objform->btn_copyKeys}}',
							copySuccess: {
								_: '%d {{$objform->btn_copySuccess1}}',
								1: '1  {{$objform->btn_copySuccess2}}'
							}
						},
						{
							extend: 'excelHtml5',
							title: nombre,
							messageTop:  'PDF created by MTC: {{$Nbnk}}.'
						},
						{
							extend:   	 'pdfHtml5',
							title:	  	 nombre,
							messageTop:  'PDF created by MTC: {{$Nbnk}}.',
							orientation: 'landscape',
							pageSize:	 'A4'
						},
						{
							text:		 'CSV',
							title:	  	 nombre,
							extend: 	 'csvHtml5',
							fieldSeparator: '<?php echo $delimiter; ?>',
							extension: 	 '.csv',
							className:   'red'
						}
					]
					
				});
				console.log('luego table sett');
				table.buttons().container().appendTo( '#IdDT_paginate .col-md-6:eq(0)' );				
				
			}

		</script>
	</div>
@endsection