<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenParam extends Model{
    //
	protected $table = 'gen_param';
	protected $primaryKey = 'par_code';
}
