<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");

$cmb_tipo_pago     =  $arr_combos['cmb_tipo_pago'];
$cmb_forma_pago    =  $arr_combos['cmb_forma_pago'];
$cmb_tipo_cuenta   =  $arr_combos['cmb_tipo_cuenta'];
$cmb_banco         =  $arr_combos['cmb_banco'];
				   
			
					
?>
<!-- The Modal -->


<div>


<style scoped src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</style>

<style  scoped type="text/css">
	#myModalBDP2P{
		max-width: 1000px;
		

	}
	
	#myModalDebit{
	  position: absolute;
	  top: 50%;
	  left: 50%;
	  transform: translate(-50%, -50%);
	  width: 1000px;
	  height: 590px;	
	  font-size:1.5em;
	}	

	#body_mod_debit{
		max-height: 590px;	
		overflow-y: scroll;	
	}

	
</style>
<div class="modal" id="myModalDebit">

  <div class="modal-dialog modal-dialog-centered" id="myModalBDP2P">
    <div class="modal-content">
       <style scoped>
	    #frmAdicionales{
			padding:15px;
		}
		
		#myModalConfirm modal-dialog{
			max-width:300px;
		}
	   </style>
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><span class="span-title"><i class="ico_anullment_orange"></i>Debito Bancario</span></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>

      <!-- Modal body -->
      <div class="modal-body">
	  
	   <div class="alert alert-success" id="alert_success">
		<strong>Success!</strong> This alert box could indicate a successful or positive action.
	  </div>
	  <div class="alert alert-danger" id="alert_error">
		<strong>Danger!</strong> This alert box could indicate a dangerous or potentially negative action.
	  </div>  
			<div id="body_mod_debit">

							<form  id="frmDebt">
								{{ csrf_field() }}	

								<hr></hr>
								<!-- Titular -->
								<div class="tbl">
								
									<div class="row cab_r rw">
										<div class="col col_50">
											<div class="form-group">
												<label for="tipo_cuenta">Tipo Cuenta:</label>
												<select id="tipo_cuenta" name="tipo_cuenta" class="form-control" >
													<option value="0">-----</option>
													@foreach($cmb_tipo_cuenta as $cv)
															<?php  
																 $varCh ="";
																 //if($arr_formapago['tipo_cuenta'] == $cv->bac_code){
																//	 $varCh ="selected";
																// }
															?>
															<option value="{{$cv->bac_code}}"  {{$varCh}}>{{$cv->bac_name}}</option>
													@endforeach
												</select>
											</div>
										</div>
										<div class="col col_50">
											<div class="form-group">
												<label for="banco">Banco:</label>
												<select id="banco" name="banco" class="form-control" >
												<option value="0">-----</option>
													@foreach($cmb_banco as $cv)
															<?php  
																 $varCh ="";
																 //if($arr_formapago['banco'] == $cv->ban_code){
																//	 $varCh ="selected";
																 //}
															?>
															<option value="{{$cv->ban_code}}"  {{$varCh}}>{{$cv->ban_name}}</option>
													@endforeach
												</select>
   
											</div>
										</div>
									</div>
									
									
	
									<div class="row cab_r rw">
										<div class="col col_50">
											<div class="form-group">
												<label for="numero_cuenta">Número de Cuenta:</label>
											    <input type="text" id="numero_cuenta" name="numero_cuenta" value="" class="form-control" />
											</div>
										</div>
										<div class="col col_50">
											<div class="form-group">
												<label for="id_deb">Identificaci&oacute;n:</label>
												<input type="text" id="id_deb" name="id_deb" class="form-control" />
											</div>
										</div>
									</div>
									
								
									<div class="row cab_r rw">
										<div class="col col_50">
											<div class="form-group">
												<label for="nombre_deb">Nombre:</label>
												<input type="text" class="form-control" id="nombre_deb" name="nombre_deb" />
											</div>
										</div>
										<div class="col col_50">
											<div class="form-group">
												<label for="apellido_deb">Apellido:</label>
												<input type="text" class="form-control" id="apellido_deb"  name="apellido_deb" />
											</div>
										</div>
									</div>
																


								</div>
							</form>
			
			
			</div>
      </div>



      <!-- Modal footer -->
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-danger button_module_wh" data-dismiss="modal">Close</button>-->
		<button type="button" id="btnCerrarDeb" class="btn btn-danger button_module_wh" data-dismiss="modal">Cerrar</button>
&nbsp;
	    <button name="btnGuardarAnul" id="btnGuardarDeb"   class="btn btn-default">Guardar</button>		
      </div>

    </div>
  </div>
</div>	


<!-- The Modal -->
<div class="modal" id="myModalConfirm">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">



      <!-- Modal body -->
      <div class="modal-body" id="dv_confirm_additional">
			<p>Ya existe una asistencia asociada al producto seleccionado, desea ingresar otra?</p>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger button_module_wh" data-dismiss="modal">Cancelar</button>
&nbsp;
	    <button name="btnGuardarAnul" id="btnGuardarAnul" onclick="saveAdditional()"  class="btn btn-default">Guardar</button>		
      </div>

    </div>
  </div>
</div>	
</div>
<script type="text/javascript">
       $(function(){

          $('#alert_success').hide();
          $('#alert_error').hide();		  
		  
          $('#dv_alert_warn').hide();
		  
		  $('#sel_tpr').on('change',function(){
			 loadListChild('sel_tpr','sel_tctg','tipo_cat_pr');
			 $('#sel_tctg').trigger('change'); 
		  });
		  
		  
		  $('#sel_tctg').on('change',function(){
			  
			 loadListChild('sel_tctg','sel_car','cartera_tp');
			  
		  });		  
		  
		  $('#sel_car').on('change',function(){
			  
			 loadListChild('sel_car','sel_categmine','categoria_ctp');
			  
		  });		
		  
		  
		  


			$("#btnGuardarDeb").click(function( event ) {

				event.preventDefault();

					    var form = $('#frmDebt,#frmMT');
						var request = $.ajax({
							type:	'POST',
							url:	'/sales/payment/save_debit',
							data:	form.serialize(),
							success:function(data){
								console.log(data);
								if(data.success==1){
									$("#btnGuardarDeb").prop('disabled',true);
									$("#btnCerrarDeb").prop('disabled',true);									
									$('#alert_success').html(data.message);
									$('#alert_success').show();
									
									/*setInterval(function(){ 
										
										$('#myModalDebit').fadeIn("slow");
										

									}, 2000);*/
									$('#myModalDebit').modal('hide');
									$('#seccion_detalle_planes').load(data.url_redirect);	
									
									        			
									return;
								}else if(data.success==2){
									$('#alert_error').empty();
									$.each( data.errors, function(i, obj) {
										$('#alert_error').append(obj+'<br>');
										
									});
									
									$('#alert_error').show();
									/*alertPPYA(data.message);*/
									console.log(data);
									return;
								}
							}
						});
						request.fail(function( jqXHR, textStatus ) {
							alertPPYA( "Hubo un error: " + textStatus );
							console.log(jqXHR);
						});
								
				
			});


       	  $('#sel_categmine').on('change', function() {
				Pace.restart();
				
				var sel_id     = $( "#sel_categmine option:selected" ).val();
				var $child_sel = $('#sel_pr');
				$.ajax({
					type: 'GET',
					url:'/insurance/cmbProduct/'+sel_id+'/',
					success:function(data){
						if(data.success==1){

							$child_sel.find('option').remove();         
							$.each( data.json_product, function(i, obj) {
								if(i==0){
									$child_sel.append('<option value="0" selected>----</option>');
									$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
								}else{
									$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
								}
							});
								
							
							return;
						}else{
							
							alertPPYA(data.message);
							return;
						}
					}
				});	
             
		  });		  
		  
       	  $('#sel_pr').on('change', function() {
       	  	 $("#id_producto").val(this.value);
       	  	 var id_producto  = this.value;


				$.ajax({
						type:	'GET',
						url:	'/insurance/get_data_product/'+id_producto+'/',
						success:function(data){
							console.log(data);
							if(data.success==1){

								var valor_pr  = parseFloat(data.arr_product.precio_unit)*parseFloat(data.arr_product.npc);
								$("#ad_valor").val(valor_pr.toFixed(2));
								return;
							}else if(data.success==2){
								$('#dv_alert_warn').show();
								$('#dv_alert_warn').addClass('in');
								$('#dv_alert_warn').addClass('show');	
								var valor_pr  = parseFloat(data.arr_product.precio_unit)*parseFloat(data.arr_product.npc);
								$("#ad_valor").val(valor_pr.toFixed(2));								
								/*$('#dialog-confirm').dialog('open');*/
								return;
							}
						}
				}); 
             
		  });
		  
		  
		  
		  
		  
       });



		function loadListChild(parent_name,child_name,child_pref){
			Pace.restart();
			
			var sel_id = $( "#"+parent_name+" option:selected" ).val();
			var $child_sel = $('#'+child_name);
			$.ajax({
				type: 'GET',
				url:'/general/getListChild/'+child_pref+'/'+sel_id+'/',
				success:function(data){
					if(data.success==1){

					    if(child_name=='sel_car'){
							$child_sel.find('option').remove();         
							$.each( data.modelData, function(i, obj) {
								  if(i==0){
									  $child_sel.append('<option value="0" selected>----</option>');
								  }
								if(obj.por_additional == 1){
									if(i==0){
										
										$child_sel.append('<option value=' + obj.por_id + ' >' + obj.por_name + '</option>');
									}else{
										$child_sel.append('<option value=' + obj.por_id + ' >' + obj.por_name + '</option>');
									}
								}

							});
						}else{
								$child_sel.find('option').remove();         
								$.each( data.arr_dataw2ui, function(i, obj) {
									if(i==0){
										$child_sel.append('<option value="0" selected>----</option>');
										$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
									}else{
										$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
									}
								});
							
						}
						

							
						
						return;
					}else{
						
						alertPPYA(data.message);
						return;
					}
				}
			});
		}

</script>
