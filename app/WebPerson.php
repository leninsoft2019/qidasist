<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebPerson extends Model
{
    //
	protected $table = 'web_persons';
	protected $primaryKey = 'Id';
}
