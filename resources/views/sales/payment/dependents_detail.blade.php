<?php
/*
*	Página que despliega la tabla de productos a cotizar o vender
*	Page that displays the table of products to be quoted or sold
*/

?>



<div class="contenedor_mt">

<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<style scoped>


#frmMT{
	width:70%;
	margin:auto;

	border:solid 1px;
	border-radius:5px;
	margin-top:20px;
	margin-bottom:20px;
	padding:20px;
	-webkit-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
	-moz-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
	box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
	 background-color:#F3EADD;
}

#cont_dep{
	height:550px;
	overflow-y: scroll;	
}


.row {
    display: -webkit-box;
    display: -ms-flexbox;
    
	width:100%;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;
}

.row_n{
	display:table;
	width:100%;
}

.tbl{
	display:table;
	width:100%;	
}
.rw{
	display:table-row;
	width:100%;
}

.col{
    position: relative;
    width: 50%;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
	
	display:table-cell;
}


.col2{
    position: relative;
    width: 50%;
    min-height: 1px;
    padding-right: 15px;
    padding-left: 15px;
	
	display:table-cell;
}

.form-control {
    display: block;
    width: 100%;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}


.form-control {
    padding: 2px;
    height: 28px;
    border: 1px solid #dddddd;
}

h2{
	font-family: "Rubik",Helvetica,Arial,sans-serif;
	font-size:2em;
	font-weight:500;
}



.col_add{
	width:25%;
}

.col_minus{
	width:25%;
}

.col_range{
	width:25%;
}

.col_50{
	display:table-cell;
	width:50%;
}


@media (max-width: 767px) {

	.text-xxl {
		font-size: 50px !important;
		line-height: 60px !important;
	}

h1{
		 text-indent:-9999px;
		font-size:0px;
		position:absolute;
	}

	h2, .text-l {
		margin-top:20px;
		font-size: 1.5em !important;
		line-height: 1em !important;
		
	}

	h3 {
		font-size: 18px !important;
		line-height: 28px !important;
	}
	
	
	#frmMT{
		
		width:98%;
		maring:auto;
		padding:5px;
	}
	
	.row{
				display:table;
		width:100%;
	}
	.col{
		display:table-row;
		width:100%;
	}


.contenedor_mt {
    padding: 0px;
    width: 100%;

}


.res_input{
	width:40px;
	height:20px;
}

.cab_r{
	padding-left:20px;
}

.col_add{
	width:25%;
}

.col_minus{
	width:25%;
}

.col_range{
	width:25%;
}


}
</style>


   

	<form action="/action_page.php" id="frmMT">
	{{ csrf_field() }}	
	 <h2>{{$arrWebProduct->pro_name}}: Ingreso de dependientes</h2>
		<!-- Titular -->
		<?php $i=1;?>
		<div id="cont_dep">
		@foreach($objWSD as $rw)
	
		    <fieldset>
			<legend>Dependiente {{$i}}</legend>
				<div class="tbl">
				<div class="row cab_r rw">
					<div class="col col_50">
						<div class="form-group">
							<label for="nombre">Nombre:</label>
							<input type="text" class="form-control" id="nombre" />
						</div>
					</div>
					<div class="col col_50">
						<div class="form-group">
							<label for="apellido">Apellido:</label>
							<input type="text" class="form-control" id="apellido" name="apellido" />
						</div>
					</div>
				</div>
				<div class="row cab_r rw">
					<div class="col col_50">
						<div class="form-group">
							<label for="id">Id:</label>
							<input type="id" class="form-control" id="id" />
						</div>
					</div>
					<div class="col col_50">
						<div class="form-group">
							<label for="email">Correo Electrónico:</label>
							<input type="email" class="form-control" id="email" />
						</div>
					</div>
				</div>
				</div>
			</fieldset>	
			<?php $i++;?>
		
		@endforeach
		</div>
		<!-- Titulos -->

		
		<!-- Siguiente -->
		<div class="row_n">
			<div class="col2 col_range">
				<div class="form-group"></div>
			</div>
			<div class="col2 col_add">
				<div class="form-group"></div>
			</div>		
			<div class="col2">
				<button id="btnRegresar" class="btn btn-default">Regresar</button>
			</div>		
			<div class="col2">
				<button id="btnNextPy" class="btn btn-default">Continuar</button>
			</div>
		</div>
	</form>
</div>

<script>
	function recalcula(){
		var tot_lines = $("#tot_lines").val();
		var precio = $("#precio").val();
		precio = parseFloat(precio);
		var qty_line = quantity = 0;
		var tot_price = 0;
		for (var i = 0; i < tot_lines; i++) {
			qty_line = $("#quantity"+i).val();
			quantity += parseInt(qty_line);
			//alert(qty_line+" - "+quantity);
		}
		quantity = parseFloat(quantity * precio);
		//alert(quantity);
		$("#totales").val(quantity.toFixed(2));
		
	} // Fin recalcula

	function add(linea){
		var quantity = $("#quantity"+linea).val();
		quantity++;
		$("#quantity"+linea).val(quantity);
		recalcula();
	} // Fin add
	
	function minus(linea){
		var quantity = $("#quantity"+linea).val();
		quantity = quantity - 1;
		$("#quantity"+linea).val(quantity);
		recalcula();
	} // Fin add
	
	
	
	
	
	$(function () {
	
			
			$("#btnRegresar").click(function( event ) {
				event.preventDefault();
				location.reload();
				
			});
			
			$("#btnNextPy").click(function( event ) {
				event.preventDefault();
				
				var form = $('#frmMT');
				var request = $.ajax({
					type:	'POST',
					url:	'/payment/mult_save',
					data:	form.serialize(),
					success:function(data){
						console.log(data);
						if(data.success==1){
							cargarMenu('/payment/pay_sel');
							return;
						}else{
							alert('Hubo un problema al tratar de guardar la informacion');
							console.log(data);
							return;
						}
					}
				});
				request.fail(function( jqXHR, textStatus ) {
					alert( "Hubo un problema: " + textStatus );
					console.log(jqXHR);
				});				
				
				
			});
		
	
	});
		
</script>