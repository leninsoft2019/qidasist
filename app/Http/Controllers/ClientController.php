<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\GenFunction;
use App\GenLanguage;
use App\CliClient;
use App\SalSale;

use App\User;

use App\webProduct;
use App\WebRanges;
use App\WebPerson;
use App\WebSale;
use App\WebSalesDependent;

use App\BtnP2pTran;


class ClientController extends Controller{
	/**
	 *	Method that creates the tags
	 *
	 *	@param  Request  $request
	 *	@return \Illuminate\Http\Response
	*/
    public function createLabels($request){
		// Forget session
		GenFunction::deleteSessionData($request, 'objlabels');
		GenFunction::deleteSessionData($request, 'DTables');

		// labels
		if($request->has('lang_id')) $lang_id = $request->session()->get('lang_id');
		else $lang_id = 2;
		// Modulo de Cliente
		GenFunction::createLBL("cli_search_Id", 23, $lang_id);
		GenFunction::createLBL("cli_search_Name", 23, $lang_id);
		GenFunction::createLBL("cli_search_Assist", 23, $lang_id);
		GenFunction::createLBL("cli_search_Reset", 23, $lang_id);
		GenFunction::createLBL("cli_search_Submit", 23, $lang_id);
		GenFunction::createLBL("cli_search_input", 23, $lang_id);
		// search Client
		GenFunction::createLBL("cli_tbl_col1", 23, $lang_id);
		GenFunction::createLBL("cli_tbl_col2", 23, $lang_id);
		GenFunction::createLBL("cli_tbl_col3", 23, $lang_id);
		GenFunction::createLBL("cli_tbl_col4", 23, $lang_id);
		GenFunction::createLBL("cli_tbl_col5", 23, $lang_id);
		GenFunction::createLBL("cli_tbl_col6", 23, $lang_id);
		GenFunction::createLBL("cli_tbl_col7", 23, $lang_id);
		GenFunction::createLBL("cli_tbl_col8", 23, $lang_id);
		GenFunction::createLBL("cli_tbl_col9", 23, $lang_id);
		GenFunction::createLBL("cli_tbl_col10", 23, $lang_id);
		GenFunction::createLBL("cli_tbl_col11", 23, $lang_id);
		GenFunction::createLBL("cli_tbl_col12", 23, $lang_id);
		// DataTables
		GenFunction::createLBL("button_export", 23, $lang_id);
		GenFunction::createLBL("btn_copyKeys", 23, $lang_id);
		GenFunction::createLBL("btn_copySuccess1", 23, $lang_id);
		GenFunction::createLBL("btn_copySuccess2", 23, $lang_id);
		// Home Client
		GenFunction::createLBL("lbl_menu_assistance", 23, $lang_id);
		GenFunction::createLBL("lbl_menu_transaction", 23, $lang_id);
		GenFunction::createLBL("lbl_menu_subscripcion", 23, $lang_id);
		GenFunction::createLBL("msg_option", 23, $lang_id);
		// Transactions
		GenFunction::createLBL("cli_tblt_col1", 23, $lang_id);
		GenFunction::createLBL("cli_tblt_col2", 23, $lang_id);
		GenFunction::createLBL("cli_tblt_col3", 23, $lang_id);
		GenFunction::createLBL("cli_tblt_col4", 23, $lang_id);
		GenFunction::createLBL("cli_tblt_col5", 23, $lang_id);
		GenFunction::createLBL("cli_tblt_col6", 23, $lang_id);
		GenFunction::createLBL("cli_tblt_col7", 23, $lang_id);
		GenFunction::createLBL("cli_tblt_col8", 23, $lang_id);
		GenFunction::createLBL("cli_tblt_col9", 23, $lang_id);
		
 		$objlabels = GenFunction::getLabels($lang_id, 23);
		GenFunction::storeSessionData($request, 'objlabels', $objlabels);

		$DTables = GenLanguage::where('lang_code', $lang_id)
								->pluck('lang_DataTables')
								->first();
		GenFunction::storeSessionData($request, 'DTables', $DTables);
		
		return $objlabels;

    } // Fin createLabels
	
	/**
	 * Show the intro page of the module
	 *
	 * @param  Request  $request
	 * @return \Illuminate\Http\Response
	*/
    public function showHome(Request $request){
		$objlabels = $this->createLabels($request);
		
		return view('client.cons_assists')
				->with('DTables', $request->session()->get("DTables"))
				->with('objlabels', $objlabels);
		
    } // Fin showHome

	public function searchClient(Request $request){
		try{
			$objlabels = $request->session()->get("objlabels");
		
			$searchid = $request->searchid;
			$searchname = $request->searchname;
			$searchassist = $request->searchassist;
			
			// 1705859914
			$objClient = CliClient::where('cli_id', $searchid)->where('cli_status', 1)->first();
			// 792
			$sql = "SELECT sal_sales.id AS 'ID',
							 (SELECT sal_contract.sal_token FROM sal_contract WHERE sal_contract.Id = sal_sales.sal_contract) AS 'CONTRACT',
							 (SELECT CONCAT(cli_clients.cli_surname, ', ', cli_clients.cli_name) FROM cli_clients WHERE cli_clients.cli_code = sal_sales.id_person) AS 'CLIENT',
							 (SELECT sal_agencias.age_name FROM sal_agencias WHERE sal_agencias.age_code = sal_sales.agency_code) AS 'AGENCY',
							 (SELECT gen_sellers.sel_name FROM gen_sellers WHERE gen_sellers.sel_code = sal_sales.sel_code) AS 'SELLER',
							 (SELECT CONCAT(web_products.pro_name, ' ( ', web_products.pro_code, ' )') FROM web_products WHERE web_products.Id = sal_sales.pro_code) AS 'PRODUCT',
							 sal_sales.sal_dateStart AS 'START',
							 sal_sales.sal_numberMounth AS 'MOUNTH',
							 sal_sales.sal_price_unit AS 'PRICEU',
							 sal_sales.sal_taxes AS 'TAX',
							 sal_sales.sal_total AS 'TOTAL',
							 sal_sales.sal_status AS 'STATUS'
					FROM	 sal_sales
					WHERE	 sal_sales.id_person = ".$objClient->cli_code;
			$objSales  = DB::SELECT($sql);
			
			$table = "<table name='frmassist' id='frmassist' class='display nowrap' style='width:100%' >";
			
			$table .= "<thead>";
			$table .= "<tr>";
			$table .= "<th>".$objlabels->cli_tbl_col1."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col2."</th> ";
			$table .= "<th>".$objlabels->cli_tbl_col3."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col4."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col5."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col6."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col7."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col8."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col9."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col10."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col11."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col12."</th>";
			$table .= "</tr>";
			$table .= "</thead>";

			$table .= "<tbody>";
			foreach($objSales as $sales){
				$table .= "<tr>";
				$table .= "<td>".$sales->ID."</td>";
				$table .= "<td>".$sales->CONTRACT."</td>";
				$table .= "<td>".$sales->CLIENT."</td>";
				$table .= "<td>".$sales->AGENCY."</td>";
				$table .= "<td>".$sales->SELLER."</td>";
				$table .= "<td>".$sales->PRODUCT."</td>";
				$table .= "<td>".$sales->START."</td>";
				$table .= "<td>".$sales->MOUNTH."</td>";
				$table .= "<td>".$sales->PRICEU."</td>";
				$table .= "<td>".$sales->TAX."</td>";
				$table .= "<td>".$sales->TOTAL."</td>";
				$table .= "<td>".$sales->STATUS."</td>";
				$table .= "</tr>";
			}
			$table .= "</tbody>";
			
			$table .= "</table>";
			
			return response()->json(['success'=>1,'table'=>$table]);
			
		}catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}
	} // Fin searchClient

	public function assistanceClient(Request $request, $searchid="1709331589", $searchname="", $searchassist="", $objlabels=""){
		try{
			if(empty($objlabels)) $objlabels = GenFunction::getLabels($lang_id, 23);
			// 1709331589
			$objClient = CliClient::where('cli_id', $searchid)->where('cli_status', 1)->first();
			// 792
			$sql = "SELECT sal_sales.id AS 'ID',
							 (SELECT sal_contract.sal_token FROM sal_contract WHERE sal_contract.Id = sal_sales.sal_contract) AS 'CONTRACT',
							 (SELECT CONCAT(cli_clients.cli_surname, ', ', cli_clients.cli_name) FROM cli_clients WHERE cli_clients.cli_code = sal_sales.id_person) AS 'CLIENT',
							 (SELECT sal_agencias.age_name FROM sal_agencias WHERE sal_agencias.age_code = sal_sales.agency_code) AS 'AGENCY',
							 (SELECT gen_sellers.sel_name FROM gen_sellers WHERE gen_sellers.sel_code = sal_sales.sel_code) AS 'SELLER',
							 (SELECT CONCAT(web_products.pro_name, ' ( ', web_products.pro_code, ' )') FROM web_products WHERE web_products.pro_code = sal_sales.pro_code  limit 1) AS 'PRODUCT',
							 sal_sales.sal_dateStart AS 'START',
							 sal_sales.sal_numberMounth AS 'MOUNTH',
							 sal_sales.sal_price_unit AS 'PRICEU',
							 sal_sales.sal_taxes AS 'TAX',
							 sal_sales.sal_total AS 'TOTAL',
							 sal_sales.sal_status AS 'STATUS'
					FROM	 sal_sales
					WHERE	 sal_sales.id_person = ".$objClient->cli_code;
			$objSales  = DB::SELECT($sql);
			
			$table = "<table name='IdDT' id='IdDT' class='display nowrap table-striped table-bordered' style='width:100%' >";
			
			$table .= "<thead>";
			$table .= "<tr>";
			$table .= "<th>".$objlabels->msg_option."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col1."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col2."</th> ";
			$table .= "<th>".$objlabels->cli_tbl_col3."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col4."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col5."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col6."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col7."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col8."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col9."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col10."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col11."</th>";
			$table .= "<th>".$objlabels->cli_tbl_col12."</th>";
			$table .= "</tr>";
			$table .= "</thead>";

			$table .= "<tbody>";
			foreach($objSales as $sales){
				$table .= "<tr>";
				$table .= "<td><input type='radio' name='opAssistance' value='".$sales->ID."' ></td>";
				$table .= "<td>".$sales->ID."</td>";
				$table .= "<td>".$sales->CONTRACT."</td>";
				$table .= "<td>".$sales->CLIENT."</td>";
				$table .= "<td>".$sales->AGENCY."</td>";
				$table .= "<td>".$sales->SELLER."</td>";
				$table .= "<td>".$sales->PRODUCT."</td>";
				$table .= "<td>".$sales->START."</td>";
				$table .= "<td>".$sales->MOUNTH."</td>";
				$table .=  "<td>".round($sales->PRICEU, 2)."</td>"; //"<td>".$sales->PRICEU."</td>";
				$table .= "<td>".round($sales->TAX, 2)."</td>";//"<td>".$sales->TAX."</td>";
				$table .= "<td>".round($sales->TOTAL, 2)."</td>"; //"<td>".$sales->TOTAL."</td>";
				$table .= "<td>".$sales->STATUS."</td>";
				$table .= "</tr>";
			}
			$table .= "</tbody>";
			
			$table .= "</table>";
			
			return $table;
			
		}catch(\Exception $e){
			return $e->getMessage();
		}
	} // Fin transClient

	public function transactionClient(Request $request, $clientId){
		try{
			$objlabels = GenFunction::getLabels(2, 23);
			$sql = 'SELECT btn_p2p_trans.bpt_id AS "id",
							 btn_p2p_trans.bpt_status AS "resp",
							 btn_p2p_trans.bpt_message AS "msg",
							 
							 btn_p2p_trans.bpt_date AS "date",
							 CONCAT(btn_p2p_trans.bpt_surname, ", ", btn_p2p_trans.bpt_name) AS "name",
							 btn_p2p_trans.bpt_product_name AS "product",
							 btn_p2p_trans.bpt_quantity AS "qty",
							 btn_p2p_trans.bpt_price AS "price",
							 btn_p2p_trans.bpt_session AS "ref",
							 if(btn_p2p_trans.bpt_estado=1, "Registrado", "No registrado") AS "estado"
					FROM	 btn_p2p_trans
					WHERE	 btn_p2p_trans.bpt_numdoc = "'.$clientId.'"';
			$objTrans  = DB::SELECT($sql);
		   		Storage::prepend('log_module_client.log',"****************");
				Storage::prepend('log_module_client.log',$sql );  
				Storage::prepend('log_module_client.log',"****************");			
			
			$table = "<table name='IdDT' id='IdDT' class='display nowrap table-striped table-bordered' style='width:100%' >";
			$table .= "<thead>";
			$table .= "<tr>";
			$table .= "<th>".$objlabels->cli_tblt_col1."</th>";
			$table .= "<th>".$objlabels->cli_tblt_col2."</th> ";
			//$table .= "<th>".$objlabels->cli_tblt_col3."</th>";
			$table .= "<th>".$objlabels->cli_tblt_col4."</th>";
			$table .= "<th>".$objlabels->cli_tblt_col5."</th>";
			$table .= "<th>".$objlabels->cli_tblt_col6."</th>";
			$table .= "<th>".$objlabels->cli_tblt_col7."</th>";
			$table .= "<th>".$objlabels->cli_tblt_col8."</th>";
			$table .= "<th>".$objlabels->cli_tblt_col9."</th>";
			$table .= "</tr>";
			$table .= "</thead>";

			$table .= "<tbody>";
			foreach($objTrans as $trans){
				$table .= "<tr>";
				$table .= "<td>".$trans->id."</td>";
				$table .= "<td>".$trans->resp."</td>";
				//$table .= "<td>".$trans->msg."</td>";
				$table .= "<td>".$trans->date."</td>";
				$table .= "<td>".$trans->name."</td>";
				$table .= "<td>".$trans->product."</td>";
				$table .= "<td>".$trans->qty."</td>";
				$table .= "<td>".round($trans->price, 2)."</td>";//"<td>".$trans->price."</td>";
				$table .= "<td>".$trans->ref."</td>";
				$table .= "</tr>";
			}
			$table .= "</tbody>";
			
			$table .= "</table>";
			
			return response()->json(['success'=>1,'table'=>$table]);
		}catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}
	}

    public function formEditMethodSupscription(Request $request,$id_sale){
		$request->session()->forget('objSales');
		$request->session()->forget('objCli');
		$request->session()->forget('requestId');
		
		if(\Auth::user()){
			
			$identificacion  = \Auth::user()->user_id;
			
			$objCli   = CliClient::where('cli_id',$identificacion)->first();
			$objSales = SalSale::find($id_sale);
			if(!empty($objSales)){
				
				$id_product = 0;
				$objWebPr = webProduct::where('pro_code',$objSales->pro_code)->first();
				$id_product = $objWebPr->Id;
				$request->session()->put('id_product',$id_product);
				$request->session()->put('objSales',$objSales);
				$request->session()->put('id_sale_fn',$objSales->id);
				$request->session()->put('objCli',$objCli);

			}
			
						
			$arr_combos    = $this->getListsMethodPay($request);
			$selectBank    = GenFunction::getCmbBankMysql($request,1,'ban_code','B');
			$fecha_start   = date("M jS, Y", strtotime("2019-01-05"));
			
			$arr_prod=array('valor_gad'=>0);

			$objform =  $request->session()->get('frm_labels');	
			
		  /* return view('client.client_supscription_edit')*/
			return view('client.client_edit_data')						
									   ->with('objform', $objform)
									   ->with('selectBank', $selectBank)
									   ->with('arr_combos', $arr_combos)
									   ->with('arr_prod', $arr_prod)
									   ->with('fecha_start', $fecha_start);		
		}else{
			return redirect('/');
		}
	
	}

	public function getListsMethodPay($request){
         
		 
		$lang_id = 2;
		
		$sql = "SELECT a.*
                FROM	 gen_typepayment a
                WHERE	lang_code = ".$lang_id;
        $cmb_tipo_pago = DB::SELECT($sql);		
		$default_m = 0;
		if($cmb_tipo_pago){
		  $default_m = $cmb_tipo_pago[0]->id;
		}
		

		$sql = "SELECT a.*
                FROM	 gen_methodpay a
                WHERE	lang_code = ".$lang_id." and pmt_method_code = ".$default_m;
        $cmb_forma_pago = DB::SELECT($sql);	

		$sql = "SELECT a.*
                FROM	 gen_credit_cards_operators a
                ";
        $cmb_operadora = DB::SELECT($sql);	
		$default_tipt = 0;
		if($cmb_operadora){
		  $default_tipt = $cmb_operadora[0]->cco_code;
		}		
		
		$sql = "SELECT a.*
                FROM	 gen_credit_cards a
                where cco_code = ".$default_tipt;
        $cmb_tipo_tarjeta = DB::SELECT($sql);	
		
		$sql = "SELECT a.*
                FROM	 gen_bank_account a
                where lan_code = ".$lang_id;
        $cmb_tipo_cuenta = DB::SELECT($sql);	

		$sql = "SELECT a.*
		FROM	 gen_bank a";
        $cmb_banco = DB::SELECT($sql);
		
		
		//tipo de documento
		
		$sql = "SELECT a.*
                FROM	 gen_typedocument a
                WHERE	tdo_status = 1 and tdo_code in(1,2,9)";
        $cmb_typedocument = DB::SELECT($sql);	
		
        $arr_combos  = array();
		$arr_combos['cmb_tipo_pago']    =$cmb_tipo_pago;
		$arr_combos['cmb_forma_pago']   =$cmb_forma_pago;
		if($request->session()->has('cmbMethodPay')){
			$arr_combos['cmb_forma_pago']   =$request->session()->get('cmbMethodPay');
		}
		
		//states dependent
		//$objStateDep = GenStateDependent::where('lang_code',$lang_id)->get();
		
		
		$arr_combos['cmb_operadora']    =$cmb_operadora;
		$arr_combos['cmb_tipo_tarjeta'] =$cmb_tipo_tarjeta;
		$arr_combos['cmb_tipo_cuenta']  =$cmb_tipo_cuenta;	
		$arr_combos['cmb_banco']        =$cmb_banco;
		$arr_combos['cmb_typedocument'] =$cmb_typedocument;
		//$arr_combos['cmb_statedep']     =$objStateDep;	
		return $arr_combos;
  }
	
	/**
	 * Show homepage for the Client
	 *
	 * @param  Request  $request
	 * @return \Illuminate\Http\Response
	*/
	public function homeClient(Request $request){
		$idclient  = "1709331589";
		
		if(\Auth::user()){
			$idclient      = \Auth::user()->user_id;
			$nameClient    = \Auth::user()->user_name;
			$surnameClient = \Auth::user()->user_last_name; 
					
			$objlabels = $this->createLabels($request);
			$DTAClient = $this->assistanceClient($request, $idclient, "", "", $objlabels);
			
			return view('client.homeClient')
					->with('DTables', $request->session()->get("DTables"))
					->with('dataTable', $DTAClient)
					->with('objform', $objlabels)
					->with('delimiter' , ",")
					->with('Nbnk' , "Cliente: ".$surnameClient.", ".$nameClient)
					->with('colvalue', 8)
					->with('idclient', $idclient)
					->with('nameClient', $nameClient)
					->with('surnameClient', $surnameClient);
		}else{
			return redirect('/');
		}
			
		
	} // Fin homeClient
	
	/**
	 * Show page for the register
	 *
	 * @param  Request  $request
	 * @return \Illuminate\Http\Response
	*/
	public function register(){
		return view('client.register');
		
	} // Fin homeClient

}