<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\GenModule;
use App\GenModuleDetail;
use App\GenLabel;
use App\GenLanguage;
use App\GenApiMaster;
use App\GenBank;
use App\Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class GenFunction extends Model{
	/**
     * Get error
     *
     * @return string Mensaje de error
     */
    public function getError(){
        return $this->error;
    }
	
    /**
     * Set error
     *
     * @param  string $newError
     * @return object $this
     */
    public function setError($newError){
        $this->error = $newError;
        return $this;
    }	

    /**
     * Validar cédula
     *
     * @param  string  $numero  Número de cédula
     *
     * @return Boolean
     */
    public static function validarCedula($numero = ''){
        // fuerzo parametro de entrada a string
        $numero = (string)$numero;
        // borro por si acaso errores de llamadas anteriores.
         (new self)->setError('');
        // validaciones
        try {
            (new self)->validarInicial($numero, '10');
            (new self)->validarCodigoProvincia(substr($numero, 0, 2));
            (new self)->validarTercerDigito($numero[2], 'cedula');
            (new self)->algoritmoModulo10(substr($numero, 0, 9), $numero[9]);
        } catch (Exception $e) {
            //$this->setError($e->getMessage());
            return false;
        }
        return true;
    }
	
    /**
     * Validar RUC persona natural
     *
     * @param  string  $numero  Número de RUC persona natural
     *
     * @return Boolean
     */
    public static function validarRucPersonaNatural($numero = ''){
        // fuerzo parametro de entrada a string
        $numero = (string)$numero;
        // borro por si acaso errores de llamadas anteriores.
        (new self)->setError('');
        // validaciones
        try {
            (new self)->validarInicial($numero, '13');
            (new self)->validarCodigoProvincia(substr($numero, 0, 2));
            (new self)->validarTercerDigito($numero[2], 'ruc_natural');
            (new self)->validarCodigoEstablecimiento(substr($numero, 10, 3));
            (new self)->algoritmoModulo10(substr($numero, 0, 9), $numero[9]);
        } catch (Exception $e) {
            (new self)->setError($e->getMessage());
            return false;
        }
        return true;
    }

    /**
	* Returns the labels of the module depending on the language
	*
	* @param  int  $lang_id
	* @return object
	*/
	public static function getLabels($lang_id, $mod_code=23){

		$objLabelsGen  = GenLabel::whereIn("mod_code",[$mod_code, 23])
									->where("lang_code","=",$lang_id)
									->get();
		$arr_labels = array();

		foreach ($objLabelsGen as $lbl) {
			$arr_labels[$lbl->wlb_code]= $lbl->wlb_detail;
		}

		$objectLb = (object)[];//new stdClass();
		foreach ($arr_labels as $key => $value){
			$objectLb->$key = $value;
		}

		return $objectLb;

	} // Fin getLabels

    /**
	* Create one label
	*
	* @param  string	$str
	* @param  string	$mod_code
	* @return string	DT
	*/
	public static function createLBL($str="", $mod_code=23, $lang_id=1){
		try{
			$name = DB::table('web_labels')
						->where('wlb_code', $str)
						->where('mod_code', $mod_code)
						->where('lang_code', $lang_id)
						->first();
			if(empty($name)){
				$newlbl = new GenLabel;
				$newlbl->lang_code = $lang_id;
				$newlbl->mod_code = $mod_code;
				$newlbl->wlb_code = $str;
				$newlbl->wlb_detail = $str;
				$newlbl->save();
			} // If no exist

			return 1;
		}catch(\Exception $e){
			return $e->getMessage();
		}
	} // Fin createLBL

    /**
	* Create tabele with field send
	*
	* @param  string	$listfields
	* @return string	DT
	*/
	public static function getDT($listfields="", $createLabel=0, $table="", $where="", $IdDT="IdDT", $lang_id=1, $mod_code){
		$DT = "";
		$objform = GenFunction::getLabels($lang_id, $mod_code);
		
		try{
			if($listfields != ""){
				// Build
				$arrFields = array();
				$arrFields = explode(",", $listfields);
				
				if($createLabel == 1){
					for($i=0; $i<count($arrFields); $i++){
						eval("\$str = \"$arrFields[$i]\";");
						$ok = GenFunction::createLBL($str, $mod_code, $lang_id);
					} // For number fields
					$objform = GenFunction::getLabels($lang_id, $mod_code);
				} // If create
				
				$DT ='<table id="'.$IdDT.'" class="display hover compact" style="width:100%">
				<caption></caption>
				<thead>
				<tr>';
				for($i=0; $i<count($arrFields); $i++){
					eval("\$str = \"$arrFields[$i]\";");
					$lenin = $objform->$str;
					eval("\$str = \"$lenin\";");
					$DT .= '<th>'.$str.'</th>';
				}
				$DT .= '</tr>
				</thead>';

				$name = DB::table($table)
				->where($where, 1)
				->get();
				
				foreach($name as $dt){
					$DT .= "<tr>";
					for($i=0; $i<count($arrFields); $i++){
						eval("\$str = \"$arrFields[$i]\";");
						$lenin = $objform->$str;
						eval("\$str1 = \"$lenin\";");
						$lbo = $arrFields[$i];
						$lenin2 = $dt->$lbo;
						$DT .= "<td data-label='".$str1."'>".$lenin2."</td>";
					}
					$DT.="</tr>";
				}
				$DT.="</table>";

				$response = array(
					'success'	=> 1,
					'message'	=> $objform->app_message_ok,
					'DT'		=> $DT
				);
				return $DT; //response()->json($response);
			}else {
				$response = array(
					'success'	=> 1,
					'message'	=> $objform->empty_data,
					'DT'		=> ''
				);
				return $DT; // response()->json($response);
			}
		}catch(\Exception $e){
			//return response()->json(['success'=>0,'message'=>$e->getMessage()]);
			return $e->getMessage();
		}
		
	} // Fin getDT

	/**
	* Create tabele with field send
	*
	* @param  string	$listfields
	* @return string	DT
	*/
	public static function getDTArray($listfields="", $createLabel=0, $array_table,  $IdDT="IdDT", $lang_id=1, $mod_code){
		$DT = "";
		$objform = GenFunction::getLabels($lang_id, $mod_code);
		
		try{
			if($listfields != ""){
				// Build
				$arrFields = array();
				$arrFields = explode(",", $listfields);
				//dd($array_table);
				
				if($createLabel == 1){
					for($i=0; $i<count($arrFields); $i++){
						eval("\$str = \"$arrFields[$i]\";");
						$ok = GenFunction::createLBL($str, $mod_code, $lang_id);
					} // For number fields
					$objform = GenFunction::getLabels($lang_id, $mod_code);
				} // If create
				
				$DT ='<table id="'.$IdDT.'" class="table table-striped table-bordered display compact stripe order-column" style="width:100%">
				<caption></caption>
				<thead>
				<tr>';
				for($i=0; $i<count($arrFields); $i++){
					eval("\$str = \"$arrFields[$i]\";");
					$lenin = $objform->$str;
					eval("\$str = \"$lenin\";");
					$DT .= '<th>'.$str.'</th>';
				}
				$DT .= '</tr>
				</thead>';
				
				$name = $array_table;
				
				foreach($name as $dt){
					$DT .= "<tr>";
					for($i=0; $i<count($arrFields); $i++){
						eval("\$str = \"$arrFields[$i]\";");
						$lenin = $objform->$str;
						$lenin2 = $dt[$str];
						$DT .= "<td data-label='".$lenin."'>".$lenin2."</td>";
					}
					$DT.="</tr>";
				}

				$DT.="</table>";

				return $DT;

			}else {
				return $objform->empty_data;
			}
			
		}catch(\Exception $e){
			return $e->getMessage();
		}
		
	} // Fin getDT
	
/**
	* Create tabele with field send
	*
	* @param  string	$listfields
	* @return string	DT
	*/
	public static function getDTArraySel($listfields="", $createLabel=0, $array_table,  $IdDT="IdDT", $lang_id=1, $mod_code, $type="CHK", $val_field){
		$DT = "";
		$objform = GenFunction::getLabels($lang_id, $mod_code);
		
		try{
			if($listfields != ""){
				// Build
				$arrFields = array();
				$arrFields = explode(",", $listfields);
				//dd($array_table);
				
				if($createLabel == 1){
					for($i=0; $i<count($arrFields); $i++){
						eval("\$str = \"$arrFields[$i]\";");
						$ok = GenFunction::createLBL($str, $mod_code, $lang_id);
					} // For number fields
					$objform = GenFunction::getLabels($lang_id, $mod_code);
				} // If create
				
				$DT ='<table id="'.$IdDT.'" class="table table-striped table-bordered display compact stripe order-column" style="width:100%">
				<caption></caption>
				<thead>
				<tr>';
				
				$DT .= '<th>Seleccionar</th>';

				
				for($i=0; $i<count($arrFields); $i++){
					

					eval("\$str = \"$arrFields[$i]\";");
					$lenin = $objform->$str;
					eval("\$str = \"$lenin\";");
					$DT .= '<th>'.$str.'</th>';
				}
				$DT .= '</tr>
				</thead>';
				
				$name = $array_table;
				$i=0;
				foreach($name as $dt){
					$DT .= "<tr>";
                    
					$hd = '';
					$val='';

					for($i=0; $i<count($arrFields); $i++){
						eval("\$str = \"$arrFields[$i]\";");
						$lenin = $objform->$str;
						$lenin2 = $dt[$str];
						$hd .= "<td data-label='".$lenin."'>".$lenin2."</td>";
						
						if($val_field==$arrFields[$i]){
							$val=$dt[$str];
						}
					}
					

					
					
					
					if($type=="CHK"){
						$DT .= '<td data-label="sel_id_fl"><input type="checkbox" id="sel_id_fl" name="sel_id_fl" value="'.$val.'" /></td>';
					}elseif($type=="RAD"){
						$DT .= '<td data-label="sel_id_fl"><input type="radio" id="sel_id_fl" name="sel_id_fl" value="'.$val.'" /></td>';
					}					
					$DT .=	$hd;				
					$DT.="</tr>";
					$i++;
				}

				$DT.="</table>";

				return $DT;

			}else {
				return $objform->empty_data;
			}
			
		}catch(\Exception $e){
			return $e->getMessage();
		}
		
	} // Fin getDT	
	
	/*
	** Get IP address
	**
	** @return string	IP
	*/
	public static function getRealIpAddr(){
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	} // Fin getRealIpAddr
	
	/**
     * Call API
     *
     * @param  string   $api
	 * @param  string   $api_lang
	 * @param  string   $action
	 * @param  string   $user_name
	 * @param  string   $attributes
	 * @param  string   $id_list
     * @return object $json
     */
	public static function procesarApiGen($api_code,$api_lang,$action,$user_name="",$attributes="",$id_list=""){
		$ip           = GenFunction::getRealIpAddr();
		$objApi       = GenApiMaster::where('api_parent','=',$api_code)->where('api_action','=',$action)->first();
		$objPrefix    = GenParam::where('par_name','=','api_prefix')->first();
		$prefix       = $objPrefix->par_description;
		$table_arr    = explode('?',$objApi->api_table);
		$rest_pref    = empty($table_arr[1]) ? '': $table_arr[1]; 
		$prefix_table = $table_arr[0].$prefix.'?'.$rest_pref;
		$url    = $objApi->api_link.'/'.$objApi->api_module.'/'.$prefix_table.'&tipo='.$objApi->api_code_action;

		if($action=='LIST_S'){
			$url   .= '&usr='.$user_name.'&ip='.$ip.'&id='.$id_list.'&datos=';
			$url   .= '&usr='.$user_name.'&ip='.$ip.'&'.$attributes;
		}elseif($action=='LIST_S_CXC'){
			$url    = $objApi->api_link.'/'.$objApi->api_module.'/'.$prefix_table;
			$url   .= $attributes;
		}elseif($action=='SAVE_P'){
			$url    = $objApi->api_link.'/'.$objApi->api_module.'/'.$prefix_table;
			$url   .= $attributes;
		}elseif($action=='BANK_C'){
			$url    = $objApi->api_link.'/'.$objApi->api_module.'/'.$prefix_table;
			$url   .= $attributes;
		}elseif($action=='LIST_I'){
			$url    = $objApi->api_link.'/'.$objApi->api_module.'/'.$prefix_table;
			$url   .= "tabla=poliza&tipo=".$id_list;
			$url   .= '&usr='.$user_name.'&ip='.$ip.'&id='.$attributes.'&datos=';
		}elseif($action=='LIST_INF'){
			$url    = $objApi->api_link.'/'.$objApi->api_module.'/'.$prefix_table;
			$url   .= "tabla=poliza&tipo=S";
			$url   .= '&usr='.$user_name.'&ip='.$ip.'&id='.$attributes.'&datos=';
		}elseif($action=='LIST_CON'){
			$url    = $objApi->api_link.'/'.$objApi->api_module.'/'.$prefix_table;
			$url   .= "tabla=poliza&tipo=2";
			$url   .= '&usr='.$user_name.'&ip='.$ip.'&id='.$attributes.'&datos=';
		}elseif($action=='LIST_HISTPO'){
			#http://35.222.116.193:8181/mtcuio/api_sel_histpoliza_me?contrato=PG031259
			$url    = $objApi->api_link.'/'.$objApi->api_module.'/'.$prefix_table;
			$url   .= "contrato=".$attributes;
		}elseif($action=='LIST_NOT'){
			#http://35.222.116.193:8181/mtcuio/api_sel_histpoliza_me?contrato=PG031259
			$url    = $objApi->api_link.'/'.$objApi->api_module.'/'.$prefix_table;
			$url   .= "contrato=".$attributes;
		}elseif($action=='LIST_QUO'){
			#http://35.222.116.193:8181/mtcuio/api_sel_histpoliza_me?contrato=PG031259
			$url    = $objApi->api_link.'/'.$objApi->api_module.'/'.$prefix_table;
			$url   .= "poliza=".$attributes;
		}elseif($action=='LIST_HIST_PAGOS'){
			#http://35.222.116.193:8181/mtcuio/api_sel_histpoliza_me?contrato=PG031259
			$url    = $objApi->api_link.'/'.$objApi->api_module.'/'.$prefix_table;
			$url   .= "poliza=".$attributes;
		}elseif($action=='LIST_HIST_RBANCOS'){
			#http://35.222.116.193:8181/mtcuio/api_sel_histpoliza_me?contrato=PG031259
			$url    = $objApi->api_link.'/'.$objApi->api_module.'/'.$prefix_table;
			$url   .= "poliza=".$attributes;
		}elseif($action=='LIST_HIST_RTARJE'){
			#http://35.222.116.193:8181/mtcuio/api_sel_histpoliza_me?contrato=PG031259
			$url    = $objApi->api_link.'/'.$objApi->api_module.'/'.$prefix_table;
			$url   .= "poliza=".$attributes;
		}elseif ($action == 'DATOS_CONTRATO') {
            #http://35.222.116.193:8181/mtcuio/api_sel_histcxc_me?contrato=PF002481
            $url = $objApi->api_link . '/' . $objApi->api_module . '/' . $prefix_table;
            $url .= "contrato=" . $attributes;
        }elseif ($action == 'CAMBIO_FECHA') {

            //http: //35.222.116.193:8181/mtcuio/api_cambio_fechacxc_me?tabla=cuentasxcobrar&tipo=1&usr=mpazmino&ip=127.0.0.1&id=&datos=255725,2019-07-15]255726,2019-08-15
            $url = $objApi->api_link . '/' . $objApi->api_module . '/' . $prefix_table;
            $url .= "tabla=cuentasxcobrar&tipo=1";
            $url .= '&usr=' . $user_name . '&ip=' . $ip . '&id=&datos='. $attributes ;

        }else{
			$url   .= '&usr='.$user_name.'&ip='.$ip.'&'.$attributes;
		}
		$fecha = GenFunction::getDateDB();
		Storage::prepend('log_api_gen.log',"****************");
		Storage::prepend('log_api_gen.log',"API=".$api_code."::".$fecha);              
		Storage::prepend('log_api_gen.log',$url);   

		$json = GenFunction::file_get_contents_curl($url);
		$json = json_decode($json, true);
		$response_json  = explode(',',$objApi->api_response_json);
		$level_arr      = array();

		for($i=0;$i<count($response_json);$i++){
			$level_str = $response_json[$i];
			if($i==0){
				$level_str= $level_str.$prefix;
				$level_arr = $json[$level_str];
			}else{
				$level_arr = $level_arr[$level_str];
			}
		}
		$fecha = GenFunction::getDateDB();
		Storage::prepend('log_api_gen.log',"Fin::".$fecha);
		Storage::prepend('log_api_gen.log',"****************");
		return $level_arr;
     } // Fin procesarApiGen
	
	/**
    * File get contents via URL
    *
    * @param  string $url
    * @return object $data
    */
	public static function file_get_contents_curl($url) {
		$clientID = array(
			'Client-ID: 0000000000000000000000000000000'
		);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $clientID);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		$data = curl_exec($ch);
		curl_close($ch);

		return $data;
	} // Fin file_get_contents_curl

	/**
    * Get date by database
    *
    * @return object $date
    */
	public static function getDateDB(){
		$sql = "SELECT NOW() as fecha";
		$datetime = DB::SELECT($sql);
		
		return $datetime[0]->fecha;
	} // Fin getDateDB
	
	/**
    * Replace char latin by htmlentities
    *
    * @return object change
    */
	public static function TildesHtml($cadena){ 
		return str_replace(array("á","é","í","ó","ú","ñ","Á","É","Í","Ó","Ú","Ñ"),
			array("&aacute;","&eacute;","&iacute;","&oacute;","&uacute;","&ntilde;","&Aacute;","&Eacute;","&Iacute;","&Oacute;","&Uacute;","&Ntilde;"), 
			$cadena);
	}
	
	/*
	*	Funcion que calcula la edad en base a una fecha
	*/
	public static function Edad($fecha_nacimiento){
		$dia=date("d");
		$mes=date("m");
		$ano=date("Y");

		$dianaz=date("d",strtotime($fecha_nacimiento));
		$mesnaz=date("m",strtotime($fecha_nacimiento));
		$anonaz=date("Y",strtotime($fecha_nacimiento));

		//si el mes es el mismo pero el día inferior aun no ha cumplido años, le quitaremos un año al actual
		if (($mesnaz == $mes) && ($dianaz > $dia)) {
			$ano=($ano-1);
		}

		//si el mes es superior al actual tampoco habrá cumplido años, por eso le quitamos un año al actual
		if ($mesnaz > $mes) {
			$ano=($ano-1);
		}

		//ya no habría mas condiciones, ahora simplemente restamos los años y mostramos el resultado como su edad
		$edad=($ano-$anonaz);

		return $edad;

	} // Fin Edad
	
	/**
	* Set the variables session
	*
	* @param  int  $lang_id
	* @param  int  $mod_code
	*
	* @return session
	*/
	public static function cargaSession($request, $mod_code, $lang_id){
		$request->session()->put('mod_code', $mod_code);
		$request->session()->put('lang_id', $lang_id);
		
		if($request->session()->has('user_code')){
			$user_code  = $request->session()->get('user_code');
		}else{
			$user_code  = 1;
			$request->session()->put('user_code', $user_code);
		}
		
		$ip	= GenFunction::getRealIpAddr();
		$request->session()->put('ip', $ip);

		if($request->session()->has('id_pais')){
			$id_pais  = $request->session()->get('id_pais');
		}else{
			$id_pais  = 63;
			$request->session()->put('id_pais', $id_pais);
		}
	} // Fin cargaSession
	
	/**
	 * get data for Select Bank
	 *
	 * @param  Request  $request
	 * @param  integer  $ins_code
	 * @param  decimal  $tot_price
	 *
	 * @return view		getCmbBank
	*/
	public static function getCmbBank($request, $cmb=0, $name="", $type='B'){
		try{
			GenFunction::createLBL("lbl_select_record", $request->session()->get('mod_code'), $request->session()->get('lang_id'));
			$objform = GenFunction::getLabels($request->session()->get('lang_id'), $request->session()->get('mod_code'));
			$attribute = "formap=BH,MIS.BANCOS";
			$json_product = GenFunction::procesarApi(26, $request->session()->get('lang_id'), "BANK_C", $request->session()->get('user_code'), $attribute, "");
			//
			// Construyo CMB
			//
			$lboAPI = $var1 = $json_product['api_sel_bantj_me']['CLIENT_LIST'];
			if($var1 != 'Error: Archivo  NO EXISTE'){
				// Validate Bank
				foreach($lboAPI as $val){
					$lbo = explode(',', $val);
					// Validate Bank
					$bank1 = GenBank::where('ban_id', $lbo[0])->first();
					if(empty($bank1)){
						$bank = new GenBank;
						$bank->ban_id = $lbo[0];
						$bank->ban_name = $lbo[1];
						$bank->ban_type = $type;
						$bank->ban_status = 1;
						$bank->user_code = $request->session()->get('user_code');
						$bank->user_ip = GenFunction::getRealIpAddr();
						$bank->save();
					}// Fin validacion
				} // Fin validacion bank
				if($cmb == 1){
					$lbos = GenBank::select('ban_id', 'ban_name')->where('ban_type', $type)->orderBy('ban_name')->get();
					$hmtl_var  = '<select id="'.$name.'" name="'.$name.'" class="form-control" data-style="btn-primary" >';
					$hmtl_var .= '<option value="0">'.$objform->lbl_select_record.'</option>';
					foreach($lbos as $lbo){
						$hmtl_var .= '<option value="'.$lbo->ban_id.'">'.$lbo->ban_name.'</option>';
					}
					$hmtl_var .= '</select>';
				}else{
					$hmtl_var = $var1;
				} // Fin cmb

			}else{
				if($cmb == 1){
					$hmtl_var = '<select id="'.$name.'" name="'.$name.'">
					<option value="0">'.$objform->lbl_select_record.'</option>
					</select>';
				}else{
					$hmtl_var = "";
				} // Fin cmb
			}

			return $hmtl_var;

		}catch(\Exception $e){
			return $e->getMessage();
		}
	} // Fin getCmbBank
	
	/**
	 * get data for Select Bank - Mysql
	 *
	 * @param  Request  $request
	 * @param  integer  $ins_code
	 * @param  decimal  $tot_price
	 *
	 * @return view		getCmbBank
	*/
	public static function getCmbBankMysql($request, $cmb=0, $name="", $type='B'){
		try{
			GenFunction::createLBL("lbl_select_record", $request->session()->get('mod_code'), $request->session()->get('lang_id'));
			$objform = GenFunction::getLabels(2, 16);

				if($cmb == 1){
					$lbos = GenBank::select('ban_id', 'ban_name')->where('ban_type', $type)->orderBy('ban_name')->get();
					$hmtl_var  = '<select id="'.$name.'" name="'.$name.'" class="form-control" data-style="btn-primary" >';
					$hmtl_var .= '<option value="0">'.$objform->lbl_select_record.'</option>';
					foreach($lbos as $lbo){
						$hmtl_var .= '<option value="'.$lbo->ban_id.'">'.$lbo->ban_name.'</option>';
					}
					$hmtl_var .= '</select>';
				}else{
					$hmtl_var = "";
				} // Fin cmb

			return $hmtl_var;

		}catch(\Exception $e){
			return $e->getMessage();
		}
	} // Fin getCmbBank Sin Api

	/**
	 * get data for Select Operator
	 *
	 * @param  Request  $request
	 * @param  integer  $ins_code
	 * @param  decimal  $tot_price
	 *
	 * @return object/cmb	getOperator
	*/
	public static function getOperator($request, $cmb=0, $name=""){
		try{
			GenFunction::createLBL("lbl_select_record", $request->session()->get('mod_code'), $request->session()->get('lang_id'));
			$objform = GenFunction::getLabels($request->session()->get('lang_id'), $request->session()->get('mod_code'));
			$hmtl_var = GenFunction::getCmbBank($request, $cmb, $name, 'T');
			/*
			$var1 = GenCreditCardsOperator::where("cco_status", "=", 1)->get();
			//
			// Construyo CMB
			//
			if(!empty($var1)){
				if($cmb == 1){
					$lbos = GenBank::select('ban_id', 'ban_name')->where('ban_type', 'T')->orderBy('ban_name')->get();
					$hmtl_var = '<select id="'.$name.'" name="'.$name.'" class="form-control" data-style="btn-primary" >';
					$hmtl_var .= '<option value="0">'.$objform->lbl_select_record.'</option>';
					//foreach($lbos as $lbo){
					foreach($var1 as $lbo){
						$hmtl_var .= '<option value="'.$lbo->cco_Id.'">'.$lbo->cco_name.'</option>';
					}
					$hmtl_var .= '</select>';
				}else{
					$hmtl_var = $var1;
				} // Fin cmb
			}else{
				if($cmb == 1){
					$hmtl_var = '<select id="'.$name.'" name="'.$name.'">
					<option value="0">'.$objform->lbl_select_record.'</option>
					</select>';
				}else{
					$hmtl_var = "";
				} // Fin cmb
			}
			*/
			return $hmtl_var;

		}catch(\Exception $e){
			return $e->getMessage();
		}
	} // Fin getOperator

	/**
	 * get data for Operator Systems
	 *
	 * @return string	OS
	 * Example:
	 *			if(GenFunction::OS() === "Windows" ){
	 *				$Windows = true;
	 *			}
	*/
	public static function OS(){
		if(strpos($_SERVER['HTTP_USER_AGENT'],"iPhone")!== false ) $ret = "iPhone";
		else if(strpos($_SERVER['HTTP_USER_AGENT'],"Android")!== false ) $ret = "iPhone";
		else if(strpos($_SERVER['HTTP_USER_AGENT'],"webOS")!== false ) $ret = "webOS";
		else if(strpos($_SERVER['HTTP_USER_AGENT'],"BlackBerry")!== false ) $ret = "BlackBerry";
		else if(strpos($_SERVER['HTTP_USER_AGENT'],"iPod")!== false ) $ret = "iPod";
		else if(strpos($_SERVER['HTTP_USER_AGENT'],"iPad")!== false ) $ret = "iPad";
		else if(strpos($_SERVER['HTTP_USER_AGENT'],"Windows")!== false ) $ret = "Windows";
		else if(strpos($_SERVER['HTTP_USER_AGENT'],"Macintosh")!== false ) $ret = "Macintosh";
		else if(strpos($_SERVER['HTTP_USER_AGENT'],"Linux")!== false ) $ret = "Linux";
		
		return $ret;
	} // Fin _OS
	
	/**
	 * Create a directory
	 *
	 * @param  string  Name directory
	 *
	 * @return string	OS
	*/
	public static function createDirectory($directoryName){
		if(!is_dir($directoryName)){
			//Directory does not exist, so lets create it.
			mkdir($directoryName, 0755, true);
		}
	} // Fin createDirectory

	/**
	 * Create a access Session Data
	 *
	 * @param  string	nameSession
	 *
	 * @return string	value session
	*/
	public static function accessSessionData($request, $nameSession) {
		if($request->session()->has($nameSession))
			$msg = $request->session()->get($nameSession);
		else
			$msg = 0;
		return $msg;
	} // Fin accessSessionData
	
	/**
	 * Store Session Data
	 *
	 * @param  string	nameSession
	 * @param  string	valueSession
	 *
	 * @return string	session
	*/
	public static function storeSessionData($request, $nameSession, $valueSession) {
		$request->session()->put($nameSession, $valueSession);
		return 1;
	} // Fin storeSessionData
	
	/**
	 * Delete Session Data
	 *
	 * @param  string	nameSession
	 *
	 * @return destroy session
	*/
	public static function deleteSessionData($request, $nameSession) {
		$request->session()->forget($nameSession);
		return 1;
	} // Fin deleteSessionData

	/**
	 * Obtiene el valor de una moneda a otra / Get the value from one currency to another
	 *
	 * @param  string	from
	 * @param  string	to
	 * @param  string	amount
	 *
	 * @return object
	*/
	public static function converCurrency($from,$to,$amount){
		$url = "http://www.google.com/finance/converter?a=$amount&from=$from&to=$to"; 
		$request = curl_init(); 
		$timeOut = 0; 
		curl_setopt ($request, CURLOPT_URL, $url); 
		curl_setopt ($request, CURLOPT_RETURNTRANSFER, 1); 
		curl_setopt ($request, CURLOPT_USERAGENT,"Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1)"); 
		curl_setopt ($request, CURLOPT_CONNECTTIMEOUT, $timeOut); 
		$response = curl_exec($request); 
		curl_close($request); 
		return $response;
    } // Fin converCurrency

	/*
	*	Genera una clave acorde a una longitud
	*	Generates a key according to a length
	*/
	public static function generarToken($cantidad=1, $longitud=16, $incluyeChar=true){ 
		$caracteres = "1234567890"; 
		if($incluyeChar) 
			$caracteres .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 

		$arrPassResult=array(); 
		$index=0; 
		while($index<$cantidad){ 
			$tmp="VIR"; 
			for($i=0;$i<$longitud;$i++){ 
				$tmp.=$caracteres[rand(0,strlen($caracteres)-1)]; 
			} 
			if(!in_array($tmp, $arrPassResult)){ 
				$arrPassResult[]=$tmp; 
				$index++; 
			} 
		} 
		return $tmp; 
	} // Fin function generarCodigos

    /**
     * Validaciones iniciales para CI y RUC
     *
     * @param  string  $numero      CI o RUC
     * @param  integer $caracteres  Cantidad de caracteres requeridos
     *
     * @return Boolean
     *
     * @throws exception Cuando valor esta vacio, cuando no es dígito y
     * cuando no tiene cantidad requerida de caracteres
     */
    protected function validarInicial($numero, $caracteres){
        if (empty($numero)) {
            throw new ModelNotFoundException('Valor no puede estar vacio');
        }
        if (!ctype_digit($numero)) {
            throw new ModelNotFoundException('Valor ingresado solo puede tener dígitos');
        }
        if (strlen($numero) != $caracteres) {
            throw new ModelNotFoundException('Valor ingresado debe tener '.$caracteres.' caracteres');
        }
        return true;
    }
    
	/**
     * Validación de código de provincia (dos primeros dígitos de CI/RUC)
     *
     * @param  string  $numero  Dos primeros dígitos de CI/RUC
     *
     * @return boolean
     *
     * @throws exception Cuando el código de provincia no esta entre 00 y 24
     */
    protected function validarCodigoProvincia($numero){
        if ($numero < 0 OR $numero > 24) {
            throw new ModelNotFoundException('Codigo de Provincia (dos primeros dígitos) no deben ser mayor a 24 ni menores a 0');
        }
        return true;
    }
	
    /**
     * Validación de tercer dígito
     *
     * Permite validad el tercer dígito del documento. Dependiendo
     * del campo tipo (tipo de identificación) se realizan las validaciones.
     * Los posibles valores del campo tipo son: cedula, ruc_natural, ruc_privada
     *
     * Para Cédulas y RUC de personas naturales el terder dígito debe
     * estar entre 0 y 5 (0,1,2,3,4,5)
     *
     * Para RUC de sociedades privadas el terder dígito debe ser
     * igual a 9.
     *
     * Para RUC de sociedades públicas el terder dígito debe ser 
     * igual a 6.
     *
     * @param  string $numero  tercer dígito de CI/RUC
     * @param  string $tipo  tipo de identificador
     *
     * @return boolean
     *
     * @throws exception Cuando el tercer digito no es válido. El mensaje
     * de error depende del tipo de Idenficiación.
     */
    protected function validarTercerDigito($numero, $tipo){
        switch ($tipo) {
            case 'cedula':
            case 'ruc_natural':
                if ($numero < 0 OR $numero > 5) {
                    throw new ModelNotFoundException('Tercer dígito debe ser mayor o igual a 0 y menor a 6 para cédulas y RUC de persona natural');
                }
                break;
            case 'ruc_privada':
                if ($numero != 9) {
                    throw new ModelNotFoundException('Tercer dígito debe ser igual a 9 para sociedades privadas');
                }
                break;
            case 'ruc_publica':
                if ($numero != 6) {
                    throw new ModelNotFoundException('Tercer dígito debe ser igual a 6 para sociedades públicas');
                }
                break;
            default:
                throw new ModelNotFoundException('Tipo de Identificación no existe.');
                break;
        }
        return true;
    }
    
	/**
     * Validación de código de establecimiento
     *
     * @param  string $numero  tercer dígito de CI/RUC
     *
     * @return boolean
     *
     * @throws exception Cuando el establecimiento es menor a 1
     */
    protected function validarCodigoEstablecimiento($numero){
        if ($numero < 1) {
            throw new ModelNotFoundException('Código de establecimiento no puede ser 0');
        }
        return true;
    }
    
	/**
     * Algoritmo Modulo10 para validar si CI y RUC de persona natural son válidos.
     *
     * Los coeficientes usados para verificar el décimo dígito de la cédula,
     * mediante el algoritmo “Módulo 10” son:  2. 1. 2. 1. 2. 1. 2. 1. 2
     *
     * Paso 1: Multiplicar cada dígito de los digitosIniciales por su respectivo
     * coeficiente.
     *
     *  Ejemplo
     *  digitosIniciales posicion 1  x 2
     *  digitosIniciales posicion 2  x 1
     *  digitosIniciales posicion 3  x 2
     *  digitosIniciales posicion 4  x 1
     *  digitosIniciales posicion 5  x 2
     *  digitosIniciales posicion 6  x 1
     *  digitosIniciales posicion 7  x 2
     *  digitosIniciales posicion 8  x 1
     *  digitosIniciales posicion 9  x 2
     *
     * Paso 2: Sí alguno de los resultados de cada multiplicación es mayor a o igual a 10,
     * se suma entre ambos dígitos de dicho resultado. Ex. 12->1+2->3
     *
     * Paso 3: Se suman los resultados y se obtiene total
     *
     * Paso 4: Divido total para 10, se guarda residuo. Se resta 10 menos el residuo.
     * El valor obtenido debe concordar con el digitoVerificador
     *
     * Nota: Cuando el residuo es cero(0) el dígito verificador debe ser 0.
     *
     * @param  string $digitosIniciales   Nueve primeros dígitos de CI/RUC
     * @param  string $digitoVerificador  Décimo dígito de CI/RUC
     *
     * @return boolean
     *
     * @throws exception Cuando los digitosIniciales no concuerdan contra
     * el código verificador.
     */
    protected function algoritmoModulo10($digitosIniciales, $digitoVerificador){
        $arrayCoeficientes = array(2,1,2,1,2,1,2,1,2);
        $digitoVerificador = (int)$digitoVerificador;
        $digitosIniciales = str_split($digitosIniciales);
        $total = 0;
        foreach ($digitosIniciales as $key => $value) {
            $valorPosicion = ( (int)$value * $arrayCoeficientes[$key] );
            if ($valorPosicion >= 10) {
                $valorPosicion = str_split($valorPosicion);
                $valorPosicion = array_sum($valorPosicion);
                $valorPosicion = (int)$valorPosicion;
            }
            $total = $total + $valorPosicion;
        }
        $residuo =  $total % 10;
        if ($residuo == 0) {
            $resultado = 0;
        } else {
            $resultado = 10 - $residuo;
        }
        if ($resultado != $digitoVerificador) {
            throw new ModelNotFoundException('Dígitos iniciales no validan contra Dígito Idenficador');
        }
        return true;
    }
	
    /**
     * Algoritmo Modulo11 para validar RUC de sociedades privadas y públicas
     *
     * El código verificador es el decimo digito para RUC de empresas privadas
     * y el noveno dígito para RUC de empresas públicas
     *
     * Paso 1: Multiplicar cada dígito de los digitosIniciales por su respectivo
     * coeficiente.
     *
     * Para RUC privadas el coeficiente esta definido y se multiplica con las siguientes
     * posiciones del RUC:
     *
     *  Ejemplo
     *  digitosIniciales posicion 1  x 4
     *  digitosIniciales posicion 2  x 3
     *  digitosIniciales posicion 3  x 2
     *  digitosIniciales posicion 4  x 7
     *  digitosIniciales posicion 5  x 6
     *  digitosIniciales posicion 6  x 5
     *  digitosIniciales posicion 7  x 4
     *  digitosIniciales posicion 8  x 3
     *  digitosIniciales posicion 9  x 2
     *
     * Para RUC privadas el coeficiente esta definido y se multiplica con las siguientes
     * posiciones del RUC:
     *
     *  digitosIniciales posicion 1  x 3
     *  digitosIniciales posicion 2  x 2
     *  digitosIniciales posicion 3  x 7
     *  digitosIniciales posicion 4  x 6
     *  digitosIniciales posicion 5  x 5
     *  digitosIniciales posicion 6  x 4
     *  digitosIniciales posicion 7  x 3
     *  digitosIniciales posicion 8  x 2
     *
     * Paso 2: Se suman los resultados y se obtiene total
     *
     * Paso 3: Divido total para 11, se guarda residuo. Se resta 11 menos el residuo.
     * El valor obtenido debe concordar con el digitoVerificador
     *
     * Nota: Cuando el residuo es cero(0) el dígito verificador debe ser 0.
     *
     * @param  string $digitosIniciales   Nueve primeros dígitos de RUC
     * @param  string $digitoVerificador  Décimo dígito de RUC
     * @param  string $tipo Tipo de identificador
     *
     * @return boolean
     *
     * @throws exception Cuando los digitosIniciales no concuerdan contra
     * el código verificador.
     */
    protected function algoritmoModulo11($digitosIniciales, $digitoVerificador, $tipo){
        switch ($tipo) {
            case 'ruc_privada':
                $arrayCoeficientes = array(4, 3, 2, 7, 6, 5, 4, 3, 2);
                break;
            case 'ruc_publica':
                $arrayCoeficientes = array(3, 2, 7, 6, 5, 4, 3, 2);
                break;
            default:
                throw new ModelNotFoundException('Tipo de Identificación no existe.');
                break;
        }
        $digitoVerificador = (int)$digitoVerificador;
        $digitosIniciales = str_split($digitosIniciales);
        $total = 0;
        foreach ($digitosIniciales as $key => $value) {
            $valorPosicion = ( (int)$value * $arrayCoeficientes[$key] );
            $total = $total + $valorPosicion;
        }
        $residuo =  $total % 11;
        if ($residuo == 0) {
            $resultado = 0;
        } else {
            $resultado = 11 - $residuo;
        }
        if ($resultado != $digitoVerificador) {
            throw new ModelNotFoundException('Dígitos iniciales no validan contra Dígito Idenficador');
        }
        return true;
    }
 }