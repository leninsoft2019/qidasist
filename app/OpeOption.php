<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OpeOption extends Model{
    //
	protected $table = 'ope_options';
	protected $primaryKey = 'Id';
}
