<?php
	// $lang_id
?>
<html>
	<head>
	<style>
		.dv_header_h{
			width:100%;
			height:100px;
		}
		.dv_header{
			width:100%;
			min-height:100px;
			background:#4b545c;
			color:#ffffff;
			text-align:center;
			font-weight:bold;
			font-size:30px;
			padding-top:10px;
			padding-bottom:10px;
		}
		.dv_head_ft{
			width:100%;
			height:10px; 
			background:#f25022;
		}
	</style>
	</head>
<body>
	<div class="dv_header_h">
		<div class="dv_header">
			<h1>Asistencia No {{$id_sale}}</h1>
		</div>
		<div class='dv_head_ft'></div>
	</div>
	<br>
	<b>Estimado/a  {{$nombre}} {{$apellido}}</b>
	{{$email_msg_body}}
	<br>
	<br>
	{{$email_msg_footer}}
	<br>
	<footer>
		<a href="https://memorialtechnologies.com/">MTC</a>
	</footer>
</body>
</html>