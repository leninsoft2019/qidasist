<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalContract extends Model
{
    //
	protected $table = 'sal_contract';
	protected $primaryKey = 'Id';
}
