<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");

?>
<!-- The Modal -->
<style type="text/css">
	.modal-dialog{
		max-width: 2000px;
		

	}

	#body_mod_terms{
		max-height: 600px;	
		overflow-y: scroll;	
	}

	#myModalTerms{
		max-width: 2000px;
		max-height: 800px;	

	}
	
#myModalTerms{
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  width: 2000px;
  height:800px;
}	


#body_mod_terms{
	color:black;
}

p{
	color:black;
	padding:10px;
}
	
</style>

<div class="modal" id="myModalTerms">
  <div class="modal-dialog modal-dialog-centered" id="myModalAdContent">
    <div class="modal-content">
       <style scoped>

	   </style>
      <!-- Modal Header -->
      <div class="modal-header">
        <h4 class="modal-title"><span class="span-title"><i class="ico_anullment_orange"></i>Terminos y Condiciones</span></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>

      <!-- Modal body -->
      <div class="modal-body">
			<div id="body_mod_terms">
			<!--------- ------------>
			
			
			
<p><strong>TERMINOS Y CONDICIONES</strong></p>
<p><strong>CONDICIONES GENERALES DE CONTRATACI&Oacute;N </strong></p>
<p>El acceso, reproducci&oacute;n y uso de los servicios requiere la aceptaci&oacute;n previa de las Condiciones de Uso vigentes en cada momento; LA EMPRESA se reserva el derecho de modificar dichas Condiciones cuando lo considere oportuno, mediante la publicaci&oacute;n del nuevo texto en la Web.</p>
<p>Es responsabilidad del usuario conocer las Condiciones de Uso antes de acceder a los productos y servicios de la Web; en caso de no estar conforme con las mismas, le rogamos, se abstenga de utilizarla.</p>
<p><strong>PROPIEDAD</strong></p>
<p>La Web es una obra compuesta de diversos elementos integrados e inseparables (texto, ilustraciones, fotograf&iacute;as, im&aacute;genes animadas, v&iacute;deos, programas de ordenador, cuya Propiedad Intelectual le corresponde a LA EMPRESA, salvo en lo referente a aquellos materiales obtenidos bajo licencia de terceros.</p>
<p>LA EMPRESA mantiene en todo momento la Propiedad Intelectual sobre la Web y sobre los distintos elementos que la componen, individualmente considerados, en todas las copias que se realicen (cualquiera que sea el soporte al que se incorporen). Cualquier derecho que no sea expresamente cedido se entiende reservado. Adem&aacute;s de lo anterior, LA EMPRESA, es responsable de la selecci&oacute;n, dise&ntilde;o de la estructura y disposici&oacute;n de los contenidos de la Web, as&iacute; como quien ha tomado la iniciativa y asumido el riesgo de efectuar las inversiones sustanciales orientadas a la obtenci&oacute;n, digitalizaci&oacute;n y presentaci&oacute;n de la misma. LA EMPRESA es tambi&eacute;n el &uacute;nico due&ntilde;o del dise&ntilde;o e imagen gr&aacute;fica de la Web, reserv&aacute;ndose las acciones legales pertinentes que le pudieran corresponder contra las personas que realicen imitaciones o usos desleales del mismo.</p>
<p><strong>CONTENIDOS Y COMPORTAMIENTO DEL USUARIO</strong></p>
<p>Como cliente o usuario de la Web, Ud. se compromete a hacer un uso adecuado de los contenidos y servicios ofrecidos a trav&eacute;s de esta y a no emplearlos para:</p>
<ol>
<li>Incurrir en actividades il&iacute;citas, ilegales o contrarias a la buena fe y al orden p&uacute;blico.</li>
<li>Difundir contenidos o propaganda de car&aacute;cter racista, xen&oacute;fobo, pornogr&aacute;fico, que haga apolog&iacute;a del terrorismo o que atente contra los derechos humanos.</li>
<li>Provocar da&ntilde;os en los sistemas f&iacute;sicos y l&oacute;gicos de LA EMPRESA, de sus proveedores o de terceras personas, introducir o difundir en la red virus inform&aacute;ticos o cualesquiera otros sistemas f&iacute;sicos o l&oacute;gicos que sean susceptibles de provocar los da&ntilde;os anteriormente mencionados.</li>
<li>Difundir contenidos que atenten contra la imagen y reputaci&oacute;n de LA EMPRESA o de terceros.</li>
<li>Atentar contra los derechos de Propiedad Intelectual, Industrial, de imagen, honor u otros que correspondan a LA EMPRESA o a terceros.</li>
</ol>
<p>LA EMPRESA tendr&aacute; plena libertad de decisi&oacute;n sobre si las colaboraciones y mensajes son finalmente publicadas en la Web o no, quedando facultada para retirarlos cuando estime oportuno.</p>
<p>La infracci&oacute;n de cualquiera de las normas contenidas en estas Condiciones de uso y muy especialmente, de lo previsto en la presente cl&aacute;usula, facultar&aacute;n a LA EMPRESA para darle de baja de forma inmediata como usuario o suscriptor de la Web.</p>
<p><strong>PROCEDIMIENTO DE CONTRATACION Y CONDICIONES DE VENTA</strong></p>
<p><strong>Objetivo:&nbsp;</strong>estas Condiciones de Venta&nbsp;regulan el acuerdo entre LA EMPRESA y el Cliente, a trav&eacute;s de la Web</p>
<p><strong>Elegibilidad para comprar:&nbsp;</strong>para poder hacer un pedido a trav&eacute;s de la web, el Cliente debe ser mayor de edad (18 a&ntilde;os o m&aacute;s). Los menores de edad quedan excluidos de forma expresa.</p>
<p><strong>C&oacute;mo hacer un pedido: &nbsp;</strong>para hacer un pedido a trav&eacute;s de la web, el Cliente debe seguir estos pasos:</p>
<ul>
<li>Una vez elegido el producto que desea puede pinchar el bot&oacute;n &ldquo;a&ntilde;adir al carro&rdquo; puede a&ntilde;adir al carro cuantos productos desee&nbsp;y comprarlos</li>
<li>Introducir los datos de usuario y la direcci&oacute;n de entrega.</li>
<li>Elegir la forma de pago y hacer clic en &ldquo;REALIZAR PEDIDO&rdquo;. Al pulsar el bot&oacute;n &ldquo;REALIZAR PEDIDO&rdquo; en el formulario de pedido, el Cliente acepta sin reservas todas las Condiciones de Venta que aparecen en la web en el momento de la contrataci&oacute;n.</li>
<li>Realizar el pago.</li>
<li>Si el pago est&aacute; autorizado, el Cliente recibe poco despu&eacute;s una confirmaci&oacute;n de recepci&oacute;n del pedido por email a su direcci&oacute;n electr&oacute;nica. Excepto en las circunstancias descritas m&aacute;s adelante en los siguientes apartados, esta confirmaci&oacute;n constituye la aceptaci&oacute;n por parte de LA EMPRESA del pedido del Cliente y supone un contrato vinculante entre el Cliente y LA EMPRESA. Es recomendable que el Cliente imprima y/o guarde tanto una copia de las Condiciones de venta al realizar el pedido, como del propio correo electr&oacute;nico, para posibles futuras consultas.</li>
<li>LA EMPRESA puede cambiar las Condiciones de Venta en cualquier momento, sin aviso&nbsp;previo directo al Cliente. Sin embargo, LA EMPRESA anunciar&aacute; previamente dichos cambios en el Sitio web. Adem&aacute;s, cuando el Cliente haya recibido de LA EMPRESA la confirmaci&oacute;n de recepci&oacute;n de su pedido, las Condiciones de Venta aplicables al mismo no se pueden cambiar.</li>
</ul>
<p><strong>Derecho de LA EMPRESA de cancelar un pedido:</strong>&nbsp;LA EMPRESA se reserva el derecho de rechazar cualquier pedido, cancelarlo o no aceptar un pedido confirmado por, entre otras, las siguientes razones:</p>
<p>Se produjo un error t&eacute;cnico o de precios en el Sitio web cuando se realiz&oacute; el pedido. La informaci&oacute;n de pago facilitada era incorrecta o no verificable. Los sistemas de seguridad indican que el pedido es an&oacute;malo o puede ser fraudulento. LA EMPRESA no ha podido, tras 2 intentos, entregar el pedido a la direcci&oacute;n facilitada.</p>
<p><strong>Verificaci&oacute;n de datos:&nbsp;</strong>Antes de procesar un pedido realizado por el Cliente, LA EMPRESA puede verificar los datos de este. Esto puede significar la verificaci&oacute;n de la direcci&oacute;n y la solvencia del Cliente y si puede haber alg&uacute;n fraude implicado. Respecto al posible fraude, LA EMPRESA puede utilizar una verificaci&oacute;n parcialmente autom&aacute;tica de todas las compras para filtrar las que se consideren an&oacute;malas o sospechosas y las sospechosas de fraude. Los pedidos sospechosos de fraude se investigar&aacute;n, y LA EMPRESA se reserva el derecho de reclamar a las autoridades pertinentes una acci&oacute;n judicial contra el Cliente.</p>
<p><strong>&nbsp;FORMAS DE PAGO</strong></p>
<p><strong>Precios y moneda:</strong>&nbsp;los precios que se presentan en la web incluyen IVA (Impuesto sobre el Valor Agregado) VERIFICAR y s&oacute;lo son v&aacute;lidos para los pedidos realizados en l&iacute;nea a trav&eacute;s del propio Sitio web.</p>
<p><strong>Forma de pago:</strong>&nbsp;para pagar el pedido, el Cliente puede hacerlo a trav&eacute;s de su tarjeta de cr&eacute;dito o mediante transferencia bancaria directa.</p>
<p><strong>Cambios en el precio:</strong>&nbsp;los precios que aparecen en la web est&aacute;n sujetos a cambios. LA EMPRESA se reserva el derecho de cambiar los precios sin aviso previo. Sin embargo, tras la recepci&oacute;n de la confirmaci&oacute;n del pedido, los precios a pagar por dicho pedido no se cambiar&aacute;n. LA EMPRESA informa que, a pesar de las actualizaciones que se realizan a los precios del Sitio web, &eacute;stos podr&iacute;an contener errores. Todos los errores que aparezcan en los precios se corregir&aacute;n con prontitud y no ser&aacute;n vinculantes en LA EMPRESA.</p>
<p><strong>Impuesto sobre el Valor Agregado:&nbsp;</strong>de esta forma, todas las compras realizadas a trav&eacute;s de la web estar&aacute;n sujetas al IVA,</p>
<p><strong>&nbsp;PLAZOS DE ENTREGA</strong></p>
<p><strong>D&oacute;nde y cu&aacute;ndo:</strong>&nbsp;Las entregas se realizan todos los d&iacute;as de la semana excepto los fines de semana (entendiendo fines de semana como s&aacute;bados y domingos) y festivos del lugar de destino. Los productos s&oacute;lo se pueden entregar en residencias privadas o en oficinas en las provincias del Ecuador</p>
<p>Por eso, se pide a los Clientes que utilicen sus direcciones de trabajo u otras direcciones donde haya alguien que pueda recibir el paquete.</p>
<p>Si se quiere recibir un accesorio de LA EMPRESA en cualquier otro lugar no mencionado en este documento, las condiciones variar&aacute;n seg&uacute;n la pol&iacute;tica de env&iacute;o y coste extra del proveedor log&iacute;stico en el momento de la compra.</p>
<p><strong>Plazo de entrega:</strong>&nbsp;Los pedidos se entregan en un per&iacute;odo m&aacute;ximo de 8 d&iacute;as. Normalmente se cumple con el plazo de entrega y, aunque LA EMPRESA no puede garantizar la puntualidad ya que depende a todos los efectos del operador log&iacute;stico, todos los pedidos se entregan en un per&iacute;odo de 30 d&iacute;as tras la fecha de confirmaci&oacute;n de recepci&oacute;n del pedido, como m&aacute;ximo. Una vez enviado el pedido, el Cliente recibe un correo electr&oacute;nico de notificaci&oacute;n.</p>
<p><strong>POL&Iacute;TICA DE CAMBIOS</strong></p>
<p><strong>&nbsp;</strong>Para solicitar un cambio de destino, el cliente debe contactar con atenci&oacute;n al cliente por medio del correo electr&oacute;nico email a <a href="mailto:correo1@memorial.ec"><strong>correo1@memorial.ec</strong></a><strong>; </strong><a href="mailto:correo2@memorial.ec"><strong>correo2@memorial.ec</strong></a> o a los tel&eacute;fonos <strong>1800memoial</strong>, con horario de atenci&oacute;n al p&uacute;blico de: 9H00 a 18H00 de Lunes a Viernes.</p>
<p>Le informamos tambi&eacute;n de que no aceptaremos cambios que no hayan sido previamente comunicados por las v&iacute;as indicadas.</p>
<p><strong>PROTECCI&Oacute;N DE DATOS</strong></p>
<p>La informaci&oacute;n o datos personales que nos facilite ser&aacute;n tratados con arreglo a lo establecido en la Pol&iacute;tica de Privacidad. Al hacer uso de esta p&aacute;gina web se consiente el tratamiento de dicha informaci&oacute;n y datos y se declara que toda la informaci&oacute;n o datos que nos facilite son veraces y se corresponden con la realidad.</p>
<p><strong>&nbsp;MODIFICACIONES</strong></p>
<p>LA EMPRESA se reserva el derecho de efectuar, sin previo aviso, las modificaciones que considere oportunas en la Web, pudiendo cambiar, suprimir o a&ntilde;adir tanto los contenidos y servicios que se presten a trav&eacute;s de esta, como la forma en la que &eacute;stos aparezcan presentados o localizados.</p>
<p>Aunque LA EMPRESA pondr&aacute; sus mayores esfuerzos en mantener actualizada y libre de errores la informaci&oacute;n contenida en la Web no ofrece garant&iacute;a alguna respecto de su exactitud y puesta al d&iacute;a. Tampoco est&aacute; garantizada la obtenci&oacute;n de ning&uacute;n resultado o fin concreto, por lo que el acceso y utilizaci&oacute;n de la Web, es de exclusiva responsabilidad de los usuarios y clientes.</p>
<p><strong>ACCIONES LEGALES</strong></p>
<p>LA EMPRESA perseguir&aacute; el incumplimiento de estas Condiciones de Uso, as&iacute; como cualquier utilizaci&oacute;n indebida de la Web o de sus contenidos, las infracciones de los derechos que le correspondan a ella o a sus licenciantes, especialmente los de Propiedad Intelectual e Industrial, ejercitando todas las acciones, civiles y penales, que le puedan corresponder en Derecho.</p>
<p><strong>LEGISLACI&Oacute;N APLICABLE</strong></p>
<p>Las compras realizadas a trav&eacute;s del Sitio web de <strong>MEMORIAL</strong> y bajo estas Condiciones de venta est&aacute;n sujetas a la legislaci&oacute;n ECUATORIANA.</p>
<p><strong>JURISDICCION COMPETENTE</strong></p>
<p>Cualquier disputa o discrepancia que surja de la aplicaci&oacute;n o interpretaci&oacute;n de las Condiciones de venta, as&iacute; como los contratos que incorpora, se resolver&aacute; en los tribunales de Conciliaci&oacute;n de la C&aacute;mara de Comercio o del Colegio Abogados, que tendr&aacute; competencia exclusiva por encima de cualquier otra jurisdicci&oacute;n. Sin embargo, si el Cliente goza del estado de consumidor seg&uacute;n la Ley para la defensa de los consumidores y los usuarios, los tribunales competentes ser&aacute;n los del lugar de residencia del consumidor en Ecuador</p>
<p><strong>CONTACTAR CON LA EMPRESA</strong></p>
<p>Para cualquier consulta o incidente respecto al pedido, el Cliente puede ponerse en contacto con LA EMPRESA enviando un email a <a href="mailto:correo1@memorial.ec"><strong>correo1@memorial.ec</strong></a><strong>; </strong><a href="mailto:correo2@memorial.ec"><strong>correo2@memorial.ec</strong></a> o a los tel&eacute;fonos <strong>1800memoial</strong></p>			
			
			
			
			
			
			
			<!--------   ---------->
			</div>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-danger button_module_wh" data-dismiss="modal">Close</button>-->
&nbsp;
	    <button type="button" class="btn btn-danger button_module_wh" data-dismiss="modal">Cerrar</button>	
      </div>

    </div>
  </div>
</div>	


<!-- The Modal -->
<div class="modal" id="myModalConfirm">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">



      <!-- Modal body -->
      <div class="modal-body" id="dv_confirm_additional">
			<p>Ya existe una asistencia asociada al producto seleccionado, desea ingresar otra?</p>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger button_module_wh" data-dismiss="modal">Cancelar</button>
&nbsp;
	    <button name="btnGuardarAnul" id="btnGuardarAnul" onclick="saveAdditional()"  class="button_module">Guardar</button>		
      </div>

    </div>
  </div>
</div>	

<script type="text/javascript">
       $(function(){

          $('#dv_alert_warn').hide();
		  
		  $('#sel_tpr').on('change',function(){
			 loadListChild('sel_tpr','sel_tctg','tipo_cat_pr');
			 $('#sel_tctg').trigger('change'); 
		  });
		  
		  
		  $('#sel_tctg').on('change',function(){
			  
			 loadListChild('sel_tctg','sel_car','cartera_tp');
			  
		  });		  
		  
		  $('#sel_car').on('change',function(){
			  
			 loadListChild('sel_car','sel_categmine','categoria_ctp');
			  
		  });		


       	  $('#sel_categmine').on('change', function() {
				Pace.restart();
				
				var sel_id     = $( "#sel_categmine option:selected" ).val();
				var $child_sel = $('#sel_pr');
				$.ajax({
					type: 'GET',
					url:'/insurance/cmbProduct/'+sel_id+'/',
					success:function(data){
						if(data.success==1){

							$child_sel.find('option').remove();         
							$.each( data.json_product, function(i, obj) {
								if(i==0){
									$child_sel.append('<option value="0" selected>----</option>');
									$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
								}else{
									$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
								}
							});
								
							
							return;
						}else{
							
							alertPPYA(data.message);
							return;
						}
					}
				});	
             
		  });		  
		  
       	  $('#sel_pr').on('change', function() {
       	  	 $("#id_producto").val(this.value);
       	  	 var id_producto  = this.value;


				$.ajax({
						type:	'GET',
						url:	'/insurance/get_data_product/'+id_producto+'/',
						success:function(data){
							console.log(data);
							if(data.success==1){

								var valor_pr  = parseFloat(data.arr_product.precio_unit)*parseFloat(data.arr_product.npc);
								$("#ad_valor").val(valor_pr.toFixed(2));
								return;
							}else if(data.success==2){
								$('#dv_alert_warn').show();
								$('#dv_alert_warn').addClass('in');
								$('#dv_alert_warn').addClass('show');	
								var valor_pr  = parseFloat(data.arr_product.precio_unit)*parseFloat(data.arr_product.npc);
								$("#ad_valor").val(valor_pr.toFixed(2));								
								/*$('#dialog-confirm').dialog('open');*/
								return;
							}
						}
				}); 
             
		  });
		  
		  
		  
		  
		  
       });



		function loadListChild(parent_name,child_name,child_pref){
			Pace.restart();
			
			var sel_id = $( "#"+parent_name+" option:selected" ).val();
			var $child_sel = $('#'+child_name);
			$.ajax({
				type: 'GET',
				url:'/general/getListChild/'+child_pref+'/'+sel_id+'/',
				success:function(data){
					if(data.success==1){

					    if(child_name=='sel_car'){
							$child_sel.find('option').remove();         
							$.each( data.modelData, function(i, obj) {
								  if(i==0){
									  $child_sel.append('<option value="0" selected>----</option>');
								  }
								if(obj.por_additional == 1){
									if(i==0){
										
										$child_sel.append('<option value=' + obj.por_id + ' >' + obj.por_name + '</option>');
									}else{
										$child_sel.append('<option value=' + obj.por_id + ' >' + obj.por_name + '</option>');
									}
								}

							});
						}else{
								$child_sel.find('option').remove();         
								$.each( data.arr_dataw2ui, function(i, obj) {
									if(i==0){
										$child_sel.append('<option value="0" selected>----</option>');
										$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
									}else{
										$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
									}
								});
							
						}
						

							
						
						return;
					}else{
						
						alertPPYA(data.message);
						return;
					}
				}
			});
		}

</script>
