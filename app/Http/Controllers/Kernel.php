<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;

use App\GenFunction;
use App\webProduct;
use App\WebRanges;
use App\WebPerson;
use App\WebSale;
use App\WebSalesDependent;
use App\GenFunctionP2P;
use App\BtnP2pParam;
use App\BtnP2pTran;
use App\SalContract;
use App\SalSale;
use App\SalMethodPay;
use App\SalMethodpaySubscription;
use App\CliClient;
use App\SalSxk;
use Auth;


class SalesController extends Controller
{
    //


	/**
	 * Show the intro page of the module
	 *
	 * @param  Request  $request
	 * @return \Illuminate\Http\Response
	*/
    public function showHome(Request $request,$api_code,$mod_code){
		$lang_id= $request->session()->get('lang_id');
		$request->session()->put('api_code',$api_code);
		$request->session()->put('mod_code',$mod_code);
		$modules = GenModuleDetail::where('lang_code','=',$lang_id)->get();

        $request->session()->forget('frm_labels');
        $this->createLabelsModule($mod_code, $lang_id);
        $objForm = GenFunction::getLabels($lang_id, $mod_code);

		$request->session()->put('frm_labels',$objForm);
		$request->session()->forget('arr_insurance_tmp');
		$request->session()->forget('arr_tmp_titular');
		$request->session()->forget('arr_tmp_dependiente');
		$request->session()->forget('arr_tmp_titular_find');
		$request->session()->forget('action_ins');
		$request->session()->forget('action');
		$request->session()->forget('lock_edit');
		$request->session()->forget('objGenPrAge');

        $request->session()->forget('edit_only_tit');
        $request->session()->forget('html_var_search');
        $request->session()->forget('tarjeta_virtual');
        $request->session()->forget('string_attach');
        $request->session()->forget('string_cards');  
        $request->session()->forget('cmbMethodPay');  		


		$DTables = GenLanguage::where('lang_code', $lang_id)
								->pluck('lang_DataTables')
								->first();   
		$request->session()->put('DTables',$DTables);     

		return view('insurance.home')
				->with('modules',$modules)
				->with('DTables',$DTables)
				->with('objform',$objForm)
				->with('api_code',$api_code);
    } // Fin showHome


    public function showListProductsSeller(){
		$arrWebProduct = webProduct::where('pro_status', 1)->get();
		return view('home')
				->with('arrWebProduct', $arrWebProduct);
	}



    public function showListProducts(){
		$arrWebProduct = webProduct::where('pro_status', 1)->get();
		return view('sales.home_product_list')
				->with('arrWebProduct', $arrWebProduct);
	}

/**
     * Quote the selected product.
     *
     * @return \Illuminate\Http\Response
     */
    public function detailWebProduct(Request $request, $idProduct){
        //
                               $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
							   $arrWebProduct = webProduct::find($idProduct);
                               $precio = $arrWebProduct->pro_valor;
							   $request->session()->put('id_product',$idProduct);
							   
                               
                               return view('sales.quotation.quotation')
                                                               ->with('arrWebRanges', $arrWebRanges)
                                                               ->with('idProduct', $idProduct)
															   ->with('arrWebProduct', $arrWebProduct)
                                                               ->with('precio', $precio);
    } // detailWebProduct

	
/**
     * Quote the selected product.
     *
     * @return \Illuminate\Http\Response
     */
    public function detailWebProductTit(Request $request, $idProduct){
        //
                               $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
							   $arrWebProduct = webProduct::find($idProduct);
                               $precio = $arrWebProduct->pro_price;
							   $request->session()->put('id_product',$idProduct);
                               
                               return view('sales.quotation.titular_data')
                                                               ->with('arrWebRanges', $arrWebRanges)
                                                               ->with('idProduct', $idProduct)
															   ->with('arrWebProduct', $arrWebProduct)
                                                               ->with('precio', $precio);
    } // detailWebProduct
	



   public function getDataClient(Request $request){
	   
	   try{
		   
		   
		   $cli_id    = $request->session()->get('cli_id');
		   $objPerson =  WebPerson::find($cli_id);
		   if(empty($objPerson)){
			 return response()->json(['success'=>0,'message'=>'No existen datos de cliente '.$cli_id]);  
		   }
		   $data_person = array();
		   $data_person['nombre']    = $objPerson->per_name;
		   $data_person['apellido']  = $objPerson->per_surname;
		   $data_person['id']        = $objPerson->per_id;
		   $data_person['direccion'] = $objPerson->per_address;
		   $data_person['email'] = $objPerson->per_email;
		   
		   
		   $status = 1;
			$mensaje='OK';
			$response = array(
				'success' 		 => $status,
				'message' 		 => $mensaje,
				'data_person' 	 => $data_person
			);
			return response()->json($response);		   
		   
	   }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
	}
   }

   public function titSave(Request $request){
	   
	   try{
		   
		  
		    $idProduct = $request->session()->get('id_product');
		    $objPerson =  WebPerson::where('per_id',$request->id)->first();
			if(empty($objPerson)){
				$objPerson = new WebPerson;
			}
			
			$objPerson->per_id      = $request->id;
			$objPerson->per_name    = $request->nombre;
			$objPerson->per_surname = $request->apellido;
			$objPerson->per_email   = $request->email;
			$objPerson->per_address = $request->per_address;
			$objPerson->per_phone   = $request->per_phone;
			$objPerson->per_type    = 1;
			$objPerson->per_status  = 1;
		    $objPerson->save();
			
			if(empty($objPerson)){
				return response()->json(['success'=>0,'message'=>'Hubo un error al guardar la informacion del titular']);
			}
			$request->session()->put('per_id',$request->id);
			$request->session()->put('cli_id',$objPerson->Id);
			
			
			$objSale =  new WebSale;
			$objSale->id_product  = $idProduct;
			$objSale->id_person   = $objPerson->Id;
			$objSale->sal_date    = now();
			$objSale->sal_token   = $idProduct.'_'.now();
			$objSale->sal_reference   ='';
			$objSale->agency_code   =1;
			$objSale->seller_code   =1;
			$objSale->sal_periodo   ='1,12,4';
			$objSale->sal_status   =1;
			$objSale->save();
			
			$request->session()->put('sale_id',$objSale->id);
			
			$status = 1;
			$mensaje='OK';
			$response = array(
				'success' 		 => $status,
				'message' 		 => $mensaje
			);
			return response()->json($response);		   
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}
   }
   

   public function multSave_old(Request $request){
	   
	   try{
		    
		    $idProduct = $request->session()->get('id_product');
		    $objPerson =  WebPerson::where('per_id',$request->id)->first();
			if(empty($objPerson)){
				$objPerson = new WebPerson;
			}
			
			$objPerson->per_id      = $request->id;
			$objPerson->per_name    = $request->nombre;
			$objPerson->per_surname = $request->apellido;
			$objPerson->per_email   = $request->email;
			$objPerson->per_address = $request->per_address;
			$objPerson->per_phone   = $request->per_phone;
			$objPerson->per_type    = 1;
			$objPerson->per_status  = 1;
		    $objPerson->save();
			
			if(empty($objPerson)){
				return response()->json(['success'=>0,'message'=>'Hubo un error al guardar la informacion del titular']);
			}
			$request->session()->put('per_id',$request->id);
			
			
			$objSale =  new WebSale;
			$objSale->id_product  = $idProduct;
			$objSale->id_person   = $objPerson->Id;
			$objSale->sal_date    = now();
			$objSale->sal_token   = $idProduct.'_'.now();
			$objSale->sal_reference   ='';
			$objSale->agency_code  =1;
			$objSale->seller_code  =1;
			$objSale->sal_periodo  ='1,4,12';
			$objSale->sal_status   =1;
			$objSale->save();
			
			$request->session()->put('sale_id',$objSale->id);
			
			
			///dependents
			$arrWebRanges  = WebRanges::where('ran_status', 1)->get();
			for($i=0;$i<count($arrWebRanges);$i++){
				$id_range =  $arrWebRanges[$i]->Id;
				$total    =  $request->input('quantity'.$i);
				for($j=0;$j<$total;$j++){
						$objPersonDep = new WebPerson;
						
						$objPersonDep->per_id      = '';
						$objPersonDep->per_name    = '';
						$objPersonDep->per_surname = '';
						$objPersonDep->per_email   = '';
						$objPersonDep->per_address = '';
						$objPersonDep->per_phone   = '';
						$objPersonDep->per_type    = 2;
						$objPersonDep->per_status  = 1;
						$objPersonDep->save();	

					    $objWSD = new WebSalesDependent;
						$objWSD->id_person = $objPersonDep->Id;
						$objWSD->id_sales  = $objSale->id;
						$objWSD->sxd_status  = 1;
						$objWSD->id_range    = $id_range;
						$objWSD->save();
						 
				}
			}
			
			
			$status = 1;
			$mensaje='OK';
			$response = array(
				'success' 		 => $status,
				'message' 		 => $mensaje
			);
			return response()->json($response);		   
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}
   }   
   
   public function placeToPayVal(Request $request){
 	   try{
		    $idProduct =  $request->session()->get('id_product');
		    $objWebPr  = webProduct::find($idProduct);		    
		    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
		    $arrWebProduct = webProduct::find($idProduct);
		    $precio        = $objWebPr->pro_valor;
		    $tax_val       = 0.15;
			
			$id_sale  = $request->session()->get('sale_id');
			$objWSD   = WebSalesDependent::where('id_sales',$id_sale)->get();

			$data_login = GenFunctionP2P::createTranskeyP2P();

			$var_endpoint_return = BtnP2pParam::find(5);
			$var_return = $var_endpoint_return->p2p_value;
			$reference  =  GenFunctionP2P::generarCodigos(1,16,true);

			//return response()->json(['success'=>0,'message'=>$idProduct]);
			$request_arr = [
                          "auth" => [
                              "login"   => $data_login['login'],
                              "seed"    => $data_login['seed'],
                              "nonce"   => $data_login['nonce'],
                              "tranKey" =>  $data_login['tranKey']

                          ],
                          "locale" => 'es_EC',
                          "buyer"=>[
                            "document" => $request->id, 
                            "documentType" => "CI", //CI cedula, RUC, PPN -> Pasaporte
                            "name" => preg_replace('/[0-9]+/', '', $request->nombre),
                            "surname" => preg_replace('/[0-9]+/', '', $request->apellido),
                            "email" => ( !is_null( $request->email ) ) ? $request->email :'adominguez@memorialtechnologies.com',
                            "mobile" =>'',
                            "address" =>  [
                              "street" => $request->direccion, 
                              "city" =>  ( is_object($request->ciudad) ) ? $request->ciudad: 'Quito',
                              "country" => "EC"
                            ]
                          ],
						 "subscription"=> [
						        "reference"=>$reference,
						        "description"=>"Supscricion Cliente ".$request->id
						    ],                         
                        /* "payment" => [
                              "reference" =>$reference , //ID generado por el comercio
                              "description" => "Memorial Ecuador",
                              "amount" => [
                                  "currency" => "USD",
                                  "total" => (float)$precio,
                                  "taxes" => [
                                           "kind"=> "iva",
                                           "amount"=> (float)$precio - ( (float)$precio / $tax_val) ,
                                           "base"=> (float)$precio /$tax_val,
                                   ],
                                  "details" => [
                                       [
                                           "kind" => "subtotal",
                                           "amount" => $precio

                                       ]
                                   ]
                              ],
                              "allowPartial" => false,
                          ],*/
                          "expiration" => date('c', strtotime('+30 minutes')), // tiempo para pago antes de caducar sesión
                          "returnUrl" => $var_return,
                          "ipAddress" => GenFunction::getRealIpAddr(),//;"173.230.130.8",
                          "userAgent" => $_SERVER['HTTP_USER_AGENT']//'WEB',
                      ];

           // return response()->json(['success'=>0,'message'=>$request_arr]);
            $url_process = GenFunctionP2P::createSessionP2P($request_arr,$request);
            if($url_process['status']['status']=='OK'){


				/*
					$transaction = new BtnP2pTran;
					$transaction->bpt_status = "SOLICITADO";
					$transaction->bpt_reason = $url_process['status']['reason'];
					$transaction->bpt_message = $url_process['status']['message'];
					$transaction->bpt_date = $url_process['status']['date'];
					$transaction->bpt_requestId = $url_process['requestId'];
					$transaction->bpt_session =  $reference;
					$transaction->bpt_processUrl = $url_process['processUrl'];
				    $transaction->bpt_name    = $request_arr['buyer']['name'];
					$transaction->bpt_surname = $request_arr['buyer']['surname'];
					$transaction->bpt_numdoc  = $request->id;
					$transaction->bpt_email = $request->email;
					$transaction->bpt_phone = '';
					$transaction->bpt_product_id = $objWebPr->pro_code;
					$transaction->bpt_product_name = $objWebPr->pro_name;
					$transaction->bpt_quantity = 1;
					$transaction->bpt_price = $objWebPr->pro_valor;
					$transaction->bpt_estado = 1;
					$transaction->save();
					// Hasta aqui el almacenamiento de la transacion

				$request->session()->put('requestId',$url_process['requestId']);
				$status = 1;
				$mensaje='OK';
				$response = array(
					'success' 		 => $status,
					'message' 		 => $mensaje,
					'url_process'    => $url_process
				);
				return response()->json($response);*/	
				
				
				return $this->collectP2P($request);

            }else{
            	return response()->json(['success'=>0,'message'=>'Hubo un problema al conectarse con Place to Pay','p2p_msg'=>$url_process]);
            }
	   
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}
                               	  
   }
   
   public function getTokenP2P(Request $request){
	   
   }
   
   public function collectP2P(Request $request){

	    $token  = $this->getTokenP2P($request, $requestId);
//get token
		try{
		    $idProduct =  $request->session()->get('id_product');
		    $objWebPr  = webProduct::find($idProduct);		    
		    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
		    $arrWebProduct = webProduct::find($idProduct);
		    $precio        = $objWebPr->pro_valor;
		    $tax_val       = 0.15;
			
			$id_sale  = $request->session()->get('sale_id');
			$objWSD   = WebSalesDependent::where('id_sales',$id_sale)->get();

			$data_login = GenFunctionP2P::createTranskeyP2P();

			$var_endpoint_return = BtnP2pParam::find(5);
			$var_return = $var_endpoint_return->p2p_value;
			$reference  =  GenFunctionP2P::generarCodigos(1,16,true);

			//return response()->json(['success'=>0,'message'=>$idProduct]);
			$request_arr = [
                          "auth" => [
                              "login"   => $data_login['login'],
                              "seed"    => $data_login['seed'],
                              "nonce"   => $data_login['nonce'],
                              "tranKey" =>  $data_login['tranKey']

                          ],
                          "locale" => 'es_EC',
                          "payer"=>[
                            "document" => $request->id, 
                            "documentType" => "CI", //CI cedula, RUC, PPN -> Pasaporte
                            "name" => preg_replace('/[0-9]+/', '', $request->nombre),
                            "surname" => preg_replace('/[0-9]+/', '', $request->apellido),
                            "email" => ( !is_null( $request->email ) ) ? $request->email :'adominguez@memorialtechnologies.com'
                          ],
						 "instrument"=> [
							  "token"=> [
										"token"=> $token
									]
						    ],                         
                         "payment" => [
                              "reference" =>$reference , //ID generado por el comercio
                              "description" => "Memorial Ecuador",
                              "amount" => [
                                  "currency" => "USD",
                                  "total" => (float)$precio,
                                  "taxes" => [
                                           "kind"=> "iva",
                                           "amount"=> (float)$precio - ( (float)$precio / $tax_val) ,
                                           "base"=> (float)$precio /$tax_val,
                                   ],
                                  "details" => [
                                       [
                                           "kind" => "subtotal",
                                           "amount" => $precio

                                       ]
                                   ]
                              ],
                              "allowPartial" => false,
                          ],
                          "expiration" => date('c', strtotime('+30 minutes')), // tiempo para pago antes de caducar sesión
                          "returnUrl" => $var_return,
                          "ipAddress" => GenFunction::getRealIpAddr(),//;"173.230.130.8",
                          "userAgent" => $_SERVER['HTTP_USER_AGENT']//'WEB',
                      ];

           // return response()->json(['success'=>0,'message'=>$request_arr]);
            $url_process = GenFunctionP2P::createSessionP2P($request_arr,$request);
            if($url_process['status']['status']=='OK'){



					$transaction = new BtnP2pTran;
					$transaction->bpt_status = "SOLICITADO";
					$transaction->bpt_reason = $url_process['status']['reason'];
					$transaction->bpt_message = $url_process['status']['message'];
					$transaction->bpt_date = $url_process['status']['date'];
					$transaction->bpt_requestId = $url_process['requestId'];
					$transaction->bpt_session =  $reference;
					$transaction->bpt_processUrl = $url_process['processUrl'];
				    $transaction->bpt_name    = $request_arr['buyer']['name'];
					$transaction->bpt_surname = $request_arr['buyer']['surname'];
					$transaction->bpt_numdoc  = $request->id;
					$transaction->bpt_email = $request->email;
					$transaction->bpt_phone = '';
					$transaction->bpt_product_id = $objWebPr->pro_code;
					$transaction->bpt_product_name = $objWebPr->pro_name;
					$transaction->bpt_quantity = 1;
					$transaction->bpt_price = $objWebPr->pro_valor;
					$transaction->bpt_estado = 1;
					$transaction->save();
					// Hasta aqui el almacenamiento de la transacion

				$request->session()->put('requestId',$url_process['requestId']);
				$status = 1;
				$mensaje='OK';
				$response = array(
					'success' 		 => $status,
					'message' 		 => $mensaje,
					'url_process'    => $url_process
				);
				return response()->json($response);	

            }else{
            	return response()->json(['success'=>0,'message'=>'Hubo un problema al conectarse con Place to Pay','p2p_msg'=>$url_process]);
            }
	   
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}



   }

   public function notifyP2P(Request $request){

   	      $data = json_decode($request->getContent(), true);
   		  $msg  = $data['status']['status'];
          $msg .= $data['status']['message'];
          $msg .= $data['status']['reason'];
          $msg .= $data['status']['date'];
		  $msg .= $data['requestId'];
		  $msg .= $data['reference'];
		  $msg .= $data['signature'];
		  $requestId = $request->session()->get('requestId');
		   if($data['requestId']){

					$fecha = GenFunction::getDateDB();
					Storage::prepend('log_p2p.log',"****************");
					Storage::prepend('log_p2p.log',"NOTIFY=".$data['status']['status']."::".$fecha);              
					Storage::prepend('log_p2p.log',$data['requestId']);  
					Storage::prepend('log_p2p.log',$msg); 
					Storage::prepend('log_p2p.log',"****************"); 		   	

					$transaction =  BtnP2pTran::where('bpt_requestId',$url_process['requestId'])->first();
					if(!empty($transaction)){

							$transaction->bpt_status  = $data['status']['status'];
							$transaction->bpt_session = $data['requestId'];
							$transaction->save();
					}


		    } else {
		      
					$fecha = GenFunction::getDateDB();
					Storage::prepend('log_p2p.log',"****************");
					Storage::prepend('log_p2p.log',"NOTIFY=ERROR::".$fecha);              
					Storage::prepend('log_p2p.log', $requestId);   
					Storage::prepend('log_p2p.log',"****************");

		    }

   }

   public function responseP2P(Request $request){
	     if($request->session()->has('requestId')){
			 $requestId = $request->session()->get('requestId');
		 }else{
			  $requestId = 116263;
		 }
		 
   		 $resArr = GenFunctionP2P::responseSessionP2P($requestId,$request);
   		 $msg = 'NO SE PUDO OBTENER INFORMACION DE PLACE TO PAY';
   		 //$resArr['status']['status']=='OK'
   		 if(!empty($resArr['status']['status']))
   		 {
   		 	$status  = $resArr['status']['status'];

   		 	switch ($status) {
   		 		case 'APPROVED':
   		 			 $msg = 'TRANSACCION APROBADA';
   		 			break;
				case 'REJECTED':
   		 			 $msg = 'TRANSACCION RECHAZADA';
   		 			break;      		 					 		
   		 		default:
   		 			$msg = 'TRANSACCION PENDIENTE';
   		 			break;
   		 	}


			$fecha = GenFunction::getDateDB();
			Storage::prepend('log_p2p.log',"****************");
			Storage::prepend('log_p2p.log',"RESPONSE=".$resArr['status']['status']."::".$fecha);              
			//Storage::prepend('log_p2p.log',$resArr['requestId']);  
			Storage::prepend('log_p2p.log',$msg); 
			Storage::prepend('log_p2p.log',"****************"); 		   	

			$transaction =  BtnP2pTran::where('bpt_requestId',$requestId)->first();
			if(!empty($transaction)){

					$transaction->bpt_status  = $resArr['status']['status'];
					$transaction->bpt_session = $requestId;
					$transaction->save();
			}



   		 }//end if
   		 

   		  return view('sales.payment.p2p_notify')->with('msg', $msg);

   }



	public function saveDebit(Request $request)
	{
		DB::beginTransaction();
		try{
	        $validator = \Validator::make($request->all(), [
	            'tipo_cuenta' => 'required',
	            'banco' => 'required',
	            'numero_cuenta' => 'required',
				'id_deb' => 'required',
				'nombre_deb' => 'required',	
				'apellido_deb' => 'required'				
	        ],['tipo_cuenta.required' => 'El tipo de cuenta es requerida.',
	           'banco.required' => 'El banco  es requerido.',
	           'numero_cuenta.required' => 'El numero de cuenta  es requerido.',
			   'nombre_deb.required' => 'El nombre de titular de la cuenta  es requerido.',
			   'apellido_deb.required' => 'El Apellido del titular de la cuenta  es requerido.']);

            //['sc_razon_social.required' => ':attribute  es requerido.']	        
	        if ($validator->fails())
	        {
	            return response()->json(['success'=>2,'errors'=>$validator->errors()->all()]);
	        }
			
			
			$idProduct =  $request->session()->get('id_product');
		    $objWebPr  = webProduct::find($idProduct);		   			
			
			$id_websale  = $request->session()->get('sale_id');			
			$objWSale    = WebSale::find($id_websale);
			
			$number_months = 12;
			$reference     = 'NA';
			
			$price_unit    = $objWebPr->pro_valor;
			$npc           = $objWebPr->pro_npc; 
			$sal_taxes = 0.15;
			if($request->session()->has('requestId')){
				$reference = $request->session()->get('requestId');
			}
			$sal_total = ($price_unit*$number_months) + (($price_unit*$number_months)*$sal_taxes);
		    $cli_id  = $request->session()->get('cli_id');
			
			
			$objContract = new SalContract;
			$objContract->sal_token = $objWSale->sal_token;
			$objContract->con_attach   = 0;
			$objContract->con_card     = 0;
			$objContract->con_status   = 1;
			$objContract->save();
			
			
			$objSale = new SalSale;
			$objSale->sal_contract = $objContract->Id;
			$objSale->agency_code = $objWSale->agency_code;
			$objSale->sel_code = Auth::id();
			$objSale->pro_code    = $objWebPr->pro_code;
			$objSale->sal_token   = $objWSale->sal_token;
			$objSale->id_person   = $cli_id;
			$objSale->sal_dateStart      = $objWSale->sal_date;
			$objSale->sal_numberMounth   = $number_months;
			$objSale->sal_reference      = $reference;
			$objSale->sal_frecuencyCollect  = 'NA';
			$objSale->sal_price_unit        = $price_unit;
			$objSale->sal_taxes     = $sal_taxes;
			$objSale->sal_total     = $sal_total;
			$objSale->sal_status    = 1;
			$objSale->save();
			
			$objNewSale = SalSale::find($objSale->id);
			$objSxk = SalSxk::where('cli_code',$objNewSale->id_person)
							  ->where('kin_code',1)
							  ->where('sal_id',$objNewSale->id)->first();
			
			if(empty($objSxk)){
				$objSxk = new SalSxk;
			}	
			$objSxk->sal_id   = $objSale->id;
			$objSxk->cli_code = $objNewSale->id_person;
			$objSxk->kin_code = 1;
			$objSxk->save();			
			
			$objMethodPay  = new SalMethodPay;
			$objMethodPay->sal_id = $objSale->id;
			$objMethodPay->smp_typesales     = 2;
			$objMethodPay->ban_code          = $request->banco;
			$objMethodPay->smp_typeaccount   = $request->tipo_cuenta;
			$objMethodPay->smp_numberaccount = $request->numero_cuenta;
			$objMethodPay->smp_reference     = $reference;
			$objMethodPay->smp_value         = $price_unit;
			$objMethodPay->smp_titularname   = $request->nombre_deb;
			$objMethodPay->smp_titularsurname   = $request->apellido_deb;
			$objMethodPay->smp_titularidnumber  = $request->id_deb;
			$objMethodPay->smp_status        = 1;
			$objMethodPay->save();
			 DB::commit();
		    $url_redirect = '/sales/payment/supscription';
			if($npc>1){
				
			}
			$request->session()->put('id_sale_fn',$objSale->id);

			$mensaje      = 'OK';
			$status       =  1;
			
			$response = array(
				'success' 		 => $status,
				'message' 		 => $mensaje,
				'url_redirect' 	 => $url_redirect
			);
			return response()->json($response);		   
		   
	    }catch(\Exception $e){
			 DB::rollback();
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}  		 

		
	}


  public function saveTransfer(Request $request){
	

	try{
	        $validator = \Validator::make($request->all(), [
	            'nombre' => 'required',
	            'apellido' => 'required',
	            'ban_code' => 'required',
				'identificacion' => 'required',
				'referencia' => 'required',	
				'valor' => 'required'				
	        ],['nombre.required' => 'El nombre es requerido.',
	           'apellido.required' => 'El apellido  es requerido.',
	           'ban_code.required' => 'El banco  es requerido.',
			   'identificacion.required' => 'La identificacion es requerida.',
			   'referencia.required' => 'la referencia es requerida.',
			   'valor.required' => 'El valor es requerido.']);

            //['sc_razon_social.required' => ':attribute  es requerido.']	        
	        if ($validator->fails())
	        {
	            return response()->json(['success'=>2,'errors'=>$validator->errors()->all()]);
	        }
			
			
			$idProduct =  $request->session()->get('id_product');
		    $objWebPr  = webProduct::find($idProduct);		   			
			
			
			
			$id_websale  = $request->session()->get('sale_id');			
			$objWSale    = WebSale::find($id_websale);
			
			$number_months = 12;
			$reference     = $request->referencia;
			
			$price_unit    = $objWebPr->pro_valor;
			$npc           = $objWebPr->pro_npc; 
			$sal_taxes = 0.15;

			$sal_total = $price_unit*$number_months + ($price_unit*$number_months)*$sal_taxes;
			$cli_id    = $request->session()->get('cli_id');
			
			$objContract = new SalContract;
			$objContract->sal_token = $objWSale->sal_token;
			$objContract->con_attach   = 0;
			$objContract->con_card     = 0;
			$objContract->con_status   = 1;
			$objContract->save();			
			
			$objSale = new SalSale;
			$objSale->sal_contract = $objContract->Id;
			$objSale->agency_code = $objWSale->agency_code;
			$objSale->sel_code    = Auth::id();
			$objSale->id_person   = $cli_id;
			$objSale->pro_code    = $objWebPr->pro_code;
			$objSale->sal_token   = $objWSale->sal_token;
			$objSale->sal_dateStart      = $objWSale->sal_date;
			$objSale->sal_numberMounth   = $number_months;
			$objSale->sal_reference      = $reference;
			$objSale->sal_frecuencyCollect  = 'NA';
			$objSale->sal_price_unit        = $price_unit;
			$objSale->sal_taxes     = $sal_taxes;
			$objSale->sal_total     = $sal_total;
			$objSale->sal_status    = 1;
			$objSale->save();
			
			$objNewSale = SalSale::find($objSale->id);
			$objSxk = SalSxk::where('cli_code',$objNewSale->id_person)
							  ->where('kin_code',1)
							  ->where('sal_id',$objNewSale->id)->first();
			
			if(empty($objSxk)){
				$objSxk = new SalSxk;
			}	
			$objSxk->sal_id   = $objSale->id;
			$objSxk->cli_code = $objNewSale->id_person;
			$objSxk->kin_code = 1;
			$objSxk->save();
			
			
			$objMethodPay  = new SalMethodPay;
			$objMethodPay->sal_id = $objSale->id;
			$objMethodPay->smp_typesales     = 2;
			$objMethodPay->ban_code          = $request->ban_code;
			$objMethodPay->smp_typeaccount   = 0;
			$objMethodPay->smp_numberaccount = '';
			$objMethodPay->smp_reference     = $reference;
			$objMethodPay->smp_value         = $request->valor;
			$objMethodPay->smp_titularname   = $request->nombre;
			$objMethodPay->smp_titularsurname   = $request->apellido;
			$objMethodPay->smp_titularidnumber  = $request->identificacion;
			$objMethodPay->smp_status        = 1;
			$objMethodPay->save();
			
		    $url_redirect = '/sales/payment/supscription';
			if($npc>1){
				$url_redirect = '/sales/payment/supscription';
			}
			$request->session()->put('id_sale_fn',$objSale->id);

			$mensaje      = 'OK';
			$status       =  1;
			
			$response = array(
				'success' 		 => $status,
				'message' 		 => $mensaje,
				'url_redirect' 	 => $url_redirect
			);
			return response()->json($response);		   
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}	
	  
  }


  public function saveFormCreditCard(Request $request,$pay_day){
	 
	try{
	        $validator = \Validator::make($request->all(), [
	            'nombres_tit' => 'required',
	            'ciudad' => 'required'	
	        ],['nombres_tit.required' => 'Los nombres son requeridos.',
	           'ciudad.required' => 'La ciudad es requerida.']);

            //['sc_razon_social.required' => ':attribute  es requerido.']	        
	        if ($validator->fails())
	        {
	            return response()->json(['success'=>2,'errors'=>$validator->errors()->all()]);
	        }
			
			
			$idProduct =  $request->session()->get('id_product');
		    $objWebPr  = webProduct::find($idProduct);		   			
			
			
			
			$id_websale  = $request->session()->get('sale_id');			
			$objWSale    = WebSale::find($id_websale);
			
			$number_months = 12;
			$reference     = $request->referencia;
			
			$price_unit    = $objWebPr->pro_valor;
			$npc           = $objWebPr->pro_npc; 
			$sal_taxes = 0.15;

			$objMethodPay  = new SalMethodpaySubscription;


			$sal_total = $price_unit*$number_months + ($price_unit*$number_months)*$sal_taxes;
			$cli_id    = $request->session()->get('cli_id');
			$md_code     = 1;
			$num_cuenta_debito = '';
			$banco_debito = 0;
			$num_tarjeta_credito = '';
			$tipo_tarjeta=0;
			$mes_ven_cc = 0;
			$anio_ven_cc=0;
			if($request->input('rol_pagos')){
				$md_code   = 1;
			}elseif($request->input('cuenta_corriente')){
				$md_code   = 2;
				$num_cuenta_debito = $request->input('num_cuenta_corriente');
				$banco_debito      = $request->input('banco_cc');
				$objMethodPay->mps_typeaccount          = 2;
				$objMethodPay->mps_numberaccount        = $num_cuenta_debito;
				$objMethodPay->ban_code                 = $banco_debito;
				
			}elseif($request->input('cuenta_ahorros')){
				$md_code   = 3;
				$num_cuenta_debito = $request->input('num_cuenta_ahorros');
				$banco_debito      = $request->input('banco_ca');
				$objMethodPay->mps_typeaccount          = 1;
				$objMethodPay->mps_numberaccount        = $num_cuenta_debito;
				$objMethodPay->ban_code                 = $banco_debito;				
			}elseif($request->input('tarjeta_credito')){
				$md_code   = 4;
				$num_tarjeta_credito = $request->input('num_tarjeta_credito');
				$tipo_tarjeta        = $request->input('tipo_tarjeta');	
                $mesanio             = explode('-',$request->input('mes_anio'));	
				$mes_ven_cc          = 	$mesanio[0];
				$anio_ven_cc         = 	$mesanio[1];	

				$objMethodPay->mps_cc_type          = $request->tipo_tarjeta;
				$objMethodPay->mps_cc_number        = $request->numero_tarjeta;
				$objMethodPay->mps_cc_date_end      = $request->fecha_vg_tarjeta;
				$objMethodPay->mps_cc_year_exp      = $request->anio_ven_cc;
				$objMethodPay->mps_cc_month_exp     = $request->mes_ven_cc;
		
			}			
						
			
			$sal_id = $request->session()->get('id_sale_fn');

			$objMethodPay->sal_id = $sal_id;
			$objMethodPay->mps_typesales     = 3;
			$objMethodPay->mps_name_company  = $request->destino_nombres;
			
			$objMethodPay->md_code           = $md_code;			
			$objMethodPay->mps_day_pay       = $pay_day;
			$objMethodPay->mps_value         = $price_unit;
			
			if($request->has('chk_cancelado')){
				$objMethodPay->mps_type_method_pay         = $request->chk_cancelado;
			}else{
			    $objMethodPay->mps_type_method_pay         = $request->chk_acreditado;
				$objMethodPay->mps_account_number_cp       = $request->num_cuenta_mem;
				$objMethodPay->mps_bank_code_cp            = $request->banco_mem;
			}
				
			$names = explode(' ',$request->nombres_tit);
			
			$objMethodPay->mps_titularname          = $names[0];//$request->titular_tarjeta;
			if(!empty($names[1])){
				$objMethodPay->mps_titularsurname   = $names[0];//$request->apellido_cd;
			}
			$objMethodPay->mps_titularidnumber      = $request->identificacion;//$request->titular_tarjeta;
			$objMethodPay->mps_city_supscription    = $request->ciudad;
			$objMethodPay->mps_day_supscription     = $request->dia;
			$objMethodPay->mps_month_supscription   = $request->mes;
			$objMethodPay->mps_year_supscription    = $request->anio;
			
			//$objMethodPay->smp_titularidnumber  = $request->identificacion;
			$objMethodPay->mps_status        = 1;
			$objMethodPay->save();
			
		    $url_redirect = '/sales/preview';
			if($npc>1){
				$url_redirect = '/sales/dependents';
			}
			

			$mensaje      = 'OK';
			$status       =  1;
			
			$response = array(
				'success' 		 => $status,
				'message' 		 => $mensaje,
				'url_redirect' 	 => $url_redirect
			);
			return response()->json($response);		   
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}	
	  
  }


public function showFormDependents(Request $request){
	$idProduct =  $request->session()->get('id_product');
	    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
	    $arrWebProduct = webProduct::find($idProduct);
	    $precio        = $arrWebProduct->pro_valor;
	    $tax_val       = 0.15;
		
		$id_sale  = $request->session()->get('sale_id');
		$objWSD   = WebSalesDependent::where('id_sales',$id_sale)->get();
		$cmbKinship = DB::SELECT("select * from gen_kinship where lang_code=2 and kin_code!=1 ");
			
		$npc  = $arrWebProduct->pro_npc;
		
		if($npc==0){
			 return view('sales.dependents_detail')
									   ->with('arrWebRanges', $arrWebRanges)
									   ->with('objWSD', $objWSD)
									   ->with('idProduct', $idProduct)
									   ->with('cmbKinship', $cmbKinship)
									   ->with('arrWebProduct', $arrWebProduct)
									   ->with('precio', $precio);
		}elseif($npc>1)	{
			 return view('sales.dependents_detail')
									   ->with('arrWebRanges', $arrWebRanges)
									   ->with('npc', $npc)
									   ->with('idProduct', $idProduct)
									   ->with('cmbKinship', $cmbKinship)
									   ->with('arrWebProduct', $arrWebProduct)
									   ->with('precio', $precio);
		}
		else{
			 return view('sales.payment.final_detail')
						   ->with('arrWebRanges', $arrWebRanges)
						   ->with('idProduct', $idProduct)
						   ->with('npc', $npc)
						   ->with('arrWebProduct', $arrWebProduct)
						   ->with('precio', $precio);
		}	
}

public function multSave(Request $request){
   
	DB::beginTransaction();
	try{
		
		$tot = count($request->input('id'));
		$rules = [];
		for($j=1;$j<=$tot;$j++){
			$tmp=array('id.'.$j.'.required'=>'La identificacion del dependiente '.$j.' es requerida');
			$rules=array_merge($rules,$tmp);
			
		}
		
		//return response()->json(['success'=>0,'errors'=>$rules]);
		$validator = \Validator::make($request->all(), [
			'nombre' => 'required',
			'id.*' => 'required|max:255',
			'apellido' => 'required',
			'email' => 'required',
			'kinship' => 'required'				
		],$rules);

		//['sc_razon_social.required' => ':attribute  es requerido.']	        
		if ($validator->fails())
		{
		  return response()->json(['success'=>2,'errors'=>$validator->errors()->all()]);
		}
		
		$idProduct =  $request->session()->get('id_product');
		$objWebPr  = webProduct::find($idProduct);		   			
		
		$id_websale  = $request->session()->get('sale_id');			
		$objWSale    = WebSale::find($id_websale);
		
		$number_months = 12;
		$reference     = 'NA';
		
		$price_unit    = $objWebPr->pro_valor;
		$npc           = $objWebPr->pro_npc; 
		
		$id_sales  = $request->session()->get('id_sale_fn');

		$objSale = SalSale::find($id_sales);
		//CliClient

					
		$data = [];
		
		for($i=0;$i<$tot;$i++)
		{
			if(empty($request->input('id')[$i])){
				continue;
			}
			$ident = $request->input('id')[$i];
			$objCliClient =  CliClient::find($ident);
			if(empty($objCliClient)){
				$objCliClient = new CliClient;
			}
			$nombre = '';
			if(!empty($request->input('nombre')[$i])){
				$nombre = $request->input('nombre')[$i];
			}	
			$apellido = '';
			if(!empty($request->input('apellido')[$i])){
				$apellido = $request->input('apellido')[$i];
			}			

			$kinship = 2;
			if(!empty($request->input('kinship')[$i])){
				$kinship = $request->input('kinship')[$i];
			}	

			$birthday = now();
			if(!empty($request->input('fecnac')[$i])){
				$birthday = $request->input('fecnac')[$i];
			}		

			$age = now();
			if(!empty($request->input('edad')[$i])){
				$age = $request->input('edad')[$i];
			}			
			$objCliClient->gen_code = 1;
			$objCliClient->tdo_code = 1;
			$objCliClient->gsc_code = 1;
			$objCliClient->toc_code = 1;
			$objCliClient->cli_parent  = $objSale->id_person;
			$objCliClient->cli_id      = $ident;
			$objCliClient->cli_name    = $nombre;
			$objCliClient->cli_surname = $apellido;
			$objCliClient->cli_birthday = $birthday;
			$objCliClient->cli_age      = $age;
			
			
			$objCliClient->save();
			
			$objSxk = SalSxk::where('cli_code',$objCliClient->cli_code)
							  ->where('kin_code',$kinship)
							  ->where('sal_id',$objSale->id)->first();
			
			if(empty($objSxk)){
				$objSxk = new SalSxk;
			}	
			$objSxk->sal_id   = $objSale->id;
			$objSxk->cli_code = $objCliClient->cli_code;
			$objSxk->kin_code = $kinship;
			$objSxk->save();
				
					/* $data[]= array ('item_id'=>$input[$i]['item_id'],
									 'item_description'=>$input[$i]['item_description']);
									 'units'=>$input[$i]['units']);
									 'rate'=>$input[$i]['rate']);
									 'initial_amount'=>$input[$i]['initial_amount']);*/
		}
			DB::commit();
		$url_redirect = '/sales/preview';
		$status       = 1;
		$mensaje      = 'OK';
		$response = array(
			'success' 		 => $status,
			'url_redirect'   => $url_redirect,
			'message' 		 => $mensaje
		);
		return response()->json($response);		   
	   
	}catch(\Exception $e){
		DB::rollback();
		return response()->json(['success'=>0,'message'=>$e->getMessage()]);
	}
}   
      
   

  public function showPreview(Request $request){
	  
	  	$idProduct =  $request->session()->get('id_product');
	    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
	    $arrWebProduct = webProduct::find($idProduct);
	    $precio        = $arrWebProduct->pro_valor;
	    $tax_val       = 0.15;
		
		$id_sale  = $request->session()->get('sale_id');
		$objWSD   = WebSalesDependent::where('id_sales',$id_sale)->get();
		
		$idProduct =  $request->session()->get('id_product');
		$objWebPr  = webProduct::find($idProduct);		   			
		
		$id_websale  = $request->session()->get('sale_id');			
		$objWSale    = WebSale::find($id_websale);
		
		$number_months = 12;
		$reference     = 'NA';
	
		
		$id_sales  = $request->session()->get('id_sale_fn');

		$objSale      = SalSale::find($id_sales);	
        $id_titular   = $objSale->id_person;	
        $objCliClient = CliClient::find($id_titular);	
        
		
		
		$sql = "select b.cli_name, b.cli_surname, c.kin_name
				from  sal_sxk a
				left join cli_clients b ON b.cli_code = a.cli_code
				left join gen_kinship c ON c.kin_code = a.kin_code
				where sal_id  = ".$id_sales;
		$objSalSxk =  DB::SELECT($sql);//SalSxk::where('sal_id',$id_sales)->get();		
        $tot_dep =  0;
		if(!empty($objSalSxk)){
			$tot_dep = count($objSalSxk);
		}
		
	  return view('sales.payment.final_detail')
						   ->with('arrWebRanges', $arrWebRanges)
						   ->with('idProduct', $idProduct)
						   ->with('objSale', $objSale)
						   ->with('objCliClient', $objCliClient)
						   ->with('tot_dep', $tot_dep)
						   ->with('objSalSxk', $objSalSxk)
						   ->with('arrWebProduct', $arrWebProduct)
						   ->with('precio', $precio);
  }


  public function cmbCreditCard(Request $request, $id_operadora){
		$status  = 0;
		$mensaje = 'Success';
		if($request->session()->has('lang_id')){
						$lang_id  = $request->session()->get('lang_id');
		}else{
						$lang_id  = 2;
		}
		if($request->session()->has('user_name')){
						$user_name  = $request->session()->get('user_name');
		}else{
						$user_name  = "user";
		}
		// Product
		try{

  			$sql  = "select * from gen_credit_cards where cco_code=?";
			$objCD  = DB::SELECT($sql,[$id_operadora]);
			

			$request->session()->put('cmbCards',$objCD);
			$status = 1;
			$response = array(
				'success' 	 => $status,
				'json_cd' 	 => $objCD,
				'message' 	 => $mensaje
			);
			return response()->json($response);
		}catch(\Exception $e){
						return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}
	}	
	 



	public function supscriptionMethod(Request $request){
		$idProduct =  $request->session()->get('id_product');
	    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
	    $arrWebProduct = webProduct::find($idProduct);
	    $precio        = $arrWebProduct->pro_price;
		$arr_combos    = $this->getListsMethodPay($request);
		$selectBank    = GenFunction::getCmbBankMysql($request,1,'ban_code','B');
	    $fecha_start   =date("M jS, Y", strtotime("2019-01-05"));
		
		$arr_prod=array('valor_gad'=>0);

	    $objform =  $request->session()->get('frm_labels');						   
                               
	   return view('sales.payment.payment_supscription')
									   ->with('arrWebRanges', $arrWebRanges)
									   ->with('objform', $objform)
									   ->with('selectBank', $selectBank)
									   ->with('arr_combos', $arr_combos)
									   ->with('arr_prod', $arr_prod)
									   ->with('fecha_start', $fecha_start)
									   ->with('idProduct', $idProduct)
									   ->with('arrWebProduct', $arrWebProduct)
									   ->with('precio', $precio);		
		
	}
	
	public function testFormAuthDeb(Request $request){
		$idProduct =  $request->session()->get('id_product');
	    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
	    $arrWebProduct = webProduct::find($idProduct);
	    $precio        = $arrWebProduct->pro_price;
		$arr_combos    = $this->getListsMethodPay($request);
		$selectBank    = GenFunction::getCmbBankMysql($request,1,'ban_code','B');
	    $fecha_start   =date("M jS, Y", strtotime("2019-01-05"));
		
		$arr_prod=array('valor_gad'=>0);

	    $objform =  $request->session()->get('frm_labels');						   
                               
	   return view('sales.includes.modalFormDebitCreditCard')
									   ->with('arrWebRanges', $arrWebRanges)
									   ->with('objform', $objform)
									   ->with('selectBank', $selectBank)
									   ->with('arr_combos', $arr_combos)
									   ->with('arr_prod', $arr_prod)
									   ->with('fecha_start', $fecha_start)
									   ->with('idProduct', $idProduct)
									   ->with('arrWebProduct', $arrWebProduct)
									   ->with('precio', $precio);		
		
	}
	

   public function dependentLast(Request $request){
	    $idProduct =  $request->session()->get('id_product');
	    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
	    $arrWebProduct = webProduct::find($idProduct);
	    $precio        = $arrWebProduct->pro_valor;
	    $tax_val       = 0.15;
		
		$id_sale  = $request->session()->get('sale_id');
		$objWSD   = WebSalesDependent::where('id_sales',$id_sale)->get();

		
		if(!empty($objWSD) && count($objWSD)>0)	{
			 return view('payment.dependents_detail')
									   ->with('arrWebRanges', $arrWebRanges)
									   ->with('objWSD', $objWSD)
									   ->with('idProduct', $idProduct)
									   ->with('arrWebProduct', $arrWebProduct)
									   ->with('precio', $precio);
		}else{
			 return view('sales.payment.final_detail')
						   ->with('arrWebRanges', $arrWebRanges)
						   ->with('idProduct', $idProduct)
						   ->with('arrWebProduct', $arrWebProduct)
						   ->with('precio', $precio);
		}	
                               
	  
   }
   

   public function validateStateP2P(Request $request){

   		try{
	   	     $requestId = $request->session()->get('requestId');
	   		 $resArr = GenFunctionP2P::responseSessionP2P($requestId,$request);
	   		 $msg = 'NO SE PUDO OBTENER INFORMACION DE PLACE TO PAY';
	   		 $url_redirect = '';
	   		 $status       = 1;
	   		 //$resArr['status']['status']=='OK'
	   		 if(!empty($resArr['status']['status']))
	   		 {
	   		 	$status  = $resArr['status']['status'];

	   		 	switch ($status) {
	   		 		case 'APPROVED':

						$idProduct =  $request->session()->get('id_product');
					    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
					    $arrWebProduct = webProduct::find($idProduct);
					    $precio        = $arrWebProduct->pro_valor;
					    $tax_val       = 0.15;
						$id_sale       = $request->session()->get('sale_id');
						$objWSD        = WebSalesDependent::where('id_sales',$id_sale)->get();
						
						$msg = 'TRANSACCION APROBADA';
						//$url_redirect= '/sales/preview';
						$url_redirect= '/sales/payment/supscription';

						$request->session()->put('aplica_p2p',1);
						
						//if(!empty($objWSD) && count($objWSD)>0)	{
						//		$url_redirect= '/sales/dependents';
						//}


	   		 			break;
					case 'REJECTED':
					     $status       = 0;
	   		 			 $msg = 'TRANSACCION RECHAZADA';
	   		 			break;      		 					 		
	   		 		default:
	   		 		    $status       = 0;
	   		 			$msg = 'TRANSACCION PENDIENTE';
	   		 			break;
	   		 	}


	   		 }//end if
	   		 

 			
			$mensaje='OK';
			$response = array(
				'success' 		 => $status,
				'message' 		 => $msg,
				'url_redirect' 	 => $url_redirect
			);
			return response()->json($response);		   
		   
	    }catch(\Exception $e){
			return response()->json(['success'=>0,'message'=>$e->getMessage()]);
		}  		 



   }

   public function redirectNextStepP2p(){
   				$idProduct =  $request->session()->get('id_product');
			    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
			    $arrWebProduct = webProduct::find($idProduct);
				$id_sale  = $request->session()->get('sale_id');
				$objWSD   = WebSalesDependent::where('id_sales',$id_sale)->get();
		 		$msg = 'TRANSACCION APROBADA';


	 			if(!empty($objWSD) && count($objWSD)>0)	{
					return view('sales.dependents_detail')
					   ->with('arrWebRanges', $arrWebRanges)
					   ->with('idProduct', $idProduct)
					   ->with('arrWebProduct', $arrWebProduct)
					   ->with('precio', $precio);

	 			}else{

					return view('sales.certificate_prev')
					   ->with('arrWebRanges', $arrWebRanges)
					   ->with('idProduct', $idProduct)
					   ->with('arrWebProduct', $arrWebProduct)
					   ->with('precio', $precio); 		 				
	 			}


   }
   
 /**
     * Quote the selected product.
     *
     * @return \Illuminate\Http\Response
     */
    public function paySel(Request $request){
        //
		$request->session()->forget('aplica_p2p');
		$idProduct =  $request->session()->get('id_product');
	    $arrWebRanges  = WebRanges::where('ran_status', 1)->get();
	    $arrWebProduct = webProduct::find($idProduct);
	    $precio        = $arrWebProduct->pro_price;
		$arr_combos    = $this->getListsMethodPay($request);
		$selectBank    = GenFunction::getCmbBankMysql($request,1,'ban_code','B');
	
	    $objform =  $request->session()->get('frm_labels');						   
                               
	   return view('sales.payment.payment_select')
									   ->with('arrWebRanges', $arrWebRanges)
									   ->with('objform', $objform)
									   ->with('selectBank', $selectBank)
									   ->with('arr_combos', $arr_combos)
									   ->with('idProduct', $idProduct)
									   ->with('arrWebProduct', $arrWebProduct)
									   ->with('precio', $precio);
    } // detailWebProduct
	  
   



  public function getListsMethodPay($request){
         
		 
		$lang_id = 2;
		
		$sql = "SELECT a.*
                FROM	 gen_typepayment a
                WHERE	lang_code = ".$lang_id;
        $cmb_tipo_pago = DB::SELECT($sql);		
		$default_m = 0;
		if($cmb_tipo_pago){
		  $default_m = $cmb_tipo_pago[0]->id;
		}
		

		$sql = "SELECT a.*
                FROM	 gen_methodpay a
                WHERE	lang_code = ".$lang_id." and pmt_method_code = ".$default_m;
        $cmb_forma_pago = DB::SELECT($sql);	

		$sql = "SELECT a.*
                FROM	 gen_credit_cards_operators a
                ";
        $cmb_operadora = DB::SELECT($sql);	
		$default_tipt = 0;
		if($cmb_operadora){
		  $default_tipt = $cmb_operadora[0]->cco_code;
		}		
		
		$sql = "SELECT a.*
                FROM	 gen_credit_cards a
                where cco_code = ".$default_tipt;
        $cmb_tipo_tarjeta = DB::SELECT($sql);	
		
		$sql = "SELECT a.*
                FROM	 gen_bank_account a
                where lan_code = ".$lang_id;
        $cmb_tipo_cuenta = DB::SELECT($sql);	

		$sql = "SELECT a.*
		FROM	 gen_bank a";
        $cmb_banco = DB::SELECT($sql);
		
        $arr_combos  = array();
		$arr_combos['cmb_tipo_pago']    =$cmb_tipo_pago;
		$arr_combos['cmb_forma_pago']   =$cmb_forma_pago;
		if($request->session()->has('cmbMethodPay')){
			$arr_combos['cmb_forma_pago']   =$request->session()->get('cmbMethodPay');
		}
		
		//states dependent
		//$objStateDep = GenStateDependent::where('lang_code',$lang_id)->get();
		
		
		$arr_combos['cmb_operadora']    =$cmb_operadora;
		$arr_combos['cmb_tipo_tarjeta'] =$cmb_tipo_tarjeta;
		$arr_combos['cmb_tipo_cuenta']  =$cmb_tipo_cuenta;	
		$arr_combos['cmb_banco']        =$cmb_banco;
		//$arr_combos['cmb_statedep']     =$objStateDep;	
		return $arr_combos;
  }





}
