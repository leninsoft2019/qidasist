							
	
					
<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");

$cmb_operadora     =  $arr_combos['cmb_operadora'];
$cmb_tipo_tarjeta  =  $arr_combos['cmb_tipo_tarjeta'];
$cmb_banco         =  $arr_combos['cmb_banco'];
				   
			
					
?>
<!-- The Modal -->


<div>


<style scoped src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
</style>

<style  scoped type="text/css">

	
	
	#frmDCreditC{
		padding:20px;
	}
    .cs_footer{
		font-size:8px;
	}
	.col10{
		display:table-cell;
		width:10%;
	}
	
	.col_100{
		display:table-cell;
		width:100%;
	}
	
	.col_50{
		display:table-cell;
		width:50%;
	}	
	
	.col_25{
		display:table-cell;
		width:25%;
	}	
	
	.tbl{
		display:table;
		width:100%;
	}
	
	.rw{
		display:table-row;
	}
	
	#myModalBDDB{
		color:black;
	}
</style>
<div class="modal" id="myModalDebitCreditC">

  <div class="modal-dialog modal-dialog-centered" id="myModalBDDB">
    <div class="modal-content">
       <style scoped>
	    #frmAdicionales{
			padding:15px;
		}
		
		#myModalConfirm modal-dialog{
			max-width:300px;
		}
	   </style>
      <!-- Modal Header -->
      <div class="modal-header">
        <img src="/img/Prever_logo.png" height="100px" width="200px"/>
		<h4 class="modal-title"><span class="span-title"><i class="ico_anullment_orange"></i>AUTORIZACION DE DEBITO</span></h4>
        <!--<button type="button" class="close" data-dismiss="modal">&times;</button>-->
      </div>

      <!-- Modal body -->
      <div class="modal-body">
	  
	   <div class="alert alert-success" id="alert_success_cc">
		<strong>Success!</strong> This alert box could indicate a successful or positive action.
	  </div>
	  <div class="alert alert-danger" id="alert_error_cc">
		<strong>Danger!</strong> This alert box could indicate a dangerous or potentially negative action.
	  </div>  
			<div id="body_mod_debitcreditc">

							<form  id="frmDCreditC">
								{{ csrf_field() }}	

								
								<div class="tbl">
									<div class="row cab_r rw">
										<div class="col col_100">
											<div class="form-group">
												<label for="destino_nombres">Se&#241;ores:</label>
												<input type="text" class="form-control" name="destino_nombres" id="destino_nombres" value="MEMORIAL" />
											</div>
										</div>

									</div>
								</div>	
								<div class="tbl">
									<div class="row cab_r rw">
										<div class="col col_100">
											<div class="form-group">
												<span>Presente.-<br>
												De mi consideraci&oacute;n
												</span>
											</div>
										</div>

									</div>	
								</div>
								<div class="tbl">
									<div class="row cab_r rw">
										<div class="col col_100">
											<div class="form-group">
												<span>Yo
											    <input type="text" class="form-control" name="nombres_tit" id="nombres_tit" placeholder="Dos Nombres y Dos Apellidos" />
												solicito a ustedes se sirvan de debitar de mi:
												
												</span>
											</div>
										</div>

									</div>										
								</div>
								
								<div class="tbl">
									<div class="row cab_r rw">
										<div class="col col_25">
											<div class="form-group">
											    <input type="checkbox" class="form-control" name="rol_pagos" id="rol_pagos" value="1" />
												<label for="rol_pagos">Rol de pagos</label>
												
											</div>
										</div>
										<div class="col col_25">
											<div class="form-group">					
											</div>
										</div>	
										<div class="col col_25">
											<div class="form-group">					
											</div>
										</div>	
										<div class="col col_25">
											<div class="form-group">					
											</div>
										</div>											

									</div>		

									<div class="row cab_r rw">
										<div class="col col_25">
											<div class="form-group">
											    <input type="checkbox" class="form-control" name="cuenta_corriente" id="cuenta_corriente" value="2" />
												<label for="cuenta_corriente">Cuenta Corriente</label>
												
											</div>
										</div>
										<div class="col col_25">
											<div class="form-group">	
											    <label for="num_cuenta_corriente">No</label>	
											    <input type="text" class="form-control" name="num_cuenta_corriente" id="num_cuenta_corriente" value="" />
																						
											</div>
										</div>	
										<div class="col col_25">
											<div class="form-group">
											    <label for="banco_cc">Banco</label>	
											    <select id="banco_cc" name="banco_cc" class="form-control" >
												<option value="0">-----</option>
													@foreach($cmb_banco as $cv)
															<?php  
																 $varCh ="";
																 //if($arr_formapago['banco'] == $cv->ban_code){
																//	 $varCh ="selected";
																 //}
															?>
															<option value="{{$cv->ban_code}}"  {{$varCh}}>{{$cv->ban_name}}</option>
													@endforeach
												</select>											
											</div>
										</div>	
										<div class="col col_25">
											<div class="form-group">					
											</div>
										</div>											

									</div>	
									
									<div class="row cab_r rw">
										<div class="col col_25">
											<div class="form-group">
											    <input type="checkbox" class="form-control" name="cuenta_ahorros" id="cuenta_ahorros" value="3" />
												<label for="cuenta_ahorros">Cuenta de ahorros</label>
												
											</div>
										</div>
										<div class="col col_25">
											<div class="form-group">	
											    <label for="num_cuenta_ahorros">No</label>	
											    <input type="text" class="form-control" name="num_cuenta_ahorros" id="num_cuenta_ahorros" value="" />
																						
											</div>
										</div>	
										<div class="col col_25">
											<div class="form-group">
											    <label for="banco_ca">Banco</label>	
											    <select id="banco_ca" name="banco_ca" class="form-control" >
												<option value="0">-----</option>
													@foreach($cmb_banco as $cv)
															<?php  
																 $varCh ="";
																 //if($arr_formapago['banco'] == $cv->ban_code){
																//	 $varCh ="selected";
																 //}
															?>
															<option value="{{$cv->ban_code}}"  {{$varCh}}>{{$cv->ban_name}}</option>
													@endforeach
												</select>											
											</div>
										</div>	
										<div class="col col_25">
											<div class="form-group">					
											</div>
										</div>											

									</div>	

									<div class="row cab_r rw">
										<div class="col col_25">
											<div class="form-group">
											    <input type="checkbox" class="form-control" name="tarjeta_credito" id="tarjeta_credito" value="4" />
												<label for="tarjeta_credito">Tarjeta de cr&eacute;dito</label>
												
											</div>
										</div>
										<div class="col col_25">
											<div class="form-group">	
											    <label for="num_tarjeta_credito">No</label>	
											    <input type="text" class="form-control" name="num_tarjeta_credito" id="num_tarjeta_credito" value="" />
																						
											</div>
										</div>	
										<div class="col col_25">
											<div class="form-group">
											    <label for="tipo_tarjeta">Tipo:</label>	
											   	<select id="tipo_tarjeta" name="tipo_tarjeta"  class="form-control">
													<option value="0">-----</option>
													@foreach($cmb_tipo_tarjeta as $cv)
															<?php  
																 $varCh ="";
																// if($arr_formapago['tipo_tarjeta'] == $cv->cca_code){
																//	 $varCh ="selected";
																 //}
															?>
															<option value="{{$cv->cca_code}}"  {{$varCh}}>{{$cv->cca_name}}</option>
													@endforeach
												</select>									
											</div>
										</div>	
										<div class="col col_25">
											<div class="form-group">

											    <label for="num_tarjeta_credito">Fecha Venc.</label>	
											    <input type="text" class="form-control cs_mes_anio" name="mes_anio" id="mes_anio" value="" placeholder="MES/A&#209;O" />
																				
											</div>
										</div>											

									</div>	

									
									
								</div>
								<div class="tbl">
									<div class="row cab_r rw">
										<div class="col col_100">
											<div class="form-group">
												<span>La Cantidad de $.
											    <input type="text" class="form-control" name="precio_unit" id="precio_unit" placeholder="valor cuota" value="{{$arrWebProduct->pro_valor}}" />
												por concepto de Asistencia Exequial,y $:
											    <input type="text" class="form-control" name="valor_gad" id="valor_gad" placeholder="valor gad" value="{{$arr_prod['valor_gad']}}" />
												por concepto de Gastos Administrativos de Cartera; en periodos:
				
												
												</span>
											</div>
										</div>

									</div>									
								
								</div>
								
								
								<div class="tbl">
									<div class="row cab_r rw">
										<div class="col col_25">
											<div class="form-group">
											    
											    <input type="checkbox" class="form-control" name="chk_mensual" id="chk_mensual" value="4" />
												<label for="chk_mensual">Mensual</label>	
											
											</div>
										</div>
										<div class="col col_25">
											<div class="form-group">
											    
												<input type="checkbox" class="form-control" name="chk_trimestral" id="chk_trimestral" value="3" />
												<label for="chk_trimestral">Trimestrales</label>
											
											</div>
										</div>	
										<div class="col col_25">
											<div class="form-group">
											    
												<input type="checkbox" class="form-control" name="chk_semestral" id="chk_semestral" value="2" />
												<label for="chk_semestral">Semestrales</label>
											
											</div>
										</div>											
										<div class="col col_25">
											<div class="form-group">
											    
												<input type="checkbox" class="form-control" name="chk_anual" id="chk_anual" value="1" />
												<label for="chk_anual">Anuales</label>
											
											</div>
										</div>																			

									</div>									
								
								</div>		

								<div class="tbl">
									<div class="row cab_r rw">
										<div class="col col_100">
											<div class="form-group">
												<span>A partir de
											    <input type="text" class="form-control" name="fecha_start" id="fecha_start" placeholder="valor cuota" value="{{$fecha_start}}" />
												por tiempo indefinido este valor deber&aacute; ser acreditado a Memorial International of Ecuador S.A. por concepto de la
												contrataci&oacute;n del Plan Exequial.
				
												
												</span>
											</div>
										</div>

									</div>									
								
								</div>
								
								<div class="tbl">
									<div class="row cab_r rw">
										<div class="col col_25">
											<div class="form-group">
											    
											    <input type="checkbox" class="form-control" name="chk_cancelado" id="chk_cancelado" value="1" />
												<label for="chk_mensual">Cancelado</label>	
											
											</div>
										</div>
										<div class="col col_25">
											<div class="form-group">
											    
												<input type="checkbox" class="form-control" name="chk_acreditado" id="chk_acreditado" value="2" />
												<label for="chk_acreditado">Acreditado en la cuenta</label>
											
											</div>
										</div>	
										<div class="col col_25">
											<div class="form-group">
											    <label for="num_cuenta_mem">No.</label>
												<input type="text" class="form-control" name="num_cuenta_mem" id="num_cuenta_mem" value="" />
												
											
											</div>
										</div>											
										<div class="col col_25">
											<div class="form-group">
											    <label for="banco_mem">del Banco</label>
												<select id="banco_mem" name="banco_mem" class="form-control" >
												<option value="0">-----</option>
													@foreach($cmb_banco as $cv)
															<?php  
																 $varCh ="";
																 //if($arr_formapago['banco'] == $cv->ban_code){
																//	 $varCh ="selected";
																 //}
															?>
															<option value="{{$cv->ban_code}}"  {{$varCh}}>{{$cv->ban_name}}</option>
													@endforeach
												</select>	
												de Memorial International of Ecuador S.A.
												
											
											</div>
										</div>																			

									</div>									
								
								</div>		



								<div class="tbl">
									<div class="row cab_r rw">
										<div class="col col_25">
											<div class="form-group">
											    
											 
												<span>Me Comprometo a mantener y cubrir en mi:</span>	
											
											</div>
										</div>
										<div class="col col_25">
											<div class="form-group">
											    
												<input type="checkbox" class="form-control" name="chk_comp_ca" id="chk_comp_ca" value="1" />
												<label for="chk_comp_ca">Cta de Ahorros</label>
											
											</div>
										</div>	
										<div class="col col_25">
											<div class="form-group">
												<input type="checkbox" class="form-control" name="chk_comp_cc" id="chk_comp_cc" value="2" />
												<label for="chk_comp_cc">Cta Corriente</label>
												
											</div>
										</div>											
										<div class="col col_25">
											<div class="form-group">

												<input type="checkbox" class="form-control" name="chk_comp_cc" id="chk_comp_cc" value="2" />
												<label for="chk_comp_cc">Tarjeta de cr&eacute;dito,el monto</label>	
											
											</div>
										</div>																			

									</div>									
								
								</div>	

								
								
								<div class="tbl">
									<div class="row cab_r rw">
										<div class="col col_100">
											<div class="form-group">
												<span>Autorizo a Memorial International of Ecuador S.A. a realizar el incremento respectivo de acuerdo a load
													  establecido en el contrato.
													  <br>
													  Cualquier intrucci&oacuten; para que se valide esta autorizaci&oacute;n, la presentar&eacute con 30 d&iacutes
													  de anticipaci&oacute a ustedes y a Memorial International of Ecuador S.A.
												</span>
											</div>
										</div>

									</div>	

									<div class="row cab_r rw">
										<div class="col col_100">
											<div class="form-group">
												<span>Por la atenci&oacute;n prestada a la presente, me subscribo:
												</span>
											</div>
										</div>

									</div>										
								
								</div>		


								<div class="tbl">
									<div class="row cab_r rw">
										<div class="col col_25">
											<div class="form-group">
											    
											 
												<label for="ciudad">En</label>
												<input type="text" class="form-control" name="ciudad" id="ciudad" value="" placeholder="Ciudad" />
											</div>
										</div>
										<div class="col col_25">
											<div class="form-group">
												<label for="dia">al</label>											    
												<input type="text" class="form-control" name="dia" id="dia" value=""  placeholder="dia" />

											
											</div>
										</div>	
										<div class="col col_25">
											<div class="form-group">
												<label for="mes">de</label>											    
												<input type="text" class="form-control" name="mes" id="mes" value=""  placeholder="mes" />
												
											</div>
										</div>											
										<div class="col col_25">
											<div class="form-group">

												<label for="anio">de</label>											    
												<input type="text" class="form-control" name="anio" id="anio" value=""  placeholder="anio" />
											
											</div>
										</div>																			

									</div>									
								
								</div>	


								<div class="tbl">
									<div class="row cab_r rw">
										<div class="col col_25">
											<div class="form-group">
											    
												<hr></hr>
												<span>Firma</span>
									
											</div>
										</div>
										<div class="col col_25">
											<div class="form-group">
																					    
												<input type="text" class="form-control" name="identificacion" id="identificacion" value=""  placeholder="identificacion" />
												<br><label for="dia">No CEDULA DE IDENTIDAD</label>		
											
											</div>
										</div>	
										<div class="col col_25">
											<div class="form-group">
											
												
											</div>
										</div>											
										<div class="col col_25">
											<div class="form-group">
												<img src="/img/memorial-logo.png" height="60px" width="160px" />
											
											</div>
										</div>																			

									</div>									
								
								</div>									
								
							    <hr></hr>
								
								
								
								<div class="tbl">
									<div class="row cab_r rw">
										<div class="col col10">
												<span class="cs_footer">QUITO</span>
										</div>
										<div class="col col10">
												<span class="cs_footer">(02)2447431</span><br/>
												<span class="cs_footer">(02)2253890</span>
										</div>										
										<div class="col col10">
												<span class="cs_footer">QUITO SUR</span>
										</div>
										<div class="col col10">
												<span class="cs_footer">(02)2614497</span><br/>
												<span class="cs_footer">(02)2661737</span>
										</div>		
										<div class="col col10">
												<span class="cs_footer">GUAYAQUIL</span>
										</div>
										<div class="col col10">
												<span class="cs_footer">(04)2631452</span>
										</div>	
										<div class="col col10">
												<span class="cs_footer">CUENCA</span>
										</div>
										<div class="col col10">
												<span class="cs_footer">(07)2824742</span><br/>
												<span class="cs_footer">(07)2836658</span>
										</div>	
										<div class="col col10">
												<span class="cs_footer">MANTA</span>
										</div>
										<div class="col col10">
												<span class="cs_footer">(05)2623502</span><br/>
												<span class="cs_footer">(05)2628865</span>
										</div>											
										<div class="col col10">
												@include('layouts.contact_phones')
										</div>
									</div>									
								
								</div>									
								
							</form>
							
							
			
			
			</div>
      </div>



      <!-- Modal footer -->
      <div class="modal-footer">
        <!--<button type="button" class="btn btn-danger button_module_wh" data-dismiss="modal">Close</button>-->
		<button type="button" id="btnCerrarDeb" class="btn btn-danger button_module_wh" data-dismiss="modal">Cerrar</button>
&nbsp;
	    <button name="btnGuardarCd" id="btnGuardarCd"   class="btn btn-default">Guardar</button>		
      </div>

    </div>
  </div>
</div>	


<!-- The Modal -->
<div class="modal" id="myModalConfirm">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">



      <!-- Modal body -->
      <div class="modal-body" id="dv_confirm_additional">
			<p>Ya existe una asistencia asociada al producto seleccionado, desea ingresar otra?</p>
      </div>

      <!-- Modal footer -->
      <div class="modal-footer">
        <button type="button" class="btn btn-danger button_module_wh" data-dismiss="modal">Cancelar</button>
&nbsp;
	    <button name="btnGuardarAnul" id="btnGuardarAnul" onclick="saveAdditional()"  class="btn btn-default">Guardar</button>		
      </div>

    </div>
  </div>
</div>	
</div>
<script type="text/javascript">
       $(function(){
		   


          $('#alert_success_cc').hide();
          $('#alert_error_cc').hide();		  
		  
          $('#dv_alert_warn').hide();
		  
		  $('#sel_tpr').on('change',function(){
			 loadListChild('sel_tpr','sel_tctg','tipo_cat_pr');
			 $('#sel_tctg').trigger('change'); 
		  });
		  
		  
		  $('#sel_tctg').on('change',function(){
			  
			 loadListChild('sel_tctg','sel_car','cartera_tp');
			  
		  });		  
		  
		  $('#sel_car').on('change',function(){
			  
			 loadListChild('sel_car','sel_categmine','categoria_ctp');
			  
		  });		
		  
			$("#operador_tarjeta").on('change',  function() {
    		   var operador_tarjeta = $("#operador_tarjeta option:selected").val();

			   
                $.ajax({
                    type:'GET',
                    url:'/sales/payment/cmbCreditCard/'+operador_tarjeta,
                    success:function(data){
                        console.log(data);
                        if(data.success==1){
                            var cmbmp  = data.json_cd;
							var len = cmbmp.length;

							$("#tipo_tarjeta").empty();
							for( var i = 0; i<len; i++){
								var id = cmbmp[i]['cca_code'];
								var name = cmbmp[i]['cca_name'];

								$("#tipo_tarjeta").append("<option value='"+id+"'>"+name+"</option>");
							}						
                            return;
                        }else{
                            
                            return;
                        }
                    }
                });  			   
    		});					  
		  


			$("#btnGuardarCd").click(function( event ) {

				event.preventDefault();

					    var form = $('#frmDCreditC');
						var pay_day  = $("#period_pago option:selected").val();
						var request = $.ajax({
							type:	'POST',
							url:	'/sales/payment/save_credir_card_form/'+pay_day,
							data:	form.serialize(),
							success:function(data){
								console.log(data);
								if(data.success==1){
									$("#btnGuardarDeb").prop('disabled',true);
									$("#btnCerrarDeb").prop('disabled',true);									
									$('#alert_success_cc').html(data.message);
									$('#alert_success_cc').show();
									
									/*setInterval(function(){ 
										
										$('#myModalDebit').fadeIn("slow");
										

									}, 2000);*/
									$('#myModalDebitCreditC').modal('hide');
									$('#seccion_detalle_planes').load(data.url_redirect);	
									
									        			
									return;
								}else if(data.success==2){
									/*$('#alert_error_cc').empty();*/
									$.each( data.errors, function(i, obj) {
										$('#alert_error_cc').append(obj+'<br>');
										
									});
									
									$('#alert_error_cc').show();
									/*alertPPYA(data.message);*/
									console.log(data);
									return;
								}
							}
						});
						request.fail(function( jqXHR, textStatus ) {
							alertPPYA( "Hubo un error: " + textStatus );
							console.log(jqXHR);
						});
								
				
			});


       	  $('#sel_categmine').on('change', function() {
				Pace.restart();
				
				var sel_id     = $( "#sel_categmine option:selected" ).val();
				var $child_sel = $('#sel_pr');
				$.ajax({
					type: 'GET',
					url:'/insurance/cmbProduct/'+sel_id+'/',
					success:function(data){
						if(data.success==1){

							$child_sel.find('option').remove();         
							$.each( data.json_product, function(i, obj) {
								if(i==0){
									$child_sel.append('<option value="0" selected>----</option>');
									$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
								}else{
									$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
								}
							});
								
							
							return;
						}else{
							
							alertPPYA(data.message);
							return;
						}
					}
				});	
             
		  });		  
		  
       	  $('#sel_pr').on('change', function() {
       	  	 $("#id_producto").val(this.value);
       	  	 var id_producto  = this.value;


				$.ajax({
						type:	'GET',
						url:	'/insurance/get_data_product/'+id_producto+'/',
						success:function(data){
							console.log(data);
							if(data.success==1){

								var valor_pr  = parseFloat(data.arr_product.precio_unit)*parseFloat(data.arr_product.npc);
								$("#ad_valor").val(valor_pr.toFixed(2));
								return;
							}else if(data.success==2){
								$('#dv_alert_warn').show();
								$('#dv_alert_warn').addClass('in');
								$('#dv_alert_warn').addClass('show');	
								var valor_pr  = parseFloat(data.arr_product.precio_unit)*parseFloat(data.arr_product.npc);
								$("#ad_valor").val(valor_pr.toFixed(2));								
								/*$('#dialog-confirm').dialog('open');*/
								return;
							}
						}
				}); 
             
		  });
		  
		  
		  
		  
		  
       });



		function loadListChild(parent_name,child_name,child_pref){
			Pace.restart();
			
			var sel_id = $( "#"+parent_name+" option:selected" ).val();
			var $child_sel = $('#'+child_name);
			$.ajax({
				type: 'GET',
				url:'/general/getListChild/'+child_pref+'/'+sel_id+'/',
				success:function(data){
					if(data.success==1){

					    if(child_name=='sel_car'){
							$child_sel.find('option').remove();         
							$.each( data.modelData, function(i, obj) {
								  if(i==0){
									  $child_sel.append('<option value="0" selected>----</option>');
								  }
								if(obj.por_additional == 1){
									if(i==0){
										
										$child_sel.append('<option value=' + obj.por_id + ' >' + obj.por_name + '</option>');
									}else{
										$child_sel.append('<option value=' + obj.por_id + ' >' + obj.por_name + '</option>');
									}
								}

							});
						}else{
								$child_sel.find('option').remove();         
								$.each( data.arr_dataw2ui, function(i, obj) {
									if(i==0){
										$child_sel.append('<option value="0" selected>----</option>');
										$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
									}else{
										$child_sel.append('<option value=' + obj.id + ' >' + obj.text + '</option>');
									}
								});
							
						}
						

							
						
						return;
					}else{
						
						alertPPYA(data.message);
						return;
					}
				}
			});
		}

</script>

							