<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenModuleWeb extends Model{
    //
	protected $table = 'gen_module_webs';
	protected $primaryKey = 'mdw_code';
}
