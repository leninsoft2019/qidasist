<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BtnP2pTran extends Model{
    //
	protected $table = 'btn_p2p_trans';
	protected $primaryKey = 'bpt_id';
}
