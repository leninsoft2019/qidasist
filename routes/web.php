<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'ProductController@showHome');
Route::get('/edad/', 'ProductController@Edad');
Route::get('/get_pr_detail/{id_pr}', 'ProductController@getProductDetail');


/** Quote **/
Route::group(['prefix' => 'quote' ], function(){
	
	Route::get('/index/', 'ProductController@showWebProduct');
	Route::get('/detail/{op1}/', 'ProductController@detailWebProduct');
	Route::get('/detail_tit/{op1}/', 'ProductController@detailWebProductTit');
	Route::get('/detail_tit_col/{op1}/{op2}/{op3?}', 'ProductController@detailWebProductTitCol');

});

/** Payment **/
Route::group(['prefix' => 'payment' ], function(){
	Route::post('/tit_save/', 'ProductController@titSave');
	Route::post('/mult_save/', 'ProductController@multSave');
	
	Route::get('/pay_sel/', 'ProductController@paySel');
	Route::get('/dependent_last/', 'ProductController@dependentLast');
	Route::get('/get_data_client/', 'ProductController@getDataClient');


});

/** Generic **/
Route::group(['prefix' => 'generic' ], function(){
	Route::get('/edad/', 'ProductController@Edad');
	Route::get('/birthday/', 'ProductController@birthday');
	Route::get('/transactionList/', 'ProductController@transactionList');
	// Ventas P2P solicitando Tarjet de Credito
	// P2P sales requesting Credit Card
	Route::get('/showHome/', 'TmpSalesController@showHome');
	Route::get('/getCmb/{op?}', 'TmpSalesController@getCmb');
});

/** Sales **/
Route::group(['prefix' => 'sales' ], function(){
	Route::get('/product_list', 'SalesController@showListProducts');
	Route::get('/intro/{api_code}/{mod_code}/', 'SalesController@showHome');		
	Route::get('/dependents/', 'SalesController@showFormDependents');	
	Route::get('/preview/', 'SalesController@showPreview');	
	Route::get('/test_form_auth_debit/', 'SalesController@testFormAuthDeb');	
	Route::get('/previous_page/{op}', 'SalesController@previousPage');	

	/** Type: categories and Product **/
	Route::group(['prefix' => 'quote' ], function(){
		
		Route::get('/index/', 'SalesController@showWebProduct');
		Route::get('/detail/{op1}/', 'SalesController@detailWebProduct');
		Route::get('/detail_tit/{op1}/', 'SalesController@detailWebProductTit');

	});
	
	/** Payment **/
	Route::group(['prefix' => 'payment' ], function(){
		Route::post('/save_debit/', 'SalesController@saveDebit');
		Route::post('/save_trf/', 'SalesController@saveTransfer');
		Route::get('/supscription/', 'SalesController@supscriptionMethod');
		Route::get('/cmbCreditCard/{id_op}', 'SalesController@cmbCreditCard');
		Route::post('/save_credir_card_form/{pay_day}', 'SalesController@saveFormCreditCard');
		Route::post('/save_credir_card_form_fp/{pay_day}', 'SalesController@saveFormCreditCardFP');

	});
	
	/* 
	*	Leninsoft: 2019-09-04
	*	Sales of the Columbarium 
	*/
	Route::get('/columbarium/{op?}', 'SalesController@showColumbariums');
	Route::get('/getColumbariums/{op1?}/{op2?}/', 'SalesController@getColumbariums');

	
});

/** Payment **/
Route::group(['prefix' => 'payment' ], function(){
	Route::post('/tit_save/', 'SalesController@titSave');
	Route::post('/mult_save/', 'SalesController@multSave');
	
	Route::get('/pay_sel/', 'SalesController@paySel');
	Route::get('/dependent_last/', 'SalesController@dependentLast');
	Route::get('/get_data_client/', 'SalesController@getDataClient');
	Route::post('/place_to_pay_val/', 'SalesController@placeToPayVal');

});

/** P2P **/
Route::group(['prefix' => 'p2p' ], function(){
	Route::post('/notify', 'SalesController@notifyP2P');
	Route::get('/response', 'SalesController@responseP2P');
	//Route::get('/validate_state', 'SalesController@validateStateP2P');
	Route::get('/validate_state', 'SalesController@validateStateP2P');
	Route::post('/collect_payp2p', 'SalesController@collectP2P');
	Route::post('/save_recurrent', 'SalesController@saveRecurrentP2P');	
	Route::get('/collect_recurrent', 'SalesController@collectRecurrentP2P');	
	Route::post('/save_pay_col', 'SalesController@savePayCol');	
	
	
	
});	

/** Client **/
Route::group(['prefix' => 'client' ], function(){
	Route::get('/home/', 'ClientController@showHome');
	Route::get('/search/', 'ClientController@searchClient');
	
	Route::get('/register/', 'ClientController@register');
	Route::get('/edit_data/{id_sale?}', 'ClientController@formEditMethodSupscription');
	

});

Auth::routes();

/*Route::get('/home', 'HomeController@index')->name('home');*/
Route::get('/home', 'SalesController@showListProductsSeller')->name('home');
Route::get('/freq_quest', 'ProductController@showFrequentQ');
// Leninsoft, creacion del modulo de clientes para P2P
// 2019-09-02
Route::get('/homeClient', 'ClientController@homeClient')->name('homeClient');
Route::get('/transactionClient/{op1}', 'ClientController@transactionClient');
Route::get('/assistanceClient/{op1?}/{op2?}/{op3?}/{op4?}/', 'ClientController@assistanceClient');
