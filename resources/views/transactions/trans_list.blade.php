
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Memorial Ecuador</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>	
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

	<style type="text/css">
		.container1{
			width:99%;
			margin:auto;
			padding:20px;
		}
		
		#customers {
		  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
		  border-collapse: collapse;
		  width: 100%;
		}

		#customers td, #customers th {
		  border: 1px solid #ddd;
		  padding: 8px;
		}

		#customers tr:nth-child(even){background-color: #f2f2f2;}

		#customers tr:hover {background-color: #ddd;}

		#customers th {
		  padding-top: 12px;
		  padding-bottom: 12px;
		  text-align: left;
		  background-color:#2c0373 /*#4CAF50*/;
		  color: white;
		}		
	</style>
</head>

<body>		
  <section class="container1" id="customers">
	<table id="transactions" class="display table-striped table-bordered" style="width:100%">
	   <thead>
		<tr>
			<th class="">{{$objlabels->wlb_trans_id}}</th>
			<th class="">{{$objlabels->wlb_trans_status}}</th>
			<td class="">{{$objlabels->wlb_trans_message}}</td>
			<th class="">{{$objlabels->wlb_trans_date}}</th>
			<th class="">{{$objlabels->wlb_trans_requestid}}</th>
			<th class="">{{$objlabels->wlb_trans_proprice}}</th>
			<th class="">{{$objlabels->wlb_trans_cliname}}</th>
			<th class="">{{$objlabels->wlb_trans_clisurname}}</th>
			<th class="">{{$objlabels->wlb_trans_clinumdoc}}</th>
			<th class="">{{$objlabels->wlb_trans_clienmail}}</th>
			<th class="">{{$objlabels->wlb_trans_cliphone}}</th>
			<th class="">{{$objlabels->wlb_trans_prodname}}</th>
			<th class="">{{$objlabels->wlb_trans_proqty}}</th>

		</tr>
		</thead>
		<tbody>
		@foreach($arrWebtrans as $arrWebtran)
		<tr>
			<td class="">{{$arrWebtran->bpt_id}}</td>
			<td class="">{{$arrWebtran->bpt_status}}</td>
			<td class="">{{$arrWebtran->bpt_message}}</td>
			<td class="">{{$arrWebtran->bpt_date}}</td>
			<td class="">{{$arrWebtran->bpt_session}}</td>
			<td class="">{{$arrWebtran->bpt_price}}</td>
			<td class="">{{$arrWebtran->bpt_name}}</td>
			<td class="">{{$arrWebtran->bpt_surname}}</td>
			<td class="">{{$arrWebtran->bpt_numdoc}}</td>
			<td class="">{{$arrWebtran->bpt_email}}</td>
			<td class="">{{$arrWebtran->bpt_phone}}</td>
			<td class="">{{$arrWebtran->bpt_product_name}}</td>
			<td class="">{{$arrWebtran->bpt_quantity}}</td>

		</tr>
		@endforeach
		</tbody>
	</table>
	<script>
		$(document).ready(function() {
			$('#transactions').DataTable({ "pageLength": 6});
		} );
	</script>
</section>
</body>
</html>