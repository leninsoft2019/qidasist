


$(document).ready(function(){
	
	
	$("#btnAddNew").click(function(event){
		nuevaAsistencia();
	});
  
   $("#btnBuscar").click(function(event){
	   event.preventDefault();
	
	   Pace.restart();
		
       buscarDatosFiltros();
  });
  
  
   $("#btnEdit").click(function(event){
	   event.preventDefault();
	
	   Pace.restart();
	   var cl_iden = $("#cli_identificacion_2").val();	
	   $("#cli_identificacion").val(cl_iden);	
       buscarDatosFiltros();
  });
  
  
  
  

  
    $(document).ajaxStart(function(event, jqXHR) {
        //Show spinner
		$('#preloader').show();
    });
    //This is called after all the AJAX request are stopped.
    $(document).ajaxStop(function(event, jqXHR) {
        //Hide spinner
			$('#preloader').fadeOut('slow');
    });
  
     
  
});



function nuevaAsistencia(){
	$('#cont_cat').load('/sales/product_list');
}







function cargarPantallMasivo(){

		$("#pop_masivos").dialog({
								modal: true,
								width: 500,
								height:400,
									close: function(event, ui) {
										$("#pop_masivos").hide();
										}
									});
	
} // Fin cagarPolizaAnular



function validateP2P(){
		var request = $.ajax({
			type:	'GET',
			url:	'/p2p/validate_state',
			success:function(data){
				console.log(data);
				if(data.success==1){
					/*$('#myModalP2P').modal('hide');
				    $('.contenedor_mt').load(data.url_redirect);*/			
					firstPaymentP2P('');					
					return;
				}else{
					$('#myModalP2P').modal('hide');
					alertPPYA(data.message);
					console.log(data);
					return;
				}
			}
		});
		request.fail(function( jqXHR, textStatus ) {
			$('#myModalP2P').modal('hide');
			alertPPYA( "{{$objform->lbl_requestfailed}}: " + textStatus );
			console.log(jqXHR);
		});
}


function firstPaymentP2P(msg){
	        var form = $('#frmMT');
			console.log('---------');
			console.log('---------');
			var request = $.ajax({
			type:	'POST',
			url:	'/p2p/collect_payp2p',
			data:	form.serialize(),
			success:function(data){
				/*console.log(data);*/
				console.log(JSON.stringify(data));
			
				if(data.success==1){
					/*$('#myModalP2P').modal('hide');*/
					alertPPYA( msg+'<br>'+ data.message);
				    $('#seccion_detalle_planes').load(data.url_redirect);					
					return;
				}else{
					/*$('#myModalP2P').modal('hide');*/
					alertPPYA(data.message);
					console.log(data);
					return;
				}
			}
		});
		request.fail(function( jqXHR, textStatus ) {
			$('#myModalP2P').modal('hide');
			alertPPYA( "{{$objform->lbl_requestfailed}}: " + textStatus );
			console.log(jqXHR);
		});
}

function paymentCol(msg){
	 var form = $('#frmMT');
			console.log('---------');
			console.log('---------');
			var request = $.ajax({
			type:	'POST',
			url:	'/p2p/save_pay_col',
			data:	form.serialize(),
			success:function(data){
				/*console.log(data);*/
				console.log(JSON.stringify(data));
			
				if(data.success==1){
					/*$('#myModalP2P').modal('hide');*/
					alertPPYA( msg+'<br>'+ data.message);
				    $('#seccion_detalle_planes').load(data.url_redirect);					
					return;
				}else{
					/*$('#myModalP2P').modal('hide');*/
					alertPPYA(data.message);
					console.log(data);
					return;
				}
			}
		});
		request.fail(function( jqXHR, textStatus ) {
			$('#myModalP2P').modal('hide');
			alertPPYA( "{{$objform->lbl_requestfailed}}: " + textStatus );
			console.log(jqXHR);
		});
}

function nextStageP2P(){
		var request = $.ajax({
			type:	'GET',
			url:	'/p2p/validate_state',
			success:function(data){
				console.log(data);
				if(data.success==1){
					$('#myModalP2P').modal('hide');
				    $('#seccion_detalle_planes').load('sales/dependents');				
					return;
				}else{
					$('#myModalP2P').modal('hide');
					alertPPYA(data.message);
					console.log(data);
					return;
				}
			}
		});
		request.fail(function( jqXHR, textStatus ) {
			$('#myModalP2P').modal('hide');
			alertPPYA( "{{$objform->lbl_requestfailed}}: " + textStatus );
			console.log(jqXHR);
		});
}


function recuperarDatosFactura(){
			event.preventDefault();
			
			var request = $.ajax({
				type:	'GET',
				url:	'/payment/get_data_client',
				success:function(data){
					console.log(data);
					if(data.success==1){
						
						$('#nombre').val(data.data_person.nombre);
						$('#apellido').val(data.data_person.apellido);
						$('#id').val(data.data_person.id);
						$('#email').val(data.data_person.email);
						$('#direccion').val(data.data_person.direccion);
						$('#phone').val(data.data_person.phone);
						
						return;
					}
				}
			});
			request.fail(function( jqXHR, textStatus ) {
				alert( "Hubo un problema: " + textStatus );
				console.log(jqXHR);
			});				
}