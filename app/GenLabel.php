<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenLabel extends Model{
    //
    protected $table = 'web_labels';
	protected $primaryKey = 'wlb_id';
}