<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenApiMaster extends Model
{
    //
    protected $table = 'gen_api_master';
}
