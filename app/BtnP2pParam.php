<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BtnP2pParam extends Model{
    //
	protected $table = 'btn_p2p_params';
	protected $primaryKey = 'p2p_id';
}
