<?php
/*
*	Página que despliega los datos del titular para la venta
*	Page that displays the data of the owner for sale
*/

	$nombre   = "";
	$apellido = "";
	$id       = "";
	$email    = "";
	$per_address = "";
	$per_phone = "";
	$fbirthday = "";
	$edad      = "";	 
	
if(Session::has('sale_id')){
	$objWebPerson = $arr_edit['WebPerson'];
	$nombre   = $objWebPerson->per_name;		
    $id       = $objWebPerson->per_id;	
    $apellido = $objWebPerson->per_surname;		
    $per_address = $objWebPerson->per_address;	
	$per_phone   = $objWebPerson->per_phone;	
	$fbirthday   = $objWebPerson->per_fbirthday;	
	$edad        = $objWebPerson->per_age;	
	$email       = $objWebPerson->per_email;		

	
}
	
?>

<div class="contenedor_mt">

	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<style scoped>


		#frmMT{
			width:70%;
			margin:auto;
			
			border:solid 1px  rgb(56, 30, 61,0.9);
			/*border-radius:5px;*/
			margin-top:20px;
			margin-bottom:20px;
			padding:20px;
			/*-webkit-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
			-moz-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
			box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);*/
			background-color:white;
			color:white;
		}
		.row {
			display: -webkit-box;
			display: -ms-flexbox;
			
			width:100%;
			-ms-flex-wrap: wrap;
			flex-wrap: wrap;
			margin-right: -15px;
			margin-left: -15px;
		}

		.row_n{
			display:table;
			width:100%;
		}

		.tbl{
			display:table;
			width:100%;	
			
		}
		.rw{
			display:table-row;
			width:100%;
		}

		.col{
			position: relative;
			width: 50%;
			min-height: 1px;
			padding-right: 15px;
			padding-left: 15px;
			
			display:table-cell;
		}


		.col2{
			position: relative;
			width: 50%;
			min-height: 1px;
			padding-right: 15px;
			padding-left: 15px;
			
			display:table-cell;
		}

		.form-control {
			display: block;
			width: 100%;
			padding: .375rem .75rem;
			font-size: 1rem;
			line-height: 1.5;
		   /* color: #495057;*/
			background-color: #fff;
			background-clip: padding-box;
			border: 1px solid #ced4da;
			border-radius: .25rem;
			transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
		}


		.form-control {
			padding: 2px;
			height: 28px;
			border: 1px solid #dddddd;
		}

		h2{
			font-family: "Rubik",Helvetica,Arial,sans-serif;
			font-size:2em;
			font-weight:500;
		}



		.col_add{
			width:25%;
		}

		.col_minus{
			width:25%;
		}

		.col_range{
			width:25%;
		}

		.col_50{
			display:table-cell;
			width:50%;
		}


		label{
			color:rgb(56, 30, 61,0.9);
		}

		.cs_footer_title{
			color:rgb(56, 30, 61,0.9);
			font-size:2em;
			font-weight:bold;
		}

		.cs_footer_text{
			color:black;
		}

		#footer_form{
			width:100%;
			background-color:#E8DDF6;
			padding:20px;
		}



		.tbl_2{
			display:table;
			padding:20px;
			width:80%;
			margin:auto;	
			
		}



		@media (max-width: 767px) {

			.text-xxl {
				font-size: 50px !important;
				line-height: 60px !important;
			}

			h1{
				text-indent:-9999px;
				font-size:0px;
				position:absolute;
			}

			h2, .text-l {
				margin-top:20px;
				font-size: 1.5em !important;
				line-height: 1em !important;
				
			}

			h3 {
				font-size: 18px !important;
				line-height: 28px !important;
			}
			
			
			#frmMT{
				
				width:98%;
				maring:auto;
				padding:5px;
			}
			
			.row{
						display:table;
				width:100%;
			}
			
			.col{
				display:table-row;
				width:100%;
			}


			.contenedor_mt {
				padding: 0px;
				width: 100%;
			}

			.res_input{
				width:40px;
				height:20px;
			}

			.cab_r{
				padding-left:20px;
			}

			.col_add{
				width:25%;
			}

			.col_minus{
				width:25%;
			}

			.col_range{
				width:25%;
			}

		}
	</style>


	<form  id="frmMT" enctype="multipart/form-data">
		{{ csrf_field() }}	
		<h2>
			Usted a seleccionado el producto: {{$arrWebProduct->pro_name}}<br>
			Total a pagar: {{$arrWebProduct->pro_price}}
			@if(Session::has('val_sel_col'))
				<br>
			Valor aplicado: {{Session::get('val_sel_col')}}
			@endif	
			<?php
			$str_cond = '';
			if(Session::has('product_cond')){
				if(!empty(Session::get('product_cond'))){
					$arr_pr  = Session::get('product_cond');
					foreach($arr_pr as $rr){
					  if($rr->kin_code==1){				  
						$str_cond = ', El rango de edades permitido es de '.$rr->pac_age_begin.' hasta '.$rr->pac_age_end;
					  }
					}//end of foreach
				}
		    }
			
			echo $str_cond;
		?>
		</h2>
		<hr></hr>
		<!-- Titular -->
		<div class="tbl">
			<div class="row cab_r rw">
				<div class="col col_50">
					<div class="form-group">
						<label for="nombre">Nombre:</label>
						<input type="text" class="form-control" id="nombre" name="nombre" value="{{$nombre}}" />
					</div>
				</div>
				<div class="col col_50">
					<div class="form-group">
						<label for="apellido">Apellido:</label>
						<input type="text" class="form-control" id="apellido"  name="apellido" value="{{$apellido}}"  />
					</div>
				</div>
			</div>
			
			<div class="row cab_r rw">
				<div class="col col_50">
					<div class="form-group">
						<label for="id">Identificaci&oacute;n:</label>
						<input type="text" class="form-control" id="id" name="id" value="{{$id}}" />
					</div>
				</div>
				<div class="col col_50">
					<div class="form-group">
						<label for="email">Correo Electr&oacute;nico:</label>
						<input type="email" class="form-control" id="email" name="email" value="{{$email}}" />
					</div>
				</div>
			</div>
			
			<div class="row cab_r rw">
				<div class="col col_50">
					<div class="form-group">
						<label for="id">Direcci&oacute;n:</label>
						<input type="text" class="form-control" id="per_address" name="per_address"  value="{{$per_address}}"  />
					</div>
				</div>
				<div class="col col_50">
					<div class="form-group">
						<label for="email">Tel&eacute;fono:</label>
						<input type="text" class="form-control" id="per_phone" name="per_phone"  value="{{$per_phone}}"  />
					</div>
				</div>
			</div>
			
			<div class="row cab_r rw">
				<div class="col col_50">
					<div class="form-group">
						<label for="fbirthday">Fecha de nacimiento:</label>
						<input type="text" class="form-control datepicker cls_date"  autocomplete="off" id="fbirthday" name="fbirthday"  value="{{$fbirthday}}" placeholder="yyyy-mm-dd" onChange="calculaEdad(1);" />
					</div>
				</div>
				<div class="col col_50">
					<div class="form-group">
						<label for="edad">Edad:</label>
						<input type="text" class="form-control" id="edad" name="edad" onChange="calculaEdad(2);" value="{{$edad}}" />
					</div>
				</div>
			</div>
			
			@guest
			
			@else
			<div class="row cab_r rw">
				<div class="col col_50">
					<div class="form-group">
						<label for="fl_ident">Cargar Fotograf&iacute;a:</label>
						<input type="file" class="form-control "  id="fl_ident" name="fl_ident"  />
					</div>
				</div>
				<div class="col col_50">
					<div class="form-group">
						
					</div>
				</div>
			</div>				
			@endguest	
			
			<div class="row cab_r rw">
				<div class="col col_50">
					<div class="form-group">
						<button id="btnRegresar" class="btn btn-default">Regresar</button>
					</div>
				</div>
				<div class="col col_50">
					<div class="form-group">
						<button id="btnNextPy" class="btn btn-default">Continuar</button>
					</div>
				</div>
			</div>

		</div>
	</form>

</div>

<script>

	function calculaEdad(op){
		if(op==1){
			var birthday = $("#fbirthday").val();
			birthday = birthday.replace("/", "-");
			birthday = birthday.replace("/", "-");
			var request = $.ajax({
				type:	'GET',
				url:	'/generic/edad',
				data:	{'birthday':birthday},
				success:function(data){
					console.log(data);
					if(data.success==1){
						$("#edad").val(data.age);
						return;
					}else{
						//alert('Hubo un problema al tratar de generar la informacion');
						alertPPYA(data.message);
						$("#edad").val(0);
						/*
						$("#edad").trigger('change');*/
						console.log(data);
						return;
					}
				}
			});
			request.fail(function( jqXHR, textStatus ) {
				alert( "Hubo un problema: " + textStatus );
				console.log(jqXHR);
			});
		}else{
			var edad = $("#edad").val();
			var request = $.ajax({
				type:	'GET',
				url:	'/generic/birthday',
				data:	{'edad':edad},
				success:function(data){
					console.log(data);
					if(data.success==1){
						$("#fbirthday").val(data.birthday);
						return;
					}else{
						/*alert('Hubo un problema al tratar de generar la informacion');*/
						alertPPYA(data.message);
						$("#edad").val(0);
						/*$("#edad").val(0);*/
						/*$("#edad").trigger('change');*/
						console.log(data);
						return;
					}
				}
			});
			request.fail(function( jqXHR, textStatus ) {
				alert( "Hubo un problema: " + textStatus );
				console.log(jqXHR);
			});
		}
		
	} // Fin calculaEdad


	$(function () {
		/*$('.datepicker').datepicker({
				format: 'yyyy-mm-dd'
				});*/
				
		$.validator.addMethod("character", validateCharacter);
		$.validator.addMethod("numbers", validateNumbers);
		jQuery.validator.addMethod("accept", function(value, element, param) {
		  return value.match(new RegExp("." + param + "$"));
		});		
		
		$('#frmMT').validate({
			rules: {
				nombre: { 
					required: true,
					character:true,
					minlength: 4,
                    accept: "[a-zA-Z]+" 
				},
				apellido: { 
					required: true,
					character:true,
					minlength: 4,
                    accept: "[a-zA-Z]+"
				},
				id: { 
					required: true,
					character:true
				},
                edad:{
					required:true,
					numbers:true
				}				
			},
			messages: {
				nombre: {
					required: 'Se requiere este campo.',
					character: 'No se aceptan caracteres especiales.',
					minlength: 'Debe ingresar al menos 4 caracteres',
					accept: 'No se admiten numeros'
				},
				apellido: {
					required: 'Se requiere este campo.',
					character: 'No se aceptan caracteres especiales.',
					minlength: 'Debe ingresar al menos 4 caracteres',
					accept: 'No se admiten numeros'							
				},
				id: {
					required: 'Se requiere este campo.',
					character: 'No se aceptan caracteres especiales.'
				},
				edad: {
					required: 'Se requiere este campo.',
					numbers: 'La edad debe ser mayor a cero.'
				}					
			},
			onfocusout: false,
			errorElement: 'div',
			invalidHandler: function(form, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {                    
					validator.errorList[0].element.focus();
				}
			},
			highlight: function (element) { 
				$(element)
				.closest('.form-group')
				.addClass('has-error'); 
			},
			errorPlacement: function(error, element){
				error.appendTo(element.parent());
			},
			success: function (element) {
				element
				.closest('.form-group')
				.removeClass('has-error');
				element.remove();
			},
			submitHandler: function(form){
				
			}
	});
	
	
		$('.cls_date').mask('0000-00-00');
		$('#fbirthday').mask('0000-00-00');
		$('#fbirthday').datepicker({
			dateFormat: 'yy-mm-dd',
			/*dateFormat: 'yy-mm-dd',*/
			changeMonth: true,
			changeYear: true
		});
		
		
	
			$("#btnRegresar").click(function( event ) {
				event.preventDefault();
				cargaDivPage('/sales/product_list');
				
			});
			
			$("#btnNextPy").click(function( event ) {
				event.preventDefault();

				if (!$("#frmMT").valid()) {
						return;
				}			
				var form = $('#frmMT');
				var request = $.ajax({
					type:	'POST',
					url:	'/payment/tit_save',
					/*data:	form.serialize(),*/
					  data:new FormData($("#frmMT")[0]),
					  processData: false,  // tell jQuery not to process the data
					  contentType: false,  // tell jQuery not to set contentType   					
					success:function(data){
						console.log(data);
						if(data.success==1){
							cargaDivPage('/payment/pay_sel');
							return;
						}else if(data.success==2){
							var mess = '';
							$.each( data.errors, function(i, obj) {
								  mess = mess + obj+'<br>';
										
							});
							alertPPYA(mess);
							return;
						}
						else{
							alert('Hubo un problema al tratar de guardar la informacion');
							console.log(data);
							return;
						}
					}
				});
				request.fail(function( jqXHR, textStatus ) {
					alert( "Hubo un problema: " + textStatus );
					console.log(jqXHR);
				});				
				
				
			});
		
		
		
		
	});

</script>