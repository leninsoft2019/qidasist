
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="generator" content="wordpress">
    <link rel="pingback" href="https://mutualidad.mippya.com/xmlrpc.php" />
            <link rel="shortcut icon" href="https://mutualidad.mippya.com/wp-content/uploads/2019/05/favicon-1.png" />
        <title>Memorial Ecuador</title>
<link rel='dns-prefetch' href='//fonts.googleapis.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="La mutualidad &raquo; Feed" href="https://mutualidad.mippya.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="La mutualidad &raquo; RSS de los comentarios" href="https://mutualidad.mippya.com/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/mutualidad.mippya.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.2.2"}};
			!function(a,b,c){function d(a,b){var c=String.fromCharCode;l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,a),0,0);var d=k.toDataURL();l.clearRect(0,0,k.width,k.height),l.fillText(c.apply(this,b),0,0);var e=k.toDataURL();return d===e}function e(a){var b;if(!l||!l.fillText)return!1;switch(l.textBaseline="top",l.font="600 32px Arial",a){case"flag":return!(b=d([55356,56826,55356,56819],[55356,56826,8203,55356,56819]))&&(b=d([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]),!b);case"emoji":return b=d([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340]),!b}return!1}function f(a){var c=b.createElement("script");c.src=a,c.defer=c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var g,h,i,j,k=b.createElement("canvas"),l=k.getContext&&k.getContext("2d");for(j=Array("flag","emoji"),c.supports={everything:!0,everythingExceptFlag:!0},i=0;i<j.length;i++)c.supports[j[i]]=e(j[i]),c.supports.everything=c.supports.everything&&c.supports[j[i]],"flag"!==j[i]&&(c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&c.supports[j[i]]);c.supports.everythingExceptFlag=c.supports.everythingExceptFlag&&!c.supports.flag,c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.everything||(h=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",h,!1),a.addEventListener("load",h,!1)):(a.attachEvent("onload",h),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),g=c.source||{},g.concatemoji?f(g.concatemoji):g.wpemoji&&g.twemoji&&(f(g.twemoji),f(g.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}

.contenedor_mt{
 padding:30px;
 width:80%;
margin:auto; 
/* background-color:#F3EADD;*/
}

/*
#seccion_detalle_planes{
	 background-color:#F3EADD;
}*/


@media (min-width: 768px) and (max-width: 979px) {
  .contenedor_mt {
   width:100%;
   padding:5%;
  }
  
  #frmMT{
	width:100%;
	margin:auto;
	
	}
	
	h1{
		 text-indent:-9999px;
		font-size:0px;
		position:absolute;
	}
}


@media (max-width: 767px) {

	.text-xxl {
		font-size: 50px !important;
		line-height: 60px !important;
	}

h1{
		 text-indent:-9999px;
		font-size:0px;
		position:absolute;
	}

	h2, .text-l {
		font-size: 30px !important;
		line-height: 36px !important;
	}

	h3 {
		font-size: 18px !important;
		line-height: 28px !important;
	}
	
	.col{
		display:table-row;
	}

}

</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<link rel='stylesheet' id='wp-block-library-css'  href='https://mutualidad.mippya.com/wp-includes/css/dist/block-library/style.min.css?ver=5.2.2' type='text/css' media='all' />
<link rel='stylesheet' id='hc-bootstrap-css'  href='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/scripts/bootstrap/css/bootstrap.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='hc-style-css'  href='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/style.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='hc-animations-css'  href='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/css/animations.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='hc-css/content-box.css-css'  href='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/css/content-box.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='hc-css/components.css-css'  href='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/css/components.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='font-awesome-css'  href='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/scripts/font-awesome/css/font-awesome.min.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='hc_css_skin-css'  href='https://mutualidad.mippya.com/wp-content/themes/landkit/skin.css?ver=1.0' type='text/css' media='all' />



<style id='hc_css_skin-inline-css' type='text/css'>
.album-box .caption, header .btn-default, .advs-box-side-img hr, .accordion-list .list-group-item:before,.woocommerce .product span.onsale, .white.btn, .circle-button, .btn.circle-button, .header-bootstrap, .popup-banner .panel-body, .pagination > li > a, .pagination > li > span, .pagination > .disabled > a, .pagination > .disabled > a:focus, .header-title hr, .nav.inner.ms-rounded > li.active > a, .btn-default, .panel-default > .panel-heading, .panel-default .panel-footer, .advs-box-top-icon .icon, .advs-box-top-icon-img .icon, i.circle, .advs-box-multiple div.circle, .advs-box-side-img hr, .call-action-box, .title-base hr, .white .title-base hr, .header-video.white .title-base hr, .header-slider.white .title-base hr, .header-animation.white .title-base hr, .header-title.white .title-base hr, .nav.inner.ms-mini, .bg-color, .title-base .scroll-top, .btn, .title-modern .scroll-top, i.square, .header-base, .popup-banner.full-width-top, .popup-banner.full-width-bottom, .progress-bar, .tagbox span, .btn-border, #mc-embedded-subscribe, .widget #searchsubmit, .adv-img-button-content .caption:hover, input[type="submit"], .woocommerce .price_slider_wrapper .ui-slider .ui-slider-handle, .woocommerce .price_slider_wrapper .ui-slider .ui-slider-range, .cart-buttons .cart-view {
        background-color: rgb(149, 106, 45);
    }

    .call-action-box, .social-line .social-group i.circle, .bg-color,.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt, .woocommerce #respond input#submit, .woocommerce a.button, .woocommerce button.button, .woocommerce input.button {
        background-color: rgb(149, 106, 45) !important;
    }

    header .btn-default:hover, .btn-primary:active:hover, .btn-primary.active:hover, .open > .dropdown-toggle.btn-primary:hover, .btn-primary:active:focus, .btn-primary.active:focus, .open > .dropdown-toggle.btn-primary:focus, .btn-primary:active.focus, .btn-primary.active.focus, .open > .dropdown-toggle.btn-primary.focus, .btn-primary:focus, .btn-primary.focus, .btn-primary.btn-default:active, .btn-default.active, .open > .dropdown-toggle.btn-default, .pagination > li > a:focus, .circle-button:hover, .btn.circle-button:hover, .pagination > .active > a:hover, .pagination > .disabled > a:hover, .pagination > li > a:hover, .btn-default:hover, .minisocial-group.circle i:hover, .pagination > .active > a, .btn-border:hover,  .bg-transparent .menu-cta.btn-border:hover {
        background-color: rgb(72, 12, 103);
        border-color: rgb(72, 12, 103);
    }

	#mc-embedded-subscribe:hover, .widget #searchsubmit:hover, input[type="submit"]:hover, .btn:hover {
		background-color: rgb(72, 12, 103);
	}

	::selection {
	background: #956A2D;
	}

	::-moz-selection {
	background: #956A2D;
	}

    .btn-primary:focus, .btn-primary.focus, .slimScrollBar, .white .btn:hover, .white.circle-button:hover,.woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover, .btn-border:hover, .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, .bg-transparent .menu-cta.circle-button:hover, .bg-transparent .menu-cta.btn-default:hover, .bg-transparent .menu-cta.btn-border:hover {
        background-color: rgb(72, 12, 103) !important;
    }

    i.icon, .fullpage-menu .active i, .datepicker-panel > ul > li.picked, .datepicker-panel > ul > li.picked:hover, footer h4, .quote-author, .box-menu-inner .icon-box i,
    .caption-bottom p, div.adv-img .caption-bottom p, .mi-menu li .fa, .fullpage-arrow.arrow-circle .arrow i, .text-color,
    .accordion-list .list-group-item > a i, .mega-menu .fa-ul .fa-li, .adv-circle.adv-circle-center i, .mi-menu a > .fa,
    li.panel-item .fa-li, header .social-group a i, .icon-menu .navbar-collapse ul.nav i, .side-menu i, .side-menu i, .side-menu ul a i, .bs-menu li:hover > a, .bs-menu li.active > a, .img-box.adv-circle i, .advs-box-side .icon, .advs-box-side-icon i, .tag-row i, .tag-row a i.circle, .social-group i.circle, .social-button i.circle,
    .niche-box-testimonails h5, .title-icon i, .fullpage-menu.white li.active a i, ul.list-texts li b, .timeline > li > .timeline-label h4,
    .footer-center .footer-title, .accordion-list .list-group-item > a.active, header .social-group a i.fa, a, .btn-border, .btn-border.btn i, .nav-pills > li > a:hover, .nav.inner.ms-rounded li a, i, .menu-cta.btn-border {
        color: #956A2D;
    }

	a:hover, .anima-button.btn-text:hover i, .btn-text:hover, footer a:hover {
		color: rgb(72, 12, 103);
	}

    .footer-minimal .footer-title, .advs-box-top-icon.boxed .circle-button,.woocommerce div.product p.price, .woocommerce div.product span.price, .text-color, .boxed .circle-button:hover i, .boxed .btn:hover i, .woocommerce .star-rating, .woocommerce .price_slider_wrapper .price_slider_amount .button, .woocommerce ul.products li.product .price {
        color: rgb(149, 106, 45) !important;
    }

    .half-side.left, .mi-menu .side-menu li.active, .tab-box.right .nav-tabs > li.active > a, .bs-menu li:hover > a, .bs-menu li.active > a, .side-menu.ms-minimal li:hover > a, .side-menu.ms-minimal li:hover > span, .adv-circle.adv-circle-center-2 .caption p, .side-menu.ms-simple li.active {
        border-right-color: rgb(149, 106, 45) !important;
    }

    .half-side.right, .tab-box.left .nav-tabs > li.active > a, .tab-box.left .nav-tabs > li.active > a:hover, .tab-box.left .nav-tabs > li.active > a:focus, .bs-menu.menu-left li:hover > a, .bs-menu.menu-left li.active > a, .bs-callout {
        border-left-color: rgb(149, 106, 45) !important;
    }

    .datepicker-top-left, .datepicker-top-right, .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus, .mi-menu .sidebar-nav {
        border-top-color: rgb(149, 106, 45) !important;
    }

    .tab-box.inverse .nav-tabs li.active a, .datepicker-top-left:before, .datepicker-top-right:before, .nav.ms-minimal > li.active > a, .nav.ms-minimal li a:hover, .popover.bottom > .arrow:after, .title-modern h1, .title-modern h2, .title-modern h3, .nav.ms-minimal > li:hover > a, .nav.ms-minimal > li.active > a, .nav-pills > li.active > a, .nav-pills > li.active > a:hover, .nav-pills > li.active > a:focus {
        border-bottom-color: rgb(149, 106, 45) !important;
    }

    .twoside-open hr, header .btn-default, .social-line .social-group i.circle, hr.e, div.call-action-box, .white.btn, .circle-button, .btn.circle-button, .pagination > .active > a:hover, .pagination > li.disabled > a:hover, .pagination > li > a:hover, .pagination > li > a:focus, .pagination > li > a, .pagination > li > span, .pagination > li.disabled > a, .nav.inner.ms-rounded li a, .btn-default, .btn, .bs-panel, .tag-row i.circle, .niche-box-team .content-box hr, .call-action-box, .pagination > .active > a, .accordion-list .list-group-item > a.active, .social-group i.circle, .social-button i.circle, .accordion-list .list-group-item > a.active:hover {
        border-color: rgb(149, 106, 45);
    }
	.list-texts-justified li b, .list-texts-justified li span {
		background:  !important;
	}

	.gradient-bg, .gradient-text {
		background-image: linear-gradient( 135deg, rgb(149, 106, 45) 0%, rgb(72, 12, 103) 100%);
	}
	@keyframes borderPulse {
  		0% {
    		box-shadow: inset 0px 0px 0px 5px [MAIN-COLOR-2],.4), 0px 0px 0px 0px [MAIN-COLOR-2],1);
  		}
  		100% {
    		box-shadow: inset 0px 0px 0px 3px [MAIN-COLOR-2],.2), 0px 0px 0px 15px [MAIN-COLOR-2],0);
  		}
}

	
.navbar-brand img,header .brand img { margin-top: 10px; }
.navbar.navbar-main{
  background-color:#f8f2eb;
}

#table-plans-home{
 width:100%;
}

#table-plans-home tbody tr:nth-child(1) td{
       border-top-width: 0px;
}

#table-plans-home tbody tr:nth-child(3) td{
       border-bottom-width: 0px;
}

#table-plans-home tbody tr td:nth-child(1){
       border-left-width: 0px;
}

#table-plans-home tbody tr td:nth-child(2){
       border-right-width: 0px;
}

#table-plans-home tbody tr td:nth-child(3){
       border-left-width: 0px;
       border-right-width: 0px;
}

#table-plans-home tbody tr td{
       border-color:#5D5D5D;
}

#table-plans-home tbody tr td:nth-child(2){
           font-weight: 500;
}

table#table-plans-price-home{
  width:100%;
}

table#table-plans-price-home thead tr th{
  border-top-width:0px;
}

table#table-plans-price-home thead tr th:nth-child(1){
  border-left-width:0px;
  border-right-width:0px;
}
table#table-plans-price-home thead tr th:nth-child(2){
  border-left-width:0px;
}

table#table-plans-price-home thead tr th:nth-child(4){
  border-right-width:0px;
}

table#table-plans-price-home thead tr:nth-child(1){
  border-bottom-width:0px;
  line-height: 0px;
}

table#table-plans-price-home thead tr:nth-child(1) th{
  border-bottom-width:0px;
}

table#table-plans-price-home thead tr:nth-child(1) th{
  heigth:10px;
}

table#table-plans-price-home thead tr:nth-child(2){
  height: 10px;
  border-top-width:0px;
  border-bottom-width:0px;
}

table#table-plans-price-home thead tr:nth-child(1) th:nth-child(2){
  border-right-width: 2px;
 border-right-color:#C58D3C;
}

table#table-plans-price-home thead tr:nth-child(1) th:nth-child(3){
  border-right-width: 2px;
 border-right-color:#C58D3C;
}

table#table-plans-price-home tbody tr:nth-child(1) td{
  border-top-width:0px;
}

table#table-plans-price-home tbody tr td{
  border-left-width:0px;
  border-right-width:0px;
}

table#table-plans-price-home tbody tr:nth-child(3) td{
  border-bottom-width:0px;
}

#boton_ver_planes a{
   color: #480C67 !important;
}

#boton_ver_planes a:hover{
   color: #FFFFFF !important;
   background-color:#480C67 !important;
}

.center_icon .icon-box-cell{
  display:table;
  margin:auto;
}

@media screen and (min-width: 10px) and (max-width:910px) {
  #seccion_detalle_planes_mutual{
        height:770px !important;
  }
  #seccion_beneficiarios_mutualidad{
       height:770px !important;
   }
}

@media screen and (min-width: 10px) and (max-width:770px) {
  #seccion_detalle_planes_mutual .col-md-12.hc_image_cnt img{
        margin:auto;
        display:table;
  }
}


.circle-button {
    border-radius: 5px;
}
input{
	height:30px;
	padding:0px;
}


.form-control {
    display: block;
    width: 100%;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 0.5;
    color: #495057;
    background-color: #fff;
    background-clip: padding-box;
    border: 1px solid #ced4da;
    border-radius: .25rem;
    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}

.cs_wht{
	color:white;
	font-weight:bold;
	font-size:2em;
}
</style>
<link rel='stylesheet' id='hc_css_custom-css'  href='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/custom/custom.css?ver=1.0' type='text/css' media='all' />
<link rel='stylesheet' id='google-font-css'  href='https://fonts.googleapis.com/css?family=Rubik%3A300%2C300i%2C400%2C400i%2C500%2C500i%2C700%2C700i%2C900%2C900i&#038;ver=1.0' type='text/css' media='all' />
<script type='text/javascript' src='https://mutualidad.mippya.com/wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>
<script type='text/javascript' src='https://mutualidad.mippya.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<link rel='https://api.w.org/' href='https://mutualidad.mippya.com/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="https://mutualidad.mippya.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="https://mutualidad.mippya.com/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 5.2.2" />
<link rel="canonical" href="https://mutualidad.mippya.com/" />
<link rel='shortlink' href='https://mutualidad.mippya.com/' />
<link rel="alternate" type="application/json+oembed" href="https://mutualidad.mippya.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fmutualidad.mippya.com%2F" />
<link rel="alternate" type="text/xml+oembed" href="https://mutualidad.mippya.com/wp-json/oembed/1.0/embed?url=https%3A%2F%2Fmutualidad.mippya.com%2F&#038;format=xml" />
		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		    </head>
    <body class="home page-template-default page page-id-273" data-spy="scroll" data-target="#hc-inner-menu" data-offset="200" >
        <div id="preloader"></div>
<div class="">

<header class="scroll-change fixed-top " >
    <div class="navbar navbar-default navbar-fixed-top " role="navigation">
                <div class="navbar navbar-main ">
            <div class="container">
                                <div class="navbar-header">
                    <button type="button" class="navbar-toggle">
                        <i class="scn-menu"></i>
                    </button>
                    <a class='navbar-brand' href='/'><img class='logo-default' src='https://mutualidad.mippya.com/wp-content/uploads/2019/05/Memorial-International-Logo-1.png' alt='' style='margin-top: 10px' /><img class='logo-retina' src='https://mutualidad.mippya.com/wp-content/uploads/2019/05/Memorial-International-Logo-1.png' alt='' style='margin-top: 10px' /></a>                </div>
                <div class="collapse navbar-collapse">
                                        <div class="nav navbar-nav navbar-right">
                        <ul id="hc-inner-menu" class="nav navbar-nav  one-page-menu" style="">
							<li class=" " >
								<a href="#seccion_detalles_servicio">Detalles del servicio</a>
							</li>
							<li class=" " >
								<a href="#seccion_detalle_planes">Planes</a></li>
							<li class=" " >
								<a href="#seccion_beneficiarios_mutualidad">Beneficiarios</a>
							</li>
							<li class=" " >
								<a href="/login">Ingresar</a>
							</li>							
						</ul>                                                                        
						<div class="btn-group navbar-social">
                            <div class="btn-group social-group">
                                <a target='_blank' rel='nofollow' href='https:&#x2F;&#x2F;www.facebook.com&#x2F;memorialint&#x2F;'><i class='fa fa-facebook'></i></a>
								<a target='_blank' rel='nofollow' href='https:&#x2F;&#x2F;www.instagram.com&#x2F;memorial_int&#x2F;'><i class='fa fa-instagram'></i></a>
								<a target='_blank' rel='nofollow' href='https:&#x2F;&#x2F;www.youtube.com&#x2F;channel&#x2F;UCMFeubDAEmxSEoxC-f-gnqw'><i class='fa fa-youtube'></i></a>
								<a target='_blank' rel='nofollow' href='https:&#x2F;&#x2F;www.linkedin.com&#x2F;company&#x2F;memorial-international&#x2F;'><i class='fa fa-linkedin'></i></a>                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
                    </div>
    </div>
	<h1>Memorial Ecuador</h1>
</header>
<!--class="section-item section-empty    " -->
<div id="seccion_detalle_planes"  class="section-item section-bg-image     parallax-window  "
    data-anima="fade-in"  data-timeline="asc"  data-bleed="0" data-parallax="scroll" data-natural-height="805" data-natural-width="1920" data-position="top" data-image-src="https://mutualidad.mippya.com/wp-content/uploads/2019/05/LANDING-PAGEHOME1.jpg"    style="min-height:710px;">
    
<!--<div id="seccion_detalle_planes"  class="section-item section-empty    "   style="">-->
		@yield('content')
</div>



<!--
<div id="section_5ZtkF" class="section-item section-bg-image     parallax-window  "
    data-anima="fade-in"  data-timeline="asc"  data-bleed="0" data-parallax="scroll" data-natural-height="805" data-natural-width="1920" data-position="top" data-image-src="https://mutualidad.mippya.com/wp-content/uploads/2019/05/LANDING-PAGEHOME1.jpg"    style="height:600px;">
    <div class="content container " style="">
		<div class="row ">
		
			<div id="column_BuOXv" class="hc_column_cnt col-md-8  anima white  " style="" >
		<div class="row"><div class="col-md-12 hc_space_cnt"><hr class="space m    "  />
	</div><div class="col-md-12 hc_title_tag_cnt"><h3 id="Gqkdv" class='   ' style=''>Memorial International en alianza</br>con la Mutualidad del Ejército y Aviación</h3></div><div class="col-md-12 hc_space_cnt"><hr class="space xs    " style=" height: 10px" />
	</div><div class="col-md-12 hc_title_tag_cnt"><h5 id="V3poZ" class='   ' style='font-weight: 100;'>Tienen el agrado de presentarle un nuevo servicio <br />  de asistencia funeraria para apoyarlo en el <br /> momento que más lo necesite.</h5></div><div class="col-md-12 hc_space_cnt"><hr class="space xs    "  />
	</div><div class="col-md-12 hc_button_cnt"><div id="boton_ver_planes" class="button-cnt    "
		style="display: block; color:#480C67;text-align:left">
		<a
			 class="btn  scroll-to " href="#seccion_detalle_planes">
			Ver Planes    </a>
		</div>
	</div></div></div>


	<div id="column_9Uc0H" class="hc_column_cnt col-md-12  anima  " style="" >
		<div class="row"><div class="col-md-12 hc_image_cnt"><img class=" col-center   " style="display:block;" src="https://mutualidad.mippya.com/wp-content/uploads/2019/05/LOGOS.png" alt="" /></div></div></div>
		</div>
	</div>
</div>



<div id="seccion_detalle_planes" class="section-item section-empty    "  style="">
    <div class="content container " style="">
    <div class="row ">
        <div id="5b1PZ" class="hc_column_cnt col-md-12   " style="" >
    <div class="row"><div class="col-md-12 hc_space_cnt"><hr class="space l    "  />
</div></div></div>
<div id="MSXJt" class="hc_column_cnt col-md-8  col-center  " style="" >
    <div class="row"><div class="col-md-12 hc_title_tag_cnt"><h3 id="2RazN" class=' text-center   ' style=''>Pensando en usted y su familia, Memorial International <br /> se ajusta a sus necesidades</h3></div></div></div>
<div id="5xNL6" class="hc_column_cnt col-md-12   " style="" >
    <div class="row"><div class="col-md-12 hc_space_cnt"><hr class="space xs    "  />
</div></div></div>
<div id="column_Q1bd4" class="hc_column_cnt col-md-8  col-center  " style="" >
    <div class="row">
	
	   @foreach($arrWebProduct as $webProduct)
		   <div id="column_JmteR" class="hc_column_cnt col-md-4   " style="" >
			<div class="row">
			   <div class="col-md-12 hc_niche_content_box_pricing_table_cnt"><div class="list-group pricing-table    " style="">
				 <div class="list-group-item pricing-price">{{$webProduct->pro_price}}</div><div class="list-group-item pricing-name"><h3>{{$webProduct->pro_name}}</h3></div> <div class="list-group-item">{{$webProduct->pro_description1}} </div> <div class="list-group-item">{{$webProduct->pro_description2}}</div><div class="list-group-item pricing-btn"><a class="btn circle-button" href="{{$webProduct->pro_linkweb}}">Contratar</a></div></div>
			   </div>
			 </div>
		   </div>
	   @endforeach
	<div id="column_95xMm" class="hc_column_cnt col-md-4   " style="" >
		<div class="row"><div class="col-md-12 hc_niche_content_box_pricing_table_cnt"><div class="list-group pricing-table    " style="">
		<div class="list-group-item pricing-price">$19.266 <span>Anual (-5%)</span></div><div class="list-group-item pricing-name"><h3>Plan Duo</h3></div> <div class="list-group-item">El titular de la cuenta afiliado  al Ejército o a la Fuerza Aérea y su cónyugue</div> <div class="list-group-item">5% de descuento anual</div><div class="list-group-item pricing-btn"><a class="btn circle-button" href="https://quote.mippya.com/form-mutualidad">Contratar</a></div></div>
	</div></div></div>
	<div id="column_h1V5G" class="hc_column_cnt col-md-4   " style="" >
		<div class="row"><div class="col-md-12 hc_niche_content_box_pricing_table_cnt"><div class="list-group pricing-table    " style="">
		<div class="list-group-item pricing-price">$30.666 <span>Anual (-5%)</span></div><div class="list-group-item pricing-name"><h3>Plan Familiar</h3></div> <div class="list-group-item">El titular de la cuenta afiliado al Ejército  o a la Fuerza Aérea , su cónyugue y tres (3) hijos</div> <div class="list-group-item">5% de descuento anual</div><div class="list-group-item pricing-btn"><a class="btn circle-button" href="https://quote.mippya.com/form-mutualidad">Contratar</a></div></div>
	</div></div></div>
	</div>
</div>
  
  </div>
</div>
</div>-->

<!--
<div id="seccion_test" class="section-item section-empty    "  style="display:none;">
    <div class="content container " style="">
    <div class="row ">
        <div id="column_YOOZZ" class="hc_column_cnt col-md-12   " style="" >
    <div class="row"><div class="col-md-12 hc_space_cnt"><hr class="space l    "  />
</div></div></div>
<div id="column_3mfEl" class="hc_column_cnt col-md-2   " style="" >
    <div class="row"></div></div>
<div id="column_zKSDF" class="hc_column_cnt col-md-8   " style="" >
    <div class="row"><div class="col-md-12 hc_title_tag_cnt"><h3 id="55Nlp" class=' text-center   ' style=''>Pensando en usted y su familia, Memorial International <br /> se ajusta a sus necesidades</h3></div></div></div>
<div id="column_cJWXG" class="hc_column_cnt col-md-2   " style="" >
    <div class="row"></div></div>
<div id="column_jt8W8" class="hc_column_cnt col-md-12   " style="" >
    <div class="row"><div class="col-md-12 hc_space_cnt"><hr class="space l    "  />
</div></div></div>
<div id="column_J3ALj" class="hc_column_cnt col-md-3   " style="" >
    <div class="row"></div></div>
<div id="column_HUTQQ" class="hc_column_cnt col-md-6   " style="" >
    <div class="row"><div class="col-md-12 hc_code_block_cnt"><div class="   " ><table id = "table-plans-home">
	<tbody>
		<tr>
			<td><i class="fa fa-user"></i></td>
			<td>Plan <br/> individual</td>
			<td></i>El titular de la cuenta afiliado <br/> al Ejército o a la Fuerza Aérea</td>
		</tr>
		<tr>
			<td><i class="fa fa-user-friends"></i></td>
			<td>Plan <br/> Duo</td>
			<td>El titular de la cuenta afiliado <br/> al Ejército o a la Fuerza Aérea y su cónyugue</td>
		</tr>
		<tr>
			<td><i class="fa fa-users"></td>
			<td>Plan <br/> Familiar</td>
			<td>El titular de la cuenta afiliado al Ejército <br/> o a la Fuerza Aérea , su cónyugue y  tres (3) hijos</td>
		</tr>
	</tbody>
</table></div></div></div></div>
<div id="column_Lqrci" class="hc_column_cnt col-md-3   " style="" >
    <div class="row"></div></div>
<div id="column_aTfIp" class="hc_column_cnt col-md-12   " style="" >
    <div class="row"><div class="col-md-12 hc_space_cnt"><hr class="space l    "  />
</div></div></div>
    </div>
</div>
</div>-->
<!--
<div id="seccion_detalle_planes_mutual"   style="height:600px;">
    <div class="content " style="">
    <div class="row ">
        <div id="column_97wlW" class="hc_column_cnt col-md-6   " style="" >
    <div class="row"></div></div>
<div id="column_tnD7C" class="hc_column_cnt col-md-6   " style="" >
    <div class="row"><div id="column_IJRf0" class="hc_column_cnt col-md-12   " style="" >
    <div class="row"><div class="col-md-12 hc_space_cnt"><hr class="space l    " style=" height: 150px" />
</div><div id="column_KPAgJ" class="hc_column_cnt col-md-12   " style="" >
    <div class="row"><div class="col-md-12 hc_title_tag_cnt"><h2 id="kbFMx" class=' white   ' style='font-weight: 400;'>Nos encargamos de todos los trámites</h2></div><div id="column_FtRAy" class="hc_column_cnt col-md-9   " style="" >
    <div class="row"><div class="col-md-12 hc_title_tag_cnt"><h3 id="lT7IV" class=' white   ' style='    font-weight: 100;'>para que pueda pasar el tiempo necesario con su familia</h3></div></div></div>
<div id="column_nAhMf" class="hc_column_cnt col-md-3   " style="" >
    <div class="row"></div></div>
</div></div>
<div id="column_ijw5q" class="hc_column_cnt col-md-9   " style="" >
    <div class="row"><div class="col-md-12 hc_space_cnt"><hr class="space m    "  />
</div></div></div>
<div id="column_8PqJf" class="hc_column_cnt col-md-3   " style="" >
    <div class="row"></div></div>
<div id="column_eHZ3y" class="hc_column_cnt col-md-8   " style="" >
    <div class="row"><div class="col-md-12 hc_title_tag_cnt"><h3 id="GhuZ1" class=' white   ' style=''><strong>Memorial Ecuador</strong> está con usted en el momento que mas lo necesita</h3></div></div></div>
<div id="column_Mbz0T" class="hc_column_cnt col-md-4   " style="" >
    <div class="row"></div></div>
</div></div>
</div></div>
    </div>
</div>
</div>-->

<div id="seccion_detalles_servicio" class="section-item section-empty  no-paddings-y   "  style="background-color:#F8F2EB;">
    <div class="content container " style="">
    <div class="row ">
        <div id="column_DiqOo" class="hc_column_cnt col-md-12   " style="" >
    <div class="row"><div id="column_vw9og" class="hc_column_cnt col-md-12   " style="" >
    <div class="row"><div class="col-md-12 hc_space_cnt"><hr class="space s    "  />
</div><div class="col-md-12 hc_wp_editor_cnt"><div id="OvYpZ" class="main-text wysiwyg-editor    " style="background-color:#f8f2eb">
    <p><span style="color: #000000;">Trámites y asesorías para la inhumación o cremación<span style="font-weight: 400;">.</span></span></p>
<p><span style="color: #000000;">El deudo y/o familia serán apoyados por Memorial International Chile, además de asignarle una funeraria de nuestra red según la localidad en que se encuentre y con disponibilidad las 24 horas del día, para la tramitación de los documentos que exige la ley para declarar el deceso, así como los demás trámites que se detallan a continuación:</span></p>
<ol>
<li style="font-weight: 400;"><span style="color: #000000;"><span style="font-weight: 400;">Asesoría para la obtención del certificado</span> <span style="font-weight: 400;">médico de defunción.</span></span></li>
<li style="font-weight: 400;"><span style="color: #000000;">Inscripción<span style="font-weight: 400;"> de la defunción en registro civil.</span></span></li>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #000000;">Obtención del pase de sepultación.</span></li>
<li style="font-weight: 400;"><span style="color: #000000;"><span style="font-weight: 400;">Asesoría de Memorial International Chile</span> <span style="font-weight: 400;">y coordinación para los trámites de la compra, arriendo o apertura de la sepultura o nicho en cementerio público, privado o parque cementerio según corresponda.</span></span></li>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #000000;">Asesoría del representante de la funeraria asignada, para la obtención del permiso para la cremación, manifiesto de voluntad y autorización judicial si corresponde.</span></li>
</ol>
<p><span style="color: #000000;">Sala de velación en la ciudad que se llevará a cabo el servicio de inhumación con un tope de 3 UF <span style="font-weight: 400;">Memorial International Chile</span> <span style="font-weight: 400;">proveerá una sala de velación, según disponibilidad en la red de prestadores:</span></span></p>
<ol>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #000000;">Lugar donde se lleva a cabo la ceremonia de velación del difunto.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #000000;">El servicio puede ser otorgado en un salón de la misma funeraria asignada o si la familia desea puede en una capilla o en el domicilio.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #000000;">El servicio será otorgado en la misma ciudad donde el difunto será sepultado o cremado.</span></li>
</ol>
<p><span style="color: #000000;">Servicio de preparación del cuerpo. <span style="font-weight: 400;">Memorial International Chile</span> <span style="font-weight: 400;">proveerá:</span></span></p>
<ol>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #000000;">Servicio de limpieza externa del cuerpo.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #000000;">Servicio de maquillaje neutralizador.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #000000;">Servicio de colocación de prendas de vestir.</span></li>
</ol>
</div>
</div></div></div>
</div></div>
    </div>
</div>
</div>
<div id="section_H4Kky" class="section-item section-empty  no-paddings-y   "  style="background-color:#f8f2eb">
    <div class="content container " style="">
    <div class="row ">
        <div id="column_cYTjF" class="hc_column_cnt col-md-12   " style="" >
    <div class="row"><div class="col-md-12 hc_wp_editor_cnt"><div id="eHWH4" class="main-text wysiwyg-editor    " style="">
    <p><span style="color: #000000;">Decoración de salas de velación y ataúd<span style="font-weight: 400;">. Memorial International Chile proveerá:</span></span></p>
<ol>
<li style="font-weight: 400;"><span style="color: #000000;"><span style="font-weight: 400;">Ataúd </span><b>&nbsp;</b><span style="font-weight: 400;">de madera multilaminado de corte lineal.</span></span></li>
<li style="font-weight: 400;"><span style="color: #000000;"><span style="font-weight: 400;">Decoración</span> <span style="font-weight: 400;">de salas de velación con un arreglo floral cubre urna.</span></span></li>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #000000;">Base de velación, considerando una cruz de madera o metal, cirios de madera o lámparas eléctricas, porta velones y un pedestal tarjetero.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #000000;">Un libro de condolencias que se facilita a los deudos y/o familia para que los asistentes al velatorio y funeral, puedan dejar escritas sus condolencias.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #000000;">50 tarjetas de agradecimiento a los asistentes a la ceremonia.</span></li>
</ol>
<p><span style="color: #000000;">Traslados a salas de velación y campo santo en carroza. <span style="font-weight: 400;">Memorial International Chile proveerá</span> <span style="font-weight: 400;">a través de su red, una carroza para el traslado del difunto.</span></span></p>
<ol>
<li style="font-weight: 400;"><span style="color: #000000;"><span style="font-weight: 400;">Servicio de traslado del difunto en un automóvil adecuado especialmente para poder transportar el ataúd con el cuerpo del difunto, acondicionado debidamente con todas las comodidades que se requieren para tal efecto</span><b>.</b></span></li>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #000000;">El traslado se realizará dentro de la misma ciudad donde se realizará la velación y posterior sepultación o cremación.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #000000;">No considera traslados fuera de la ciudad y para estos casos la funeraria les hará un 10 % de descuento en traslados terrestres.</span></li>
</ol>
<p><span style="font-weight: 400; color: #000000;">*Aplica para los tres (3) planes.</span></p>
</div>
</div><div class="col-md-12 hc_space_cnt"><hr class="space s    "  />
</div></div></div>
    </div>
</div>
</div>
<div id="seccion_beneficiarios_mutualidad" class="section-item section-bg-image  white    parallax-window  "
     data-bleed="0" data-parallax="scroll" data-natural-height="807" data-natural-width="1920" data-position="top" data-image-src="https://mutualidad.mippya.com/wp-content/uploads/2019/05/LANDING-PAGEBENEFICIARIOS.jpg"    style="height:600px;">
    <div class="content " style="">
    <div class="row ">
        <div id="column_9C4Lc" class="hc_column_cnt col-md-12   " style="" >
    <div class="row"><div class="col-md-12 hc_space_cnt"><hr class="space l    "  />
</div><div class="col-md-12 hc_space_cnt"><hr class="space l    "  />
</div></div></div>
<div id="column_XxxFh" class="hc_column_cnt col-md-6   " style="" >
    <div class="row"><div id="column_yFGAf" class="hc_column_cnt col-md-4   " style="" >
    <div class="row"></div></div>
<div id="column_mtzqP" class="hc_column_cnt col-md-8   " style="" >
    <div class="row"><div id="column_GcRei" class="hc_column_cnt col-md-10   " style="" >
    <div class="row"><div class="col-md-12 hc_title_tag_cnt"><h3 id="NPma4" class='   ' style=''>Este servicio está dirigido a cualquier persona:</h3></div><div class="col-md-12 hc_title_tag_cnt"><h4 id="DHTzt" class='   ' style=''>1. Activo <br /> 2. En condición de retiro <br /></h4></div></div></div>
<div id="column_lvoQ6" class="hc_column_cnt col-md-10   " style="" >
    <div class="row"><div id="27Mxv" class="hc_column_cnt col-md-12   " style="" >
    <div class="row"><div class="col-md-12 hc_title_tag_cnt"><h4 id="lgjsr" class=' white   ' style=''>Le ofrecemos el Servicio Funerario de Memorial Ecuador, porque sabemos que su tranquilidad y la de su familia es lo más importante. </h4></div></div></div>
</div></div>
</div></div>
</div></div>
<div id="seccion_beneficiarios" class="hc_column_cnt col-md-6   " style="" >
    <div class="row"><div class="col-md-12 hc_space_cnt"><hr class="space l    "  />
</div><div class="col-md-12 hc_space_cnt"><hr class="space xs    "  />
</div><div class="col-md-12 hc_image_cnt"><img class=" col-center middle-content   " style="display:block;width: 140px;" src="https://mutualidad.mippya.com/wp-content/uploads/2019/05/LOGO-MUTUALIDAD.png" alt="" /></div></div></div>
    </div>
</div>
</div>
<div id="section_cYPdY" class="section-item section-empty    "  style="background-color:#F8F2EB;">
    <div class="content " style="">
    <div class="row ">
        <div id="Bo8NK" class="hc_column_cnt col-md-12   " style="" >
    <div class="row"><div class="col-md-12 hc_space_cnt"><hr class="space m    "  />
</div></div></div>
<div id="column_qLylI" class="hc_column_cnt col-md-12   " style="" >
    <div class="row"><div id="oJ0R7" class="hc_column_cnt col-md-6  col-center  " style="" >
    <div class="row"><div class="col-md-12 hc_wp_editor_cnt"><div id="aN7BS" class="main-text wysiwyg-editor  col-center   " style="">
    <p style="text-align: center;"><span style="color: #c58d3c;"><b>Condiciones generales:</b></span></p>
<ol style="color: #C58D3C;">
<li style="font-weight: 400;"><span style="font-weight: 400; color: #c58d3c;">La edad de incorporación será a partir de los 18 años hasta los 74 años y 364 días.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #c58d3c;">La edad de permanencia será hasta que se lleve a cabo el siniestro.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #c58d3c;">Los hijos estarán cubiertos desde un año de edad hasta los 24 años con 364 días.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #c58d3c;">Suicidio estará cubierto después del primer año contratado.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #c58d3c;">Enfermedades preexistentes se encuentran cubiertas, posterior a 90 días, contados desde el inicio de la cobertura.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #c58d3c;">Muerte por enfermedad: carencia de 90 días, contados desde el inicio de la cobertura.</span></li>
<li style="font-weight: 400;"><span style="font-weight: 400; color: #c58d3c;">Se excluyen: Catástrofes naturales, guerra, terrorismo, sedición, motín, huelga, asonada, declarados o no.</span></li>
</ol>
</div>
</div></div></div>
</div></div>
<div id="dYG9L" class="hc_column_cnt col-md-12   " style="" >
    <div class="row"><div class="col-md-12 hc_space_cnt"><hr class="space m    "  />
</div><div class="col-md-12 hc_space_cnt"><hr class="space m    "  />
</div></div></div>
<div id="column_bUr36" class="hc_column_cnt col-md-12   " style="" >
    <div class="row"><div class="col-md-12 hc_code_block_cnt"><div class="  center_icon " ><a href = "https://quote.mippya.com/form-mutualidad"><div class="icon-box-cell"><i class="text-xxl fa fa-shopping-bag "></i></div></a></div></div><div id="column_7tmAH" class="hc_column_cnt col-md-12  col-center  " style="" >
    <div class="row"><div class="col-md-12 hc_title_tag_cnt"><h3 id="lSJEr" class=' text-center   ' style='color:black;text-decoration: underline;   text-decoration-color: #C58D3C;'>Contratar</h3></div><div id="column_shvMg" class="hc_column_cnt col-md-12   " style="" >
    <div class="row"><div class="col-md-12 hc_space_cnt"><hr class="space l    "  />
</div></div></div>
<div class="col-md-12 hc_title_tag_cnt"><h3 id="CjTwZ" class=' text-center   ' style='color:black;'>La prevención le asegura tranquilidad en el momento que más lo necesita.</h3></div><div class="col-md-12 hc_title_tag_cnt"><h3 id="KypqK" class=' text-center   ' style='color:#C58D3C;'>Adquiera el Plan de Asistencia Funeraria con Memorial International</h3></div></div></div>
</div></div>
    </div>
</div>
</div>




<footer class="default-wp-footer">
    <div class="content">
        <div class="container">
            <div style = "text-align: center;">Copyright Memorial Ecuador. All Rights Reserved. Powered by <a href = "https://memorialtechnologies.com" target = "_blank" style = "color:blue;">MTC</a></div>
        </div>
    </div>
</footer>   
<?
} else { ?>
<!-- <footer class="default-wp-footer">
    <div class="content">
        <div class="container">
            <div style = "text-align: center;">Copyright Memorial International. All Rights Reserved. Powered by <a href = "https://memorialtechnologies.com" style = "color:blue;">MTC</a></div>
        </div>
    </div>
</footer>     -->
<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            call: "+56800719090", // Call phone number
            call_to_action: "LLamanos", // Call to action
            position: "right", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
	
	
	function cargarMenu(l_url){
		$("#seccion_detalle_planes").load(l_url);
	}
</script>
<!-- /WhatsHelp.io widget --><script type="text/javascript" id="sns_scripts">jQuery(function() {
    jQuery("#hc-inner-menu li").on("click",function(){
		if(jQuery("button.navbar-toggle:visible").length > 0){
			jQuery("button.navbar-toggle:visible").click();
		}
	});
});</script><script type='text/javascript' src='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/scripts/script.js?ver=1.0'></script>
<script type='text/javascript'>
 var ajax_url = 'https://mutualidad.mippya.com/wp-admin/admin-ajax.php';
</script>
<script type='text/javascript' src='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/scripts/bootstrap/js/bootstrap.min.js?ver=1.0'></script>
<script type='text/javascript' src='https://mutualidad.mippya.com/wp-includes/js/imagesloaded.min.js?ver=3.2.0'></script>
<script type='text/javascript' src='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/custom/custom.js?ver=1.0'></script>
<script type='text/javascript' src='https://mutualidad.mippya.com/wp-content/plugins/hybrid-composer/scripts/parallax.min.js?ver=1.0'></script>
<script type='text/javascript' src='https://mutualidad.mippya.com/wp-includes/js/wp-embed.min.js?ver=5.2.2'></script>
</body>
</html>

<!--
Performance optimized by W3 Total Cache. Learn more: https://www.w3-edge.com/products/


Served from: mutualidad.mippya.com @ 2019-07-24 21:32:24 by W3 Total Cache
-->