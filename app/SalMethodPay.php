<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalMethodPay extends Model{
    //
	protected $table = 'sal_methodpays';
	protected $primaryKey = 'smp_id';
}
