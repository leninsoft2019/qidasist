<?php
//@extends('layouts.app')

?>
@extends('layouts.home_quoting_template')
@section('content')
<div class="container" id="ct_register">
    <style scoped>
		.card2{
			width:50%;
			border:solid 1px;
			border-radius:5px;
			margin-top:20px;
			margin-bottom:20px;
			padding:20px;
			-webkit-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
			-moz-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
			box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
			background-color:white;
			color:black;
			color:black;
			margin: 0;
			position: absolute;
			top: 50%;
			left:25%;
			-ms-transform: translateY(-50%);
			transform: translateY(-50%);	
			margin:auto;
			
		}
		
		.col-form-label{
			color:black;
		}
		
		#ct_register{
			width:50%;
			border:solid 1px;
			border-radius:5px;
			margin-top:20px;
			margin-bottom:20px;
			padding:20px;
			-webkit-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
			-moz-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
			box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
			background-color:white;
			color:black;
			color:black;
			margin: 0;
			position: absolute;
			top: 50%;
			left:25%;
			-ms-transform: translateY(-50%);
			transform: translateY(-50%);	
			margin:auto;
			
		}
		#cl_reg{
			width:100%;
		}
	</style>
    <div class="row justify-content-center" style="width:100%;">
        <div class="col-md-8" id="cl_reg">
            <div class="card">
                <div class="card-header">{{ __('Registrar') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Nombre Usuario') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
						<div class="form-group row">
                            <label for="identificator" class="col-md-4 col-form-label text-md-right">{{ __('Identificación') }}</label>

                            <div class="col-md-6">
                                <input id="user_id" type="text" class="form-control @error('user_id') is-invalid @enderror" name="user_id" value="{{ old('user_id') }}" required />

                                @error('user_id')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Clave') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirmar Clave') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
