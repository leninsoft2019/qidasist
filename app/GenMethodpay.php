<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GenMethodpay extends Model{
    //
	protected $table = 'gen_methodpay';
	protected $primaryKey = 'pmt_code';
}
