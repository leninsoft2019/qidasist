
	<?php

		$cmb_typedocument         =  $arr_combos['cmb_typedocument'];
		$cmb_banco                 =  $arr_combos['cmb_banco'];
	
	?>
	<section>




	<div class="container1">
	<div class="items" id='content_form'>


						<div class="contenedor_mt">

						<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
						<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

						<style scoped>
						/*
						.contenedor_mt {
						    padding: 10px;
						    width: 98%;
						    margin: auto;
						    background-color: #F3EADD;
						}*/
						#frmMT{
							width:90%;
							margin:auto;
							
							border:solid 1px  rgb(56, 30, 61,0.9);
							/*border-radius:5px;*/
							margin-top:20px;
							margin-bottom:20px;
							padding:20px;
							/*-webkit-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
							-moz-box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);
							box-shadow: -1px 0px 5px 0px rgba(0,0,0,0.75);*/
						 background-color:white;
							color:white;
						}
						.row {
						    display: -webkit-box;
						    display: -ms-flexbox;
						    
							width:100%;
						    -ms-flex-wrap: wrap;
						    flex-wrap: wrap;
						    margin-right: -15px;
						    margin-left: -15px;
						}

						.row_n{
							display:table;
							width:100%;
						}

						.tbl{
							display:table;
							width:100%;	
							
						}
						.rw{
							display:table-row;
							width:100%;
						}

						.col{
						    position: relative;
						    width: 50%;
						    min-height: 1px;
						    padding-right: 5px;
						    padding-left: 5px;
							
							display:table-cell;
						}


						.col2{
						    position: relative;
						    width: 50%;
						    min-height: 1px;
						    padding-right: 15px;
						    padding-left: 15px;
							
							display:table-cell;
						}

						.form-control {
						    display: block;
						    width: 100%;
						    padding: .375rem .75rem;
						    font-size: 1rem;
						    line-height: 1.5;
						    color: #495057;
						    background-color: #fff;
						    background-clip: padding-box;
						    border: 1px solid #ced4da;
						    border-radius: .25rem;
						    transition: border-color .15s ease-in-out,box-shadow .15s ease-in-out;
						}


						.form-control {
						    padding: 2px;
						    height: 28px;
						    border: 1px solid #dddddd;
						}

						h2{
							font-family: "Rubik",Helvetica,Arial,sans-serif;
							font-size:2em;
							font-weight:500;
						}

						.col_slim{
							width:10%;
						}

						.col_add{
							width:25%;
						}

						.col_minus{
							width:25%;
						}

						.col_range{
							width:25%;
						}

						.col_50{
							display:table-cell;
							width:50%;
						}
						.col_cont{
							border:solid 1px;
							border-radius:5px;
						}
						
						.col_90{
							display:table-cell;
							width:90%;
						}


						label{
							color:rgb(56, 30, 61,0.9);
						}

						.cs_footer_title{
							color:rgb(56, 30, 61,0.9);
							font-size:2em;
							font-weight:bold;
						}

						.cs_footer_text{
							color:black;
						}

						#footer_form{
							background-color:#E8DDF6;
							padding:20px;
							width:100%
						}



						.tbl_2{
							display:table;
							padding:20px;
							width:80%;
						    margin:auto;	
							
						}



						.cs_head_title{
							color: black;
							font-weight: bold;
						}


						@media (max-width: 767px) {

							.text-xxl {
								font-size: 50px !important;
								line-height: 60px !important;
							}

						h1{
								 text-indent:-9999px;
								font-size:0px;
								position:absolute;
							}

							h2, .text-l {
								margin-top:20px;
								font-size: 1.5em !important;
								line-height: 1em !important;
								
							}

							h3 {
								font-size: 18px !important;
								line-height: 28px !important;
							}
							
							
							#frmMT{
								
								width:98%;
								maring:auto;
								padding:5px;
							}
							
							.row{
										display:table;
								width:100%;
							}
							.col{
								display:table-row;
								width:100%;
							}


						.contenedor_mt {
						    padding: 0px;
						    width: 100%;

						}


						.res_input{
							width:40px;
							height:20px;
						}

						.cab_r{
							padding-left:20px;
						}

						.col_add{
							width:25%;
						}

						.col_minus{
							width:25%;
						}

						.col_range{
							width:25%;
						}


						}
						
						
						#dv_terms_cond{
							display:none;
						}
						
						.cs_link_dash_2{
						   color:#956A2D;
							font-weight:bold;	
                           text-decoration:underline;							
						}
						</style>


							<center><h3><span class="cs_head_title">Pago de primera cuota</span></h3></center>

							<form action="/action_page.php" id="frmMT">
								{{ csrf_field() }}	

							<span class="cs_head_title">Producto: {{$arrWebProduct->pro_name}}
							 <br>Cuota mensual a pagar: {{$arrWebProduct->pro_price}}
							 </span>
							 <hr></hr>
								<!-- Titular -->
						<div class="tbl">		
							<div class="row cab_r rw">
							  <div class="col col_50">
										<div class="tbl">
										<div class="row cab_r rw">
											<div class="col col_slim">
												<div class="form-group">
													<input type="radio" class="form-control" id="paysel"  name="paysel" value="1" />
												</div>
											</div>
											<div class="col col_90">
												<div class="form-group">
													<img src="img/p2p/Logo_PlacetoPay.png" width="300px" height="60px;" alt="place to pay" />
												</div>
											</div>
										</div>
														
                                        @guest
										@else										 
										<div class="row cab_r rw">
											<div class="col col_slim">
												<div class="form-group">
													<input type="radio" class="form-control" id="paysel"  name="paysel" value="2" />
												</div>
											</div>
											<div class="col col_90">
												<div class="form-group">
													<span class="cs_head_title">Debito Bancario</span>
												</div>
											</div>
										</div>

										<!--<div class="row cab_r rw">
											<div class="col col_slim">
												<div class="form-group">
													<input type="radio" class="form-control" id="paysel"  name="paysel" value="3" />
												</div>
											</div>
											<div class="col ">
												<div class="form-group">
													<span class="cs_head_title">Efectivo</span>
												</div>
											</div>
										</div>-->

										<div class="row cab_r rw">
											<div class="col col_slim">
												<div class="form-group">
													<input type="radio" class="form-control" id="paysel"  name="paysel" value="4" />
												</div>
											</div>
											<div class="col col_90">
												<div class="form-group">
													<span class="cs_head_title">Transferencia</span>
												</div>
											</div>
										</div>	
										<div class="row cab_r rw">
											<div class="col col_slim">
												<div class="form-group">
													<input type="radio" class="form-control" id="paysel"  name="paysel" value="5" />
												</div>
											</div>
											<div class="col col_90">
												<div class="form-group">
													<span class="cs_head_title">Tarjeta de Credito</span>
												</div>
											</div>
										</div>											
                                        @endguest


										<div class="row cab_r rw" id="dv_terms_cond">
										   
																			
													   
												<div class="col col_slim">
													<div class="form-group">
														<input type="checkbox" class="form-control" id="terminos" style="min-width: 10px" />
													</div>
												</div>
												<div class="col col_90">
													<div class="form-group">
														<a href="javascript:showTerms();" class="cs_link_dash_2">Aceptar Terminos y Condiciones</a>
														<br>
														<img src="/img/p2p/mastercard.png" width="70px" height="60px" alt="LOGO MASTERCARD" />
														<br>
														<img src="/img/p2p/logosEC.png" width="500px" height="60px" alt="logos VISA, DINNERS CLUB" />

														
													</div>
											
											     </div>
										</div>		
														
										
										</div>
								
							  </div>	
							  
							    <div class="col col_50 col_cont">
										<h3><span class="cs_footer_text">Datos de Factura</span></h3>
										<div class="tbl">
											
											
											<div class="row cab_r rw">
												<div class="col col_50">
													<div class="form-group">
														<label for="id">Copiar datos de Titular:</label>
														<input type="checkbox" class="form-control" id="chkCopia"  name="chkCopia"  />
													</div>
												</div>
												<div class="col col_50">
													<div class="form-group">
														<label for="email">Correo Electrónico:</label>
														<input type="email" class="form-control" id="email"  name="email" />
													</div>
												</div>
											</div>		
											<div class="row cab_r rw">
												<div class="col col_50">
													<div class="form-group">
														<label for="nombre">Nombre:</label>
														<input type="text" class="form-control" id="nombre" name="nombre" />
													</div>
												</div>
												<div class="col col_50">
													<div class="form-group">
														<label for="apellido">Apellido:</label>
														<input type="text" class="form-control" id="apellido"  name="apellido" />
													</div>
												</div>
											</div>					
											<div class="row cab_r rw">
												<div class="col col_50">
													<div class="form-group">
														<label for="id">Tipo Identifaci&oacute;n:</label>
														<select id="tipo_id" name="tipo_id" class="form-control">
														    @foreach($cmb_typedocument as $rw)
																<option value="{{$rw->tdo_code}}">{{$rw->tdo_name}}</option>
															@endforeach
															
														</select>
													</div>
												</div>
												<div class="col col_50">
													<div class="form-group">
														<label for="id">Identifaci&oacute;n:</label>
														<input type="text" class="form-control" id="id" name="id" />
													</div>
												</div>
											</div>
											
											<div class="row cab_r rw">
												<div class="col col_50">
													<div class="form-group">
														<label for="id">Celular:</label>
														<input type="text" class="form-control" id="phone" name="phone" />
													</div>
												</div>
												<div class="col col_50">
													<div class="form-group">
														<label for="email">Direcci&oacute;n:</label>
														<input type="text" class="form-control" id="direccion" name="direccion" />
													</div>
												</div>
											</div>
											
											<div class="row cab_r rw">
												<div class="col col_50">
													<div class="form-group">
														<button id="btnRegresar"  class="btn btn-default">Regresar</button>
													</div>
												</div>
												<div class="col col_50">
													<div class="form-group">
														<button id="btnNextFinal" class="btn btn-default">Continuar</button>
													</div>
												</div>
											</div>
										</div>
							  </div> 
						    </div>
						  </div>	
							</form>
							<br>


							
						</div>



	</div>
	</div>
	</section>

	@include('sales.includes.modalP2P');
    @include('sales.includes.modalTerms');
	@include('sales.includes.modalDebit');
	@include('sales.includes.modalTransf');
	@include('sales.includes.modalFormCreditCard');	
	

<script>
	function recalcula(){
		var tot_lines = $("#tot_lines").val();
		var precio = $("#precio").val();
		precio = parseFloat(precio);
		var qty_line = quantity = 0;
		var tot_price = 0;
		for (var i = 0; i < tot_lines; i++) {
			qty_line = $("#quantity"+i).val();
			quantity += parseInt(qty_line);
			//alert(qty_line+" - "+quantity);
		}
		quantity = parseFloat(quantity * precio);
		//alert(quantity);
		$("#totales").val(quantity.toFixed(2));
		
	} // Fin recalcula

	function add(linea){
		var quantity = $("#quantity"+linea).val();
		quantity++;
		$("#quantity"+linea).val(quantity);
		recalcula();
	} // Fin add
	
	function minus(linea){
		var quantity = $("#quantity"+linea).val();
		quantity = quantity - 1;
		$("#quantity"+linea).val(quantity);
		recalcula();
	} // Fin add
	
	function recuperarDatosFactura(){
				event.preventDefault();
				
				var request = $.ajax({
					type:	'GET',
					url:	'/payment/get_data_client',
					success:function(data){
						console.log(data);
						if(data.success==1){
							
							$('#nombre').val(data.data_person.nombre);
							$('#apellido').val(data.data_person.apellido);
							$('#id').val(data.data_person.id);
							$('#email').val(data.data_person.email);
							$('#direccion').val(data.data_person.direccion);
							$('#phone').val(data.data_person.phone);
							
							return;
						}
					}
				});
				request.fail(function( jqXHR, textStatus ) {
					alert( "Hubo un problema: " + textStatus );
					console.log(jqXHR);
				});				
	}


	function validarPlaceToPay(){

	    var form = $('#frmMT');
		var request = $.ajax({
			type:	'POST',
			url:	'/payment/place_to_pay_val',
			data:	form.serialize(),
			success:function(data){
				console.log(data);
				if(data.success==1){
						/*$.ajax({
						  url: data.url_process.processUrl,
						  headers: {  'Access-Control-Allow-Origin': '*' },
						  type:'GET',
						   beforeSend: function(xhr){xhr.setRequestHeader('Access-Control-Allow-Origin', '*');},     
						  }).done(function(data) {
						     $('#body_mod_p2p').html(data);
						});*/

							P.init(data.url_process.processUrl);
						  var object =     '<object type="text/html" width="100%" height="900px" id="frame_id" data="'+data.url_process.processUrl+'">'+
        '              <p>Si puede leer este mensaje es que su navegador no soporta correctamente el elemento <code>object</code></p>'+
        '           </object>';

								/*$('#body_mod_p2p').empty();
              

			              $(object).appendTo('#body_mod_p2p').ready(function(){

			                  });*/

						/*$('#body_mod_p2p').load(data.url_process.processUrl);	*/
						
						
						/*$('#myModalP2P').modal({backdrop: 'static', keyboard: false})  
						$('#myModalP2P').modal('show');*/
										
					return;
				}else{

					alertPPYA(data.message);
					console.log(data);
					return;
				}
			}
		});
		request.fail(function( jqXHR, textStatus ) {
			alertPPYA( "Hubo un error: " + textStatus );
			console.log(jqXHR);
		});
    }

	


	function showTerms(){
		console.log('showing terms');
			$('#myModalTerms').modal({backdrop: 'static', keyboard: false})  
			$('#myModalTerms').modal('show');
	}
	

	
	
	$(function () {
		

						
				$.validator.addMethod("character", validateCharacter);
				$.validator.addMethod("numbers", validateNumbers);
				
				jQuery.validator.addMethod("accept", function(value, element, param) {
				  return value.match(new RegExp("." + param + "$"));
				});
				$('#frmMT').validate({
					rules: {
						nombre: { 
							required: true,
							character:true,
							minlength: 4,
                             accept: "[a-zA-Z]+" 
						},
						apellido: { 
							required: true,
							character:true,
							minlength: 4,
                             accept: "[a-zA-Z]+" 
						},
						id: { 
							required: true,
							character:true
						},
                        phone: {
							required: true
						}						
					},
					messages: {
						nombre: {
							required: 'Se requiere este campo.',
							character: 'No se aceptan caracteres especiales.',
							minlength: 'Debe ingresar al menos 4 caracteres',
							accept: 'No se admiten numeros'
						},
						apellido: {
							required: 'Se requiere este campo.',
							character: 'No se aceptan caracteres especiales.',
							minlength: 'Debe ingresar al menos 4 caracteres',
							accept: 'No se admiten numeros'							
						},
						id: {
							required: 'Se requiere este campo.',
							character: 'No se aceptan caracteres especiales.'
						},
						phone: {
							required: 'Se requiere este campo.'
							
						}					
					},
					onfocusout: false,
					errorElement: 'div',
					invalidHandler: function(form, validator) {
						var errors = validator.numberOfInvalids();
						if (errors) {                    
							validator.errorList[0].element.focus();
						}
					},
					highlight: function (element) { 
						$(element)
						.closest('.form-group')
						.addClass('has-error'); 
					},
					errorPlacement: function(error, element){
						error.appendTo(element.parent());
					},
					success: function (element) {
						element
						.closest('.form-group')
						.removeClass('has-error');
						element.remove();
					},
					submitHandler: function(form){
						
					}
			});
		
		
		
            run_waitMe(current_effect);

			P.on('response', function(data) {
				console.log(data);
				@if(Session::has('idCol'))
					if(data.status.status=="APPROVED"){
						alertPPYA('Su pago con referencia '+data.reference+' , ha sido Aprobado');
						var msg='Su pago con referencia '+data.reference+' , ha sido Aprobado';
						paymentCol(msg);
					}else if(data.status.status=="PENDING"){
						/*alertPPYA('Su peticion esta en estado Pendiente, por favor vuelva a tratar de nuevo en 5 minutos');*/
						alertPPYA('Su pago con referencia '+data.reference+' , esta PENDIENTE, para poder proseguir con el proceso intente de nuevo');
						return;
					}else{
						/*alertPPYA('Su peticion ha sido rechazada, por favor ingrese otra tarjeta o seleccione un metodo de pago diferente');*/
						alertPPYA('Su pago con referencia '+data.reference +' , ha sido RECHAZADO');
						return;
					}						
				
			    @else
					if(data.status.status=="APPROVED"){
						alertPPYA('Su subscripcion con referencia '+data.reference+' , ha sido Aprobada');
						var msg='Su subscripcion con referencia '+data.reference+' , ha sido Aprobada';
						firstPaymentP2P(msg);
					}else if(data.status.status=="PENDING"){
						/*alertPPYA('Su peticion esta en estado Pendiente, por favor vuelva a tratar de nuevo en 5 minutos');*/
						alertPPYA('Su subscripcion con referencia '+data.reference+' , esta PENDIENTE, para poder proseguir con el proceso intente de nuevo');
						return;
					}else{
						/*alertPPYA('Su peticion ha sido rechazada, por favor ingrese otra tarjeta o seleccione un metodo de pago diferente');*/
						alertPPYA('Su subscripcion con referencia '+data.reference +' , ha sido RECHAZADA');
						return;
					}				
				@endif

				/*$("#lightbox-response").html(JSON.stringify(data, null, 2));*/
			});
			$("#btnRegresar").click(function( event ) {
				event.preventDefault();
				$('#seccion_detalle_planes').load('/sales/previous_page/2');		
				
			});
			
			$("input:radio[name ='paysel']").click(function( event ) {
				
				console.log('paysel');
				var rdSel  = $("input:radio[name ='paysel']:checked").val();
				console.log(rdSel);
				if(rdSel==1){
					
					$("#dv_terms_cond").show();	
					$("#dv_terms_cond").prop('style',"display:table-row");
				
					
				}else{
					
					$("#terminos").prop('checked', false);
					$("#dv_terms_cond").hide();
				}
				
				
			});			
			
			$("#btnNextFinal").click(function( event ) {
				event.preventDefault();

				var rdSel  = $("input:radio[name ='paysel']:checked").val();
				if(rdSel==1){
					if(!$("#terminos").is(':checked')){
						alert('Es necesario aceptar los terminos y condiciones para continuar');
						return;
					}	

					if (!$("#frmMT").valid()) {
							return;
					}						
					validarPlaceToPay();
				}else if(rdSel==2){
				    $('#myModalDebit').modal({backdrop: 'static', keyboard: false});  
					$('#myModalDebit').modal('show');
				}else if(rdSel==4){
				    $('#myModalTransf').modal({backdrop: 'static', keyboard: false});  
					$('#myModalTransf').modal('show');
				}else if(rdSel==5){
				    $('#myModalCreditC').modal({backdrop: 'static', keyboard: false});  
					$('#myModalCreditC').modal('show');
				}
			
				/*cargarMenu('/payment/dependent_last');*/
				
				
			});
			
			
			$("#chkCopia").change(function( event ) {
				if($("#chkCopia").is(':checked')){
					recuperarDatosFactura();
				}
			
				
			});
			
			
		
	
	});
</script>