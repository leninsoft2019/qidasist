
	<!-- Leninsoft, 2019-09-03: Botones para el modulo de Cientes -->
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.colVis.min.js"></script>

    <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" />
    <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.dataTables.min.css" />
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css" />
    <link href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css" />
    <link href="https://cdn.datatables.net/buttons/1.5.6/css/buttons.bootstrap4.min.css" />
<?php
/*
usuario test@tes.com
clave	123456789
tarjeta	4381080000000003
cedula	1709331589
*/
?>
<?php
/*@extends('client.home_quoting_template')

@section('content')
*/
?>
	<style scoped>
		#clientData{
			color: black;
			width:90%;
			background:white;
			margin:auto;
			border:solid 1px #f2f4f4;
			border-radius:5px;
			margin-top:30px;
			padding:20px;
		}
		#clientData table{
			color: black;
		}
		ul>li{
			padding-left:10px;
		}
		
		h3{
			color:black;
			font-weight:bold;
			padding-top:20px;
		}
		
		.buttons-html5{
		   background:#f2f4f4;
           color:black;
			border:solid 1px #9C9C9C;	
		   padding-left:10px;
		   margin-left:10px;
		   
			text-align:center;
			
			display: inline-block;
			*display: inline;
			*zoom: 1;
			padding: 4px 12px;
			margin-bottom: 0;
			font-size: 14px;
			line-height: 20px;
			text-align: center;
			vertical-align: middle;
			cursor: pointer;
			color: #333333;
			text-shadow: 0 1px 1px rgba(255, 255, 255, 0.75);
			background-color: #f5f5f5;
			background-image: -moz-linear-gradient(top, #ffffff, #e6e6e6);
			background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ffffff), to(#e6e6e6));
			background-image: -webkit-linear-gradient(top, #ffffff, #e6e6e6);
			background-image: -o-linear-gradient(top, #ffffff, #e6e6e6);
			background-image: linear-gradient(to bottom, #ffffff, #e6e6e6);
			background-repeat: repeat-x;
			filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffffffff', endColorstr='#ffe6e6e6', GradientType=0);
			border-color: #e6e6e6 #e6e6e6 #bfbfbf;
			border-color: rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.1) rgba(0, 0, 0, 0.25);
			*background-color: #e6e6e6;
			filter: progid:DXImageTransform.Microsoft.gradient(enabled = false);
			border: 1px solid #cccccc;
			*border: 0;
			border-bottom-color: #b3b3b3;
			-webkit-border-radius: 4px;
			-moz-border-radius: 4px;
			border-radius: 4px;
			*margin-left: .3em;
			-webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
			-moz-box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);
			box-shadow: inset 0 1px 0 rgba(255,255,255,.2), 0 1px 2px rgba(0,0,0,.05);			
			
		}
		
		.paginate_button page-item previous disabled{
			color:white;
		}
		
		.paginate_button{
			color:white;
			padding:3px;
			padding-left:0px;
			font-size:small;
		}
		
		.pagination > li.disabled > a {
			background-color: #9C9C9C;
			color: white;
		}
		
		#IdDT_filter{
			float:right;
			position:relative;
		}
		
		.nav-item{
			
			background-color: #f2f2f2;
            border:solid 1px #9C9C9C;
		}
		
		#IdDT_filter>label{
			display:flex;
		}
		
		.form-control-sm{
				height:25px;
		}
		
	
		.sel_row{
			background-color:rgb(227, 192, 66) !important;
		}
		

	</style>
	
	
	<!--modal -->
	<div class="modal" id="mdConfirmDataCol" tabindex="-1" role="dialog">
	  <style scoped>
	    .modal-dialog {
			max-width:600px;
			color:black;
		}
		
		#alert_modal_cf{
			color:black;
			font-weight:bold;
			/*min-height:120px;*/
			/*width:90%;*/
		}
		
		.form-group{
			padding:10px;
		}
	  </style>
	  <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title">Detalle de Columbario</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
			
			
		  </div>
		  <div class="modal-body" id="body_col">
		    <div class="alert alert-danger" role="alert" id="alert_modal_cf">
			  
			</div>
			
			
			<br>

				  
				  
			<div class="form-group">
			   <label for="txt_cod_sel">Columbario Seleccionado:</label> 
							  <input type="text" name="txt_cod_sel" id="txt_cod_sel" readonly class="form-control" />
			  </div>
			  <div class="form-group">
						<label for="txt_val_sel">Valor:</label>
							  <input type="text" name="txt_val_sel" id="txt_val_sel" value="{{$precio}}"  class="form-control" />
							  <input type="hidden" name="txt_val_sel_min" id="txt_val_sel_min" value="{{$precio_min}}" />
			  </div>				  
				  
		  </div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-primary" id="btnSaveCol">Guardar</button>
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
		  </div>
		</div>
	  </div>
	</div>	
	<!--modal -->
	<div id="clientData">
		<header>
			<nav class="navbar navbar-expand-lg navbar-light bg-light">
			
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<center><h3>Columbariums, {{$objlabels->lbl_strPiso}}: <?php echo $cmbFloors; ?></h3></center>
			</nav>
		</header>
		<article>

			<section style="width:100%; margin:auto; padding:0px;" >
			    <br/>
				<center><button id="btnComprar" class="btn">Comprar</button></center><br/>
				<div id="showResult"><?php echo $dataTable; ?></div>
                
			</section>
			
		</article>
		
		<aside></aside>

		<script>
			function selectCmbFloors(){
				var idfloor = $("#cmbFloors").val();
				var path = "/sales/getColumbariums/"+idfloor+'/1';
				console.log(path);
				$("#showResult").html("");
				$.ajax({
					type:	'GET',
					url:	path,
					success:function(data){
						if(data.success==1){
							$("#showResult").html("");
							$("#showResult").empty();
							$("#showResult").html(data.tabla);
							$("#spanPiso").text("");
							$("#spanPiso").text(idfloor);
							setDataTab();
							return;
						}else{
							$("#showResult").html("");
							$("#showResult").empty();
							$("#showResult").html(data.message);
							return;
						}
					}
				}); // Fin AJAX
			} // Fin searchTrans		
			
			$(document).ready(function() {
				
				
				$('#alert_modal_cf').hide();
				
				var fecha = new Date();
				var nombre = "MTC_"+fecha.getFullYear();
				nombre += fecha.getMonth();
				nombre += fecha.getDate();
				nombre += fecha.getHours();
				nombre += fecha.getMinutes();
				nombre += fecha.getSeconds();
				var table = $('#IdDT').DataTable({
					//scrollY:        "300px",
					lengthChange:	false,
					stateSave: 		true,
					scrollX:        true,
					fixedColumns:   {
						leftColumns: 5,
						scrollCollapse: true
						//rightColumns: 1
					},
					"language": {
						"url": "{{$DTables}}"
					},
					dom: 'Bfrtip',
					buttons: [
						{
							extend:    'copyHtml5',
							copyTitle: '{{$objlabels->button_export}} - {{$Nbnk}}',
							copyKeys: '{{$objlabels->btn_copyKeys}}',
							copySuccess: {
								_: '%d {{$objlabels->btn_copySuccess1}}',
								1: '1  {{$objlabels->btn_copySuccess2}}'
							}
						},
						{
							extend: 'excelHtml5',
							title: nombre,
							messageTop:  'PDF created by MTC: {{$Nbnk}}.'
						},
						{
							extend:   	 'pdfHtml5',
							title:	  	 nombre,
							messageTop:  'PDF created by MTC: {{$Nbnk}}.',
							orientation: 'landscape',
							pageSize:	 'A4'
						},
						{
							text:		 'CSV',
							title:	  	 nombre,
							extend: 	 'csvHtml5',
							fieldSeparator: '<?php echo $delimiter; ?>',
							extension: 	 '.csv',
							className:   'red'
						}
					],
					"footerCallback": function ( row, data, start, end, display ) {
						var api = this.api(), data;
						
						// Remove the formatting to get integer data for summation
						var intVal = function ( i ) {
							return typeof i === 'string' ?
							i.replace(/[\$,]/g, '')*1 :
							typeof i === 'number' ?
							i : 0;
						};
						
						// Total over all pages
						total = api
						.column( <?php echo $colvalue; ?> )
						.data()
						.reduce( function (a, b) {
							return intVal(a) + intVal(b);
						}, 0 );
						
						// Total over this page
						pageTotal = api
						.column( <?php echo $colvalue; ?>, { page: 'current'} )
						.data()
						.reduce( function (a, b) {
							return intVal(a) + intVal(b);
						}, 0 );
						
						// Update footer
						$( api.column( <?php echo $colvalue; ?> ).footer() ).html(
							'$'+pageTotal +' ( $'+ total +' total)'
						);
					}
				});
				table.buttons().container().appendTo( '#IdDT_paginate .col-md-6:eq(0)' );
				
				
				$('#btnComprar').click(function(event){
					event.preventDefault();				
					console.log($("input[name=sel_id_fl]:checked"));
					//if(!$('#sel_id_fl').is(':checked')){
					if(!$("input[name='sel_id_fl']:checked")){
						alertPPYA('Debe seleccionar un registro');
						return;
					}	
					
					 if(!$('input[name=sel_id_fl]').is(':checked')) 
					 { 
					     alertPPYA('Debe seleccionar un registro');
						return; 
					  }
					/*$('.sel_row').removeClass('sel_row');
					
					$("#sel_id_fl input[name='sel_id_fl']:checked").closest('tr').addClass('sel_row');
					$("input[name='sel_id_fl']:checked").css('width','500px');
					$('#sel_id_fl').is(':checked').css('width','500px');*/
					setModalCol();
					/*var colm_sel = $('#sel_id_fl').val();
					 $('#seccion_detalle_planes').load('/quote/detail_tit_col/6/'+colm_sel);*/
				
				});
				
				$("input[name='sel_id_fl']").change(function(){

					$('.sel_row').removeClass('sel_row');
					$('tr').removeClass('sel_row');
					$(this).closest('tr').addClass('sel_row');
					$('#txt_cod_sel').val($(this).val());
				});
				
				
				$('#btnSaveCol').click(function(event){
					
					var colm_sel = $('#sel_id_fl').val();
					var val_sel  = $('#txt_val_sel').val();
					$('#mdConfirmDataCol').modal('hide');
					 $('#seccion_detalle_planes').load('/quote/detail_tit_col/6/'+colm_sel+'/'+val_sel);
				});	

                $("#txt_val_sel").change(function(event){
					$('#alert_modal_cf').hide();
					var val_min = $('#txt_val_sel_min').val();
					console.log(val_min);
					console.log($(this).val());
					if(parseFloat($(this).val())<parseFloat(val_min)){
						$('#alert_modal_cf').empty();
						$('#alert_modal_cf').html('El monto no puede ser menor a '+val_min);
						$('#alert_modal_cf').show();
						$(this).val(val_min);
					}
				});				
				
			} );
			
			function setModalCol(){
				$('#mdConfirmDataCol').modal('show');
			}
			
			function setDataTab(){
				console.log('setting table');
				var fecha = new Date();
				var nombre = "MTC_"+fecha.getFullYear();
				nombre += fecha.getMonth();
				nombre += fecha.getDate();
				nombre += fecha.getHours();
				nombre += fecha.getMinutes();
				nombre += fecha.getSeconds();
				var table = $('#IdDT').DataTable({
					lengthChange:	false,
					stateSave: 		true,
					scrollX:        true,
					fixedColumns:   {
						leftColumns: 5,
						scrollCollapse: true
						//rightColumns: 1
					},
					"language": {
						"url": "{{$DTables}}"
					},
					dom: 'Bfrtip',
					buttons: [
						{
							extend:    'copyHtml5',
							copyTitle: '{{$objlabels->button_export}} - {{$Nbnk}}',
							copyKeys: '{{$objlabels->btn_copyKeys}}',
							copySuccess: {
								_: '%d {{$objlabels->btn_copySuccess1}}',
								1: '1  {{$objlabels->btn_copySuccess2}}'
							}
						},
						{
							extend: 'excelHtml5',
							title: nombre,
							messageTop:  'PDF created by MTC: {{$Nbnk}}.'
						},
						{
							extend:   	 'pdfHtml5',
							title:	  	 nombre,
							messageTop:  'PDF created by MTC: {{$Nbnk}}.',
							orientation: 'landscape',
							pageSize:	 'A4'
						},
						{
							text:		 'CSV',
							title:	  	 nombre,
							extend: 	 'csvHtml5',
							fieldSeparator: '<?php echo $delimiter; ?>',
							extension: 	 '.csv',
							className:   'red'
						}
					]
					
				});
				console.log('luego table sett');
				table.buttons().container().appendTo( '#IdDT_paginate .col-md-6:eq(0)' );				
				
			}

		</script>
	</div>
<?php
//@endsection ?>
	
