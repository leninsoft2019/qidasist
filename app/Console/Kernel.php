<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule){
        // $schedule->command('inspire')
        //          ->hourly();
		
		/*
		*	Creado por: Leninsoft, 2019-08-28
		*	Cron (en Linux) para la llamada a SONDA de P2P para los procesos pendientes
		*	y que deben actualizarse segun respuesta enviada, el comando es:
		*	Cron (on Linux) for the P2P PROBE call for pending processes
		*	and that must be updated according to the response sent, the command is:
		*	* /2 * * * * php /var/www/qmemorial/artisan schedule:run >> /var/log/scheduleLeninsoft.log 2>&1
		*/
		$schedule->call('App\Http\Controllers\SalesController@sondaP2P')->everyMinute();
		
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands(){
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
